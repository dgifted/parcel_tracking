<?php

namespace App;

use App\Scopes\OriginScope;
use App\Location;


class Origin extends Location
{
    protected static function booted()
    {
        parent::booted();

        static::addGlobalScope(new OriginScope);
    }

    public function shipmentInfo()
    {
        return $this->belongsTo('App\ShipmentInfo', 'location_id');
    }

}
