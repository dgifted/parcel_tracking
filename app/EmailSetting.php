<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class EmailSetting extends Model
{
    const FOR_ADMIN = 1;
    const FOR_RECIPIENT = 2;
    const FOR_SHIPPER = 3;

    const CLIENT_EMAIL_NOTIFY_ON = 1;
    const CLIENT_EMAIL_NOTIFY_OFF = 0;

    protected $guarded = [
        'id'
    ];

    public static function generateReferenceId()
    {
        return Str::random(16);
    }
}
