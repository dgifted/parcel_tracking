<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    protected $guarded = ['id'];


    public function agent()
    {
        return $this->belongsTo('App\Agent', 'agent_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Receiver', 'receiver_id');
    }

    public function shipper()
    {
        return $this->belongsTo('App\Shipper', 'shipper_id');
    }

    public function shipmentInfo()
    {
        return $this->hasOne('App\ShipmentInfo', 'shipment_id');
    }

    public function packages()
    {
        return $this->hasMany('App\Package', 'shipment_id');
    }

    public function histories()
    {
        return $this->hasMany('App\History', 'shipment_id');
    }

    protected static function generateReferenceId()
    {
        return Str::random(16);
    }


}
