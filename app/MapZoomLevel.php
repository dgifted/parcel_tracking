<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapZoomLevel extends Model
{
    const IN_USE_ON = 1;
    const IN_USE_OFF = 0;

    protected $guarded = ['id'];
    protected $appends = ['map_zoom_level_to_number'];

    public function mapSettings()
    {
        return $this->belongsToMany(MapSetting::class, 'map_setting_zoom_level', 'map_zoom_level_id', 'map_setting_id');
    }

    public function getMapZoomLevelToNumberAttribute()
    {
        $level_to_number = 0;

        switch(strtolower($this->name)) {
            case 'world':
                $level_to_number = 1;
                break;
            case 'continent':
                $level_to_number = 5;
                break;
            case 'city':
                $level_to_number = 10;
                break;
            case 'streets':
                $level_to_number = 15;
                break;
            case 'buildings':
                $level_to_number = 20;
                break;
        }

        return $level_to_number;
    }
}
