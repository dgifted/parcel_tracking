<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapSetting extends Model
{
    protected $guarded = ['id'];

    public function mapTypes() 
    {
        return $this->belongsToMany(MapType::class, 'map_setting_type', 'map_setting_id', 'map_type_id');
    }

    public function zoomLevels()
    {
        return $this->belongsToMany(MapZoomLevel::class, 'map_setting_zoom_level', 'map_setting_id', 'map_zoom_level_id');
    }
}
