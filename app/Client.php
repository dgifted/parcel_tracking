<?php

namespace App;

use App\Scopes\ClientScope;
use App\User;
use Parental\HasParent;

class Client extends User
{
    // use HasParent;
    // protected $table = 'users';

    public static function booted()
    {
        parent::booted();

        static::addGlobalScope(new ClientScope);
    }

    public function shipments()
    {
        return $this->hasMany('App\Shipment', 'client_id');
    }
}
