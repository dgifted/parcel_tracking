<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapType extends Model
{
    const IN_USE_ON = 1;
    const IN_USE_OFF = 0;

    protected $guarded =['id'];

    public function mapSettings()
    {
        return $this->belongsToMany(MapSetting::class, 'map_setting_type', 'map_type_id', 'map_setting_id');
    }

}
