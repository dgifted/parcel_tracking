<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeightUnit extends Model
{
    const IN_USE_ON = 1;
    const IN_USE_OFF = 0;

    protected $guarded = ['id'];

    public function packageSettings()
    {
        return $this->belongsToMany(PackageSetting::class, 'package_setting_weight_unit', 'weight_unit_id', 'package_setting_id');
    }

    public function scopeActive($query)
    {
        return $query->where('in_use', $this::IN_USE_ON);
    }
}
