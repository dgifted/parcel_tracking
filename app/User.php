<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $table = 'users';

    const USER_TYPE_AGENT = 'agent';
    const USER_TYPE_CLIENT = 'client';
    const USER_TYPE_RECEIVER = 'receiver';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'slug',
        'phone',
        'email_verified_at',
        'role',
        'address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function generateUserSlug()
    {
        return Str::random(16);
    }

    public static function generateRememberToken()
    {
        return Str::random(16);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function plainPassword()
    {
        return $this->hasOne('App\Password', 'user_id');
    }
}
