<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $guarded = ['id'];

    public function shipment()
    {
        return $this->belongsTo('App\Shipment', 'shipment_id');
    }

    public function pieceType()
    {
        return $this->belongsTo('App\PieceType', 'piece_type_id');
    }
}
