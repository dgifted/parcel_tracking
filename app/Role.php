<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Role extends Model
{
    protected $guarded = ['id'];

    public static function generateReferenceId()
    {
        return Str::random(16);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
