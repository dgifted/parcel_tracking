<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $guarded = ['id'];

    public function locations()
    {
        return $this->belongsToMany('App\Location', 'history_location', 'history_id', 'location_id');
    }

    public function shipmentStatuses()
    {
        return $this->belongsToMany('App\ShipmentStatus', 'history_shipment_status', 'history_id', 'shipment_status_id');
    }

    public function shipment()
    {
        return $this->belongsTo('App\Shipment', 'shipment_id');
    }
}
