<?php

namespace App;

use App\Scopes\ShipperScope;
use App\User;
use Parental\HasParent;

class Shipper extends User
{
    // use HasParent;

    protected static function booted()
    {
        parent::booted();

        static::addGlobalScope(new ShipperScope);
    }

    public function shipments()
    {
        return $this->hasMany(Shipment::class, 'user_id');
    }
}
