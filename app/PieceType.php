<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PieceType extends Model
{
    protected $fillable =['name', 'description'];

    public function shortDescription()
    {
        return substr($this->description, 0, 35) . ' ' . '...';
    }

    public function package()
    {
        return $this->hasOne('App\Package', 'piece_type_id');
    }
}
