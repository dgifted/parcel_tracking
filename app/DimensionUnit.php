<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DimensionUnit extends Model
{
    const IN_USE_ON = 1;
    const IN_USE_OFF = 0;

    protected $guarded = ['id'];

    //Relationship
    public function packageSettings()
    {
        return $this->belongsToMany(PackageSetting::class, 'dimension_unit_package_setting', 'dimension_unit_id', 'package_setting_id');
    }

    public function scopeActive($query)
    {
        return $query->where('in_use', $this::IN_USE_ON);
    }
}
