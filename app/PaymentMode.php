<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PaymentMode extends Model
{
    protected $fillable = ['name', 'ref_id', 'description'];

    public function shortDescription()
    {
        return substr($this->description, 0, 35) . ' ' . '...';
    }

    public static function generateReferenceId()
    {
        return Str::random(16);
    }

    public function shipmentInfos()
    {
        return $this->belongsToMany('App\ShipmentInfo', 'shipment_payment_mode', 'payment_mode_id', 'shipment_info_id');
    }

}
