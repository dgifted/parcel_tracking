<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    const TYPE_ORIGIN = 'origin';
    const TYPE_DESTINATION = 'destination';

    protected $guarded = ['id'];
    protected $table = 'locations';

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function histories()
    {
        return $this->belongsToMany('App\History', 'history_location', 'location_id', 'history_id');
    }
}
