<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ShipmentStatus extends Model
{
    protected $appends = ['input_name'];

    protected $fillable = ['name', 'ref_id', 'description'];

    public function shortDescription()
    {
        return substr($this->description, 0, 35) . ' ' . '...';
    }

    public function getNameAttribute()
    {
        return strtolower($this->attributes['name']);
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = strtolower($name);
    }

    public function getInputNameAttribute()
    {
        return strtolower(str_replace(' ', '-', $this->name));
    }

    public static function generateReferenceId()
    {
        return Str::random(16);
    }

    public function histories()
    {
        return $this->belongsToMany('App\History', 'history_shipment_status', 'shipment_status_id', 'history_id');
    }
}
