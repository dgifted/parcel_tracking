<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentInfo extends Model
{
    protected $guarded = ['id'];

    public function shipment()
    {
        return $this->belongsTo('App\Shipment', 'shipment_info_id');
    }

    public function shipmentTypes()
    {
        return $this->belongsToMany('App\ShipmentType', 'shipment_info_type', 'shipment_info_id', 'shipment_type_id');
    }

    public function shipmentModes()
    {
        return $this->belongsToMany('App\ShipmentMode', 'shipment_info_mode', 'shipment_info_id', 'shipment_mode_id');
    }

    public function destination()
    {
        return $this->hasOne('App\Destination', 'shipment_info_id');
    }

    public function origin()
    {
        return $this->hasOne('App\Origin', 'shipment_info_id');
    }

    public function paymentModes()
    {
        return $this->belongsToMany('App\PaymentMode', 'shipment_payment_mode', 'shipment_info_id', 'payment_mode_id');
    }

    public function carriers()
    {
        return $this->belongsToMany('App\Carrier', 'carrier_shipment_info', 'shipment_info_id', 'carrier_id');
    }
}
