<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $guarded = ['id'];

    protected $appends = [
        'coordinates',
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function getCoordinatesAttribute()
    {
        $coords = explode(' ', $this->latlng);

        return [
            'latitude' => $coords[0],
            'longitude' => $coords[1],
        ];
    }

    public function locations()
    {
        return $this->hasMany(Location::class, 'country_id');
    }

    public function states()
    {
        return $this->hasMany(State::class, 'country_id');
    }
}
