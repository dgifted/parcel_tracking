<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CustomController extends Controller
{
    public static function generateTrackingID($prefix)
    {
        return strtoupper($prefix) . mt_rand(100000, 999999);
    }

    public static function filterInputDateTimeString($date_string)
    {
        $date_parts = explode(' ', str_replace('- ', '', $date_string));
        $date_parts[1] = self::convertMonthStringToNumber($date_parts[1]);
        $formatted_date_time_string = '';
        for($i = 0; $i < count($date_parts); $i++) {
            if($i === 0 || $i === 1) {
                $formatted_date_time_string .= $date_parts[$i] . '-';
            } elseif($i === count($date_parts) - 1) {
                $formatted_date_time_string .= $date_parts[$i];
            }
             else {
                $formatted_date_time_string .= $date_parts[$i] . ' ';
            }
        }
        return $formatted_date_time_string;
    }

    private static function convertMonthStringToNumber($month)
    {
        $month = strtolower($month);
        $month_number = 0;

        switch($month) {
            case 'january':
                $month_number = 1; break;
            case 'february':
                $month_number = 2; break;
            case 'march':
                $month_number = 3; break;
            case 'april':
                $month_number = 4; break;
            case 'may':
                $month_number = 5; break;
            case 'june':
                $month_number = 6; break;
            case 'july':
                $month_number = 7; break;
            case 'august':
                $month_number = 8; break;
            case 'september':
                $month_number = 9; break;
            case 'october':
                $month_number = 10; break;
            case 'november':
                $month_number = 11; break;
            case 'december':
                $month_number = 12; break;
            default:
                $month_number = 0; break;
        }

        return $month_number;
    }

    public static function filterInputTimeString($time_string)
    {
        $time = explode(' ', $time_string);

        $past_noon  = strtolower($time[1]) === 'pm' ? true : false;

        if($past_noon) {
            $time_parts = explode(':', $time[0]);
            $time_parts[0] = $time_parts[0] + 12;
            
            if((int)$time_parts[0] === 24) {
                $time_parts[0] = 0;
            }
            return implode(':', $time_parts);
        }
        return $time[0];
    }
}
