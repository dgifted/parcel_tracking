<?php

namespace App\Http\Controllers;

use App\Receiver;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function getAllUsers()
    {
        $users = User::with('plainPassword')->latest()->get()->filter(function($user) {
            return $user->role !== 'admin';
        });
        return $users;
    }

    public function getUser($id)
    {
        $user = User::find($id);
        return $user;
    }

    public function getReceiverFromEmail($email)
    {
        $receiver = Receiver::where('email', $email)->first();
        if (!is_null($receiver)) {
            return $receiver;
        }
        return null;
    }

    public function showAllUsers()
    {
        $users = $this->getAllUsers();
        $roles = Role::all();

        return view('users.users-index')->with([
            'users' => $users,
            'roles' => $roles,
        ]);
    }

    public function createUser()
    {
        // return view();
    }

    public function storeUser(Request $request)
    {
        // dd($request->password);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'role' => 'required'
        ]);

       $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'phone' => $request->phone,
            'address' => $request->address,
            'slug' => User::generateUserSlug(),
            'password' => Hash::make($request->password),
        ])->plainPassword()->create([
            'password_string' => $request->password,
        ]);

        return redirect()->route('admin.show-all-users');
    }

    public function editUser($id)
    {

    }

    public function updateUser(Request $request, $id)
    {
        // dd($request->address);
        $user = User::find($id);

        $user->fill($request->only([
            'name',
            'phone',
            'address',
        ]));

        $user->save();
        return redirect()->route('admin.show-all-users');
    }

    public function deleteUser($id)
    {
        $user = User::find($id);
        $user->plainPassword()->delete();
        $user->delete();
        return redirect()->route('admin.show-all-users');
    }
}
