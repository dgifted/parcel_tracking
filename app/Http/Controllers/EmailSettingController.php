<?php

namespace App\Http\Controllers;

use App\EmailSetting;
use App\ShipmentStatus;
use Illuminate\Http\Request;

class EmailSettingController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateEmailSetting(Request $request)
    {
        $email_setting = EmailSetting::find(1);
        $shipment_status = ShipmentStatus::all();
        $email_setting->client_email_notify = $request->client_email_notify === 'on' ? EmailSetting::CLIENT_EMAIL_NOTIFY_ON : EmailSetting::CLIENT_EMAIL_NOTIFY_OFF;

        $statuses = array();
        foreach($shipment_status as $status)
        {
            if($request[$status->input_name] !== null && $request[$status->input_name] === 'on') {
                $statuses[] = $status->input_name;
            }
        }
        $statuses = implode(' ', $statuses);

        $raw_cc = json_decode($request->cc);
        $i_cc = [];
        $cc = '';
        if(!is_null($raw_cc)) {
            foreach ($raw_cc as $k => $val) {
                $i_cc[] = $val->value;
            }
            $cc = implode(',', $i_cc);
        } else {
            $cc = null;
        }


        $raw_bcc = json_decode($request->bcc);
        $i_bcc = [];
        $bcc = '';
        if(!is_null($request->bcc)) {
            foreach ($raw_bcc as $k => $val) {
                $i_bcc[] = $val->value;
            }
            $bcc = implode(',', $i_bcc);
        } else {
            $bcc = null;
        }

        $email_setting->status_email_to_notify = $statuses;
        $email_setting->domain_email = $request->domain_email;
        $email_setting->cc = $cc;
        $email_setting->bcc = $bcc;
        $email_setting->subject = $request->subject;
        $email_setting->content = $request->content;
        $email_setting->save();

        return redirect()->route('admin.settings-email');
    }

}
