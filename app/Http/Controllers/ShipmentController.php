<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Carrier;
use App\Client;
use App\Country;
use App\EmailSetting;
use App\Jobs\SendEmail;
use App\Location;
use App\PackageSetting;
use App\PaymentMode;
use App\PieceType;
use App\Receiver;
use App\Shipment;
use App\ShipmentMode;
use App\ShipmentStatus;
use App\ShipmentType;
use App\Shipper;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ShipmentController extends Controller
{
    public function index()
    {
        $shipments = Shipment::with([
            'agent',
            'receiver',
            'shipper',
            'shipmentInfo.shipmentTypes',
            'shipmentInfo.shipmentModes',
            'histories',
            'histories.shipmentStatuses',
            ])->get();

        return view('shipments.all-shipments')->with([
            'shipments' => $shipments,
        ]);
    }

    public function create()
    {
        $shippers = Shipper::all();
        $receivers = Receiver::all();
        $clients = Client::all();
        $agents = Agent::all();

        $tracking_id = CustomController::generateTrackingID('jsxv');

        $package_setting = PackageSetting::with(['dimensionUnits', 'weightUnits'])->firstOrCreate([]);
//        dd($package_setting->weightUnits()->first());
        $shipment_types = ShipmentType::all();
        $shipment_modes = ShipmentMode::all();
        $payment_modes = PaymentMode::all();
        $shipment_statuses = ShipmentStatus::all();
        $carriers = Carrier::all();
        $countries = Country::all();
        $piece_types = PieceType::all();

        return view('shipments.create-shipment')->with([
            'tracking_id' => $tracking_id,
            'shippers' => $shippers,
            'receivers' => $receivers,
            'clients' => $clients,
            'agents' => $agents,
            'package_setting' => $package_setting,
            'shipment_types' => $shipment_types,
            'shipment_modes' => $shipment_modes,
            'payment_modes' => $payment_modes,
            'shipment_statuses' => $shipment_statuses,
            'carriers' => $carriers,
            'countries' => $countries,
            'piece_types' => $piece_types,
        ]);
    }

    public function edit($ref_id)
    {
        $total_package_weight = 0;
        $total_package_vol_weight = 0;
        $shipment = Shipment::with([
            'agent', 'receiver', 'shipper', 'client',
            'shipmentInfo', 'shipmentInfo.shipmentTypes', 'shipmentInfo.shipmentModes',
            'shipmentInfo.destination', 'shipmentInfo.origin', 'shipmentInfo.paymentModes',
            'shipmentInfo.carriers',  'packages', 'histories',
        ])->where('ref_id', $ref_id)->first();
        foreach($shipment->packages as $package) {
            $total_package_weight += $package->weight;
            $total_package_vol_weight += $package->volume;
        }
        $shipment_types = ShipmentType::all();
        $shipment_modes = ShipmentMode::all();
        $countries = Country::all();
        $payment_modes = PaymentMode::all();
        $carriers = Carrier::all();
        $piece_types = PieceType::all();


        return view('shipments.edit-shipment')->with([
            'shipment' => $shipment,
            'shipment_types' => $shipment_types,
            'shipment_modes' => $shipment_modes,
            'countries' => $countries,
            'payment_modes' => $payment_modes,
            'carriers' => $carriers,
            'piece_types' => $piece_types,
            'volume_weight' => $total_package_vol_weight,
            'weight' => $total_package_weight,
        ]);
    }

    public function location()
    {
        $shipments = Shipment::all()->pluck('tracking_id');
        $countries = Country::all();
        $statuses = ShipmentStatus::all();

        return view('shipments.shipment-location')->with([
            'shipments' => $shipments,
            'countries' => $countries,
            'statuses' => $statuses,
        ]);
    }

    public function storeShipment(Request $request)
    {


        $email_preference = EmailSetting::firstOrCreate([]);
        $status_email_send = explode(' ', $email_preference->status_email_to_notify);
        $notify_client = $email_preference->client_email_notify === 1 ? true : false;
        $email_from = $email_preference->domain_email;
        $subject = $email_preference->subject;
        $content = $email_preference->content;
        // dd(CustomController::filterInputTimeString($request->history_time));
        // dd(Carbon::parse($request->history_date));
        $rules = [
            'shipment_track_id' => 'required',
            'shipper_name' => 'required',
            'shipper_email' => 'required|email',
            'receiver_name' => 'required',
            'receiver_email' => 'required|email',
            'shipment_type' => 'required',
            'shipment_mode' => 'required',
            'destination' => 'required',
            // 'pickup_time' => 'nullable|date',
            'payment_mode' => 'required',
            'carrier' => 'required',
            'origin' => 'required',
            'pickup_date' => 'nullable|date',
            'expected_delivery_date' => 'nullable|date',
            'packages' => 'required',
            // 'piece_type' => 'required',
            // 'description ' => 'required|string',
            // 'total_volume' => 'required|numeric',
            // 'total_weight' => 'required|numeric',
            'client' => 'required',
            'agent' => 'required',
            'location' => 'required',
            'shipment_status' => 'required',
        ];

        // $this->validate($request, $rules);
        $request->validate($rules);

        DB::transaction(function () use ($request, $notify_client, $status_email_send, $email_from, $subject, $content) {

            $random_password = Str::random(8);
            $shipper = Shipper::where('email', $request->shipper_email)->exists()
                        ? Shipper::where('email', $request->shipper_email)->first()
                        : Shipper::create([
                            'name' => $request->shipper_name,
                            'email' => $request->shipper_email,
                            'phone' => $request->shipper_phone,
                            'address' => $request->shipper_address,
                            'role' => 'shipper',
                            'password' => Hash::make($random_password),
                            'slug' => User::generateUserSlug(),
                        ]);
            // $shipper->plainPassword()->create(['password_string' => $random_password]);

            $random_password = Str::random(8);

            // $receiver = null;

            if(Receiver::where('email', $request->receiver_email)->exists()) {
                $receiver = Receiver::where('email', $request->receiver_email)->first();
            } else {
                $receiver = Receiver::create([
                    'name' => $request->receiver_name,
                    'email' => $request->receiver_email,
                    'phone' => $request->receiver_phone,
                    'address' => $request->receiver_address,
                    'role' => User::USER_TYPE_RECEIVER,
                    'password' => Hash::make($random_password),
                    'slug' => User::generateUserSlug(),
                ]);

                $receiver->plainPassword()->create(['password_string' => $random_password]);
            }

            $shipment = Shipment::create([
                'shipper_id' => $shipper->id,
                'receiver_id' => $receiver->id,
                'agent_id' => $request->agent,
                'client_id' => $request->client,
                'tracking_id' => $request->shipment_track_id,
                'ref_id' => Shipment::generateReferenceId(),
            ]);

            $shipment_info = $shipment->shipmentInfo()->create([
                'total_freight' => $request->total_freight,
                'pickup_time' => Carbon::parse(CustomController::filterInputDateTimeString($request->pickup_time)),
                'pickup_date' => Carbon::parse($request->pickup_date),
                'expected_delivery_date' => Carbon::parse($request->expected_delivery_date),
                'comment' => $request->comment,
                'cost' => $request->currency . ' ' . $request->cost,
                'departure_time' => Carbon::parse(CustomController::filterInputDateTimeString($request->departure_time)),
            ]);

            $shipment_info->shipmentTypes()->sync($request->shipment_type);
            $shipment_info->shipmentModes()->sync($request->shipment_mode);
            $shipment_info->paymentModes()->sync($request->payment_mode);

            $origin = $shipment_info->origin()->create([
                'country_id' => $request->origin,
                'city' => $request->origin_city,
                'type' => Location::TYPE_ORIGIN,
            ]);

            $destination = $shipment_info->destination()->create([
                'country_id' => $request->destination,
                'city' => $request->destination_city,
                'type' => Location::TYPE_DESTINATION,
            ]);

            foreach($request->packages as $rPackage) {
                $shipment->packages()->create([
                    'piece_type_id' => $rPackage['piece_type'],
                    'quantity' => $rPackage['quantity'],
                    'unit_weight' => $rPackage['weight'],
                    'weight' => $request->total_weight,
                    'volume' => $request->total_volume,
                    'description' => $rPackage['description'],
                ]);
            }


            $history = $shipment->histories()->create([
                'date' => Carbon::parse($request->history_date),
                'time' => CustomController::filterInputTimeString($request->history_time),
                'remark' => $request->remark
            ]);

            $history->shipmentStatuses()->sync($request->shipment_status);
            $location = Location::create([
                'country_id' => $request->location,
                'city' => $request->history_city,
            ]);
            $history->locations()->attach($location->id);


            //Check the shipment status and notify according to preference
            // if($notify_client && count($status_email_send) > 0) {
            //     $details = [];
            //     $status = ShipmentStatus::find($request->shipment_status);
            //     $status = $status->input_name;

            //     if(in_array($status, $status_email_send)) {
            //         $details['email'] = $receiver->email;
            //         $details['shipment'] = $shipment;
            //         $details['from'] = $email_from;
            //         $details['subject'] = $subject;
            //         $details['content'] = $request->comment;

            //         dispatch(new SendEmail($details));
            //     }
            // }

        });

        return redirect()->route('admin.view-shipments');

    }

    public function updateShipment(Request $request, $ref_id)
    {
        $shipment = Shipment::where('ref_id', $ref_id)->first();

        //Not yet implemented
    }

    public function getShipment($tracking_id)
    {
        $shipment = Shipment::with([
            'agent', 'receiver', 'shipper', 'client',
            'shipmentInfo', 'shipmentInfo.shipmentTypes', 'shipmentInfo.shipmentModes',
            'shipmentInfo.destination', 'shipmentInfo.origin', 'shipmentInfo.paymentModes',
            'shipmentInfo.carriers',  'packages', 'histories'
        ])->where('tracking_id', $tracking_id)->first();
        // dd($shipment);
        if($shipment) {
            return $shipment;
        }

        return null;
    }

    public function getShipmentLocations($tracking_id)
    {
        $shipment = Shipment::with([
            'shipmentInfo.destination.country', 'shipmentInfo.origin.country', 'histories.locations.country', 'histories.shipmentStatuses', 'agent'
        ])->where('tracking_id', $tracking_id)->first();


        return $shipment;
    }

    public function updateShipmentLocation(Request $request, $tracking_id)
    {
        // dd($request->all());
        // dd(CustomController::filterInputTimeString($request->time));
        $error = true;
        $shipment = Shipment::where('tracking_id', $tracking_id)->first();

        DB::transaction(function () use ($request, $shipment, &$error) {
            $history = $shipment->histories()->create([
                'date' => Carbon::parse($request->date),
                'time' => CustomController::filterInputTimeString($request->time),
                'remark' => $request->remark,
            ]);

            $location = Location::create([
                'country_id' => $request->country,
                'city' => $request->city,
            ]);

            $history->locations()->attach($location->id);
            $history->shipmentStatuses()->attach($request->status);
            $error = false;
        });

        if($error) {
            return response()->json([
                'success' => false,
                'alert' => 'error',
                'data' => '',
                'message' => 'Shipment location update failed!',
            ]);
        }

        return response()->json([
            'success' => true,
            'alert' => 'success',
            'data' => '',
            'message' => 'Shipment location updated successfully!',
        ]);
    }

    public function deleteShipment($tracking_id)
    {
        $shipment = Shipment::where('tracking_id', $tracking_id)->first();

        if($shipment) {
            DB::transaction(function () use ($shipment) {
                $shipment->shipmentInfo->delete();

                $shipment->packages->each(function ($package) {
                    $package->delete();
                });

                $shipment->histories->each(function($history) {
                    $history->delete();
                });

                $shipment->delete();
            });
        }

        return redirect()->route('admin.view-shipments');
    }

    public function generateTrackingId()
    {
        return CustomController::generateTrackingID('jsxv');
    }
}
