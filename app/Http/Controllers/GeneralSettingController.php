<?php

namespace App\Http\Controllers;

use App\Carrier;
use App\PaymentMode;
use App\ShipmentMode;
use App\ShipmentStatus;
use App\ShipmentType;
use Illuminate\Http\Request;

class GeneralSettingController extends Controller
{
    public function addShipmentType(Request $request)
    {
        if(is_null($request->name)) {
            return redirect()->route('admin.settings-general')->with('error', 'Please provide shipment type name');
        }

        ShipmentType::create([
            'name' => $request->name,
            'ref_id' => ShipmentType::generateReferenceId(),
            'description' => $request->description,
        ]);

        return redirect()->route('admin.settings-general');
    }

    public function updateShipmentType(Request $request, $id)
    {
        $shipment_type = ShipmentType::find($id);
        $shipment_type->fill($request->only(['name', 'description']));
        $shipment_type->save();

        return redirect()->route('admin.settings-general');
    }

    public function deleteShipmentType($id)
    {
        $shipment_type = ShipmentType::find($id);
        $shipment_type->delete();

        return redirect()->route('admin.settings-general');
    }

    public function addShipmentMode(Request $request)
    {
        if(is_null($request->name)) {
            return redirect()->route('admin.settings-general')->with('error', 'Please provide shipment mode name');
        }

        ShipmentMode::create([
            'name' => $request->name,
            'ref_id' => ShipmentMode::generateReferenceId(),
            'description' => $request->description,
        ]);

        return redirect()->route('admin.settings-general');
    }

    public function updateShipmentMode(Request $request, $id)
    {
        $shipment_mode = ShipmentMode::find($id);
        $shipment_mode->fill($request->only(['name', 'description']));
        $shipment_mode->save();

        return redirect()->route('admin.settings-general');
    }

    public function deleteShipmentMode($id)
    {
        $shipment_mode = ShipmentMode::find($id);
        $shipment_mode->delete();

        return redirect()->route('admin.settings-general');
    }

    public function addCarrier(Request $request)
    {
        if(is_null($request->name)) {
            return redirect()->route('admin.settings-general')->with('error', 'Please provide carrier name');
        }

        Carrier::create([
            'name' => $request->name,
            'ref_id' => Carrier::generateReferenceId(),
            'description' => $request->description,
        ]);

        return redirect()->route('admin.settings-general');
    }

    public function updateCarrier(Request $request, $id)
    {
        $carrier = Carrier::find($id);
        $carrier->fill($request->only(['name', 'description']));
        $carrier->save();

        return redirect()->route('admin.settings-general');
    }

    public function deleteCarrier($id)
    {
        $carrier = Carrier::find($id);
        $carrier->delete();

        return redirect()->route('admin.settings-general');
    }

    public function addPaymentMode(Request $request)
    {
        if(is_null($request->name)) {
            return redirect()->route('admin.settings-general')->with('error', 'Please provide payment type name');
        }

        PaymentMode::create([
            'name' => $request->name,
            'ref_id' => PaymentMode::generateReferenceId(),
            'description' => $request->description,
        ]);

        return redirect()->route('admin.settings-general');
    }

    public function updatePaymentMode(Request $request, $id)
    {
        $payment_mode = PaymentMode::find($id);
        $payment_mode->fill($request->only(['name', 'description']));
        $payment_mode->save();

        return redirect()->route('admin.settings-general');
    }

    public function deletePaymentMode($id)
    {
        $payment_mode = PaymentMode::find($id);
        $payment_mode->delete();

        return redirect()->route('admin.settings-general');
    }

    public function addShipmentStatus(Request $request) 
    {
        if(is_null($request->name)) {
            return redirect()->route('admin.settings-general')->with('error', 'Please provide shipment status name');
        }

        ShipmentStatus::create([
            'name' => $request->name,
            'ref_id' => ShipmentStatus::generateReferenceId(),
            'description' => $request->description,
        ]);

        return redirect()->route('admin.settings-general');
    }

    public function updateShipmentStatus(Request $request, $id)
    {
        $shipment_status = ShipmentStatus::find($id);
        $shipment_status->fill($request->only(['name', 'description']));
        $shipment_status->save();

        return redirect()->route('admin.settings-general');
    }

    public function deleteShipmentStatus($id)
    {
        $shipment_status = ShipmentStatus::find($id);
        $shipment_status->delete();

        return redirect()->route('admin.settings-general');
    }
}
