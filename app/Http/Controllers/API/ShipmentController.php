<?php

namespace App\Http\Controllers\API;

use App\DimensionUnit;
use App\Http\Controllers\Controller;
use App\Shipment;
use App\WeightUnit;
use Illuminate\Http\Request;

class ShipmentController extends Controller
{
    public function index()
    {
        $shipments = Shipment::all();

        return response()->json([
            'shipments' => $shipments,
            200,
        ]);
    }

    public function show($tracking_id)
    {
        $shipment = Shipment::with([
            'agent',
            'client',
            'shipper',
            'receiver',
            'shipmentInfo',
            'shipmentInfo.destination',
            'shipmentInfo.destination.country',
            'shipmentInfo.origin',
            'shipmentInfo.origin.country',
            'packages',
            'packages.pieceType',
            'histories',
            'histories.shipmentStatuses',
            'histories.locations',
            'histories.locations.country',
        ])->where('tracking_id', $tracking_id)->first();

        $units = [];

        $dimension_unit = DimensionUnit::active()->first();
        $weight_unit = WeightUnit::active()->first();

        $units['dimension'] = $dimension_unit->abbreviation;
        $units['weight'] = $weight_unit->abbreviation;

        $units = (object)$units;

        if($shipment) {
            return response()->json(['shipment' => $shipment, 'units' => $units], 200);
        }

        return response()->json(['error' => 'Sorry the Shipment your requested does not exist'], 404);
    }
}
