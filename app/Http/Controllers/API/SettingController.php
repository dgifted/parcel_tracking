<?php

namespace App\Http\Controllers\API;

use App\EmailSetting;
use App\GeneralSetting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function emailSetting()
    {
        $setting = EmailSetting::first();
        return $setting;
    }
}
