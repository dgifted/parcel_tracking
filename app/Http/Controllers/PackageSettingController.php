<?php

namespace App\Http\Controllers;

use App\DimensionUnit;
use App\PackageSetting;
use App\WeightUnit;
use Illuminate\Http\Request;

class PackageSettingController extends Controller
{
    public function getPackageSetting()
    {
        return PackageSetting::first();
    }

    public function updatePackageSetting(Request $request)
    {
        // dd($request->multi_package_admin);

        $package_setting = PackageSetting::firstOrCreate([
            'id' => 1,
        ]);

        $package_setting->update([
            'enable_multi_package_admin' => $request->multi_package_admin === 'on' ? PackageSetting::MULTI_PACKAGE_ADMIN_ON : PackageSetting::MULTI_PACKAGE_ADMIN_OFF,
            'enable_multi_package_result' => $request->multi_package_result === 'on' ? PackageSetting::MULTI_PACKAGE_RESULT_ON : PackageSetting::MULTI_PACKAGE_RESULT_OFF,
            'enable_dimension' => $request->dimension_unit_on === 'on' ? PackageSetting::DIMENSION_UNITS_ON : PackageSetting::DIMENSION_UNITS_OFF,
        ]);

        $package_setting->dimensionUnits()->sync($request->dimension_unit);
        $package_setting->weightUnits()->sync($request->weight_unit);

        DimensionUnit::all()->filter(function($unit) use ($request) {
            return $unit->id !== $request->dimension_unit;
        })->each(function($i_unit) {
            $i_unit->in_use = DimensionUnit::IN_USE_OFF;
            $i_unit->save();
        });

        dd($request->dimension_unit);
        $t_dimension = DimensionUnit::find($request->dimension_unit);
        dd($t_dimension);

        $t_dimension->in_use = DimensionUnit::IN_USE_ON;
        $t_dimension->save();

        WeightUnit::all()->filter(function($weight) use ($request) {
            return $weight->id !== $request->weight_unit;
        })->each(function($i_weight) {
            $i_weight->in_use = WeightUnit::IN_USE_OFF;
            $i_weight->save();
        });

        $t_weight = WeightUnit::find($request->weight_unit);
        $t_weight->in_use = WeightUnit::IN_USE_ON;
        $t_weight->save();

        return redirect()->route('admin.settings-package');
    }

}
