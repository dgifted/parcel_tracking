<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use stdClass;

class CountryController extends Controller
{
    const API_URL = 'https://restcountries.eu/rest/v2/all';

    public function setupCountries()
    {
        $response = $this->apiFetchCountries();

        if ($response->successful()) {
            //If countries were successfully fetched
            //Decode the response into json object, extract each object and store to the database
            $countries = json_decode($response);

            foreach ($countries as $country) {
                Country::create([
                    'name' => $country->name,
                    'alpha_2_code' => $country->alpha2Code,
                    'alpha_3_code' => $country->alpha3Code,
                    'calling_code' => $country->callingCodes[0],
                    'latlng' => implode(' ', $country->latlng),
                    'timezones' => implode(' ', $country->timezones),
                    'flag_url' => $country->flag,
                ]);
            }
        }
    }

    public function apiFetchCountryCities($id)
    {
        $country = Country::find($id);
        $states = $country->states()->get()->pluck('name');

        if ($states->count() > 0) {
            return response()->json($states->toArray());
        }

        return response()->json(['error' => 'Error fetching cities'], 500);
    }

    private function apiFetchCountries()
    {
        return Http::get($this::API_URL);
    }
}
