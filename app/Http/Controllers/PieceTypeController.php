<?php

namespace App\Http\Controllers;

use App\PieceType;
use Illuminate\Http\Request;

class PieceTypeController extends Controller
{
    public function getAllPieceTypes()
    {
        return PieceType::all();
    }

    public function getPieceType($id)
    {
        $type = PieceType::find($id);
        if($type) {
            return response()->json([
                'success' => true,
                'alert' => 'success',
                'data' => $type,
                'message' => ''
            ]);
        }
        return response()->json([
            'success' => false,
            'alert' => 'warning',
            'data' => null,
            'message' => 'The requested resource could not be found'
        ]);
    }

    public function storePieceType(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'nullable|string|min:10'
        ]);

        $piece_type = PieceType::create([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        return redirect()->route('admin.settings-package');
    }

    public function updatePieceType(Request $request, $id)
    {
        $piece_type = PieceType::find($id);

        if($piece_type) {
            $piece_type->fill($request->all());
            if($piece_type->isDirty()) {
                $piece_type->save();
            }
        }
        return redirect()->route('admin.settings-package');
    }

    public function deletePieceType($id)
    {
        $piece_type = PieceType::find($id);

        if($piece_type) {
            $piece_type->delete();
        }

        return redirect()->route('admin.settings-package');
    }
}
