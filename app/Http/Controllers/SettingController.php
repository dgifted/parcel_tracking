<?php

namespace App\Http\Controllers;

use App\Carrier;
use App\DimensionUnit;
use App\EmailSetting;
use App\MapSetting;
use App\MapType;
use App\MapZoomLevel;
use App\PackageSetting;
use App\PaymentMode;
use App\PieceType;
use App\ShipmentMode;
use App\ShipmentStatus;
use App\ShipmentType;
use App\WeightUnit;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function general()
    {
        $shipment_types = ShipmentType::all();
        $shipment_modes = ShipmentMode::all();
        $carriers = Carrier::all();
        $payment_modes = PaymentMode::all();
        $shipment_statuses = ShipmentStatus::all();

        return view('settings.general')->with([
            'shipment_types' => $shipment_types,
            'shipment_modes' => $shipment_modes,
            'carriers' => $carriers,
            'payment_modes' => $payment_modes,
            'shipment_statuses' => $shipment_statuses,
        ]);

    }

    public function email()
    {
        $shipment_statuses = ShipmentStatus::all();
        $email_setting = EmailSetting::firstOrCreate([
            'id' => 1,
        ]);

        $email_setting->status_email_to_notify = explode(' ', $email_setting->status_email_to_notify);

        return view('settings.email')->with([
            'shipment_statuses' => $shipment_statuses,
            'email_setting' => $email_setting,
        ]);
    }

    public function package()
    {
        $piece_types = PieceType::all();
        $dimension_units = DimensionUnit::all();
        $weight_units = WeightUnit::all();
        $package_setting = PackageSetting::first();
        // dd($package_setting);
        // dd($package_setting->enable_multi_package_admin);

        return view('settings.package')->with([
            'piece_types' => $piece_types,
            'dimension_units' => $dimension_units,
            'weight_units' => $weight_units,
            'package_setting' => $package_setting,
        ]);
    }

    public function map()
    {
        $map_setting = MapSetting::with(['mapTypes', 'zoomLevels'])->first();

        $map_types = MapType::all();
        $map_zoom_levels = MapZoomLevel::all();

        return view('settings.map')->with([
            'map_types' => $map_types,
            'map_zoom_levels' => $map_zoom_levels,
            'map_setting' => $map_setting,
        ]);
    }

    public function storeGeneralSettings()
    {

    }

    public function storeEmailSettings()
    {

    }

    public function storePackageSettings(Request $request)
    {


    }


    public function storeMapSettings(Request $request)
    {
        $this->validate($request, [
            'api_key' => 'nullable|string|min:39',
            'map_type_id' => 'nullable|numeric',
            'zoom_level_id' => 'nullable|numeric',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
        ]);

        $map_setting = MapSetting::firstOrNew([
            'id' => 1,
        ]);

        $map_setting->api_key = $request->api_key ?? null;
        $map_setting->latitude = $request->latitude ?? null;
        $map_setting->longitude = $request->longitude ?? null;

        $map_setting->save();

        $map_setting->mapTypes()->sync($request->map_type_id ?? MapType::where('in_use', MapType::IN_USE_ON)->first()->id);
        $map_setting->zoomLevels()->sync($request->zoom_level_id ?? MapZoomLevel::where('in_use', MapZoomLevel::IN_USE_ON)->first()->id);

        if ($request->map_type_id !== null) {
            // dd($request->map_type_id);
            if($request->map_type_id !== MapType::where('in_use', MapType::IN_USE_ON)->first()->id) {

                $target_type = MapType::findOrFail($request->map_type_id);
                // dd($target_type);
                $target_type->in_use = MapType::IN_USE_ON;
                $target_type->save();

                MapType::all()->filter(function($type) use ($target_type) {
                    return $type->id !== $target_type->id;
                })->each(function($f_type) {
                    $f_type->update([
                        'in_use' => MapType::IN_USE_OFF,
                    ]);
                });


            }
        }

        if ($request->zoom_level_id !== null) {
            if ($request->zoom_level_id !== MapZoomLevel::where('in_use', MapZoomLevel::IN_USE_ON)->first()->id) {

                $target_zoom_level = MapZoomLevel::findOrFail($request->zoom_level_id);
                $target_zoom_level->in_use = MapZoomLevel::IN_USE_ON;
                $target_zoom_level->save();

                MapZoomLevel::all()->filter(function ($level) use ($target_zoom_level) {
                    return $level->id !== $target_zoom_level->id;
                })->each(function ($f_level) {
                    $f_level->update([
                        'in_use' => MapZoomLevel::IN_USE_OFF,
                    ]);
                });
            }
        }

        return redirect()->route('admin.settings-map');
    }

    public function getMapApiKey()
    {
        $map_setting = MapSetting::with(['zoomLevels', 'mapTypes'])->first();

        return (!is_null($map_setting))
            ? $map_setting
            : null;
    }
}
