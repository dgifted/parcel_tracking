<?php

namespace App\Http\Controllers;

use App\ShipmentStatus;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $shipment_statuses = ShipmentStatus::with(['histories.shipment'])->get();

        foreach($shipment_statuses as $status) {
            $status->shipment_count = $status->histories->count();
        }

        // foreach($shipment_statuses as $s) {
        //     dump($s);
        // }
        // dd('End');

        return view('home')->with([
            'shipment_statuses' => $shipment_statuses,
        ]);
    }
}
