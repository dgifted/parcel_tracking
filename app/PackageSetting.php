<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prophecy\Exception\Doubler\ReturnByReferenceException;

class PackageSetting extends Model
{
    const MULTI_PACKAGE_ADMIN_ON = 1;
    const MULTI_PACKAGE_ADMIN_OFF = 0;

    const MULTI_PACKAGE_RESULT_ON = 1;
    const MULTI_PACKAGE_RESULT_OFF = 0;

    const DIMENSION_UNITS_ON = 1;
    const DIMENSION_UNITS_OFF = 0;


    protected $guarded = ['id'];

    protected $hidden = [];

    public function dimensionUnits()
    {
        return $this->belongsToMany(DimensionUnit::class, 'dimension_unit_package_setting', 'package_setting_id', 'dimension_unit_id');
    }

    public function weightUnits()
    {
        return $this->belongsToMany(WeightUnit::class, 'package_setting_weight_unit', 'package_setting_id', 'weight_unit_id');
    }
}
