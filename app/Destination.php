<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\DestinationScope;

class Destination extends Location
{

    public static function booted()
    {
        parent::booted();

        static::addGlobalScope(new DestinationScope);
    }

    public function shipmentInfo()
    {
        return $this->belongsTo('App\ShipmentInfo', 'shipment_info_id');
    }

}
