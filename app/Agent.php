<?php

namespace App;

use App\Scopes\AgentScope;
use App\User;
use Parental\HasParent;

class Agent extends User
{
    // use HasParent;
    // protected $table = 'users';

    public static function booted()
    {
        parent::booted();

        static::addGlobalScope(new AgentScope);
    }

    public function shipments()
    {
        return $this->hasMany('App\Shipment', 'user_id');
    }
}
