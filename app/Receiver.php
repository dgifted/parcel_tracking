<?php

namespace App;

use App\Scopes\ReceiverScope;
use App\User;
// use Parental\HasParent;

class Receiver extends User
{
    // use HasParent;

    protected static function booted()
    {
        parent::booted();

        static::addGlobalScope(new ReceiverScope);
    }

    public function shipments()
    {
        return $this->hasMany('App\Shipment', 'receiver_id');
    }
}
