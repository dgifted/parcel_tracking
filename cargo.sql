-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 10, 2022 at 08:22 AM
-- Server version: 8.0.30-0ubuntu0.20.04.2
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cargo`
--

-- --------------------------------------------------------

--
-- Table structure for table `carriers`
--

CREATE TABLE `carriers` (
  `id` bigint UNSIGNED NOT NULL,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carriers`
--

INSERT INTO `carriers` (`id`, `ref_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'wm4HtduYpUx8EPoz', 'DHL', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(2, 'h9RUk2eb7U7smzPM', 'USPS', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(3, 'Jwl0s7FWdlZF6ZWP', 'FedEx', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52');

-- --------------------------------------------------------

--
-- Table structure for table `carrier_shipment_info`
--

CREATE TABLE `carrier_shipment_info` (
  `id` bigint UNSIGNED NOT NULL,
  `carrier_id` bigint UNSIGNED NOT NULL,
  `shipment_info_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alpha_2_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alpha_3_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `calling_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latlng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezones` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flag_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `alpha_2_code`, `alpha_3_code`, `calling_code`, `latlng`, `timezones`, `flag_url`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '93', '33 65', 'UTC+04:30', 'https://restcountries.eu/data/afg.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(2, 'Åland Islands', 'AX', 'ALA', '358', '60.116667 19.9', 'UTC+02:00', 'https://restcountries.eu/data/ala.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(3, 'Albania', 'AL', 'ALB', '355', '41 20', 'UTC+01:00', 'https://restcountries.eu/data/alb.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(4, 'Algeria', 'DZ', 'DZA', '213', '28 3', 'UTC+01:00', 'https://restcountries.eu/data/dza.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(5, 'American Samoa', 'AS', 'ASM', '1684', '-14.33333333 -170', 'UTC-11:00', 'https://restcountries.eu/data/asm.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(6, 'Andorra', 'AD', 'AND', '376', '42.5 1.5', 'UTC+01:00', 'https://restcountries.eu/data/and.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(7, 'Angola', 'AO', 'AGO', '244', '-12.5 18.5', 'UTC+01:00', 'https://restcountries.eu/data/ago.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(8, 'Anguilla', 'AI', 'AIA', '1264', '18.25 -63.16666666', 'UTC-04:00', 'https://restcountries.eu/data/aia.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(9, 'Antarctica', 'AQ', 'ATA', '672', '-74.65 4.48', 'UTC-03:00 UTC+03:00 UTC+05:00 UTC+06:00 UTC+07:00 UTC+08:00 UTC+10:00 UTC+12:00', 'https://restcountries.eu/data/ata.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(10, 'Antigua and Barbuda', 'AG', 'ATG', '1268', '17.05 -61.8', 'UTC-04:00', 'https://restcountries.eu/data/atg.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(11, 'Argentina', 'AR', 'ARG', '54', '-34 -64', 'UTC-03:00', 'https://restcountries.eu/data/arg.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(12, 'Armenia', 'AM', 'ARM', '374', '40 45', 'UTC+04:00', 'https://restcountries.eu/data/arm.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(13, 'Aruba', 'AW', 'ABW', '297', '12.5 -69.96666666', 'UTC-04:00', 'https://restcountries.eu/data/abw.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(14, 'Australia', 'AU', 'AUS', '61', '-27 133', 'UTC+05:00 UTC+06:30 UTC+07:00 UTC+08:00 UTC+09:30 UTC+10:00 UTC+10:30 UTC+11:30', 'https://restcountries.eu/data/aus.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(15, 'Austria', 'AT', 'AUT', '43', '47.33333333 13.33333333', 'UTC+01:00', 'https://restcountries.eu/data/aut.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(16, 'Azerbaijan', 'AZ', 'AZE', '994', '40.5 47.5', 'UTC+04:00', 'https://restcountries.eu/data/aze.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(17, 'Bahamas', 'BS', 'BHS', '1242', '24.25 -76', 'UTC-05:00', 'https://restcountries.eu/data/bhs.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(18, 'Bahrain', 'BH', 'BHR', '973', '26 50.55', 'UTC+03:00', 'https://restcountries.eu/data/bhr.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(19, 'Bangladesh', 'BD', 'BGD', '880', '24 90', 'UTC+06:00', 'https://restcountries.eu/data/bgd.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(20, 'Barbados', 'BB', 'BRB', '1246', '13.16666666 -59.53333333', 'UTC-04:00', 'https://restcountries.eu/data/brb.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(21, 'Belarus', 'BY', 'BLR', '375', '53 28', 'UTC+03:00', 'https://restcountries.eu/data/blr.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(22, 'Belgium', 'BE', 'BEL', '32', '50.83333333 4', 'UTC+01:00', 'https://restcountries.eu/data/bel.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(23, 'Belize', 'BZ', 'BLZ', '501', '17.25 -88.75', 'UTC-06:00', 'https://restcountries.eu/data/blz.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(24, 'Benin', 'BJ', 'BEN', '229', '9.5 2.25', 'UTC+01:00', 'https://restcountries.eu/data/ben.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(25, 'Bermuda', 'BM', 'BMU', '1441', '32.33333333 -64.75', 'UTC-04:00', 'https://restcountries.eu/data/bmu.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(26, 'Bhutan', 'BT', 'BTN', '975', '27.5 90.5', 'UTC+06:00', 'https://restcountries.eu/data/btn.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(27, 'Bolivia (Plurinational State of)', 'BO', 'BOL', '591', '-17 -65', 'UTC-04:00', 'https://restcountries.eu/data/bol.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(28, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '5997', '12.15 -68.266667', 'UTC-04:00', 'https://restcountries.eu/data/bes.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(29, 'Bosnia and Herzegovina', 'BA', 'BIH', '387', '44 18', 'UTC+01:00', 'https://restcountries.eu/data/bih.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(30, 'Botswana', 'BW', 'BWA', '267', '-22 24', 'UTC+02:00', 'https://restcountries.eu/data/bwa.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(31, 'Bouvet Island', 'BV', 'BVT', '', '-54.43333333 3.4', 'UTC+01:00', 'https://restcountries.eu/data/bvt.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(32, 'Brazil', 'BR', 'BRA', '55', '-10 -55', 'UTC-05:00 UTC-04:00 UTC-03:00 UTC-02:00', 'https://restcountries.eu/data/bra.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(33, 'British Indian Ocean Territory', 'IO', 'IOT', '246', '-6 71.5', 'UTC+06:00', 'https://restcountries.eu/data/iot.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(34, 'United States Minor Outlying Islands', 'UM', 'UMI', '', '', 'UTC-11:00 UTC-10:00 UTC+12:00', 'https://restcountries.eu/data/umi.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(35, 'Virgin Islands (British)', 'VG', 'VGB', '1284', '18.431383 -64.62305', 'UTC-04:00', 'https://restcountries.eu/data/vgb.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(36, 'Virgin Islands (U.S.)', 'VI', 'VIR', '1 340', '18.34 -64.93', 'UTC-04:00', 'https://restcountries.eu/data/vir.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(37, 'Brunei Darussalam', 'BN', 'BRN', '673', '4.5 114.66666666', 'UTC+08:00', 'https://restcountries.eu/data/brn.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(38, 'Bulgaria', 'BG', 'BGR', '359', '43 25', 'UTC+02:00', 'https://restcountries.eu/data/bgr.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(39, 'Burkina Faso', 'BF', 'BFA', '226', '13 -2', 'UTC', 'https://restcountries.eu/data/bfa.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(40, 'Burundi', 'BI', 'BDI', '257', '-3.5 30', 'UTC+02:00', 'https://restcountries.eu/data/bdi.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(41, 'Cambodia', 'KH', 'KHM', '855', '13 105', 'UTC+07:00', 'https://restcountries.eu/data/khm.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(42, 'Cameroon', 'CM', 'CMR', '237', '6 12', 'UTC+01:00', 'https://restcountries.eu/data/cmr.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(43, 'Canada', 'CA', 'CAN', '1', '60 -95', 'UTC-08:00 UTC-07:00 UTC-06:00 UTC-05:00 UTC-04:00 UTC-03:30', 'https://restcountries.eu/data/can.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(44, 'Cabo Verde', 'CV', 'CPV', '238', '16 -24', 'UTC-01:00', 'https://restcountries.eu/data/cpv.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(45, 'Cayman Islands', 'KY', 'CYM', '1345', '19.5 -80.5', 'UTC-05:00', 'https://restcountries.eu/data/cym.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(46, 'Central African Republic', 'CF', 'CAF', '236', '7 21', 'UTC+01:00', 'https://restcountries.eu/data/caf.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(47, 'Chad', 'TD', 'TCD', '235', '15 19', 'UTC+01:00', 'https://restcountries.eu/data/tcd.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(48, 'Chile', 'CL', 'CHL', '56', '-30 -71', 'UTC-06:00 UTC-04:00', 'https://restcountries.eu/data/chl.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(49, 'China', 'CN', 'CHN', '86', '35 105', 'UTC+08:00', 'https://restcountries.eu/data/chn.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(50, 'Christmas Island', 'CX', 'CXR', '61', '-10.5 105.66666666', 'UTC+07:00', 'https://restcountries.eu/data/cxr.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(51, 'Cocos (Keeling) Islands', 'CC', 'CCK', '61', '-12.5 96.83333333', 'UTC+06:30', 'https://restcountries.eu/data/cck.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(52, 'Colombia', 'CO', 'COL', '57', '4 -72', 'UTC-05:00', 'https://restcountries.eu/data/col.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(53, 'Comoros', 'KM', 'COM', '269', '-12.16666666 44.25', 'UTC+03:00', 'https://restcountries.eu/data/com.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(54, 'Congo', 'CG', 'COG', '242', '-1 15', 'UTC+01:00', 'https://restcountries.eu/data/cog.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(55, 'Congo (Democratic Republic of the)', 'CD', 'COD', '243', '0 25', 'UTC+01:00 UTC+02:00', 'https://restcountries.eu/data/cod.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(56, 'Cook Islands', 'CK', 'COK', '682', '-21.23333333 -159.76666666', 'UTC-10:00', 'https://restcountries.eu/data/cok.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(57, 'Costa Rica', 'CR', 'CRI', '506', '10 -84', 'UTC-06:00', 'https://restcountries.eu/data/cri.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(58, 'Croatia', 'HR', 'HRV', '385', '45.16666666 15.5', 'UTC+01:00', 'https://restcountries.eu/data/hrv.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(59, 'Cuba', 'CU', 'CUB', '53', '21.5 -80', 'UTC-05:00', 'https://restcountries.eu/data/cub.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(60, 'Curaçao', 'CW', 'CUW', '599', '12.116667 -68.933333', 'UTC-04:00', 'https://restcountries.eu/data/cuw.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(61, 'Cyprus', 'CY', 'CYP', '357', '35 33', 'UTC+02:00', 'https://restcountries.eu/data/cyp.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(62, 'Czech Republic', 'CZ', 'CZE', '420', '49.75 15.5', 'UTC+01:00', 'https://restcountries.eu/data/cze.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(63, 'Denmark', 'DK', 'DNK', '45', '56 10', 'UTC-04:00 UTC-03:00 UTC-01:00 UTC UTC+01:00', 'https://restcountries.eu/data/dnk.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(64, 'Djibouti', 'DJ', 'DJI', '253', '11.5 43', 'UTC+03:00', 'https://restcountries.eu/data/dji.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(65, 'Dominica', 'DM', 'DMA', '1767', '15.41666666 -61.33333333', 'UTC-04:00', 'https://restcountries.eu/data/dma.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(66, 'Dominican Republic', 'DO', 'DOM', '1809', '19 -70.66666666', 'UTC-04:00', 'https://restcountries.eu/data/dom.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(67, 'Ecuador', 'EC', 'ECU', '593', '-2 -77.5', 'UTC-06:00 UTC-05:00', 'https://restcountries.eu/data/ecu.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(68, 'Egypt', 'EG', 'EGY', '20', '27 30', 'UTC+02:00', 'https://restcountries.eu/data/egy.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(69, 'El Salvador', 'SV', 'SLV', '503', '13.83333333 -88.91666666', 'UTC-06:00', 'https://restcountries.eu/data/slv.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(70, 'Equatorial Guinea', 'GQ', 'GNQ', '240', '2 10', 'UTC+01:00', 'https://restcountries.eu/data/gnq.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(71, 'Eritrea', 'ER', 'ERI', '291', '15 39', 'UTC+03:00', 'https://restcountries.eu/data/eri.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(72, 'Estonia', 'EE', 'EST', '372', '59 26', 'UTC+02:00', 'https://restcountries.eu/data/est.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(73, 'Ethiopia', 'ET', 'ETH', '251', '8 38', 'UTC+03:00', 'https://restcountries.eu/data/eth.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(74, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '500', '-51.75 -59', 'UTC-04:00', 'https://restcountries.eu/data/flk.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(75, 'Faroe Islands', 'FO', 'FRO', '298', '62 -7', 'UTC+00:00', 'https://restcountries.eu/data/fro.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(76, 'Fiji', 'FJ', 'FJI', '679', '-18 175', 'UTC+12:00', 'https://restcountries.eu/data/fji.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(77, 'Finland', 'FI', 'FIN', '358', '64 26', 'UTC+02:00', 'https://restcountries.eu/data/fin.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(78, 'France', 'FR', 'FRA', '33', '46 2', 'UTC-10:00 UTC-09:30 UTC-09:00 UTC-08:00 UTC-04:00 UTC-03:00 UTC+01:00 UTC+03:00 UTC+04:00 UTC+05:00 UTC+11:00 UTC+12:00', 'https://restcountries.eu/data/fra.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(79, 'French Guiana', 'GF', 'GUF', '594', '4 -53', 'UTC-03:00', 'https://restcountries.eu/data/guf.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(80, 'French Polynesia', 'PF', 'PYF', '689', '-15 -140', 'UTC-10:00 UTC-09:30 UTC-09:00', 'https://restcountries.eu/data/pyf.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(81, 'French Southern Territories', 'TF', 'ATF', '', '-49.25 69.167', 'UTC+05:00', 'https://restcountries.eu/data/atf.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(82, 'Gabon', 'GA', 'GAB', '241', '-1 11.75', 'UTC+01:00', 'https://restcountries.eu/data/gab.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(83, 'Gambia', 'GM', 'GMB', '220', '13.46666666 -16.56666666', 'UTC+00:00', 'https://restcountries.eu/data/gmb.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(84, 'Georgia', 'GE', 'GEO', '995', '42 43.5', 'UTC-05:00', 'https://restcountries.eu/data/geo.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(85, 'Germany', 'DE', 'DEU', '49', '51 9', 'UTC+01:00', 'https://restcountries.eu/data/deu.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(86, 'Ghana', 'GH', 'GHA', '233', '8 -2', 'UTC', 'https://restcountries.eu/data/gha.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(87, 'Gibraltar', 'GI', 'GIB', '350', '36.13333333 -5.35', 'UTC+01:00', 'https://restcountries.eu/data/gib.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(88, 'Greece', 'GR', 'GRC', '30', '39 22', 'UTC+02:00', 'https://restcountries.eu/data/grc.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(89, 'Greenland', 'GL', 'GRL', '299', '72 -40', 'UTC-04:00 UTC-03:00 UTC-01:00 UTC+00:00', 'https://restcountries.eu/data/grl.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(90, 'Grenada', 'GD', 'GRD', '1473', '12.11666666 -61.66666666', 'UTC-04:00', 'https://restcountries.eu/data/grd.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(91, 'Guadeloupe', 'GP', 'GLP', '590', '16.25 -61.583333', 'UTC-04:00', 'https://restcountries.eu/data/glp.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(92, 'Guam', 'GU', 'GUM', '1671', '13.46666666 144.78333333', 'UTC+10:00', 'https://restcountries.eu/data/gum.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(93, 'Guatemala', 'GT', 'GTM', '502', '15.5 -90.25', 'UTC-06:00', 'https://restcountries.eu/data/gtm.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(94, 'Guernsey', 'GG', 'GGY', '44', '49.46666666 -2.58333333', 'UTC+00:00', 'https://restcountries.eu/data/ggy.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(95, 'Guinea', 'GN', 'GIN', '224', '11 -10', 'UTC', 'https://restcountries.eu/data/gin.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(96, 'Guinea-Bissau', 'GW', 'GNB', '245', '12 -15', 'UTC', 'https://restcountries.eu/data/gnb.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(97, 'Guyana', 'GY', 'GUY', '592', '5 -59', 'UTC-04:00', 'https://restcountries.eu/data/guy.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(98, 'Haiti', 'HT', 'HTI', '509', '19 -72.41666666', 'UTC-05:00', 'https://restcountries.eu/data/hti.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(99, 'Heard Island and McDonald Islands', 'HM', 'HMD', '', '-53.1 72.51666666', 'UTC+05:00', 'https://restcountries.eu/data/hmd.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(100, 'Holy See', 'VA', 'VAT', '379', '41.9 12.45', 'UTC+01:00', 'https://restcountries.eu/data/vat.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(101, 'Honduras', 'HN', 'HND', '504', '15 -86.5', 'UTC-06:00', 'https://restcountries.eu/data/hnd.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(102, 'Hong Kong', 'HK', 'HKG', '852', '22.25 114.16666666', 'UTC+08:00', 'https://restcountries.eu/data/hkg.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(103, 'Hungary', 'HU', 'HUN', '36', '47 20', 'UTC+01:00', 'https://restcountries.eu/data/hun.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(104, 'Iceland', 'IS', 'ISL', '354', '65 -18', 'UTC', 'https://restcountries.eu/data/isl.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(105, 'India', 'IN', 'IND', '91', '20 77', 'UTC+05:30', 'https://restcountries.eu/data/ind.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(106, 'Indonesia', 'ID', 'IDN', '62', '-5 120', 'UTC+07:00 UTC+08:00 UTC+09:00', 'https://restcountries.eu/data/idn.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(107, 'Côte d\'Ivoire', 'CI', 'CIV', '225', '8 -5', 'UTC', 'https://restcountries.eu/data/civ.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(108, 'Iran (Islamic Republic of)', 'IR', 'IRN', '98', '32 53', 'UTC+03:30', 'https://restcountries.eu/data/irn.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(109, 'Iraq', 'IQ', 'IRQ', '964', '33 44', 'UTC+03:00', 'https://restcountries.eu/data/irq.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(110, 'Ireland', 'IE', 'IRL', '353', '53 -8', 'UTC', 'https://restcountries.eu/data/irl.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(111, 'Isle of Man', 'IM', 'IMN', '44', '54.25 -4.5', 'UTC+00:00', 'https://restcountries.eu/data/imn.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(112, 'Israel', 'IL', 'ISR', '972', '31.5 34.75', 'UTC+02:00', 'https://restcountries.eu/data/isr.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(113, 'Italy', 'IT', 'ITA', '39', '42.83333333 12.83333333', 'UTC+01:00', 'https://restcountries.eu/data/ita.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(114, 'Jamaica', 'JM', 'JAM', '1876', '18.25 -77.5', 'UTC-05:00', 'https://restcountries.eu/data/jam.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(115, 'Japan', 'JP', 'JPN', '81', '36 138', 'UTC+09:00', 'https://restcountries.eu/data/jpn.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(116, 'Jersey', 'JE', 'JEY', '44', '49.25 -2.16666666', 'UTC+01:00', 'https://restcountries.eu/data/jey.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(117, 'Jordan', 'JO', 'JOR', '962', '31 36', 'UTC+03:00', 'https://restcountries.eu/data/jor.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(118, 'Kazakhstan', 'KZ', 'KAZ', '76', '48 68', 'UTC+05:00 UTC+06:00', 'https://restcountries.eu/data/kaz.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(119, 'Kenya', 'KE', 'KEN', '254', '1 38', 'UTC+03:00', 'https://restcountries.eu/data/ken.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(120, 'Kiribati', 'KI', 'KIR', '686', '1.41666666 173', 'UTC+12:00 UTC+13:00 UTC+14:00', 'https://restcountries.eu/data/kir.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(121, 'Kuwait', 'KW', 'KWT', '965', '29.5 45.75', 'UTC+03:00', 'https://restcountries.eu/data/kwt.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(122, 'Kyrgyzstan', 'KG', 'KGZ', '996', '41 75', 'UTC+06:00', 'https://restcountries.eu/data/kgz.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(123, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '856', '18 105', 'UTC+07:00', 'https://restcountries.eu/data/lao.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(124, 'Latvia', 'LV', 'LVA', '371', '57 25', 'UTC+02:00', 'https://restcountries.eu/data/lva.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(125, 'Lebanon', 'LB', 'LBN', '961', '33.83333333 35.83333333', 'UTC+02:00', 'https://restcountries.eu/data/lbn.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(126, 'Lesotho', 'LS', 'LSO', '266', '-29.5 28.5', 'UTC+02:00', 'https://restcountries.eu/data/lso.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(127, 'Liberia', 'LR', 'LBR', '231', '6.5 -9.5', 'UTC', 'https://restcountries.eu/data/lbr.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(128, 'Libya', 'LY', 'LBY', '218', '25 17', 'UTC+01:00', 'https://restcountries.eu/data/lby.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(129, 'Liechtenstein', 'LI', 'LIE', '423', '47.26666666 9.53333333', 'UTC+01:00', 'https://restcountries.eu/data/lie.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(130, 'Lithuania', 'LT', 'LTU', '370', '56 24', 'UTC+02:00', 'https://restcountries.eu/data/ltu.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(131, 'Luxembourg', 'LU', 'LUX', '352', '49.75 6.16666666', 'UTC+01:00', 'https://restcountries.eu/data/lux.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(132, 'Macao', 'MO', 'MAC', '853', '22.16666666 113.55', 'UTC+08:00', 'https://restcountries.eu/data/mac.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(133, 'Macedonia (the former Yugoslav Republic of)', 'MK', 'MKD', '389', '41.83333333 22', 'UTC+01:00', 'https://restcountries.eu/data/mkd.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(134, 'Madagascar', 'MG', 'MDG', '261', '-20 47', 'UTC+03:00', 'https://restcountries.eu/data/mdg.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(135, 'Malawi', 'MW', 'MWI', '265', '-13.5 34', 'UTC+02:00', 'https://restcountries.eu/data/mwi.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(136, 'Malaysia', 'MY', 'MYS', '60', '2.5 112.5', 'UTC+08:00', 'https://restcountries.eu/data/mys.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(137, 'Maldives', 'MV', 'MDV', '960', '3.25 73', 'UTC+05:00', 'https://restcountries.eu/data/mdv.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(138, 'Mali', 'ML', 'MLI', '223', '17 -4', 'UTC', 'https://restcountries.eu/data/mli.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(139, 'Malta', 'MT', 'MLT', '356', '35.83333333 14.58333333', 'UTC+01:00', 'https://restcountries.eu/data/mlt.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(140, 'Marshall Islands', 'MH', 'MHL', '692', '9 168', 'UTC+12:00', 'https://restcountries.eu/data/mhl.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(141, 'Martinique', 'MQ', 'MTQ', '596', '14.666667 -61', 'UTC-04:00', 'https://restcountries.eu/data/mtq.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(142, 'Mauritania', 'MR', 'MRT', '222', '20 -12', 'UTC', 'https://restcountries.eu/data/mrt.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(143, 'Mauritius', 'MU', 'MUS', '230', '-20.28333333 57.55', 'UTC+04:00', 'https://restcountries.eu/data/mus.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(144, 'Mayotte', 'YT', 'MYT', '262', '-12.83333333 45.16666666', 'UTC+03:00', 'https://restcountries.eu/data/myt.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(145, 'Mexico', 'MX', 'MEX', '52', '23 -102', 'UTC-08:00 UTC-07:00 UTC-06:00', 'https://restcountries.eu/data/mex.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(146, 'Micronesia (Federated States of)', 'FM', 'FSM', '691', '6.91666666 158.25', 'UTC+10:00 UTC+11', 'https://restcountries.eu/data/fsm.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(147, 'Moldova (Republic of)', 'MD', 'MDA', '373', '47 29', 'UTC+02:00', 'https://restcountries.eu/data/mda.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(148, 'Monaco', 'MC', 'MCO', '377', '43.73333333 7.4', 'UTC+01:00', 'https://restcountries.eu/data/mco.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(149, 'Mongolia', 'MN', 'MNG', '976', '46 105', 'UTC+07:00 UTC+08:00', 'https://restcountries.eu/data/mng.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(150, 'Montenegro', 'ME', 'MNE', '382', '42.5 19.3', 'UTC+01:00', 'https://restcountries.eu/data/mne.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(151, 'Montserrat', 'MS', 'MSR', '1664', '16.75 -62.2', 'UTC-04:00', 'https://restcountries.eu/data/msr.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(152, 'Morocco', 'MA', 'MAR', '212', '32 -5', 'UTC', 'https://restcountries.eu/data/mar.svg', '2021-05-18 15:44:06', '2021-05-18 15:44:06'),
(153, 'Mozambique', 'MZ', 'MOZ', '258', '-18.25 35', 'UTC+02:00', 'https://restcountries.eu/data/moz.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(154, 'Myanmar', 'MM', 'MMR', '95', '22 98', 'UTC+06:30', 'https://restcountries.eu/data/mmr.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(155, 'Namibia', 'NA', 'NAM', '264', '-22 17', 'UTC+01:00', 'https://restcountries.eu/data/nam.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(156, 'Nauru', 'NR', 'NRU', '674', '-0.53333333 166.91666666', 'UTC+12:00', 'https://restcountries.eu/data/nru.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(157, 'Nepal', 'NP', 'NPL', '977', '28 84', 'UTC+05:45', 'https://restcountries.eu/data/npl.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(158, 'Netherlands', 'NL', 'NLD', '31', '52.5 5.75', 'UTC-04:00 UTC+01:00', 'https://restcountries.eu/data/nld.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(159, 'New Caledonia', 'NC', 'NCL', '687', '-21.5 165.5', 'UTC+11:00', 'https://restcountries.eu/data/ncl.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(160, 'New Zealand', 'NZ', 'NZL', '64', '-41 174', 'UTC-11:00 UTC-10:00 UTC+12:00 UTC+12:45 UTC+13:00', 'https://restcountries.eu/data/nzl.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(161, 'Nicaragua', 'NI', 'NIC', '505', '13 -85', 'UTC-06:00', 'https://restcountries.eu/data/nic.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(162, 'Niger', 'NE', 'NER', '227', '16 8', 'UTC+01:00', 'https://restcountries.eu/data/ner.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(163, 'Nigeria', 'NG', 'NGA', '234', '10 8', 'UTC+01:00', 'https://restcountries.eu/data/nga.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(164, 'Niue', 'NU', 'NIU', '683', '-19.03333333 -169.86666666', 'UTC-11:00', 'https://restcountries.eu/data/niu.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(165, 'Norfolk Island', 'NF', 'NFK', '672', '-29.03333333 167.95', 'UTC+11:30', 'https://restcountries.eu/data/nfk.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(166, 'Korea (Democratic People\'s Republic of)', 'KP', 'PRK', '850', '40 127', 'UTC+09:00', 'https://restcountries.eu/data/prk.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(167, 'Northern Mariana Islands', 'MP', 'MNP', '1670', '15.2 145.75', 'UTC+10:00', 'https://restcountries.eu/data/mnp.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(168, 'Norway', 'NO', 'NOR', '47', '62 10', 'UTC+01:00', 'https://restcountries.eu/data/nor.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(169, 'Oman', 'OM', 'OMN', '968', '21 57', 'UTC+04:00', 'https://restcountries.eu/data/omn.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(170, 'Pakistan', 'PK', 'PAK', '92', '30 70', 'UTC+05:00', 'https://restcountries.eu/data/pak.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(171, 'Palau', 'PW', 'PLW', '680', '7.5 134.5', 'UTC+09:00', 'https://restcountries.eu/data/plw.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(172, 'Palestine, State of', 'PS', 'PSE', '970', '31.9 35.2', 'UTC+02:00', 'https://restcountries.eu/data/pse.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(173, 'Panama', 'PA', 'PAN', '507', '9 -80', 'UTC-05:00', 'https://restcountries.eu/data/pan.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(174, 'Papua New Guinea', 'PG', 'PNG', '675', '-6 147', 'UTC+10:00', 'https://restcountries.eu/data/png.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(175, 'Paraguay', 'PY', 'PRY', '595', '-23 -58', 'UTC-04:00', 'https://restcountries.eu/data/pry.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(176, 'Peru', 'PE', 'PER', '51', '-10 -76', 'UTC-05:00', 'https://restcountries.eu/data/per.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(177, 'Philippines', 'PH', 'PHL', '63', '13 122', 'UTC+08:00', 'https://restcountries.eu/data/phl.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(178, 'Pitcairn', 'PN', 'PCN', '64', '-25.06666666 -130.1', 'UTC-08:00', 'https://restcountries.eu/data/pcn.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(179, 'Poland', 'PL', 'POL', '48', '52 20', 'UTC+01:00', 'https://restcountries.eu/data/pol.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(180, 'Portugal', 'PT', 'PRT', '351', '39.5 -8', 'UTC-01:00 UTC', 'https://restcountries.eu/data/prt.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(181, 'Puerto Rico', 'PR', 'PRI', '1787', '18.25 -66.5', 'UTC-04:00', 'https://restcountries.eu/data/pri.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(182, 'Qatar', 'QA', 'QAT', '974', '25.5 51.25', 'UTC+03:00', 'https://restcountries.eu/data/qat.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(183, 'Republic of Kosovo', 'XK', 'KOS', '383', '42.666667 21.166667', 'UTC+01:00', 'https://restcountries.eu/data/kos.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(184, 'Réunion', 'RE', 'REU', '262', '-21.15 55.5', 'UTC+04:00', 'https://restcountries.eu/data/reu.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(185, 'Romania', 'RO', 'ROU', '40', '46 25', 'UTC+02:00', 'https://restcountries.eu/data/rou.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(186, 'Russian Federation', 'RU', 'RUS', '7', '60 100', 'UTC+03:00 UTC+04:00 UTC+06:00 UTC+07:00 UTC+08:00 UTC+09:00 UTC+10:00 UTC+11:00 UTC+12:00', 'https://restcountries.eu/data/rus.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(187, 'Rwanda', 'RW', 'RWA', '250', '-2 30', 'UTC+02:00', 'https://restcountries.eu/data/rwa.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(188, 'Saint Barthélemy', 'BL', 'BLM', '590', '18.5 -63.41666666', 'UTC-04:00', 'https://restcountries.eu/data/blm.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(189, 'Saint Helena, Ascension and Tristan da Cunha', 'SH', 'SHN', '290', '-15.95 -5.7', 'UTC+00:00', 'https://restcountries.eu/data/shn.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(190, 'Saint Kitts and Nevis', 'KN', 'KNA', '1869', '17.33333333 -62.75', 'UTC-04:00', 'https://restcountries.eu/data/kna.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(191, 'Saint Lucia', 'LC', 'LCA', '1758', '13.88333333 -60.96666666', 'UTC-04:00', 'https://restcountries.eu/data/lca.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(192, 'Saint Martin (French part)', 'MF', 'MAF', '590', '18.08333333 -63.95', 'UTC-04:00', 'https://restcountries.eu/data/maf.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(193, 'Saint Pierre and Miquelon', 'PM', 'SPM', '508', '46.83333333 -56.33333333', 'UTC-03:00', 'https://restcountries.eu/data/spm.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(194, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '1784', '13.25 -61.2', 'UTC-04:00', 'https://restcountries.eu/data/vct.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(195, 'Samoa', 'WS', 'WSM', '685', '-13.58333333 -172.33333333', 'UTC+13:00', 'https://restcountries.eu/data/wsm.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(196, 'San Marino', 'SM', 'SMR', '378', '43.76666666 12.41666666', 'UTC+01:00', 'https://restcountries.eu/data/smr.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(197, 'Sao Tome and Principe', 'ST', 'STP', '239', '1 7', 'UTC', 'https://restcountries.eu/data/stp.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(198, 'Saudi Arabia', 'SA', 'SAU', '966', '25 45', 'UTC+03:00', 'https://restcountries.eu/data/sau.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(199, 'Senegal', 'SN', 'SEN', '221', '14 -14', 'UTC', 'https://restcountries.eu/data/sen.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(200, 'Serbia', 'RS', 'SRB', '381', '44 21', 'UTC+01:00', 'https://restcountries.eu/data/srb.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(201, 'Seychelles', 'SC', 'SYC', '248', '-4.58333333 55.66666666', 'UTC+04:00', 'https://restcountries.eu/data/syc.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(202, 'Sierra Leone', 'SL', 'SLE', '232', '8.5 -11.5', 'UTC', 'https://restcountries.eu/data/sle.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(203, 'Singapore', 'SG', 'SGP', '65', '1.36666666 103.8', 'UTC+08:00', 'https://restcountries.eu/data/sgp.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(204, 'Sint Maarten (Dutch part)', 'SX', 'SXM', '1721', '18.033333 -63.05', 'UTC-04:00', 'https://restcountries.eu/data/sxm.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(205, 'Slovakia', 'SK', 'SVK', '421', '48.66666666 19.5', 'UTC+01:00', 'https://restcountries.eu/data/svk.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(206, 'Slovenia', 'SI', 'SVN', '386', '46.11666666 14.81666666', 'UTC+01:00', 'https://restcountries.eu/data/svn.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(207, 'Solomon Islands', 'SB', 'SLB', '677', '-8 159', 'UTC+11:00', 'https://restcountries.eu/data/slb.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(208, 'Somalia', 'SO', 'SOM', '252', '10 49', 'UTC+03:00', 'https://restcountries.eu/data/som.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(209, 'South Africa', 'ZA', 'ZAF', '27', '-29 24', 'UTC+02:00', 'https://restcountries.eu/data/zaf.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(210, 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', '500', '-54.5 -37', 'UTC-02:00', 'https://restcountries.eu/data/sgs.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(211, 'Korea (Republic of)', 'KR', 'KOR', '82', '37 127.5', 'UTC+09:00', 'https://restcountries.eu/data/kor.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(212, 'South Sudan', 'SS', 'SSD', '211', '7 30', 'UTC+03:00', 'https://restcountries.eu/data/ssd.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(213, 'Spain', 'ES', 'ESP', '34', '40 -4', 'UTC UTC+01:00', 'https://restcountries.eu/data/esp.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(214, 'Sri Lanka', 'LK', 'LKA', '94', '7 81', 'UTC+05:30', 'https://restcountries.eu/data/lka.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(215, 'Sudan', 'SD', 'SDN', '249', '15 30', 'UTC+03:00', 'https://restcountries.eu/data/sdn.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(216, 'Suriname', 'SR', 'SUR', '597', '4 -56', 'UTC-03:00', 'https://restcountries.eu/data/sur.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(217, 'Svalbard and Jan Mayen', 'SJ', 'SJM', '4779', '78 20', 'UTC+01:00', 'https://restcountries.eu/data/sjm.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(218, 'Swaziland', 'SZ', 'SWZ', '268', '-26.5 31.5', 'UTC+02:00', 'https://restcountries.eu/data/swz.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(219, 'Sweden', 'SE', 'SWE', '46', '62 15', 'UTC+01:00', 'https://restcountries.eu/data/swe.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(220, 'Switzerland', 'CH', 'CHE', '41', '47 8', 'UTC+01:00', 'https://restcountries.eu/data/che.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(221, 'Syrian Arab Republic', 'SY', 'SYR', '963', '35 38', 'UTC+02:00', 'https://restcountries.eu/data/syr.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(222, 'Taiwan', 'TW', 'TWN', '886', '23.5 121', 'UTC+08:00', 'https://restcountries.eu/data/twn.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(223, 'Tajikistan', 'TJ', 'TJK', '992', '39 71', 'UTC+05:00', 'https://restcountries.eu/data/tjk.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(224, 'Tanzania, United Republic of', 'TZ', 'TZA', '255', '-6 35', 'UTC+03:00', 'https://restcountries.eu/data/tza.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(225, 'Thailand', 'TH', 'THA', '66', '15 100', 'UTC+07:00', 'https://restcountries.eu/data/tha.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(226, 'Timor-Leste', 'TL', 'TLS', '670', '-8.83333333 125.91666666', 'UTC+09:00', 'https://restcountries.eu/data/tls.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(227, 'Togo', 'TG', 'TGO', '228', '8 1.16666666', 'UTC', 'https://restcountries.eu/data/tgo.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(228, 'Tokelau', 'TK', 'TKL', '690', '-9 -172', 'UTC+13:00', 'https://restcountries.eu/data/tkl.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(229, 'Tonga', 'TO', 'TON', '676', '-20 -175', 'UTC+13:00', 'https://restcountries.eu/data/ton.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(230, 'Trinidad and Tobago', 'TT', 'TTO', '1868', '11 -61', 'UTC-04:00', 'https://restcountries.eu/data/tto.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(231, 'Tunisia', 'TN', 'TUN', '216', '34 9', 'UTC+01:00', 'https://restcountries.eu/data/tun.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(232, 'Turkey', 'TR', 'TUR', '90', '39 35', 'UTC+03:00', 'https://restcountries.eu/data/tur.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(233, 'Turkmenistan', 'TM', 'TKM', '993', '40 60', 'UTC+05:00', 'https://restcountries.eu/data/tkm.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(234, 'Turks and Caicos Islands', 'TC', 'TCA', '1649', '21.75 -71.58333333', 'UTC-04:00', 'https://restcountries.eu/data/tca.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(235, 'Tuvalu', 'TV', 'TUV', '688', '-8 178', 'UTC+12:00', 'https://restcountries.eu/data/tuv.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(236, 'Uganda', 'UG', 'UGA', '256', '1 32', 'UTC+03:00', 'https://restcountries.eu/data/uga.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(237, 'Ukraine', 'UA', 'UKR', '380', '49 32', 'UTC+02:00', 'https://restcountries.eu/data/ukr.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(238, 'United Arab Emirates', 'AE', 'ARE', '971', '24 54', 'UTC+04', 'https://restcountries.eu/data/are.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(239, 'United Kingdom of Great Britain and Northern Ireland', 'GB', 'GBR', '44', '54 -2', 'UTC-08:00 UTC-05:00 UTC-04:00 UTC-03:00 UTC-02:00 UTC UTC+01:00 UTC+02:00 UTC+06:00', 'https://restcountries.eu/data/gbr.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(240, 'United States of America', 'US', 'USA', '1', '38 -97', 'UTC-12:00 UTC-11:00 UTC-10:00 UTC-09:00 UTC-08:00 UTC-07:00 UTC-06:00 UTC-05:00 UTC-04:00 UTC+10:00 UTC+12:00', 'https://restcountries.eu/data/usa.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(241, 'Uruguay', 'UY', 'URY', '598', '-33 -56', 'UTC-03:00', 'https://restcountries.eu/data/ury.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(242, 'Uzbekistan', 'UZ', 'UZB', '998', '41 64', 'UTC+05:00', 'https://restcountries.eu/data/uzb.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(243, 'Vanuatu', 'VU', 'VUT', '678', '-16 167', 'UTC+11:00', 'https://restcountries.eu/data/vut.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(244, 'Venezuela (Bolivarian Republic of)', 'VE', 'VEN', '58', '8 -66', 'UTC-04:00', 'https://restcountries.eu/data/ven.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(245, 'Viet Nam', 'VN', 'VNM', '84', '16.16666666 107.83333333', 'UTC+07:00', 'https://restcountries.eu/data/vnm.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(246, 'Wallis and Futuna', 'WF', 'WLF', '681', '-13.3 -176.2', 'UTC+12:00', 'https://restcountries.eu/data/wlf.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(247, 'Western Sahara', 'EH', 'ESH', '212', '24.5 -13', 'UTC+00:00', 'https://restcountries.eu/data/esh.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(248, 'Yemen', 'YE', 'YEM', '967', '15 48', 'UTC+03:00', 'https://restcountries.eu/data/yem.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(249, 'Zambia', 'ZM', 'ZMB', '260', '-15 30', 'UTC+02:00', 'https://restcountries.eu/data/zmb.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07'),
(250, 'Zimbabwe', 'ZW', 'ZWE', '263', '-20 30', 'UTC+02:00', 'https://restcountries.eu/data/zwe.svg', '2021-05-18 15:44:07', '2021-05-18 15:44:07');

-- --------------------------------------------------------

--
-- Table structure for table `dimension_units`
--

CREATE TABLE `dimension_units` (
  `id` bigint UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbreviation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_use` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dimension_units`
--

INSERT INTO `dimension_units` (`id`, `full_name`, `abbreviation`, `in_use`, `created_at`, `updated_at`) VALUES
(1, 'millimeters', 'mm', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(2, 'centimeters', 'cm', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(3, 'decimeters', 'dm', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(4, 'meters', 'm', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(5, 'dekameters', 'dam', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(6, 'kilometer', 'km', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(7, 'inch', 'in', 1, '2021-05-18 11:53:53', '2021-05-18 15:40:05'),
(8, 'foot', 'ft', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(9, 'yard', 'yd', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(10, 'mile', 'mi', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53');

-- --------------------------------------------------------

--
-- Table structure for table `dimension_unit_package_setting`
--

CREATE TABLE `dimension_unit_package_setting` (
  `id` bigint UNSIGNED NOT NULL,
  `dimension_unit_id` bigint UNSIGNED NOT NULL,
  `package_setting_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dimension_unit_package_setting`
--

INSERT INTO `dimension_unit_package_setting` (`id`, `dimension_unit_id`, `package_setting_id`, `created_at`, `updated_at`) VALUES
(1, 7, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_settings`
--

CREATE TABLE `email_settings` (
  `id` bigint UNSIGNED NOT NULL,
  `client_email_notify` tinyint NOT NULL DEFAULT '1',
  `status_email_to_notify` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domain_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `failed_jobs`
--

INSERT INTO `failed_jobs` (`id`, `connection`, `queue`, `payload`, `exception`, `failed_at`) VALUES
(1, 'database', 'default', '{\"uuid\":\"c10a8e48-a670-4e21-999c-457692d9eb5a\",\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":9:{s:10:\\\"\\u0000*\\u0000details\\\";a:9:{s:5:\\\"email\\\";s:17:\\\"payee57@gmail.com\\\";s:12:\\\"receiverName\\\";s:7:\\\"Jay Dee\\\";s:12:\\\"trackingSite\\\";s:23:\\\"https:\\/\\/excelxpress.net\\\";s:8:\\\"shipment\\\";O:12:\\\"App\\\\Shipment\\\":27:{s:10:\\\"\\u0000*\\u0000guarded\\\";a:1:{i:0;s:2:\\\"id\\\";}s:13:\\\"\\u0000*\\u0000connection\\\";s:5:\\\"mysql\\\";s:8:\\\"\\u0000*\\u0000table\\\";s:9:\\\"shipments\\\";s:13:\\\"\\u0000*\\u0000primaryKey\\\";s:2:\\\"id\\\";s:10:\\\"\\u0000*\\u0000keyType\\\";s:3:\\\"int\\\";s:12:\\\"incrementing\\\";b:1;s:7:\\\"\\u0000*\\u0000with\\\";a:0:{}s:12:\\\"\\u0000*\\u0000withCount\\\";a:0:{}s:10:\\\"\\u0000*\\u0000perPage\\\";i:15;s:6:\\\"exists\\\";b:1;s:18:\\\"wasRecentlyCreated\\\";b:1;s:13:\\\"\\u0000*\\u0000attributes\\\";a:9:{s:10:\\\"shipper_id\\\";i:2;s:11:\\\"receiver_id\\\";i:20;s:8:\\\"agent_id\\\";s:1:\\\"8\\\";s:9:\\\"client_id\\\";s:1:\\\"7\\\";s:11:\\\"tracking_id\\\";s:10:\\\"JSXV815280\\\";s:6:\\\"ref_id\\\";s:16:\\\"RlDCD0fYJh2fZeCa\\\";s:10:\\\"updated_at\\\";s:19:\\\"2021-05-18 17:55:24\\\";s:10:\\\"created_at\\\";s:19:\\\"2021-05-18 17:55:24\\\";s:2:\\\"id\\\";i:8;}s:11:\\\"\\u0000*\\u0000original\\\";a:9:{s:10:\\\"shipper_id\\\";i:2;s:11:\\\"receiver_id\\\";i:20;s:8:\\\"agent_id\\\";s:1:\\\"8\\\";s:9:\\\"client_id\\\";s:1:\\\"7\\\";s:11:\\\"tracking_id\\\";s:10:\\\"JSXV815280\\\";s:6:\\\"ref_id\\\";s:16:\\\"RlDCD0fYJh2fZeCa\\\";s:10:\\\"updated_at\\\";s:19:\\\"2021-05-18 17:55:24\\\";s:10:\\\"created_at\\\";s:19:\\\"2021-05-18 17:55:24\\\";s:2:\\\"id\\\";i:8;}s:10:\\\"\\u0000*\\u0000changes\\\";a:0:{}s:8:\\\"\\u0000*\\u0000casts\\\";a:0:{}s:17:\\\"\\u0000*\\u0000classCastCache\\\";a:0:{}s:8:\\\"\\u0000*\\u0000dates\\\";a:0:{}s:13:\\\"\\u0000*\\u0000dateFormat\\\";N;s:10:\\\"\\u0000*\\u0000appends\\\";a:0:{}s:19:\\\"\\u0000*\\u0000dispatchesEvents\\\";a:0:{}s:14:\\\"\\u0000*\\u0000observables\\\";a:0:{}s:12:\\\"\\u0000*\\u0000relations\\\";a:0:{}s:10:\\\"\\u0000*\\u0000touches\\\";a:0:{}s:10:\\\"timestamps\\\";b:1;s:9:\\\"\\u0000*\\u0000hidden\\\";a:0:{}s:10:\\\"\\u0000*\\u0000visible\\\";a:0:{}s:11:\\\"\\u0000*\\u0000fillable\\\";a:0:{}}s:12:\\\"shipmentType\\\";s:11:\\\"Air Freight\\\";s:15:\\\"expect_del_time\\\";s:25:\\\"Wed, May 19, 2021 6:50 PM\\\";s:4:\\\"from\\\";s:23:\\\"support@excelxpress.net\\\";s:7:\\\"subject\\\";s:33:\\\"Shipment created and sent to you.\\\";s:7:\\\"content\\\";s:112:\\\"A Pallet has been sent to you by Natalia Daugherty through Cargo Tracking. Below is the summary of the shipment.\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'ErrorException: file_get_contents(/var/www/html/parcel_tracking/resources/views/emails/new_shipment.blade.php): failed to open stream: No such file or directory in /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php:51\nStack trace:\n#0 [internal function]: Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError()\n#1 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php(51): file_get_contents()\n#2 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/View/Compilers/BladeCompiler.php(143): Illuminate\\Filesystem\\Filesystem->get()\n#3 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/View/Engines/CompilerEngine.php(51): Illuminate\\View\\Compilers\\BladeCompiler->compile()\n#4 /var/www/html/parcel_tracking/vendor/facade/ignition/src/Views/Engines/CompilerEngine.php(37): Illuminate\\View\\Engines\\CompilerEngine->get()\n#5 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/View/View.php(139): Facade\\Ignition\\Views\\Engines\\CompilerEngine->get()\n#6 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/View/View.php(122): Illuminate\\View\\View->getContents()\n#7 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/View/View.php(91): Illuminate\\View\\View->renderContents()\n#8 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(382): Illuminate\\View\\View->render()\n#9 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(355): Illuminate\\Mail\\Mailer->renderView()\n#10 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(273): Illuminate\\Mail\\Mailer->addContent()\n#11 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Mail/Mailable.php(177): Illuminate\\Mail\\Mailer->send()\n#12 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Support/Traits/Localizable.php(19): Illuminate\\Mail\\Mailable->Illuminate\\Mail\\{closure}()\n#13 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Mail/Mailable.php(178): Illuminate\\Mail\\Mailable->withLocale()\n#14 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(304): Illuminate\\Mail\\Mailable->send()\n#15 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(258): Illuminate\\Mail\\Mailer->sendMailable()\n#16 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Mail/PendingMail.php(122): Illuminate\\Mail\\Mailer->send()\n#17 /var/www/html/parcel_tracking/app/Jobs/SendEmail.php(37): Illuminate\\Mail\\PendingMail->send()\n#18 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): App\\Jobs\\SendEmail->handle()\n#19 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Container/Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#20 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure()\n#21 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod()\n#22 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Container/Container.php(596): Illuminate\\Container\\BoundMethod::call()\n#23 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(94): Illuminate\\Container\\Container->call()\n#24 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(128): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}()\n#25 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}()\n#26 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(98): Illuminate\\Pipeline\\Pipeline->then()\n#27 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(83): Illuminate\\Bus\\Dispatcher->dispatchNow()\n#28 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(128): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}()\n#29 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}()\n#30 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(85): Illuminate\\Pipeline\\Pipeline->then()\n#31 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(59): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware()\n#32 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Queue/Jobs/Job.php(98): Illuminate\\Queue\\CallQueuedHandler->call()\n#33 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(356): Illuminate\\Queue\\Jobs\\Job->fire()\n#34 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(306): Illuminate\\Queue\\Worker->process()\n#35 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(132): Illuminate\\Queue\\Worker->runJob()\n#36 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(112): Illuminate\\Queue\\Worker->daemon()\n#37 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(96): Illuminate\\Queue\\Console\\WorkCommand->runWorker()\n#38 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): Illuminate\\Queue\\Console\\WorkCommand->handle()\n#39 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Container/Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#40 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure()\n#41 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod()\n#42 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Container/Container.php(596): Illuminate\\Container\\BoundMethod::call()\n#43 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Console/Command.php(134): Illuminate\\Container\\Container->call()\n#44 /var/www/html/parcel_tracking/vendor/symfony/console/Command/Command.php(258): Illuminate\\Console\\Command->execute()\n#45 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Console/Command.php(121): Symfony\\Component\\Console\\Command\\Command->run()\n#46 /var/www/html/parcel_tracking/vendor/symfony/console/Application.php(920): Illuminate\\Console\\Command->run()\n#47 /var/www/html/parcel_tracking/vendor/symfony/console/Application.php(266): Symfony\\Component\\Console\\Application->doRunCommand()\n#48 /var/www/html/parcel_tracking/vendor/symfony/console/Application.php(142): Symfony\\Component\\Console\\Application->doRun()\n#49 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Console/Application.php(93): Symfony\\Component\\Console\\Application->run()\n#50 /var/www/html/parcel_tracking/vendor/laravel/framework/src/Illuminate/Foundation/Console/Kernel.php(129): Illuminate\\Console\\Application->run()\n#51 /var/www/html/parcel_tracking/artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle()\n#52 {main}', '2021-05-18 16:55:24');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

CREATE TABLE `histories` (
  `id` bigint UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shipment_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `histories`
--

INSERT INTO `histories` (`id`, `date`, `time`, `remark`, `created_at`, `updated_at`, `shipment_id`) VALUES
(1, '2021-05-18', '11:45:20', 'Remark', '2021-05-18 15:48:40', '2021-05-18 15:48:40', 1),
(5, '2021-05-18', '18:20:00', 'Remarks', '2021-05-18 16:38:37', '2021-05-18 16:38:37', 5),
(6, '2021-05-19', '18:45:20', 'Remarks', '2021-05-18 16:45:30', '2021-05-18 16:45:30', 6),
(7, '2021-05-18', '11:45:20', 'Remark', '2021-05-18 16:55:24', '2021-05-18 16:55:24', 8),
(8, '2021-05-18', '11:45:20', 'rrdde', '2021-05-18 19:01:16', '2021-05-18 19:01:16', 9),
(9, '2021-05-18', '22:40:20', 'Remarks', '2021-05-18 20:38:52', '2021-05-18 20:38:52', 10),
(10, '2021-05-19', '09:30:00', 'Your shipment is being transfered to our international cargo carriage.', '2021-05-19 07:31:15', '2021-05-19 07:31:15', 10),
(11, '2021-05-19', '09:30:00', 'Your shipment is being transfered to our international cargo carriage.', '2021-05-19 07:31:54', '2021-05-19 07:31:54', 10),
(12, '2021-05-19', '09:30:00', 'Your shipment is being transfered to our international cargo carriage.', '2021-05-19 07:33:12', '2021-05-19 07:33:12', 10),
(13, '2021-05-19', '09:30:00', 'Your shipment is being transfered to our international cargo carriage.', '2021-05-19 07:35:56', '2021-05-19 07:35:56', 10),
(14, '2021-05-19', '09:30:00', 'Your shipment is being transfered to our international cargo carriage.', '2021-05-19 07:36:47', '2021-05-19 07:36:47', 10),
(15, '2021-05-19', '09:30:00', 'Your shipment is being transfered to our international cargo carriage.', '2021-05-19 07:41:41', '2021-05-19 07:41:41', 10),
(16, '2021-05-19', '09:30:00', 'Your shipment is being transfered to our international cargo carriage.', '2021-05-19 07:43:22', '2021-05-19 07:43:22', 10),
(17, '2021-05-19', '09:30:00', 'Your shipment is being transfered to our international cargo carriage.', '2021-05-19 07:44:10', '2021-05-19 07:44:10', 10),
(18, '2021-05-19', '11:45:20', 'Shipment package on hold due to constraints. Excise some patience', '2021-05-19 08:46:18', '2021-05-19 08:46:18', 1),
(19, '2021-05-21', '11:45:20', 'Remark', '2021-05-21 04:59:26', '2021-05-21 04:59:26', 10),
(20, '2021-05-21', '11:45:20', 'Remark', '2021-05-21 05:01:19', '2021-05-21 05:01:19', 10);

-- --------------------------------------------------------

--
-- Table structure for table `history_location`
--

CREATE TABLE `history_location` (
  `id` bigint UNSIGNED NOT NULL,
  `history_id` bigint UNSIGNED NOT NULL,
  `location_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_location`
--

INSERT INTO `history_location` (`id`, `history_id`, `location_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, NULL, NULL),
(5, 5, 15, NULL, NULL),
(6, 6, 18, NULL, NULL),
(7, 7, 21, NULL, NULL),
(8, 8, 24, NULL, NULL),
(9, 9, 27, NULL, NULL),
(10, 10, 28, NULL, NULL),
(11, 11, 29, NULL, NULL),
(12, 12, 30, NULL, NULL),
(13, 13, 31, NULL, NULL),
(14, 14, 32, NULL, NULL),
(15, 15, 33, NULL, NULL),
(16, 16, 34, NULL, NULL),
(17, 17, 35, NULL, NULL),
(18, 18, 36, NULL, NULL),
(19, 19, 37, NULL, NULL),
(20, 20, 38, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `history_shipment_status`
--

CREATE TABLE `history_shipment_status` (
  `id` bigint UNSIGNED NOT NULL,
  `history_id` bigint UNSIGNED NOT NULL,
  `shipment_status_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_shipment_status`
--

INSERT INTO `history_shipment_status` (`id`, `history_id`, `shipment_status_id`, `created_at`, `updated_at`) VALUES
(1, 1, 5, NULL, NULL),
(5, 5, 4, NULL, NULL),
(6, 6, 3, NULL, NULL),
(7, 7, 2, NULL, NULL),
(8, 8, 3, NULL, NULL),
(9, 9, 1, NULL, NULL),
(10, 10, 5, NULL, NULL),
(11, 11, 5, NULL, NULL),
(12, 12, 5, NULL, NULL),
(13, 13, 5, NULL, NULL),
(14, 14, 5, NULL, NULL),
(15, 15, 5, NULL, NULL),
(16, 16, 5, NULL, NULL),
(17, 17, 5, NULL, NULL),
(18, 18, 3, NULL, NULL),
(19, 19, 5, NULL, NULL),
(20, 20, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint UNSIGNED NOT NULL,
  `reserved_at` int UNSIGNED DEFAULT NULL,
  `available_at` int UNSIGNED NOT NULL,
  `created_at` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(9, 'default', '{\"uuid\":\"80edf91a-2db8-4133-b4d1-2989f86568e1\",\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":9:{s:10:\\\"\\u0000*\\u0000details\\\";a:9:{s:5:\\\"email\\\";s:19:\\\"dgifted@yopmail.com\\\";s:12:\\\"receiverName\\\";s:7:\\\"Jay Dee\\\";s:12:\\\"trackingSite\\\";s:23:\\\"https:\\/\\/excelxpress.net\\\";s:8:\\\"shipment\\\";O:12:\\\"App\\\\Shipment\\\":27:{s:10:\\\"\\u0000*\\u0000guarded\\\";a:1:{i:0;s:2:\\\"id\\\";}s:13:\\\"\\u0000*\\u0000connection\\\";s:5:\\\"mysql\\\";s:8:\\\"\\u0000*\\u0000table\\\";s:9:\\\"shipments\\\";s:13:\\\"\\u0000*\\u0000primaryKey\\\";s:2:\\\"id\\\";s:10:\\\"\\u0000*\\u0000keyType\\\";s:3:\\\"int\\\";s:12:\\\"incrementing\\\";b:1;s:7:\\\"\\u0000*\\u0000with\\\";a:0:{}s:12:\\\"\\u0000*\\u0000withCount\\\";a:0:{}s:10:\\\"\\u0000*\\u0000perPage\\\";i:15;s:6:\\\"exists\\\";b:1;s:18:\\\"wasRecentlyCreated\\\";b:0;s:13:\\\"\\u0000*\\u0000attributes\\\";a:9:{s:2:\\\"id\\\";i:10;s:10:\\\"shipper_id\\\";i:11;s:11:\\\"receiver_id\\\";i:21;s:8:\\\"agent_id\\\";i:5;s:9:\\\"client_id\\\";i:7;s:11:\\\"tracking_id\\\";s:10:\\\"JSXV456527\\\";s:6:\\\"ref_id\\\";s:16:\\\"iEGK7mhs1VvxZtF4\\\";s:10:\\\"created_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";s:10:\\\"updated_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";}s:11:\\\"\\u0000*\\u0000original\\\";a:9:{s:2:\\\"id\\\";i:10;s:10:\\\"shipper_id\\\";i:11;s:11:\\\"receiver_id\\\";i:21;s:8:\\\"agent_id\\\";i:5;s:9:\\\"client_id\\\";i:7;s:11:\\\"tracking_id\\\";s:10:\\\"JSXV456527\\\";s:6:\\\"ref_id\\\";s:16:\\\"iEGK7mhs1VvxZtF4\\\";s:10:\\\"created_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";s:10:\\\"updated_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";}s:10:\\\"\\u0000*\\u0000changes\\\";a:0:{}s:8:\\\"\\u0000*\\u0000casts\\\";a:0:{}s:17:\\\"\\u0000*\\u0000classCastCache\\\";a:0:{}s:8:\\\"\\u0000*\\u0000dates\\\";a:0:{}s:13:\\\"\\u0000*\\u0000dateFormat\\\";N;s:10:\\\"\\u0000*\\u0000appends\\\";a:0:{}s:19:\\\"\\u0000*\\u0000dispatchesEvents\\\";a:0:{}s:14:\\\"\\u0000*\\u0000observables\\\";a:0:{}s:12:\\\"\\u0000*\\u0000relations\\\";a:2:{s:8:\\\"receiver\\\";O:12:\\\"App\\\\Receiver\\\":28:{s:8:\\\"\\u0000*\\u0000table\\\";s:5:\\\"users\\\";s:11:\\\"\\u0000*\\u0000fillable\\\";a:8:{i:0;s:4:\\\"name\\\";i:1;s:5:\\\"email\\\";i:2;s:8:\\\"password\\\";i:3;s:4:\\\"slug\\\";i:4;s:5:\\\"phone\\\";i:5;s:17:\\\"email_verified_at\\\";i:6;s:4:\\\"role\\\";i:7;s:7:\\\"address\\\";}s:9:\\\"\\u0000*\\u0000hidden\\\";a:2:{i:0;s:8:\\\"password\\\";i:1;s:14:\\\"remember_token\\\";}s:8:\\\"\\u0000*\\u0000casts\\\";a:1:{s:17:\\\"email_verified_at\\\";s:8:\\\"datetime\\\";}s:13:\\\"\\u0000*\\u0000connection\\\";s:5:\\\"mysql\\\";s:13:\\\"\\u0000*\\u0000primaryKey\\\";s:2:\\\"id\\\";s:10:\\\"\\u0000*\\u0000keyType\\\";s:3:\\\"int\\\";s:12:\\\"incrementing\\\";b:1;s:7:\\\"\\u0000*\\u0000with\\\";a:0:{}s:12:\\\"\\u0000*\\u0000withCount\\\";a:0:{}s:10:\\\"\\u0000*\\u0000perPage\\\";i:15;s:6:\\\"exists\\\";b:1;s:18:\\\"wasRecentlyCreated\\\";b:0;s:13:\\\"\\u0000*\\u0000attributes\\\";a:12:{s:2:\\\"id\\\";i:21;s:4:\\\"name\\\";s:7:\\\"Jay Dee\\\";s:5:\\\"email\\\";s:19:\\\"dgifted@yopmail.com\\\";s:5:\\\"phone\\\";s:11:\\\"08166533091\\\";s:7:\\\"address\\\";s:3:\\\"Unn\\\";s:4:\\\"role\\\";s:8:\\\"receiver\\\";s:17:\\\"email_verified_at\\\";N;s:8:\\\"password\\\";s:60:\\\"$2y$10$Fdcixf\\/7LM7C1k5l3k5ICea2IxYSk4wMC6L0ebBkdxy99Rq7p9UUK\\\";s:4:\\\"slug\\\";s:16:\\\"95JwKasfCZCdMyrZ\\\";s:14:\\\"remember_token\\\";N;s:10:\\\"created_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";s:10:\\\"updated_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";}s:11:\\\"\\u0000*\\u0000original\\\";a:12:{s:2:\\\"id\\\";i:21;s:4:\\\"name\\\";s:7:\\\"Jay Dee\\\";s:5:\\\"email\\\";s:19:\\\"dgifted@yopmail.com\\\";s:5:\\\"phone\\\";s:11:\\\"08166533091\\\";s:7:\\\"address\\\";s:3:\\\"Unn\\\";s:4:\\\"role\\\";s:8:\\\"receiver\\\";s:17:\\\"email_verified_at\\\";N;s:8:\\\"password\\\";s:60:\\\"$2y$10$Fdcixf\\/7LM7C1k5l3k5ICea2IxYSk4wMC6L0ebBkdxy99Rq7p9UUK\\\";s:4:\\\"slug\\\";s:16:\\\"95JwKasfCZCdMyrZ\\\";s:14:\\\"remember_token\\\";N;s:10:\\\"created_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";s:10:\\\"updated_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";}s:10:\\\"\\u0000*\\u0000changes\\\";a:0:{}s:17:\\\"\\u0000*\\u0000classCastCache\\\";a:0:{}s:8:\\\"\\u0000*\\u0000dates\\\";a:0:{}s:13:\\\"\\u0000*\\u0000dateFormat\\\";N;s:10:\\\"\\u0000*\\u0000appends\\\";a:0:{}s:19:\\\"\\u0000*\\u0000dispatchesEvents\\\";a:0:{}s:14:\\\"\\u0000*\\u0000observables\\\";a:0:{}s:12:\\\"\\u0000*\\u0000relations\\\";a:0:{}s:10:\\\"\\u0000*\\u0000touches\\\";a:0:{}s:10:\\\"timestamps\\\";b:1;s:10:\\\"\\u0000*\\u0000visible\\\";a:0:{}s:10:\\\"\\u0000*\\u0000guarded\\\";a:1:{i:0;s:1:\\\"*\\\";}s:20:\\\"\\u0000*\\u0000rememberTokenName\\\";s:14:\\\"remember_token\\\";}s:12:\\\"shipmentInfo\\\";O:16:\\\"App\\\\ShipmentInfo\\\":27:{s:10:\\\"\\u0000*\\u0000guarded\\\";a:1:{i:0;s:2:\\\"id\\\";}s:13:\\\"\\u0000*\\u0000connection\\\";s:5:\\\"mysql\\\";s:8:\\\"\\u0000*\\u0000table\\\";s:14:\\\"shipment_infos\\\";s:13:\\\"\\u0000*\\u0000primaryKey\\\";s:2:\\\"id\\\";s:10:\\\"\\u0000*\\u0000keyType\\\";s:3:\\\"int\\\";s:12:\\\"incrementing\\\";b:1;s:7:\\\"\\u0000*\\u0000with\\\";a:0:{}s:12:\\\"\\u0000*\\u0000withCount\\\";a:0:{}s:10:\\\"\\u0000*\\u0000perPage\\\";i:15;s:6:\\\"exists\\\";b:1;s:18:\\\"wasRecentlyCreated\\\";b:0;s:13:\\\"\\u0000*\\u0000attributes\\\";a:11:{s:2:\\\"id\\\";i:9;s:11:\\\"shipment_id\\\";i:10;s:13:\\\"total_freight\\\";s:1:\\\"1\\\";s:11:\\\"pickup_time\\\";s:8:\\\"22:35:00\\\";s:11:\\\"pickup_date\\\";s:10:\\\"2021-05-19\\\";s:22:\\\"expected_delivery_date\\\";s:10:\\\"2021-05-19\\\";s:7:\\\"comment\\\";s:9:\\\"Commonent\\\";s:14:\\\"departure_time\\\";s:8:\\\"22:35:00\\\";s:10:\\\"created_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";s:10:\\\"updated_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";s:4:\\\"cost\\\";s:6:\\\"$ 1300\\\";}s:11:\\\"\\u0000*\\u0000original\\\";a:11:{s:2:\\\"id\\\";i:9;s:11:\\\"shipment_id\\\";i:10;s:13:\\\"total_freight\\\";s:1:\\\"1\\\";s:11:\\\"pickup_time\\\";s:8:\\\"22:35:00\\\";s:11:\\\"pickup_date\\\";s:10:\\\"2021-05-19\\\";s:22:\\\"expected_delivery_date\\\";s:10:\\\"2021-05-19\\\";s:7:\\\"comment\\\";s:9:\\\"Commonent\\\";s:14:\\\"departure_time\\\";s:8:\\\"22:35:00\\\";s:10:\\\"created_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";s:10:\\\"updated_at\\\";s:19:\\\"2021-05-18 21:38:52\\\";s:4:\\\"cost\\\";s:6:\\\"$ 1300\\\";}s:10:\\\"\\u0000*\\u0000changes\\\";a:0:{}s:8:\\\"\\u0000*\\u0000casts\\\";a:0:{}s:17:\\\"\\u0000*\\u0000classCastCache\\\";a:0:{}s:8:\\\"\\u0000*\\u0000dates\\\";a:0:{}s:13:\\\"\\u0000*\\u0000dateFormat\\\";N;s:10:\\\"\\u0000*\\u0000appends\\\";a:0:{}s:19:\\\"\\u0000*\\u0000dispatchesEvents\\\";a:0:{}s:14:\\\"\\u0000*\\u0000observables\\\";a:0:{}s:12:\\\"\\u0000*\\u0000relations\\\";a:0:{}s:10:\\\"\\u0000*\\u0000touches\\\";a:0:{}s:10:\\\"timestamps\\\";b:1;s:9:\\\"\\u0000*\\u0000hidden\\\";a:0:{}s:10:\\\"\\u0000*\\u0000visible\\\";a:0:{}s:11:\\\"\\u0000*\\u0000fillable\\\";a:0:{}}}s:10:\\\"\\u0000*\\u0000touches\\\";a:0:{}s:10:\\\"timestamps\\\";b:1;s:9:\\\"\\u0000*\\u0000hidden\\\";a:0:{}s:10:\\\"\\u0000*\\u0000visible\\\";a:0:{}s:11:\\\"\\u0000*\\u0000fillable\\\";a:0:{}}s:12:\\\"shipmentType\\\";s:9:\\\"Truckload\\\";s:15:\\\"expect_del_time\\\";s:26:\\\"Wed, May 19, 2021 10:35 PM\\\";s:4:\\\"from\\\";s:23:\\\"support@excelxpress.net\\\";s:7:\\\"subject\\\";s:15:\\\"Shipment Update\\\";s:7:\\\"content\\\";s:41:\\\"You shipment has arrived Brest in Belarus\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1621576766, 1621576766);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` bigint UNSIGNED NOT NULL,
  `country_id` bigint UNSIGNED DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shipment_info_id` bigint UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `country_id`, `city`, `type`, `created_at`, `updated_at`, `shipment_info_id`) VALUES
(1, 2, 'Brändö', 'origin', '2021-05-18 15:48:40', '2021-05-18 15:48:40', 1),
(2, 1, 'Badakhshan', 'destination', '2021-05-18 15:48:40', '2021-05-18 15:48:40', 1),
(3, 2, 'Brändö', NULL, '2021-05-18 15:48:40', '2021-05-18 15:48:40', NULL),
(13, 3, 'Berat', 'origin', '2021-05-18 16:38:37', '2021-05-18 16:38:37', 5),
(14, 1, 'Badakhshan', 'destination', '2021-05-18 16:38:37', '2021-05-18 16:38:37', 5),
(15, 2, 'Brändö', NULL, '2021-05-18 16:38:37', '2021-05-18 16:38:37', NULL),
(16, 4, 'Aïn Defla', 'origin', '2021-05-18 16:45:30', '2021-05-18 16:45:30', 6),
(17, 1, 'Badakhshan', 'destination', '2021-05-18 16:45:30', '2021-05-18 16:45:30', 6),
(18, 4, 'Aïn Defla', NULL, '2021-05-18 16:45:30', '2021-05-18 16:45:30', NULL),
(19, 2, 'Brändö', 'origin', '2021-05-18 16:55:24', '2021-05-18 16:55:24', 7),
(20, 1, 'Badakhshan', 'destination', '2021-05-18 16:55:24', '2021-05-18 16:55:24', 7),
(21, 163, 'Abia', NULL, '2021-05-18 16:55:24', '2021-05-18 16:55:24', NULL),
(22, 1, 'Badakhshan', 'origin', '2021-05-18 19:01:16', '2021-05-18 19:01:16', 8),
(23, 2, 'Brändö', 'destination', '2021-05-18 19:01:16', '2021-05-18 19:01:16', 8),
(24, 1, 'Badakhshan', NULL, '2021-05-18 19:01:16', '2021-05-18 19:01:16', NULL),
(25, 2, 'Brändö', 'origin', '2021-05-18 20:38:52', '2021-05-18 20:38:52', 9),
(26, 1, 'Badakhshan', 'destination', '2021-05-18 20:38:52', '2021-05-18 20:38:52', 9),
(27, 2, 'Brändö', NULL, '2021-05-18 20:38:52', '2021-05-18 20:38:52', NULL),
(28, 4, 'Aïn Defla', NULL, '2021-05-19 07:31:15', '2021-05-19 07:31:15', NULL),
(29, 4, 'Aïn Defla', NULL, '2021-05-19 07:31:54', '2021-05-19 07:31:54', NULL),
(30, 3, 'Berat', NULL, '2021-05-19 07:33:12', '2021-05-19 07:33:12', NULL),
(31, 3, 'Berat', NULL, '2021-05-19 07:35:56', '2021-05-19 07:35:56', NULL),
(32, 3, 'Berat', NULL, '2021-05-19 07:36:47', '2021-05-19 07:36:47', NULL),
(33, 3, 'Berat', NULL, '2021-05-19 07:41:41', '2021-05-19 07:41:41', NULL),
(34, 3, 'Berat', NULL, '2021-05-19 07:43:22', '2021-05-19 07:43:22', NULL),
(35, 3, 'Berat', NULL, '2021-05-19 07:44:10', '2021-05-19 07:44:10', NULL),
(36, 1, 'Baghlan', NULL, '2021-05-19 08:46:18', '2021-05-19 08:46:18', NULL),
(37, 21, 'Brest', NULL, '2021-05-21 04:59:26', '2021-05-21 04:59:26', NULL),
(38, 23, 'Belize', NULL, '2021-05-21 05:01:19', '2021-05-21 05:01:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `map_settings`
--

CREATE TABLE `map_settings` (
  `id` bigint UNSIGNED NOT NULL,
  `api_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `map_settings`
--

INSERT INTO `map_settings` (`id`, `api_key`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 'AIzaSyBgyB2XeKekCOEhLGxOvZcEf36ZBtFo11c', NULL, NULL, '2021-05-18 16:41:52', '2021-05-18 16:41:52');

-- --------------------------------------------------------

--
-- Table structure for table `map_setting_type`
--

CREATE TABLE `map_setting_type` (
  `id` bigint UNSIGNED NOT NULL,
  `map_type_id` bigint UNSIGNED NOT NULL,
  `map_setting_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `map_setting_type`
--

INSERT INTO `map_setting_type` (`id`, `map_type_id`, `map_setting_id`, `created_at`, `updated_at`) VALUES
(2, 3, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `map_setting_zoom_level`
--

CREATE TABLE `map_setting_zoom_level` (
  `id` bigint UNSIGNED NOT NULL,
  `map_zoom_level_id` bigint UNSIGNED NOT NULL,
  `map_setting_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `map_setting_zoom_level`
--

INSERT INTO `map_setting_zoom_level` (`id`, `map_zoom_level_id`, `map_setting_id`, `created_at`, `updated_at`) VALUES
(3, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `map_types`
--

CREATE TABLE `map_types` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_use` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `map_types`
--

INSERT INTO `map_types` (`id`, `name`, `in_use`, `created_at`, `updated_at`) VALUES
(1, 'Terrain', 0, '2021-05-18 11:53:53', '2021-05-18 16:42:22'),
(2, 'Satellite', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(3, 'Roadmap', 1, '2021-05-18 11:53:53', '2021-05-18 16:42:22'),
(4, 'Hybrid', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53');

-- --------------------------------------------------------

--
-- Table structure for table `map_zoom_levels`
--

CREATE TABLE `map_zoom_levels` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_use` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `map_zoom_levels`
--

INSERT INTO `map_zoom_levels` (`id`, `name`, `in_use`, `created_at`, `updated_at`) VALUES
(1, 'World', 1, '2021-05-18 11:53:53', '2021-05-18 16:42:46'),
(2, 'Continent', 0, '2021-05-18 11:53:53', '2021-05-18 16:42:46'),
(3, 'City', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(4, 'Streets', 0, '2021-05-18 11:53:53', '2021-05-18 16:42:22'),
(5, 'Buildings', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_11_30_231038_create_roles_table', 1),
(5, '2020_11_30_231313_create_countries_table', 1),
(6, '2020_12_01_210413_create_role_user_table', 1),
(7, '2020_12_01_215451_create_shipment_types_table', 1),
(8, '2020_12_01_220143_create_shipment_modes_table', 1),
(9, '2020_12_01_220743_create_carriers_table', 1),
(10, '2020_12_01_221406_create_payment_modes_table', 1),
(11, '2020_12_01_222409_create_shipment_statuses_table', 1),
(12, '2020_12_01_224017_create_email_settings_table', 1),
(13, '2020_12_05_091353_create_piece_types_table', 1),
(14, '2020_12_05_091610_create_shipments_table', 1),
(15, '2020_12_05_091616_create_packages_table', 1),
(16, '2020_12_05_092033_create_package_piece_type_table', 1),
(17, '2020_12_05_135623_create_map_types_table', 1),
(18, '2020_12_05_140448_create_map_zoom_levels_table', 1),
(19, '2020_12_05_142939_create_map_settings_table', 1),
(20, '2020_12_05_144815_create_map_setting_zoom_level_table', 1),
(21, '2020_12_05_145525_create_map_setting_type_table', 1),
(22, '2020_12_06_151753_create_package_settings_table', 1),
(23, '2020_12_06_160032_create_dimension_units_table', 1),
(24, '2020_12_06_162833_create_dimension_unit_package_setting_table', 1),
(25, '2020_12_06_165542_create_weight_units_table', 1),
(26, '2020_12_06_172558_create_package_setting_weight_unit_table', 1),
(27, '2020_12_10_200148_create_general_settings_table', 1),
(28, '2020_12_10_210720_add_description_cols_to_model_tables', 1),
(29, '2020_12_12_134549_create_shipment_infos_table', 1),
(30, '2020_12_13_124925_add_role_col_to_users_table', 1),
(31, '2020_12_13_140912_create_histories_table', 1),
(32, '2020_12_13_142021_create_locations_table', 1),
(33, '2020_12_13_151017_create_shipment_info_type_table', 1),
(34, '2020_12_13_151657_create_shipment_info_mode_table', 1),
(35, '2020_12_13_152432_create_shipment_payment_mode_table', 1),
(36, '2020_12_13_152850_create_carrier_shipment_info_table', 1),
(37, '2020_12_13_153733_create_history_location_table', 1),
(38, '2020_12_13_154338_create_history_shipment_status_table', 1),
(39, '2020_12_13_180509_add_address_col_users_table', 1),
(40, '2020_12_13_190617_add_departure_time_col_to_shipment_infos_table', 1),
(41, '2020_12_13_201600_create_passwords_table', 1),
(42, '2020_12_14_080855_add_shipment_id_col_to_histories_table', 1),
(43, '2020_12_16_203218_add_unit_weight_col_to_packages_table', 1),
(44, '2020_12_16_211212_add_shipment_info_id_col_to_locations_table', 1),
(45, '2020_12_17_104956_create_jobs_table', 1),
(46, '2021_03_13_221640_add_cost_col_to_shipment_infos_table', 1),
(47, '2021_12_08_093350_add_deleted_at_col_to_users_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` bigint UNSIGNED NOT NULL,
  `shipment_id` bigint UNSIGNED NOT NULL,
  `piece_type_id` bigint UNSIGNED NOT NULL,
  `quantity` tinyint NOT NULL,
  `weight` double NOT NULL,
  `volume` double NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `unit_weight` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `shipment_id`, `piece_type_id`, `quantity`, `weight`, `volume`, `description`, `created_at`, `updated_at`, `unit_weight`) VALUES
(1, 1, 2, 1, 45, 34, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '2021-05-18 15:48:40', '2021-05-18 15:48:40', 4),
(5, 5, 1, 1, 22, 10, 'Description', '2021-05-18 16:38:37', '2021-05-18 16:38:37', 4),
(6, 6, 3, 2, 5, 5, 'Description', '2021-05-18 16:45:30', '2021-05-18 16:45:30', 4),
(7, 8, 1, 1, 5, 5, 'Des', '2021-05-18 16:55:24', '2021-05-18 16:55:24', 4),
(8, 9, 4, 1, 5, 5, 'frrrrrrr', '2021-05-18 19:01:16', '2021-05-18 19:01:16', 4),
(9, 10, 3, 1, 5, 5, 'Descriptions', '2021-05-18 20:38:52', '2021-05-18 20:38:52', 4);

-- --------------------------------------------------------

--
-- Table structure for table `package_piece_type`
--

CREATE TABLE `package_piece_type` (
  `package_id` bigint UNSIGNED NOT NULL,
  `piece_type_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `package_settings`
--

CREATE TABLE `package_settings` (
  `id` bigint UNSIGNED NOT NULL,
  `enable_multi_package_admin` tinyint NOT NULL DEFAULT '1',
  `enable_multi_package_result` tinyint NOT NULL DEFAULT '1',
  `enable_dimension` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_settings`
--

INSERT INTO `package_settings` (`id`, `enable_multi_package_admin`, `enable_multi_package_result`, `enable_dimension`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2021-05-18 11:53:53', '2021-05-18 11:53:53');

-- --------------------------------------------------------

--
-- Table structure for table `package_setting_weight_unit`
--

CREATE TABLE `package_setting_weight_unit` (
  `id` bigint UNSIGNED NOT NULL,
  `package_setting_id` bigint UNSIGNED NOT NULL,
  `weight_unit_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_setting_weight_unit`
--

INSERT INTO `package_setting_weight_unit` (`id`, `package_setting_id`, `weight_unit_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `passwords`
--

CREATE TABLE `passwords` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `password_string` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `passwords`
--

INSERT INTO `passwords` (`id`, `user_id`, `password_string`, `created_at`, `updated_at`) VALUES
(1, 12, 'UTnaQerj', '2021-05-18 12:39:57', '2021-05-18 12:39:57'),
(2, 14, 'Kc2ZJ1G6', '2021-05-18 15:48:40', '2021-05-18 15:48:40'),
(6, 18, '4yO6avMa', '2021-05-18 16:38:37', '2021-05-18 16:38:37'),
(7, 19, 'u4wkopr9', '2021-05-18 16:45:30', '2021-05-18 16:45:30'),
(8, 20, '3v9sdLjM', '2021-05-18 16:55:24', '2021-05-18 16:55:24'),
(9, 21, 'bKu2c5Yt', '2021-05-18 20:38:52', '2021-05-18 20:38:52'),
(10, 22, 'v*-cPYnl', '2021-06-30 08:00:53', '2021-06-30 08:00:53');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_modes`
--

CREATE TABLE `payment_modes` (
  `id` bigint UNSIGNED NOT NULL,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_modes`
--

INSERT INTO `payment_modes` (`id`, `ref_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'fvDICZKiY9YK8qSi', 'CASH', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(2, 'HTBNnlsrd7fJWqcR', 'Cheque', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(3, 'tEiylazof4u5hTmS', 'BACS', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(4, 'O8APehxWtYNPw57M', 'Credit/Debit Card', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(5, '3Cd2Uae0lF0hDBxi', 'BitCoin', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52');

-- --------------------------------------------------------

--
-- Table structure for table `piece_types`
--

CREATE TABLE `piece_types` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `piece_types`
--

INSERT INTO `piece_types` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Pallet', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(2, 'Carton', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(3, 'Crate', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(4, 'Loose', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(5, 'Others', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint UNSIGNED NOT NULL,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `ref_id`, `title`, `alias`, `description`, `created_at`, `updated_at`) VALUES
(1, 'cWwqYiQtooNYWhv4', 'admin', 'admin', 'Site administrator.', '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(2, 'MJN1rbiMp0KYgA9S', 'shipper', 'shipper', 'Shipper of the package/s.', '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(3, '5tUljDXgwjfUXMq5', 'receiver', 'receiver', 'Receiver of the shipment.', '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(4, 'c43ggdKbvBvaLfbI', 'agent', 'agent', 'Agent of the shipment.', '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(5, 'pViuWYqzzC3bc0by', 'client', 'client', 'Client of the shipment.', '2021-05-18 11:53:52', '2021-05-18 11:53:52');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(3, 2),
(4, 3),
(3, 4),
(2, 5),
(2, 6),
(4, 7),
(3, 8),
(3, 9),
(5, 10),
(3, 11);

-- --------------------------------------------------------

--
-- Table structure for table `shipments`
--

CREATE TABLE `shipments` (
  `id` bigint UNSIGNED NOT NULL,
  `shipper_id` bigint UNSIGNED NOT NULL,
  `receiver_id` bigint UNSIGNED NOT NULL,
  `agent_id` bigint UNSIGNED DEFAULT NULL,
  `client_id` bigint UNSIGNED DEFAULT NULL,
  `tracking_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipments`
--

INSERT INTO `shipments` (`id`, `shipper_id`, `receiver_id`, `agent_id`, `client_id`, `tracking_id`, `ref_id`, `created_at`, `updated_at`) VALUES
(1, 2, 14, 8, 12, 'JSXV395283', 'mw7LPyHvWa3J3i12', '2021-05-18 15:48:40', '2021-05-18 15:48:40'),
(5, 11, 18, 4, 12, 'JSXV797396', '6cdlWdVbvJJSJS2s', '2021-05-18 16:38:37', '2021-05-18 16:38:37'),
(6, 2, 19, 8, 7, 'JSXV494020', 'egYheSCW9px6UyHo', '2021-05-18 16:45:30', '2021-05-18 16:45:30'),
(8, 2, 20, 8, 7, 'JSXV815280', 'RlDCD0fYJh2fZeCa', '2021-05-18 16:55:24', '2021-05-18 16:55:24'),
(9, 10, 19, 5, 7, 'JSXV667332', 'lZYt3X9EaKfyzucv', '2021-05-18 19:01:16', '2021-05-18 19:01:16'),
(10, 11, 21, 5, 7, 'JSXV456527', 'iEGK7mhs1VvxZtF4', '2021-05-18 20:38:52', '2021-05-18 20:38:52');

-- --------------------------------------------------------

--
-- Table structure for table `shipment_infos`
--

CREATE TABLE `shipment_infos` (
  `id` bigint UNSIGNED NOT NULL,
  `shipment_id` bigint UNSIGNED NOT NULL,
  `total_freight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pickup_time` time DEFAULT NULL,
  `pickup_date` date DEFAULT NULL,
  `expected_delivery_date` date DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `departure_time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cost` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipment_infos`
--

INSERT INTO `shipment_infos` (`id`, `shipment_id`, `total_freight`, `pickup_time`, `pickup_date`, `expected_delivery_date`, `comment`, `departure_time`, `created_at`, `updated_at`, `cost`) VALUES
(1, 1, '1', '10:00:00', '2021-05-19', '2021-05-19', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '18:00:00', '2021-05-18 15:48:40', '2021-05-18 15:48:40', '$ 450'),
(5, 5, '1', '18:20:00', '2021-05-19', '2021-05-19', 'Comment', '18:20:00', '2021-05-18 16:38:37', '2021-05-18 16:38:37', '$ 2000'),
(6, 6, '1', '18:40:00', '2021-05-19', '2021-05-19', NULL, '18:40:00', '2021-05-18 16:45:30', '2021-05-18 16:45:30', '$ 4556'),
(7, 8, '1', '18:50:00', '2021-05-19', '2021-05-19', 'Comments', '18:50:00', '2021-05-18 16:55:24', '2021-05-18 16:55:24', '€ 450'),
(8, 9, '7', '20:55:00', '2021-05-19', '2021-05-19', 'comment', '20:55:00', '2021-05-18 19:01:16', '2021-05-18 19:01:16', '$ 3000'),
(9, 10, '1', '22:35:00', '2021-05-19', '2021-05-19', 'Commonent', '22:35:00', '2021-05-18 20:38:52', '2021-05-18 20:38:52', '$ 1300');

-- --------------------------------------------------------

--
-- Table structure for table `shipment_info_mode`
--

CREATE TABLE `shipment_info_mode` (
  `id` bigint UNSIGNED NOT NULL,
  `shipment_info_id` bigint UNSIGNED NOT NULL,
  `shipment_mode_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipment_info_mode`
--

INSERT INTO `shipment_info_mode` (`id`, `shipment_info_id`, `shipment_mode_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, NULL, NULL),
(5, 5, 2, NULL, NULL),
(6, 6, 2, NULL, NULL),
(7, 7, 3, NULL, NULL),
(8, 8, 2, NULL, NULL),
(9, 9, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipment_info_type`
--

CREATE TABLE `shipment_info_type` (
  `id` bigint UNSIGNED NOT NULL,
  `shipment_info_id` bigint UNSIGNED NOT NULL,
  `shipment_type_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipment_info_type`
--

INSERT INTO `shipment_info_type` (`id`, `shipment_info_id`, `shipment_type_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(5, 5, 2, NULL, NULL),
(6, 6, 2, NULL, NULL),
(7, 7, 1, NULL, NULL),
(8, 8, 3, NULL, NULL),
(9, 9, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipment_modes`
--

CREATE TABLE `shipment_modes` (
  `id` bigint UNSIGNED NOT NULL,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipment_modes`
--

INSERT INTO `shipment_modes` (`id`, `ref_id`, `name`, `created_at`, `updated_at`, `description`) VALUES
(1, 'J9hTyRYX44UJYpW3', 'Sea Transport', '2021-05-18 11:53:52', '2021-05-18 11:53:52', NULL),
(2, 'CL0bo1ducWRTzoki', 'Land Shipping', '2021-05-18 11:53:52', '2021-05-18 11:53:52', NULL),
(3, '2r56b193Y4Fds2WB', 'Air Freight', '2021-05-18 11:53:52', '2021-05-18 11:53:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipment_payment_mode`
--

CREATE TABLE `shipment_payment_mode` (
  `id` bigint UNSIGNED NOT NULL,
  `payment_mode_id` bigint UNSIGNED NOT NULL,
  `shipment_info_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipment_payment_mode`
--

INSERT INTO `shipment_payment_mode` (`id`, `payment_mode_id`, `shipment_info_id`, `created_at`, `updated_at`) VALUES
(1, 5, 1, NULL, NULL),
(5, 5, 5, NULL, NULL),
(6, 4, 6, NULL, NULL),
(7, 4, 7, NULL, NULL),
(8, 2, 8, NULL, NULL),
(9, 1, 9, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipment_statuses`
--

CREATE TABLE `shipment_statuses` (
  `id` bigint UNSIGNED NOT NULL,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipment_statuses`
--

INSERT INTO `shipment_statuses` (`id`, `ref_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'kJc658FlmQOPgiL9', 'pending', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(2, 'I1VcK2AnAEg3Eh51', 'picked up', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(3, 'G7XZQf55ODRvLDWI', 'on hold', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(4, '6GaX7xKH1vLekaNO', 'out for delivery', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(5, '06acrMoLNQ1BjvzU', 'in transit', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(6, 'y3VvoPXN2UQqEcox', 'enroute', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(7, '7ETyTkgrFbgDZqJ5', 'cancelled', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(8, 'AeoXWx9q3E9J3mHk', 'delivered', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52'),
(9, 'G69Lb20yb1G04AHE', 'returned', NULL, '2021-05-18 11:53:52', '2021-05-18 11:53:52');

-- --------------------------------------------------------

--
-- Table structure for table `shipment_types`
--

CREATE TABLE `shipment_types` (
  `id` bigint UNSIGNED NOT NULL,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipment_types`
--

INSERT INTO `shipment_types` (`id`, `ref_id`, `name`, `created_at`, `updated_at`, `description`) VALUES
(1, 'ZGsJevCxyuRrx1dM', 'Air Freight', '2021-05-18 11:53:52', '2021-05-18 11:53:52', NULL),
(2, 'nSMrGZfTZ5KbtcMB', 'International Shipping', '2021-05-18 11:53:52', '2021-05-18 11:53:52', NULL),
(3, 'hPRbmHuaSiGgaBPN', 'Truckload', '2021-05-18 11:53:52', '2021-05-18 11:53:52', NULL),
(4, 'KdAvHFQR5N01qS7F', 'Van Move', '2021-05-18 11:53:52', '2021-05-18 11:53:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint UNSIGNED NOT NULL,
  `country_id` bigint UNSIGNED NOT NULL,
  `name` varchar(125) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `country_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Badakhshan', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(2, 1, 'Badghis', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(3, 1, 'Baghlan', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(4, 1, 'Balkh', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(5, 1, 'Bamian', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(6, 1, 'Daikondi', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(7, 1, 'Farah', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(8, 1, 'Faryab', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(9, 1, 'Ghazni', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(10, 1, 'Ghowr', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(11, 1, 'Helmand', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(12, 1, 'Herat', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(13, 1, 'Jowzjan', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(14, 1, 'Kabul', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(15, 1, 'Kandahar', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(16, 1, 'Kapisa', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(17, 1, 'Khowst', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(18, 1, 'Konar', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(19, 1, 'Kondoz', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(20, 1, 'Laghman', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(21, 1, 'Lowgar', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(22, 1, 'Nangarhar', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(23, 1, 'Nimruz', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(24, 1, 'Nurestan', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(25, 1, 'Oruzgan', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(26, 1, 'Paktia', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(27, 1, 'Paktika', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(28, 1, 'Panjshir', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(29, 1, 'Parvan', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(30, 1, 'Samangan', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(31, 1, 'Sar-e Pol', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(32, 1, 'Takhar', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(33, 1, 'Vardak', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(34, 1, 'Zabol', '2021-04-28 16:33:55', '2021-04-28 16:33:55'),
(35, 2, 'Brändö', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(36, 2, 'Eckerö', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(37, 2, 'Föglö', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(38, 2, 'Finström', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(39, 2, 'Geta', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(40, 2, 'Hammarland', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(41, 2, 'Jomala', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(42, 2, 'Kökar', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(43, 2, 'Kumlinge', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(44, 2, 'Lemland', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(45, 2, 'Lumparland', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(46, 2, 'Mariehamn', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(47, 2, 'Saltvik', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(48, 2, 'Sottunga', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(49, 2, 'Sund', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(50, 2, 'Vårdö', '2021-04-28 16:33:56', '2021-04-28 16:33:56'),
(51, 3, 'Berat', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(52, 3, 'Dibër', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(53, 3, 'Durrës', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(54, 3, 'Elbasan', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(55, 3, 'Fier', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(56, 3, 'Gjirokastër', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(57, 3, 'Korçë', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(58, 3, 'Kukës', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(59, 3, 'Lezhë', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(60, 3, 'Shkodër', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(61, 3, 'Tiranë', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(62, 3, 'Vlorë', '2021-04-28 16:33:57', '2021-04-28 16:33:57'),
(63, 4, 'Aïn Defla', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(64, 4, 'Aïn Témouchent', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(65, 4, 'Adrar', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(66, 4, 'Alger', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(67, 4, 'Annaba', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(68, 4, 'Batna', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(69, 4, 'Béchar', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(70, 4, 'Béjaïa', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(71, 4, 'Biskra', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(72, 4, 'Blida', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(73, 4, 'Bordj Bou Arréridj', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(74, 4, 'Bouira', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(75, 4, 'Boumerdès', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(76, 4, 'Chlef', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(77, 4, 'Constantine', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(78, 4, 'Djelfa', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(79, 4, 'El Bayadh', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(80, 4, 'El Oued', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(81, 4, 'El Tarf', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(82, 4, 'Ghardaïa', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(83, 4, 'Guelma', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(84, 4, 'Illizi', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(85, 4, 'Jijel', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(86, 4, 'Khenchela', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(87, 4, 'Laghouat', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(88, 4, 'Mascara', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(89, 4, 'Médéa', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(90, 4, 'Mila', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(91, 4, 'Mostaganem', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(92, 4, 'Msila', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(93, 4, 'Naama', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(94, 4, 'Oran', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(95, 4, 'Ouargla', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(96, 4, 'Oum el Bouaghi', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(97, 4, 'Relizane', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(98, 4, 'Saïda', '2021-04-28 16:33:58', '2021-04-28 16:33:58'),
(99, 4, 'Sétif', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(100, 4, 'Sidi Bel Abbès', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(101, 4, 'Skikda', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(102, 4, 'Souk Ahras', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(103, 4, 'Tamanghasset', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(104, 4, 'Tébessa', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(105, 4, 'Tiaret', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(106, 4, 'Tindouf', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(107, 4, 'Tipaza', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(108, 4, 'Tissemsilt', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(109, 4, 'Tizi Ouzou', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(110, 4, 'Tlemcen', '2021-04-28 16:33:59', '2021-04-28 16:33:59'),
(111, 5, 'Eastern', '2021-04-28 16:34:00', '2021-04-28 16:34:00'),
(112, 5, 'Manu\'a', '2021-04-28 16:34:00', '2021-04-28 16:34:00'),
(113, 5, 'Unorganized', '2021-04-28 16:34:00', '2021-04-28 16:34:00'),
(114, 5, 'Western', '2021-04-28 16:34:00', '2021-04-28 16:34:00'),
(115, 6, 'Andorra la Vella', '2021-04-28 16:34:01', '2021-04-28 16:34:01'),
(116, 6, 'Canillo', '2021-04-28 16:34:01', '2021-04-28 16:34:01'),
(117, 6, 'Encamp', '2021-04-28 16:34:01', '2021-04-28 16:34:01'),
(118, 6, 'Escaldes-Engordany', '2021-04-28 16:34:01', '2021-04-28 16:34:01'),
(119, 6, 'La Massana', '2021-04-28 16:34:01', '2021-04-28 16:34:01'),
(120, 6, 'Ordino', '2021-04-28 16:34:01', '2021-04-28 16:34:01'),
(121, 6, 'Sant Julià de Lòria', '2021-04-28 16:34:01', '2021-04-28 16:34:01'),
(122, 7, 'Bengo', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(123, 7, 'Benguela', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(124, 7, 'Bié', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(125, 7, 'Cabinda', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(126, 7, 'Cuando Cubango', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(127, 7, 'Cuanza Norte', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(128, 7, 'Cuanza Sul', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(129, 7, 'Cunene', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(130, 7, 'Huambo', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(131, 7, 'Huíla', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(132, 7, 'Luanda', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(133, 7, 'Lunda Norte', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(134, 7, 'Lunda Sul', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(135, 7, 'Malanje', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(136, 7, 'Moxico', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(137, 7, 'Namibe', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(138, 7, 'Uíge', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(139, 7, 'Zaire', '2021-04-28 16:34:02', '2021-04-28 16:34:02'),
(140, 8, 'Blowing Point', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(141, 8, 'East End', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(142, 8, 'George Hill', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(143, 8, 'Island Harbour', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(144, 8, 'North Hill', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(145, 8, 'North Side', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(146, 8, 'Sandy Ground', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(147, 8, 'Sandy Hill', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(148, 8, 'South Hill', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(149, 8, 'Stoney Ground', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(150, 8, 'The Farrington', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(151, 8, 'The Quarter (including Rey Hill)', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(152, 8, 'The Valley (including Upper & So', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(153, 8, 'West End (including Long Bay)', '2021-04-28 16:34:03', '2021-04-28 16:34:03'),
(154, 10, 'Barbuda', '2021-04-28 16:34:05', '2021-04-28 16:34:05'),
(155, 10, 'Redonda', '2021-04-28 16:34:05', '2021-04-28 16:34:05'),
(156, 10, 'Saint George', '2021-04-28 16:34:05', '2021-04-28 16:34:05'),
(157, 10, 'Saint John', '2021-04-28 16:34:05', '2021-04-28 16:34:05'),
(158, 10, 'Saint Mary', '2021-04-28 16:34:05', '2021-04-28 16:34:05'),
(159, 10, 'Saint Paul', '2021-04-28 16:34:05', '2021-04-28 16:34:05'),
(160, 10, 'Saint Peter', '2021-04-28 16:34:05', '2021-04-28 16:34:05'),
(161, 10, 'Saint Philip', '2021-04-28 16:34:05', '2021-04-28 16:34:05'),
(162, 11, 'Buenos Aires', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(163, 11, 'Catamarca', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(164, 11, 'Córdoba', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(165, 11, 'Chaco', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(166, 11, 'Chubut', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(167, 11, 'Ciudad de Buenos Aires', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(168, 11, 'Corrientes', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(169, 11, 'Entre Ríos', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(170, 11, 'Formosa', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(171, 11, 'Jujuy', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(172, 11, 'La Pampa', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(173, 11, 'La Rioja', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(174, 11, 'Mendoza', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(175, 11, 'Misiones', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(176, 11, 'Neuquén', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(177, 11, 'Río Negro', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(178, 11, 'Salta', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(179, 11, 'San Juan', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(180, 11, 'San Luis', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(181, 11, 'Santa Cruz', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(182, 11, 'Santa Fe', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(183, 11, 'Santiago del Estero', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(184, 11, 'Tierra del Fuego', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(185, 11, 'Tucumán', '2021-04-28 16:34:06', '2021-04-28 16:34:06'),
(186, 12, 'Aragacot\'n', '2021-04-28 16:34:07', '2021-04-28 16:34:07'),
(187, 12, 'Ararat', '2021-04-28 16:34:07', '2021-04-28 16:34:07'),
(188, 12, 'Armavir', '2021-04-28 16:34:07', '2021-04-28 16:34:07'),
(189, 12, 'Erevan', '2021-04-28 16:34:07', '2021-04-28 16:34:07'),
(190, 12, 'Gelark\'unik\'', '2021-04-28 16:34:07', '2021-04-28 16:34:07'),
(191, 12, 'Kotayk\'', '2021-04-28 16:34:07', '2021-04-28 16:34:07'),
(192, 12, 'Loṙi', '2021-04-28 16:34:07', '2021-04-28 16:34:07'),
(193, 12, 'Syunik\'', '2021-04-28 16:34:07', '2021-04-28 16:34:07'),
(194, 12, 'Tavuš', '2021-04-28 16:34:07', '2021-04-28 16:34:07'),
(195, 12, 'Vayoc\'Jor', '2021-04-28 16:34:07', '2021-04-28 16:34:07'),
(196, 12, 'Širak', '2021-04-28 16:34:07', '2021-04-28 16:34:07'),
(197, 13, 'Aruba', '2021-04-28 16:34:08', '2021-04-28 16:34:08'),
(198, 14, 'ACT', '2021-04-28 16:34:09', '2021-04-28 16:34:09'),
(199, 14, 'Ashmore and Cartier Islands', '2021-04-28 16:34:09', '2021-04-28 16:34:09'),
(200, 14, 'Coral Sea Islands Territory', '2021-04-28 16:34:09', '2021-04-28 16:34:09'),
(201, 14, 'New South Wales', '2021-04-28 16:34:09', '2021-04-28 16:34:09'),
(202, 14, 'Northern Teritory', '2021-04-28 16:34:09', '2021-04-28 16:34:09'),
(203, 14, 'Queensland', '2021-04-28 16:34:09', '2021-04-28 16:34:09'),
(204, 14, 'South Australia', '2021-04-28 16:34:09', '2021-04-28 16:34:09'),
(205, 14, 'Tasmania', '2021-04-28 16:34:09', '2021-04-28 16:34:09'),
(206, 14, 'Victoria', '2021-04-28 16:34:09', '2021-04-28 16:34:09'),
(207, 14, 'Western Australia', '2021-04-28 16:34:09', '2021-04-28 16:34:09'),
(208, 15, 'Burgenland', '2021-04-28 16:34:10', '2021-04-28 16:34:10'),
(209, 15, 'Carinthia', '2021-04-28 16:34:10', '2021-04-28 16:34:10'),
(210, 15, 'Lower Austria', '2021-04-28 16:34:10', '2021-04-28 16:34:10'),
(211, 15, 'Salzburg', '2021-04-28 16:34:10', '2021-04-28 16:34:10'),
(212, 15, 'Styria', '2021-04-28 16:34:10', '2021-04-28 16:34:10'),
(213, 15, 'Tyrol', '2021-04-28 16:34:10', '2021-04-28 16:34:10'),
(214, 15, 'Upper Austria', '2021-04-28 16:34:10', '2021-04-28 16:34:10'),
(215, 15, 'Vienna', '2021-04-28 16:34:10', '2021-04-28 16:34:10'),
(216, 15, 'Vorarlberg', '2021-04-28 16:34:10', '2021-04-28 16:34:10'),
(217, 16, 'Abşeron', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(218, 16, 'Aǧcabədi', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(219, 16, 'Aǧdam', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(220, 16, 'Aǧdaş', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(221, 16, 'Aǧstafa', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(222, 16, 'Aǧsu', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(223, 16, 'Astara', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(224, 16, 'Babək', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(225, 16, 'Bakı', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(226, 16, 'Balakən', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(227, 16, 'Beyləqan', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(228, 16, 'Bərdə', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(229, 16, 'Biləsuvar', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(230, 16, 'Cəbrayıl', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(231, 16, 'Cəlilabad', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(232, 16, 'Culfa', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(233, 16, 'Daşkəsən', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(234, 16, 'Dəvəçi', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(235, 16, 'Füzuli', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(236, 16, 'Göyçay', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(237, 16, 'Gədəbəy', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(238, 16, 'Gəncə', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(239, 16, 'Goranboy', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(240, 16, 'Hacıqabul', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(241, 16, 'Kürdəmir', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(242, 16, 'Kəlbəcər', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(243, 16, 'Laçın', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(244, 16, 'Lerik', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(245, 16, 'Lənkəran', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(246, 16, 'Lənkəran', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(247, 16, 'Masallı', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(248, 16, 'Mingəçevir', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(249, 16, 'Naftalan', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(250, 16, 'Naxçıvan', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(251, 16, 'Neftçala', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(252, 16, 'Oǧuz', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(253, 16, 'Ordubad', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(254, 16, 'Qax', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(255, 16, 'Qazax', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(256, 16, 'Qəbələ', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(257, 16, 'Qobustan', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(258, 16, 'Quba', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(259, 16, 'Qubadlı', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(260, 16, 'Qusar', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(261, 16, 'Saatlı', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(262, 16, 'Sabirabad', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(263, 16, 'Salyan', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(264, 16, 'Samux', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(265, 16, 'Sədərək', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(266, 16, 'Siyəzən', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(267, 16, 'Sumqayıt', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(268, 16, 'Tərtər', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(269, 16, 'Tovuz', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(270, 16, 'Ucar', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(271, 16, 'Xaçmaz', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(272, 16, 'Xankəndi', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(273, 16, 'Xanlar', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(274, 16, 'Xizı', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(275, 16, 'Xocalı', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(276, 16, 'Xocavənd', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(277, 16, 'Yardımlı', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(278, 16, 'Yevlax', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(279, 16, 'Yevlax', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(280, 16, 'Zaqatala', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(281, 16, 'Zəngilan', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(282, 16, 'Zərdab', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(283, 16, 'Şahbuz', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(284, 16, 'Şamaxı', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(285, 16, 'Şəki', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(286, 16, 'Şəki', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(287, 16, 'Şəmkir', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(288, 16, 'Şərur', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(289, 16, 'Şuşa', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(290, 16, 'Şuşa', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(291, 16, 'Əli Bayramlı', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(292, 16, 'İmişli', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(293, 16, 'İsmayıllı', '2021-04-28 16:34:11', '2021-04-28 16:34:11'),
(294, 17, 'Acklins', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(295, 17, 'Berry Islands', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(296, 17, 'Bimini', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(297, 17, 'Black Point', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(298, 17, 'Cat Island', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(299, 17, 'Central Abaco', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(300, 17, 'Central Andros', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(301, 17, 'Central Eleuthera', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(302, 17, 'City of Freeport', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(303, 17, 'Crooked Island', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(304, 17, 'East Grand Bahama', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(305, 17, 'Exuma', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(306, 17, 'Grand Cay', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(307, 17, 'Harbour Island', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(308, 17, 'Hope Town', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(309, 17, 'Inagua', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(310, 17, 'Long Island', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(311, 17, 'Mangrove Cay', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(312, 17, 'Mayaguana', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(313, 17, 'Moore\'s Island', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(314, 17, 'New Providence', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(315, 17, 'North Abaco', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(316, 17, 'North Andros', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(317, 17, 'North Eleuthera', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(318, 17, 'Ragged Island', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(319, 17, 'Rum Cay', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(320, 17, 'San Salvador', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(321, 17, 'South Abaco', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(322, 17, 'South Andros', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(323, 17, 'South Eleuthera', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(324, 17, 'Spanish Wells', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(325, 17, 'West Grand Bahama', '2021-04-28 16:34:12', '2021-04-28 16:34:12'),
(326, 18, 'Capital', '2021-04-28 16:34:13', '2021-04-28 16:34:13'),
(327, 18, 'Central', '2021-04-28 16:34:13', '2021-04-28 16:34:13'),
(328, 18, 'Muharraq', '2021-04-28 16:34:13', '2021-04-28 16:34:13'),
(329, 18, 'Northern', '2021-04-28 16:34:13', '2021-04-28 16:34:13'),
(330, 18, 'Southern', '2021-04-28 16:34:13', '2021-04-28 16:34:13'),
(331, 19, 'Barisal', '2021-04-28 16:34:14', '2021-04-28 16:34:14'),
(332, 19, 'Chittagong', '2021-04-28 16:34:14', '2021-04-28 16:34:14'),
(333, 19, 'Dhaka', '2021-04-28 16:34:14', '2021-04-28 16:34:14'),
(334, 19, 'Khulna', '2021-04-28 16:34:14', '2021-04-28 16:34:14'),
(335, 19, 'Rajshahi', '2021-04-28 16:34:14', '2021-04-28 16:34:14'),
(336, 19, 'Sylhet', '2021-04-28 16:34:14', '2021-04-28 16:34:14'),
(337, 20, 'Christ Church', '2021-04-28 16:34:15', '2021-04-28 16:34:15'),
(338, 20, 'Saint Andrew', '2021-04-28 16:34:15', '2021-04-28 16:34:15'),
(339, 20, 'Saint George', '2021-04-28 16:34:15', '2021-04-28 16:34:15'),
(340, 20, 'Saint James', '2021-04-28 16:34:15', '2021-04-28 16:34:15'),
(341, 20, 'Saint John', '2021-04-28 16:34:15', '2021-04-28 16:34:15'),
(342, 20, 'Saint Joseph', '2021-04-28 16:34:15', '2021-04-28 16:34:15'),
(343, 20, 'Saint Lucy', '2021-04-28 16:34:15', '2021-04-28 16:34:15'),
(344, 20, 'Saint Michael', '2021-04-28 16:34:15', '2021-04-28 16:34:15'),
(345, 20, 'Saint Peter', '2021-04-28 16:34:15', '2021-04-28 16:34:15'),
(346, 20, 'Saint Philip', '2021-04-28 16:34:15', '2021-04-28 16:34:15'),
(347, 20, 'Saint Thomas', '2021-04-28 16:34:15', '2021-04-28 16:34:15'),
(348, 21, 'Brest', '2021-04-28 16:34:17', '2021-04-28 16:34:17'),
(349, 21, 'Homyel\'', '2021-04-28 16:34:17', '2021-04-28 16:34:17'),
(350, 21, 'Horad Minsk', '2021-04-28 16:34:17', '2021-04-28 16:34:17'),
(351, 21, 'Hrodna', '2021-04-28 16:34:17', '2021-04-28 16:34:17'),
(352, 21, 'Mahilyow', '2021-04-28 16:34:17', '2021-04-28 16:34:17'),
(353, 21, 'Minsk', '2021-04-28 16:34:17', '2021-04-28 16:34:17'),
(354, 21, 'Vitsyebsk', '2021-04-28 16:34:17', '2021-04-28 16:34:17'),
(355, 22, 'Antwerp', '2021-04-28 16:34:18', '2021-04-28 16:34:18'),
(356, 22, 'Brussels', '2021-04-28 16:34:18', '2021-04-28 16:34:18'),
(357, 22, 'East Flanders', '2021-04-28 16:34:18', '2021-04-28 16:34:18'),
(358, 22, 'Flemish Brabant', '2021-04-28 16:34:18', '2021-04-28 16:34:18'),
(359, 22, 'Hainaut', '2021-04-28 16:34:18', '2021-04-28 16:34:18'),
(360, 22, 'Liege', '2021-04-28 16:34:18', '2021-04-28 16:34:18'),
(361, 22, 'Limburg', '2021-04-28 16:34:18', '2021-04-28 16:34:18'),
(362, 22, 'Luxembourg', '2021-04-28 16:34:18', '2021-04-28 16:34:18'),
(363, 22, 'Namur', '2021-04-28 16:34:18', '2021-04-28 16:34:18'),
(364, 22, 'Walloon Brabant', '2021-04-28 16:34:18', '2021-04-28 16:34:18'),
(365, 22, 'West Flanders', '2021-04-28 16:34:18', '2021-04-28 16:34:18'),
(366, 23, 'Belize', '2021-04-28 16:34:19', '2021-04-28 16:34:19'),
(367, 23, 'Cayo', '2021-04-28 16:34:19', '2021-04-28 16:34:19'),
(368, 23, 'Corozal', '2021-04-28 16:34:19', '2021-04-28 16:34:19'),
(369, 23, 'Orange Walk', '2021-04-28 16:34:19', '2021-04-28 16:34:19'),
(370, 23, 'Stann Creek', '2021-04-28 16:34:19', '2021-04-28 16:34:19'),
(371, 23, 'Toledo', '2021-04-28 16:34:19', '2021-04-28 16:34:19'),
(372, 24, 'Alibori', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(373, 24, 'Atacora', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(374, 24, 'Atlantique', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(375, 24, 'Borgou', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(376, 24, 'Collines', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(377, 24, 'Couffo', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(378, 24, 'Donga', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(379, 24, 'Littoral', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(380, 24, 'Mono', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(381, 24, 'Ouémé', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(382, 24, 'Plateau', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(383, 24, 'Zou', '2021-04-28 16:34:20', '2021-04-28 16:34:20'),
(384, 25, 'Devonshire', '2021-04-28 16:34:21', '2021-04-28 16:34:21'),
(385, 25, 'Hamilton', '2021-04-28 16:34:21', '2021-04-28 16:34:21'),
(386, 25, 'Hamilton municipality', '2021-04-28 16:34:21', '2021-04-28 16:34:21'),
(387, 25, 'Paget', '2021-04-28 16:34:21', '2021-04-28 16:34:21'),
(388, 25, 'Pembroke', '2021-04-28 16:34:21', '2021-04-28 16:34:21'),
(389, 25, 'Saint George municipality', '2021-04-28 16:34:21', '2021-04-28 16:34:21'),
(390, 25, 'Saint George\'s', '2021-04-28 16:34:21', '2021-04-28 16:34:21'),
(391, 25, 'Sandys', '2021-04-28 16:34:21', '2021-04-28 16:34:21'),
(392, 25, 'Smiths', '2021-04-28 16:34:21', '2021-04-28 16:34:21'),
(393, 25, 'Southampton', '2021-04-28 16:34:21', '2021-04-28 16:34:21'),
(394, 25, 'Warwick', '2021-04-28 16:34:21', '2021-04-28 16:34:21'),
(395, 26, 'Bumthang', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(396, 26, 'Chhukha', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(397, 26, 'Chirang', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(398, 26, 'Daga', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(399, 26, 'Gasa', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(400, 26, 'Geylegphug', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(401, 26, 'Ha', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(402, 26, 'Lhuntshi', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(403, 26, 'Mongar', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(404, 26, 'Paro', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(405, 26, 'Pemagatsel', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(406, 26, 'Punakha', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(407, 26, 'Samchi', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(408, 26, 'Samdrup Jongkhar', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(409, 26, 'Shemgang', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(410, 26, 'Tashi Yangtse', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(411, 26, 'Tashigang', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(412, 26, 'Thimphu', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(413, 26, 'Tongsa', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(414, 26, 'Wangdi Phodrang', '2021-04-28 16:34:22', '2021-04-28 16:34:22'),
(415, 27, 'Chuquisaca', '2021-04-28 16:34:23', '2021-04-28 16:34:23'),
(416, 27, 'Cochabamba', '2021-04-28 16:34:23', '2021-04-28 16:34:23'),
(417, 27, 'El Beni', '2021-04-28 16:34:23', '2021-04-28 16:34:23'),
(418, 27, 'La Paz', '2021-04-28 16:34:23', '2021-04-28 16:34:23'),
(419, 27, 'Oruro', '2021-04-28 16:34:23', '2021-04-28 16:34:23'),
(420, 27, 'Pando', '2021-04-28 16:34:23', '2021-04-28 16:34:23'),
(421, 27, 'Potosí', '2021-04-28 16:34:23', '2021-04-28 16:34:23'),
(422, 27, 'Santa Cruz', '2021-04-28 16:34:23', '2021-04-28 16:34:23'),
(423, 27, 'Tarija', '2021-04-28 16:34:23', '2021-04-28 16:34:23'),
(424, 29, 'Distrikt Brãko', '2021-04-28 16:34:25', '2021-04-28 16:34:25'),
(425, 29, 'Federacija Bosna i Hercegovina', '2021-04-28 16:34:25', '2021-04-28 16:34:25'),
(426, 29, 'Republika Srpska', '2021-04-28 16:34:25', '2021-04-28 16:34:25'),
(427, 30, 'Central', '2021-04-28 16:34:26', '2021-04-28 16:34:26'),
(428, 30, 'Ghanzi', '2021-04-28 16:34:26', '2021-04-28 16:34:26'),
(429, 30, 'Kgalagadi', '2021-04-28 16:34:26', '2021-04-28 16:34:26'),
(430, 30, 'Kgatleng', '2021-04-28 16:34:26', '2021-04-28 16:34:26'),
(431, 30, 'Kweneng', '2021-04-28 16:34:26', '2021-04-28 16:34:26'),
(432, 30, 'North-East', '2021-04-28 16:34:26', '2021-04-28 16:34:26'),
(433, 30, 'North-West', '2021-04-28 16:34:26', '2021-04-28 16:34:26'),
(434, 30, 'South-East', '2021-04-28 16:34:26', '2021-04-28 16:34:26'),
(435, 30, 'Southern', '2021-04-28 16:34:26', '2021-04-28 16:34:26'),
(436, 31, 'Bouvet Island', '2021-04-28 16:34:27', '2021-04-28 16:34:27'),
(437, 32, 'Acre', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(438, 32, 'Alagoas', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(439, 32, 'Amapá', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(440, 32, 'Amazonas', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(441, 32, 'Bahia', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(442, 32, 'Ceará', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(443, 32, 'Distrito Federal', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(444, 32, 'Espírito Santo', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(445, 32, 'Goiás', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(446, 32, 'Maranhão', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(447, 32, 'Mato Grosso', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(448, 32, 'Mato Grosso do Sul', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(449, 32, 'Minas Gerais', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(450, 32, 'Paraíba', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(451, 32, 'Paraná', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(452, 32, 'Pará', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(453, 32, 'Pernambuco', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(454, 32, 'Piauí', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(455, 32, 'Rio de Janeiro', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(456, 32, 'Rio Grande do Norte', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(457, 32, 'Rio Grande do Sul', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(458, 32, 'Rondônia', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(459, 32, 'Roraima', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(460, 32, 'Santa Catarina', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(461, 32, 'São Paulo', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(462, 32, 'Sergipe', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(463, 32, 'Tocantins', '2021-04-28 16:34:30', '2021-04-28 16:34:30'),
(464, 33, 'British Indian Ocean Territory', '2021-04-28 16:34:31', '2021-04-28 16:34:31'),
(465, 35, 'British Virgin Islands', '2021-04-28 16:34:33', '2021-04-28 16:34:33'),
(466, 36, 'Saint Croix', '2021-04-28 16:34:34', '2021-04-28 16:34:34'),
(467, 36, 'Saint John', '2021-04-28 16:34:34', '2021-04-28 16:34:34'),
(468, 36, 'Saint Thomas', '2021-04-28 16:34:34', '2021-04-28 16:34:34'),
(469, 37, 'Belait', '2021-04-28 16:34:35', '2021-04-28 16:34:35'),
(470, 37, 'Brunei and Muara', '2021-04-28 16:34:35', '2021-04-28 16:34:35'),
(471, 37, 'Temburong', '2021-04-28 16:34:35', '2021-04-28 16:34:35'),
(472, 37, 'Tutong', '2021-04-28 16:34:35', '2021-04-28 16:34:35'),
(473, 38, 'Burgas', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(474, 38, 'Dobrich', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(475, 38, 'Gabrovo', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(476, 38, 'Stara Zagora', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(477, 38, 'Kyustendil', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(478, 38, 'Lovech', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(479, 38, 'Montana', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(480, 38, 'Plovdiv', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(481, 38, 'Ruse', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(482, 38, 'Turgovishte', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(483, 38, 'Vratsa', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(484, 38, 'Yambol', '2021-04-28 16:34:36', '2021-04-28 16:34:36'),
(485, 39, 'Balé', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(486, 39, 'Bam', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(487, 39, 'Banwa', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(488, 39, 'Bazèga', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(489, 39, 'Bougouriba', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(490, 39, 'Boulgou', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(491, 39, 'Boulkiemdé', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(492, 39, 'Ganzourgou', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(493, 39, 'Gnagna', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(494, 39, 'Gourma', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(495, 39, 'Houet', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(496, 39, 'Ioba', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(497, 39, 'Kadiogo', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(498, 39, 'Kénédougou', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(499, 39, 'Komoé', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(500, 39, 'Komondjari', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(501, 39, 'Kompienga', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(502, 39, 'Kossi', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(503, 39, 'Koulpélogo', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(504, 39, 'Kouritenga', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(505, 39, 'Kourwéogo', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(506, 39, 'Léraba', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(507, 39, 'Loroum', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(508, 39, 'Mouhoun', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(509, 39, 'Namentenga', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(510, 39, 'Naouri', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(511, 39, 'Nayala', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(512, 39, 'Noumbiel', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(513, 39, 'Oubritenga', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(514, 39, 'Oudalan', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(515, 39, 'Passoré', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(516, 39, 'Poni', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(517, 39, 'Sanguié', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(518, 39, 'Sanmatenga', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(519, 39, 'Séno', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(520, 39, 'Sissili', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(521, 39, 'Soum', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(522, 39, 'Sourou', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(523, 39, 'Tapoa', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(524, 39, 'Tui', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(525, 39, 'Yagha', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(526, 39, 'Yatenga', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(527, 39, 'Ziro', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(528, 39, 'Zondoma', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(529, 39, 'Zoundwéogo', '2021-04-28 16:34:37', '2021-04-28 16:34:37'),
(530, 40, 'Bubanza', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(531, 40, 'Bujumbura Mairie', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(532, 40, 'Bujumbura Rural', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(533, 40, 'Bururi', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(534, 40, 'Cankuzo', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(535, 40, 'Cibitoke', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(536, 40, 'Gitega', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(537, 40, 'Karuzi', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(538, 40, 'Kayanza', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(539, 40, 'Kirundo', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(540, 40, 'Makamba', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(541, 40, 'Muramvya', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(542, 40, 'Muyinga', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(543, 40, 'Mwaro', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(544, 40, 'Ngozi', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(545, 40, 'Rutana', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(546, 40, 'Ruyigi', '2021-04-28 16:34:38', '2021-04-28 16:34:38'),
(547, 41, 'Bântéay Méanchey', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(548, 41, 'Bătdâmbâng', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(549, 41, 'Kaôh Kŏng', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(550, 41, 'Kâmpóng Cham', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(551, 41, 'Kâmpóng Chhnăng', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(552, 41, 'Kâmpóng Spœ', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(553, 41, 'Kâmpóng Thum', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(554, 41, 'Kâmpôt', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(555, 41, 'Kândal', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(556, 41, 'Krâchéh', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(557, 41, 'Krong Keb', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(558, 41, 'Krong Pailin', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(559, 41, 'Krŏng Preăh Sihanouk', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(560, 41, 'Môndól Kiri', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(561, 41, 'Phnom Penh', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(562, 41, 'Poŭthĭsăt', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(563, 41, 'Prey Vêng', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(564, 41, 'Preăh Vihéar', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(565, 41, 'Rôtânôkiri', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(566, 41, 'Siĕmréab', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(567, 41, 'Stœ̆ng Trêng', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(568, 41, 'Svay Riĕng', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(569, 41, 'Takêv', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(570, 41, 'Ŏtdâr Méanchey', '2021-04-28 16:34:39', '2021-04-28 16:34:39'),
(571, 42, 'Adamaoua', '2021-04-28 16:34:40', '2021-04-28 16:34:40'),
(572, 42, 'Centre', '2021-04-28 16:34:40', '2021-04-28 16:34:40'),
(573, 42, 'Est', '2021-04-28 16:34:40', '2021-04-28 16:34:40'),
(574, 42, 'Extrême-Nord', '2021-04-28 16:34:40', '2021-04-28 16:34:40'),
(575, 42, 'Littoral', '2021-04-28 16:34:40', '2021-04-28 16:34:40'),
(576, 42, 'Nord', '2021-04-28 16:34:40', '2021-04-28 16:34:40'),
(577, 42, 'Nord-Ouest', '2021-04-28 16:34:40', '2021-04-28 16:34:40'),
(578, 42, 'Ouest', '2021-04-28 16:34:40', '2021-04-28 16:34:40'),
(579, 42, 'Sud', '2021-04-28 16:34:40', '2021-04-28 16:34:40'),
(580, 42, 'Sud-OuestTakêv', '2021-04-28 16:34:40', '2021-04-28 16:34:40'),
(581, 43, 'Alberta', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(582, 43, 'British Columbia', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(583, 43, 'Manitoba', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(584, 43, 'New Brunswick', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(585, 43, 'Newfoundland and Labrador', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(586, 43, 'Northwest Territories', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(587, 43, 'Nova Scotia', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(588, 43, 'Nunavut', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(589, 43, 'Ontario', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(590, 43, 'Prince Edward Island', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(591, 43, 'Quebec', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(592, 43, 'Saskatchewan', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(593, 43, 'Yukon Territory', '2021-04-28 16:34:41', '2021-04-28 16:34:41'),
(594, 44, 'Boa Vista', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(595, 44, 'Brava', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(596, 44, 'Maio', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(597, 44, 'Mosteiros', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(598, 44, 'Paúl', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(599, 44, 'Porto Novo', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(600, 44, 'Praia', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(601, 44, 'Ribeira Grande', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(602, 44, 'Sal', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(603, 44, 'Santa Catarina', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(604, 44, 'Santa Cruz', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(605, 44, 'São Domingos', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(606, 44, 'São Filipe', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(607, 44, 'São Miguel', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(608, 44, 'São Nicolau', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(609, 44, 'São Vicente', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(610, 44, 'Tarrafal', '2021-04-28 16:34:42', '2021-04-28 16:34:42'),
(611, 45, 'Bodden Town', '2021-04-28 16:34:43', '2021-04-28 16:34:43'),
(612, 45, 'Cayman Brac', '2021-04-28 16:34:43', '2021-04-28 16:34:43'),
(613, 45, 'East End', '2021-04-28 16:34:43', '2021-04-28 16:34:43'),
(614, 45, 'George Town', '2021-04-28 16:34:43', '2021-04-28 16:34:43'),
(615, 45, 'Little Cayman', '2021-04-28 16:34:43', '2021-04-28 16:34:43'),
(616, 45, 'North Side', '2021-04-28 16:34:43', '2021-04-28 16:34:43'),
(617, 45, 'West Bay', '2021-04-28 16:34:43', '2021-04-28 16:34:43'),
(618, 46, 'Bamingui-Bangoran', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(619, 46, 'Bangui', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(620, 46, 'Basse-Kotto', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(621, 46, 'Haut-Mbomou', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(622, 46, 'Haute-Kotto', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(623, 46, 'Kémo', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(624, 46, 'Lobaye', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(625, 46, 'Mambéré-Kadéï', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(626, 46, 'Mbomou', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(627, 46, 'Nana-Grébizi', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(628, 46, 'Nana-Mambéré', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(629, 46, 'Ombella-M\'Poko', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(630, 46, 'Ouaka', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(631, 46, 'Ouham', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(632, 46, 'Ouham-Pendé', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(633, 46, 'Sangha-Mbaéré', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(634, 46, 'Vakaga', '2021-04-28 16:34:44', '2021-04-28 16:34:44'),
(635, 47, 'Batha', '2021-04-28 16:34:45', '2021-04-28 16:34:45'),
(636, 47, 'Borkou-Ennedi-Tibesti', '2021-04-28 16:34:45', '2021-04-28 16:34:45'),
(637, 47, 'Chari-Baguirmi', '2021-04-28 16:34:45', '2021-04-28 16:34:45'),
(638, 47, 'Guéra', '2021-04-28 16:34:45', '2021-04-28 16:34:45'),
(639, 47, 'Hadjer-Lamis', '2021-04-28 16:34:45', '2021-04-28 16:34:45'),
(640, 47, 'Kanem', '2021-04-28 16:34:45', '2021-04-28 16:34:45'),
(641, 47, 'Lac', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(642, 47, 'Logone Occidental', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(643, 47, 'Logone Oriental', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(644, 47, 'Mandoul', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(645, 47, 'Mayo-Kebbi Est', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(646, 47, 'Mayo-Kebbi Ouest', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(647, 47, 'Moyen-Chari', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(648, 47, 'Ouaddaï', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(649, 47, 'Salamat', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(650, 47, 'Tandjilé', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(651, 47, 'Ville de N\'Djamena', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(652, 47, 'Wadi Fira', '2021-04-28 16:34:46', '2021-04-28 16:34:46'),
(653, 48, 'Aisén del General Carlos Ibáñ', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(654, 48, 'Antofagasta', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(655, 48, 'Araucanía', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(656, 48, 'Atacama', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(657, 48, 'Bío-Bío', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(658, 48, 'Coquimbo', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(659, 48, 'Libertador General Bernardo O\'Hi', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(660, 48, 'Los Lagos', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(661, 48, 'Magallanes y Antártica Chilena', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(662, 48, 'Maule', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(663, 48, 'Región Metropolitana de Santiag', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(664, 48, 'Tarapacá', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(665, 48, 'Valparaíso', '2021-04-28 16:34:47', '2021-04-28 16:34:47'),
(666, 49, 'Anhui', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(667, 49, 'Beijing', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(668, 49, 'Chongqing', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(669, 49, 'Fujian', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(670, 49, 'Gansu', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(671, 49, 'Guangdong', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(672, 49, 'Guangxi Zhuang', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(673, 49, 'Guizhou', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(674, 49, 'Hainan', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(675, 49, 'Hebei', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(676, 49, 'Heilongjiang', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(677, 49, 'Henan', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(678, 49, 'Hubei', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(679, 49, 'Hunan', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(680, 49, 'Jiangsu', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(681, 49, 'Jiangxi', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(682, 49, 'Jilin', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(683, 49, 'Liaoning', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(684, 49, 'Nei Mongol', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(685, 49, 'Ningxia Hui', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(686, 49, 'Qinghai', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(687, 49, 'Shaanxi', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(688, 49, 'Shandong', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(689, 49, 'Shanghai', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(690, 49, 'Shanxi', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(691, 49, 'Sichuan', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(692, 49, 'Tianjin', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(693, 49, 'Xinjiang Uygur', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(694, 49, 'Xizang', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(695, 49, 'Yunnan', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(696, 49, 'Zhejiang', '2021-04-28 16:34:48', '2021-04-28 16:34:48'),
(697, 50, 'Christmas Island', '2021-04-28 16:34:49', '2021-04-28 16:34:49'),
(698, 51, 'Cocos (keeling) Islands', '2021-04-28 16:34:50', '2021-04-28 16:34:50'),
(699, 52, 'Amazonas', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(700, 52, 'Antioquia', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(701, 52, 'Arauca', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(702, 52, 'Atlántico', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(703, 52, 'Bolívar', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(704, 52, 'Boyacá', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(705, 52, 'Caldas', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(706, 52, 'Caquetá', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(707, 52, 'Casanare', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(708, 52, 'Cauca', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(709, 52, 'Córdoba', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(710, 52, 'Cesar', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(711, 52, 'Chocó', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(712, 52, 'Cundinamarca', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(713, 52, 'Distrito Capital', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(714, 52, 'Guainía', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(715, 52, 'Guaviare', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(716, 52, 'Huila', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(717, 52, 'La Guajira', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(718, 52, 'Magdalena', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(719, 52, 'Meta', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(720, 52, 'Nariño', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(721, 52, 'Norte de Santander', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(722, 52, 'Putumayo', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(723, 52, 'Quindío', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(724, 52, 'Risaralda', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(725, 52, 'San Andrés y Providencia', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(726, 52, 'Santander', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(727, 52, 'Sucre', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(728, 52, 'Tolima', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(729, 52, 'Valle del Cauca', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(730, 52, 'Vaupés', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(731, 52, 'Vichada', '2021-04-28 16:34:51', '2021-04-28 16:34:51'),
(732, 53, 'Anjouan', '2021-04-28 16:34:52', '2021-04-28 16:34:52'),
(733, 53, 'Grande Comore', '2021-04-28 16:34:52', '2021-04-28 16:34:52'),
(734, 53, 'Mohéli', '2021-04-28 16:34:52', '2021-04-28 16:34:52'),
(735, 54, 'Bouenza', '2021-04-28 16:34:53', '2021-04-28 16:34:53'),
(736, 54, 'Brazzaville', '2021-04-28 16:34:53', '2021-04-28 16:34:53'),
(737, 54, 'Cuvette', '2021-04-28 16:34:53', '2021-04-28 16:34:53'),
(738, 54, 'Cuvette-Ouest', '2021-04-28 16:34:53', '2021-04-28 16:34:53'),
(739, 54, 'Kouilou', '2021-04-28 16:34:53', '2021-04-28 16:34:53'),
(740, 54, 'Lékoumou', '2021-04-28 16:34:53', '2021-04-28 16:34:53');
INSERT INTO `states` (`id`, `country_id`, `name`, `created_at`, `updated_at`) VALUES
(741, 54, 'Likouala', '2021-04-28 16:34:53', '2021-04-28 16:34:53'),
(742, 54, 'Niari', '2021-04-28 16:34:53', '2021-04-28 16:34:53'),
(743, 54, 'Plateaux', '2021-04-28 16:34:53', '2021-04-28 16:34:53'),
(744, 54, 'Pool', '2021-04-28 16:34:53', '2021-04-28 16:34:53'),
(745, 54, 'Sangha', '2021-04-28 16:34:53', '2021-04-28 16:34:53'),
(746, 56, 'Cook Islands', '2021-04-28 16:34:55', '2021-04-28 16:34:55'),
(747, 57, 'Alajuela', '2021-04-28 16:34:56', '2021-04-28 16:34:56'),
(748, 57, 'Cartago', '2021-04-28 16:34:56', '2021-04-28 16:34:56'),
(749, 57, 'Guanacaste', '2021-04-28 16:34:56', '2021-04-28 16:34:56'),
(750, 57, 'Heredia', '2021-04-28 16:34:56', '2021-04-28 16:34:56'),
(751, 57, 'Limón', '2021-04-28 16:34:56', '2021-04-28 16:34:56'),
(752, 57, 'Puntarenas', '2021-04-28 16:34:56', '2021-04-28 16:34:56'),
(753, 57, 'San José', '2021-04-28 16:34:56', '2021-04-28 16:34:56'),
(754, 58, 'Bjelovarska-Bilogorska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(755, 58, 'Brodsko-Posavska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(756, 58, 'Dubrovačko-Neretvanska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(757, 58, 'Grad Zagreb', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(758, 58, 'Istarska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(759, 58, 'Karlovačka', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(760, 58, 'Koprivničko-Križevačka', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(761, 58, 'Krapinsko-Zagorska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(762, 58, 'Ličko-Senjska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(763, 58, 'Međimurska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(764, 58, 'Osječko-Baranjska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(765, 58, 'Požeško-Slavonska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(766, 58, 'Primorsko-Goranska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(767, 58, 'Sisačko-Moslavačka', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(768, 58, 'Splitsko-Dalmatinska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(769, 58, 'Varaždinska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(770, 58, 'Virovitičko-Podravska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(771, 58, 'Vukovarsko-Srijemska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(772, 58, 'Zadarska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(773, 58, 'Zagrebačka', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(774, 58, 'Šibensko-Kninska', '2021-04-28 16:34:57', '2021-04-28 16:34:57'),
(775, 59, 'Camagüey', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(776, 59, 'Ciego de Ávila', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(777, 59, 'Cienfuegos', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(778, 59, 'Ciudad de la Habana', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(779, 59, 'Granma', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(780, 59, 'Guantánamo', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(781, 59, 'Holguín', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(782, 59, 'Isla de la Juventud', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(783, 59, 'La Habana', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(784, 59, 'Las Tunas', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(785, 59, 'Matanzas', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(786, 59, 'Pinar del Río', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(787, 59, 'Sancti Spíritus', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(788, 59, 'Santiago de Cuba', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(789, 59, 'Villa Clara', '2021-04-28 16:34:58', '2021-04-28 16:34:58'),
(790, 61, 'Famagusta', '2021-04-28 16:35:00', '2021-04-28 16:35:00'),
(791, 61, 'Kyrenia', '2021-04-28 16:35:00', '2021-04-28 16:35:00'),
(792, 61, 'Larnaca', '2021-04-28 16:35:00', '2021-04-28 16:35:00'),
(793, 61, 'Limassol', '2021-04-28 16:35:00', '2021-04-28 16:35:00'),
(794, 61, 'Nicosia', '2021-04-28 16:35:00', '2021-04-28 16:35:00'),
(795, 61, 'Paphos', '2021-04-28 16:35:00', '2021-04-28 16:35:00'),
(796, 62, 'Ústecký', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(797, 62, 'Jihomoravský', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(798, 62, 'Jihočeský', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(799, 62, 'Karlovarský', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(800, 62, 'Královéhradecký', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(801, 62, 'Liberecký', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(802, 62, 'Moravskoslezský', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(803, 62, 'Olomoucký', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(804, 62, 'Pardubický', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(805, 62, 'Plzeňský', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(806, 62, 'Praha', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(807, 62, 'Středočeský', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(808, 62, 'Vysočina', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(809, 62, 'Zlínský', '2021-04-28 16:35:01', '2021-04-28 16:35:01'),
(810, 63, 'Hovedstaden', '2021-04-28 16:35:02', '2021-04-28 16:35:02'),
(811, 63, 'Midtjylland', '2021-04-28 16:35:02', '2021-04-28 16:35:02'),
(812, 63, 'Nordjylland', '2021-04-28 16:35:02', '2021-04-28 16:35:02'),
(813, 63, 'Sjælland', '2021-04-28 16:35:02', '2021-04-28 16:35:02'),
(814, 63, 'Syddanmark', '2021-04-28 16:35:02', '2021-04-28 16:35:02'),
(815, 64, 'Arta', '2021-04-28 16:35:05', '2021-04-28 16:35:05'),
(816, 64, 'Dikhil', '2021-04-28 16:35:05', '2021-04-28 16:35:05'),
(817, 64, 'Djibouti', '2021-04-28 16:35:05', '2021-04-28 16:35:05'),
(818, 64, 'Obock', '2021-04-28 16:35:05', '2021-04-28 16:35:05'),
(819, 64, 'Tadjourah', '2021-04-28 16:35:05', '2021-04-28 16:35:05'),
(820, 64, '`Ali Sabieh', '2021-04-28 16:35:05', '2021-04-28 16:35:05'),
(821, 65, 'Saint Andrew', '2021-04-28 16:35:06', '2021-04-28 16:35:06'),
(822, 65, 'Saint David', '2021-04-28 16:35:06', '2021-04-28 16:35:06'),
(823, 65, 'Saint George', '2021-04-28 16:35:06', '2021-04-28 16:35:06'),
(824, 65, 'Saint John', '2021-04-28 16:35:06', '2021-04-28 16:35:06'),
(825, 65, 'Saint Joseph', '2021-04-28 16:35:06', '2021-04-28 16:35:06'),
(826, 65, 'Saint Luke', '2021-04-28 16:35:06', '2021-04-28 16:35:06'),
(827, 65, 'Saint Mark', '2021-04-28 16:35:06', '2021-04-28 16:35:06'),
(828, 65, 'Saint Patrick', '2021-04-28 16:35:06', '2021-04-28 16:35:06'),
(829, 65, 'Saint Paul', '2021-04-28 16:35:06', '2021-04-28 16:35:06'),
(830, 65, 'Saint Peter', '2021-04-28 16:35:06', '2021-04-28 16:35:06'),
(831, 66, 'Azua', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(832, 66, 'Bahoruco', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(833, 66, 'Barahona', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(834, 66, 'Dajabón', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(835, 66, 'Distrito Nacional', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(836, 66, 'Duarte', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(837, 66, 'El Seybo', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(838, 66, 'Elías Piña', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(839, 66, 'Espaillat', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(840, 66, 'Hato Mayor', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(841, 66, 'Independencia', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(842, 66, 'La Altagracia', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(843, 66, 'La Romana', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(844, 66, 'La Vega', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(845, 66, 'María Trinidad Sánchez', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(846, 66, 'Monseñor Nouel', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(847, 66, 'Monte Cristi', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(848, 66, 'Monte Plata', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(849, 66, 'Pedernales', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(850, 66, 'Peravia', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(851, 66, 'Puerto Plata', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(852, 66, 'Salcedo', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(853, 66, 'Samaná', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(854, 66, 'San Cristóbal', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(855, 66, 'San José de Ocoa', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(856, 66, 'San Juan', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(857, 66, 'San Pedro de Macorís', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(858, 66, 'Santiago', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(859, 66, 'Santiago Rodríguez', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(860, 66, 'Santo Domingo', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(861, 66, 'Sánchez Ramírez', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(862, 66, 'Valverde', '2021-04-28 16:35:07', '2021-04-28 16:35:07'),
(863, 67, 'Azuay', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(864, 67, 'Bolívar', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(865, 67, 'Cañar', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(866, 67, 'Carchi', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(867, 67, 'Chimborazo', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(868, 67, 'Cotopaxi', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(869, 67, 'El Oro', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(870, 67, 'Esmeraldas', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(871, 67, 'Galápagos', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(872, 67, 'Guayas', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(873, 67, 'Imbabura', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(874, 67, 'Loja', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(875, 67, 'Los Ríos', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(876, 67, 'Manabí', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(877, 67, 'Morona-Santiago', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(878, 67, 'Napo', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(879, 67, 'Orellana', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(880, 67, 'Pastaza', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(881, 67, 'Pichincha', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(882, 67, 'Sucumbíos', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(883, 67, 'Tungurahua', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(884, 67, 'Zamora-Chinchipe', '2021-04-28 16:35:08', '2021-04-28 16:35:08'),
(885, 68, 'Ad Daqahlīyah', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(886, 68, 'Al Bahr al Ahmar', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(887, 68, 'Al Buhayrah', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(888, 68, 'Al Fayyūm', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(889, 68, 'Al Gharbīyah', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(890, 68, 'Al Iskandarīyah', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(891, 68, 'Al Ismā`īlīyah', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(892, 68, 'Al Jīzah', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(893, 68, 'Al Minyā', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(894, 68, 'Al Minūfīyah', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(895, 68, 'Al Qalyūbīyah', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(896, 68, 'Al Qāhirah', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(897, 68, 'Al Uqsur', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(898, 68, 'Al Wādī al Jadīd', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(899, 68, 'As Suways', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(900, 68, 'Ash Sharqīyah', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(901, 68, 'Aswān', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(902, 68, 'Asyūt', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(903, 68, 'Banī Suwayf', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(904, 68, 'Būr Sa`īd', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(905, 68, 'Dumyāt', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(906, 68, 'Janūb Sīnā\'', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(907, 68, 'Kafr ash Shaykh', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(908, 68, 'Matrūh', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(909, 68, 'Qinā', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(910, 68, 'Shamal Sīnā\'', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(911, 68, 'Sūhāj', '2021-04-28 16:35:10', '2021-04-28 16:35:10'),
(912, 69, 'Ahuachapán', '2021-04-28 16:35:13', '2021-04-28 16:35:13'),
(913, 69, 'Cabañas', '2021-04-28 16:35:13', '2021-04-28 16:35:13'),
(914, 69, 'Chalatenango', '2021-04-28 16:35:13', '2021-04-28 16:35:13'),
(915, 69, 'Cuscatlán', '2021-04-28 16:35:13', '2021-04-28 16:35:13'),
(916, 69, 'La Libertad', '2021-04-28 16:35:13', '2021-04-28 16:35:13'),
(917, 69, 'La Paz', '2021-04-28 16:35:13', '2021-04-28 16:35:13'),
(918, 69, 'La Unión', '2021-04-28 16:35:13', '2021-04-28 16:35:13'),
(919, 69, 'Morazán', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(920, 69, 'San Miguel', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(921, 69, 'San Salvador', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(922, 69, 'San Vicente', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(923, 69, 'Santa Ana', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(924, 69, 'Sonsonate', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(925, 69, 'Usulután', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(926, 70, 'Annobón', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(927, 70, 'Bioko Norte', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(928, 70, 'Bioko Sur', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(929, 70, 'Centro Sur', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(930, 70, 'Kié-Ntem', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(931, 70, 'Litoral', '2021-04-28 16:35:14', '2021-04-28 16:35:14'),
(932, 70, 'Wele-Nzás', '2021-04-28 16:35:15', '2021-04-28 16:35:15'),
(933, 71, 'Anseba', '2021-04-28 16:35:15', '2021-04-28 16:35:15'),
(934, 71, 'Debub', '2021-04-28 16:35:15', '2021-04-28 16:35:15'),
(935, 71, 'Debubawi Keyih Bahri', '2021-04-28 16:35:15', '2021-04-28 16:35:15'),
(936, 71, 'Gash Barka', '2021-04-28 16:35:15', '2021-04-28 16:35:15'),
(937, 71, 'Maekel', '2021-04-28 16:35:15', '2021-04-28 16:35:15'),
(938, 71, 'Semenawi Keyih Bahri', '2021-04-28 16:35:15', '2021-04-28 16:35:15'),
(939, 72, 'Harju', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(940, 72, 'Hiiu', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(941, 72, 'Ida-Viru', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(942, 72, 'Järva', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(943, 72, 'Jõgeva', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(944, 72, 'Lääne', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(945, 72, 'Lääne-Viru', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(946, 72, 'Pärnu', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(947, 72, 'Põlva', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(948, 72, 'Rapla', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(949, 72, 'Saare', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(950, 72, 'Tartu', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(951, 72, 'Valga', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(952, 72, 'Võru', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(953, 72, 'Viljandi', '2021-04-28 16:35:16', '2021-04-28 16:35:16'),
(954, 73, 'Addis Ababa', '2021-04-28 16:35:18', '2021-04-28 16:35:18'),
(955, 73, 'Afar', '2021-04-28 16:35:18', '2021-04-28 16:35:18'),
(956, 73, 'Amhara', '2021-04-28 16:35:18', '2021-04-28 16:35:18'),
(957, 73, 'Benshangul-Gumaz', '2021-04-28 16:35:18', '2021-04-28 16:35:18'),
(958, 73, 'Dire Dawa', '2021-04-28 16:35:18', '2021-04-28 16:35:18'),
(959, 73, 'Gambela Peoples', '2021-04-28 16:35:18', '2021-04-28 16:35:18'),
(960, 73, 'Harari People', '2021-04-28 16:35:18', '2021-04-28 16:35:18'),
(961, 73, 'Oromia', '2021-04-28 16:35:18', '2021-04-28 16:35:18'),
(962, 73, 'Somali', '2021-04-28 16:35:18', '2021-04-28 16:35:18'),
(963, 73, 'Southern Nations, Nationalities', '2021-04-28 16:35:18', '2021-04-28 16:35:18'),
(964, 73, 'Tigray', '2021-04-28 16:35:18', '2021-04-28 16:35:18'),
(965, 74, 'Falkland Islands', '2021-04-28 16:35:19', '2021-04-28 16:35:19'),
(966, 75, 'Østerø', '2021-04-28 16:35:20', '2021-04-28 16:35:20'),
(967, 75, 'Norderøerne', '2021-04-28 16:35:20', '2021-04-28 16:35:20'),
(968, 75, 'Sandø', '2021-04-28 16:35:20', '2021-04-28 16:35:20'),
(969, 75, 'Strømø', '2021-04-28 16:35:20', '2021-04-28 16:35:20'),
(970, 75, 'Suderø Nordre', '2021-04-28 16:35:20', '2021-04-28 16:35:20'),
(971, 75, 'Suderø Søndre', '2021-04-28 16:35:20', '2021-04-28 16:35:20'),
(972, 75, 'Vågø', '2021-04-28 16:35:20', '2021-04-28 16:35:20'),
(973, 76, 'Central', '2021-04-28 16:35:21', '2021-04-28 16:35:21'),
(974, 76, 'Eastern', '2021-04-28 16:35:21', '2021-04-28 16:35:21'),
(975, 76, 'Northern', '2021-04-28 16:35:21', '2021-04-28 16:35:21'),
(976, 76, 'Rotuma', '2021-04-28 16:35:21', '2021-04-28 16:35:21'),
(977, 76, 'Western', '2021-04-28 16:35:21', '2021-04-28 16:35:21'),
(978, 77, 'Eastern Finland', '2021-04-28 16:35:22', '2021-04-28 16:35:22'),
(979, 77, 'Lapland', '2021-04-28 16:35:22', '2021-04-28 16:35:22'),
(980, 77, 'Oulu', '2021-04-28 16:35:22', '2021-04-28 16:35:22'),
(981, 77, 'Southern Finland', '2021-04-28 16:35:22', '2021-04-28 16:35:22'),
(982, 77, 'Western Finland', '2021-04-28 16:35:22', '2021-04-28 16:35:22'),
(983, 78, 'Ain', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(984, 78, 'Aisne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(985, 78, 'Allier', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(986, 78, 'Alpes-de-Haute-Provence', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(987, 78, 'Alpes-Maritimes', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(988, 78, 'Ardèche', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(989, 78, 'Ardennes', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(990, 78, 'Ariège', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(991, 78, 'Aube', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(992, 78, 'Aude', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(993, 78, 'Aveyron', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(994, 78, 'Bas-Rhin', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(995, 78, 'Bouches-du-Rhône', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(996, 78, 'Calvados', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(997, 78, 'Cantal', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(998, 78, 'Côte-d\'Or', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(999, 78, 'Côtes-d\'Armor', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1000, 78, 'Charente', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1001, 78, 'Charente-Maritime', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1002, 78, 'Cher', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1003, 78, 'Corrèze', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1004, 78, 'Corse-du-Sud', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1005, 78, 'Creuse', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1006, 78, 'Deux-Sèvres', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1007, 78, 'Dordogne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1008, 78, 'Doubs', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1009, 78, 'Drôme', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1010, 78, 'Essonne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1011, 78, 'Eure', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1012, 78, 'Eure-et-Loir', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1013, 78, 'Finistère', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1014, 78, 'Gard', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1015, 78, 'Gers', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1016, 78, 'Gironde', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1017, 78, 'Haut-Rhin', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1018, 78, 'Haute-Corse', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1019, 78, 'Haute-Garonne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1020, 78, 'Haute-Loire', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1021, 78, 'Haute-Marne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1022, 78, 'Haute-Saône', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1023, 78, 'Haute-Savoie', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1024, 78, 'Haute-Vienne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1025, 78, 'Hautes-Alpes', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1026, 78, 'Hautes-Pyrénées', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1027, 78, 'Hauts-de-Seine', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1028, 78, 'Hérault', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1029, 78, 'Ille-et-Vilaine', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1030, 78, 'Indre', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1031, 78, 'Indre-et-Loire', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1032, 78, 'Isère', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1033, 78, 'Jura', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1034, 78, 'Landes', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1035, 78, 'Loir-et-Cher', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1036, 78, 'Loire', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1037, 78, 'Loire-Atlantique', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1038, 78, 'Loiret', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1039, 78, 'Lot', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1040, 78, 'Lot-et-Garonne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1041, 78, 'Lozère', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1042, 78, 'Maine-et-Loire', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1043, 78, 'Manche', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1044, 78, 'Marne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1045, 78, 'Mayenne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1046, 78, 'Meurthe-et-Moselle', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1047, 78, 'Meuse', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1048, 78, 'Morbihan', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1049, 78, 'Moselle', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1050, 78, 'Nièvre', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1051, 78, 'Nord', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1052, 78, 'Oise', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1053, 78, 'Orne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1054, 78, 'Pas-de-Calais', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1055, 78, 'Puy-de-Dôme', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1056, 78, 'Pyrénées-Atlantiques', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1057, 78, 'Pyrénées-Orientales', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1058, 78, 'Rhône', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1059, 78, 'Saône-et-Loire', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1060, 78, 'Sarthe', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1061, 78, 'Savoie', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1062, 78, 'Seine-et-Marne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1063, 78, 'Seine-Maritime', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1064, 78, 'Seine-Saint-Denis', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1065, 78, 'Somme', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1066, 78, 'Tarn', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1067, 78, 'Tarn-et-Garonne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1068, 78, 'Territoire de Belfort', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1069, 78, 'Val-d\'Oise', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1070, 78, 'Val-de-Marne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1071, 78, 'Var', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1072, 78, 'Vaucluse', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1073, 78, 'Vendée', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1074, 78, 'Vienne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1075, 78, 'Ville de Paris', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1076, 78, 'Vosges', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1077, 78, 'Yonne', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1078, 78, 'Yvelines', '2021-04-28 16:35:23', '2021-04-28 16:35:23'),
(1079, 79, 'Cayenne', '2021-04-28 16:35:25', '2021-04-28 16:35:25'),
(1080, 79, 'Saint-Laurent-du-Maroni', '2021-04-28 16:35:25', '2021-04-28 16:35:25'),
(1081, 80, 'Clipperton Island', '2021-04-28 16:35:26', '2021-04-28 16:35:26'),
(1082, 80, 'Leeward Islands', '2021-04-28 16:35:26', '2021-04-28 16:35:26'),
(1083, 80, 'Marquesas Islands', '2021-04-28 16:35:26', '2021-04-28 16:35:26'),
(1084, 80, 'Tuamotu and Gambier Islands', '2021-04-28 16:35:26', '2021-04-28 16:35:26'),
(1085, 80, 'Tubuai Islands', '2021-04-28 16:35:26', '2021-04-28 16:35:26'),
(1086, 80, 'Windward Islands', '2021-04-28 16:35:26', '2021-04-28 16:35:26'),
(1087, 82, 'Estuaire', '2021-04-28 16:35:28', '2021-04-28 16:35:28'),
(1088, 82, 'Haut-Ogooué', '2021-04-28 16:35:28', '2021-04-28 16:35:28'),
(1089, 82, 'Moyen-Ogooué', '2021-04-28 16:35:28', '2021-04-28 16:35:28'),
(1090, 82, 'Ngounié', '2021-04-28 16:35:28', '2021-04-28 16:35:28'),
(1091, 82, 'Nyanga', '2021-04-28 16:35:28', '2021-04-28 16:35:28'),
(1092, 82, 'Ogooué-Ivindo', '2021-04-28 16:35:28', '2021-04-28 16:35:28'),
(1093, 82, 'Ogooué-Lolo', '2021-04-28 16:35:28', '2021-04-28 16:35:28'),
(1094, 82, 'Ogooué-Maritime', '2021-04-28 16:35:28', '2021-04-28 16:35:28'),
(1095, 82, 'Woleu-Ntem', '2021-04-28 16:35:28', '2021-04-28 16:35:28'),
(1096, 83, 'Banjul', '2021-04-28 16:35:29', '2021-04-28 16:35:29'),
(1097, 83, 'Central River', '2021-04-28 16:35:29', '2021-04-28 16:35:29'),
(1098, 83, 'Lower River', '2021-04-28 16:35:29', '2021-04-28 16:35:29'),
(1099, 83, 'North Bank', '2021-04-28 16:35:29', '2021-04-28 16:35:29'),
(1100, 83, 'Upper River', '2021-04-28 16:35:29', '2021-04-28 16:35:29'),
(1101, 83, 'Western', '2021-04-28 16:35:29', '2021-04-28 16:35:29'),
(1102, 84, 'Abkhazia', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1103, 84, 'Ajaria', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1104, 84, 'Guria', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1105, 84, 'Imereti', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1106, 84, 'Kakheti', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1107, 84, 'Kvemo Kartli', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1108, 84, 'Mtskheta-Mtianeti', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1109, 84, 'Racha-Lochkhumi-Kvemo Svaneti', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1110, 84, 'Samegrelo-Zemo Svateni', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1111, 84, 'Samtskhe-Javakheti', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1112, 84, 'Shida Kartli', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1113, 84, 'Tbilisi', '2021-04-28 16:35:30', '2021-04-28 16:35:30'),
(1114, 85, 'Baden-Württemberg', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1115, 85, 'Bayern', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1116, 85, 'Berlin', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1117, 85, 'Brandenburg', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1118, 85, 'Bremen', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1119, 85, 'Hamburg', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1120, 85, 'Hessen', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1121, 85, 'Mecklenburg-Vorpommern', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1122, 85, 'Niedersachsen', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1123, 85, 'Nordrhein-Westfalen', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1124, 85, 'Rheinland-Pfalz', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1125, 85, 'Saarland', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1126, 85, 'Sachsen', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1127, 85, 'Sachsen-Anhalt', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1128, 85, 'Schleswig-Holstein', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1129, 85, 'Thüringen', '2021-04-28 16:35:36', '2021-04-28 16:35:36'),
(1130, 86, 'Ashanti', '2021-04-28 16:35:37', '2021-04-28 16:35:37'),
(1131, 86, 'Brong-Ahafo', '2021-04-28 16:35:37', '2021-04-28 16:35:37'),
(1132, 86, 'Central', '2021-04-28 16:35:37', '2021-04-28 16:35:37'),
(1133, 86, 'Eastern', '2021-04-28 16:35:37', '2021-04-28 16:35:37'),
(1134, 86, 'Greater Accra', '2021-04-28 16:35:37', '2021-04-28 16:35:37'),
(1135, 86, 'Northern', '2021-04-28 16:35:37', '2021-04-28 16:35:37'),
(1136, 86, 'Upper East', '2021-04-28 16:35:37', '2021-04-28 16:35:37'),
(1137, 86, 'Upper West', '2021-04-28 16:35:37', '2021-04-28 16:35:37'),
(1138, 86, 'Volta', '2021-04-28 16:35:37', '2021-04-28 16:35:37'),
(1139, 86, 'Western', '2021-04-28 16:35:37', '2021-04-28 16:35:37'),
(1140, 87, 'Gibraltar', '2021-04-28 16:35:38', '2021-04-28 16:35:38'),
(1141, 88, 'Achaea', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1142, 88, 'Aitolia and Akarnania', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1143, 88, 'Arcadia', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1144, 88, 'Argolis', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1145, 88, 'Arta', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1146, 88, 'Attica', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1147, 88, 'Boeotia', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1148, 88, 'Corfu', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1149, 88, 'Corinth', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1150, 88, 'Cyclades', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1151, 88, 'Dodecanese', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1152, 88, 'Drama', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1153, 88, 'Euboea', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1154, 88, 'Evritania', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1155, 88, 'Evros', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1156, 88, 'Florina', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1157, 88, 'Fokis', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1158, 88, 'Fthiotis', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1159, 88, 'Grevena', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1160, 88, 'Heraklion', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1161, 88, 'Ilia', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1162, 88, 'Imathia', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1163, 88, 'Ioannina', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1164, 88, 'Karditsa', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1165, 88, 'Kastoria', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1166, 88, 'Kavala', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1167, 88, 'Kefallinia', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1168, 88, 'Khalkidiki', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1169, 88, 'Khania', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1170, 88, 'Khios', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1171, 88, 'Kilkis', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1172, 88, 'Kozani', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1173, 88, 'Laconia', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1174, 88, 'Larisa', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1175, 88, 'Lasithi', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1176, 88, 'Lesvos', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1177, 88, 'Levkas', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1178, 88, 'Magnesia', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1179, 88, 'Messinia', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1180, 88, 'Mount Athos', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1181, 88, 'Pella', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1182, 88, 'Pieria', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1183, 88, 'Preveza', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1184, 88, 'Rethymnon', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1185, 88, 'Rodopi', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1186, 88, 'Samos', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1187, 88, 'Serrai', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1188, 88, 'Thesprotia', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1189, 88, 'Thessaloniki', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1190, 88, 'Trikala', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1191, 88, 'Xanthi', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1192, 88, 'Zakynthos', '2021-04-28 16:35:39', '2021-04-28 16:35:39'),
(1193, 89, 'East Greenland', '2021-04-28 16:35:41', '2021-04-28 16:35:41'),
(1194, 89, 'North Greenland', '2021-04-28 16:35:41', '2021-04-28 16:35:41'),
(1195, 89, 'West Greenland', '2021-04-28 16:35:41', '2021-04-28 16:35:41'),
(1196, 90, 'Carriacou', '2021-04-28 16:35:42', '2021-04-28 16:35:42'),
(1197, 90, 'Saint Andrew', '2021-04-28 16:35:42', '2021-04-28 16:35:42'),
(1198, 90, 'Saint David', '2021-04-28 16:35:42', '2021-04-28 16:35:42'),
(1199, 90, 'Saint George', '2021-04-28 16:35:42', '2021-04-28 16:35:42'),
(1200, 90, 'Saint John', '2021-04-28 16:35:42', '2021-04-28 16:35:42'),
(1201, 90, 'Saint Mark', '2021-04-28 16:35:42', '2021-04-28 16:35:42'),
(1202, 90, 'Saint Patrick', '2021-04-28 16:35:42', '2021-04-28 16:35:42'),
(1203, 91, 'Basse-Terre', '2021-04-28 16:35:43', '2021-04-28 16:35:43'),
(1204, 91, 'Pointe-à-Pitre', '2021-04-28 16:35:43', '2021-04-28 16:35:43'),
(1205, 91, 'Saint-Barthélemy', '2021-04-28 16:35:43', '2021-04-28 16:35:43'),
(1206, 91, 'Saint-Martin', '2021-04-28 16:35:43', '2021-04-28 16:35:43'),
(1207, 92, 'Agana Heights', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1208, 92, 'Agat', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1209, 92, 'Asan', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1210, 92, 'Barrigada', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1211, 92, 'Chalan-Pago-Ordot', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1212, 92, 'Dededo', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1213, 92, 'Hagåtña', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1214, 92, 'Inarajan', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1215, 92, 'Mangilao', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1216, 92, 'Merizo', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1217, 92, 'Mongmong-Toto-Maite', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1218, 92, 'Piti', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1219, 92, 'Santa Rita', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1220, 92, 'Sinajana', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1221, 92, 'Talofofo', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1222, 92, 'Tamuning', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1223, 92, 'Umatac', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1224, 92, 'Yigo', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1225, 92, 'Yona', '2021-04-28 16:35:44', '2021-04-28 16:35:44'),
(1226, 93, 'Alta Verapaz', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1227, 93, 'Baja Verapaz', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1228, 93, 'Chimaltenango', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1229, 93, 'Chiquimula', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1230, 93, 'El Progreso', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1231, 93, 'Escuintla', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1232, 93, 'Guatemala', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1233, 93, 'Huehuetenango', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1234, 93, 'Izabal', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1235, 93, 'Jalapa', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1236, 93, 'Jutiapa', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1237, 93, 'Petén', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1238, 93, 'Quetzaltenango', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1239, 93, 'Quiché', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1240, 93, 'Retalhuleu', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1241, 93, 'Sacatepéquez', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1242, 93, 'San Marcos', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1243, 93, 'Santa Rosa', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1244, 93, 'Sololá', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1245, 93, 'Suchitepéquez', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1246, 93, 'Totonicapán', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1247, 93, 'Zacapa', '2021-04-28 16:35:45', '2021-04-28 16:35:45'),
(1248, 94, 'Alderney', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1249, 94, 'Brecqhou', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1250, 94, 'Castel', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1251, 94, 'Forest', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1252, 94, 'Herm', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1253, 94, 'Jethou', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1254, 94, 'Lihou', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1255, 94, 'Saint Andrew', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1256, 94, 'Saint Martin', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1257, 94, 'Saint Peter Port', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1258, 94, 'Saint Pierre du Bois', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1259, 94, 'Saint Sampson', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1260, 94, 'Saint Saviour', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1261, 94, 'Sark', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1262, 94, 'Torteval', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1263, 94, 'Vale', '2021-04-28 16:35:46', '2021-04-28 16:35:46'),
(1264, 95, 'Beyla', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1265, 95, 'Boffa', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1266, 95, 'Boké', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1267, 95, 'Conakry', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1268, 95, 'Coyah', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1269, 95, 'Dabola', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1270, 95, 'Dalaba', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1271, 95, 'Dinguiraye', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1272, 95, 'Dubréka', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1273, 95, 'Faranah', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1274, 95, 'Forécariah', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1275, 95, 'Fria', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1276, 95, 'Gaoual', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1277, 95, 'Guéckédou', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1278, 95, 'Kankan', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1279, 95, 'Kérouané', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1280, 95, 'Kindia', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1281, 95, 'Kissidougou', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1282, 95, 'Koubia', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1283, 95, 'Koundara', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1284, 95, 'Kouroussa', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1285, 95, 'Labé', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1286, 95, 'Lélouma', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1287, 95, 'Lola', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1288, 95, 'Macenta', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1289, 95, 'Mali', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1290, 95, 'Mamou', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1291, 95, 'Mandiana', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1292, 95, 'Nzérékoré', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1293, 95, 'Pita', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1294, 95, 'Siguiri', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1295, 95, 'Télimélé', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1296, 95, 'Tougué', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1297, 95, 'Yomou', '2021-04-28 16:35:48', '2021-04-28 16:35:48'),
(1298, 96, 'Bafatá', '2021-04-28 16:35:49', '2021-04-28 16:35:49'),
(1299, 96, 'Biombo', '2021-04-28 16:35:49', '2021-04-28 16:35:49'),
(1300, 96, 'Bissau', '2021-04-28 16:35:49', '2021-04-28 16:35:49'),
(1301, 96, 'Bolama', '2021-04-28 16:35:49', '2021-04-28 16:35:49'),
(1302, 96, 'Cacheu', '2021-04-28 16:35:49', '2021-04-28 16:35:49'),
(1303, 96, 'Gabú', '2021-04-28 16:35:49', '2021-04-28 16:35:49'),
(1304, 96, 'Oio', '2021-04-28 16:35:49', '2021-04-28 16:35:49'),
(1305, 96, 'Quinara', '2021-04-28 16:35:49', '2021-04-28 16:35:49'),
(1306, 96, 'Tombali', '2021-04-28 16:35:49', '2021-04-28 16:35:49'),
(1307, 97, 'Barima-Waini', '2021-04-28 16:35:50', '2021-04-28 16:35:50'),
(1308, 97, 'Cuyuni-Mazaruni', '2021-04-28 16:35:50', '2021-04-28 16:35:50'),
(1309, 97, 'Demerara-Mahaica', '2021-04-28 16:35:50', '2021-04-28 16:35:50'),
(1310, 97, 'East Berbice-Corentyne', '2021-04-28 16:35:50', '2021-04-28 16:35:50'),
(1311, 97, 'Essequibo Islands-West Demerara', '2021-04-28 16:35:50', '2021-04-28 16:35:50'),
(1312, 97, 'Mahaica-Berbice', '2021-04-28 16:35:50', '2021-04-28 16:35:50'),
(1313, 97, 'Pomeroon-Supenaam', '2021-04-28 16:35:50', '2021-04-28 16:35:50'),
(1314, 97, 'Potaro-Siparuni', '2021-04-28 16:35:50', '2021-04-28 16:35:50'),
(1315, 97, 'Upper Demerara-Berbice', '2021-04-28 16:35:50', '2021-04-28 16:35:50'),
(1316, 97, 'Upper Takutu-Upper Essequibo', '2021-04-28 16:35:50', '2021-04-28 16:35:50'),
(1317, 98, 'Centre', '2021-04-28 16:35:51', '2021-04-28 16:35:51'),
(1318, 98, 'Grand\' Anse', '2021-04-28 16:35:51', '2021-04-28 16:35:51'),
(1319, 98, 'L\'Artibonite', '2021-04-28 16:35:51', '2021-04-28 16:35:51'),
(1320, 98, 'Nippes', '2021-04-28 16:35:51', '2021-04-28 16:35:51'),
(1321, 98, 'Nord', '2021-04-28 16:35:51', '2021-04-28 16:35:51'),
(1322, 98, 'Nord-Est', '2021-04-28 16:35:51', '2021-04-28 16:35:51'),
(1323, 98, 'Nord-Ouest', '2021-04-28 16:35:51', '2021-04-28 16:35:51'),
(1324, 98, 'Ouest', '2021-04-28 16:35:51', '2021-04-28 16:35:51'),
(1325, 98, 'Sud', '2021-04-28 16:35:51', '2021-04-28 16:35:51'),
(1326, 98, 'Sud-Est', '2021-04-28 16:35:51', '2021-04-28 16:35:51'),
(1327, 100, 'Vatican City', '2021-04-28 16:35:53', '2021-04-28 16:35:53'),
(1328, 101, 'Atlántida', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1329, 101, 'Choluteca', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1330, 101, 'Colón', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1331, 101, 'Comayagua', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1332, 101, 'Copán', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1333, 101, 'Cortés', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1334, 101, 'El Paraíso', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1335, 101, 'Francisco Morazán', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1336, 101, 'Gracias a Dios', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1337, 101, 'Intibucá', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1338, 101, 'Islas de la Bahía', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1339, 101, 'La Paz', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1340, 101, 'Lempira', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1341, 101, 'Ocotepeque', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1342, 101, 'Olancho', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1343, 101, 'Santa Bárbara', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1344, 101, 'Valle', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1345, 101, 'Yoro', '2021-04-28 16:35:54', '2021-04-28 16:35:54'),
(1346, 102, 'Central and Western', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1347, 102, 'Eastern', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1348, 102, 'Islands', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1349, 102, 'Kowloon City', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1350, 102, 'Kwai Tsing', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1351, 102, 'Kwun Tong', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1352, 102, 'North', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1353, 102, 'Sai Kung', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1354, 102, 'Sha Tin', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1355, 102, 'Sham Shui Po', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1356, 102, 'Southern', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1357, 102, 'Tai Po', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1358, 102, 'Tsuen Wan', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1359, 102, 'Tuen Mun', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1360, 102, 'Wan Chai', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1361, 102, 'Wong Tai Sin', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1362, 102, 'Yau Tsim Mong', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1363, 102, 'Yuen Long', '2021-04-28 16:35:55', '2021-04-28 16:35:55'),
(1364, 103, 'Baranya', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1365, 103, 'Bács-Kiskun', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1366, 103, 'Békés', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1367, 103, 'Békéscsaba', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1368, 103, 'Borsod-Abaúj-Zemplén', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1369, 103, 'Budapest', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1370, 103, 'Csongrád', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1371, 103, 'Debrecen', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1372, 103, 'Dunaújváros', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1373, 103, 'Eger', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1374, 103, 'Fejér', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1375, 103, 'GyŒr', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1376, 103, 'GyŒr-Moson-Sopron', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1377, 103, 'Hajdú-Bihar', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1378, 103, 'HódmezŒvásárhely', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1379, 103, 'Heves', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1380, 103, 'Jász-Nagykun-Szolnok', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1381, 103, 'Kaposvár', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1382, 103, 'Kecskemét', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1383, 103, 'Komárom-Esztergom', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1384, 103, 'Miskolc', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1385, 103, 'Nagykanizsa', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1386, 103, 'Nógrád', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1387, 103, 'Nyíregyháza', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1388, 103, 'Pécs', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1389, 103, 'Pest', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1390, 103, 'Salgótarjan', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1391, 103, 'Somogy', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1392, 103, 'Sopron', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1393, 103, 'Szabolcs-Szatmár-Bereg', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1394, 103, 'Székesfehérvár', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1395, 103, 'Szeged', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1396, 103, 'Szekszárd', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1397, 103, 'Szolnok', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1398, 103, 'Szombathely', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1399, 103, 'Tatabánya', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1400, 103, 'Tolna', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1401, 103, 'Vas', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1402, 103, 'Veszprém', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1403, 103, 'Veszprém', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1404, 103, 'Zala', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1405, 103, 'Zalaegerszeg', '2021-04-28 16:35:56', '2021-04-28 16:35:56'),
(1406, 104, 'Capital', '2021-04-28 16:35:58', '2021-04-28 16:35:58'),
(1407, 104, 'Eastland', '2021-04-28 16:35:58', '2021-04-28 16:35:58'),
(1408, 104, 'Northland East', '2021-04-28 16:35:58', '2021-04-28 16:35:58'),
(1409, 104, 'Northland West', '2021-04-28 16:35:58', '2021-04-28 16:35:58'),
(1410, 104, 'Southern Peninsula', '2021-04-28 16:35:58', '2021-04-28 16:35:58'),
(1411, 104, 'Southland', '2021-04-28 16:35:58', '2021-04-28 16:35:58'),
(1412, 104, 'Western Fjords', '2021-04-28 16:35:58', '2021-04-28 16:35:58'),
(1413, 104, 'Westland', '2021-04-28 16:35:58', '2021-04-28 16:35:58'),
(1414, 105, 'Andaman and Nicobar Islands', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1415, 105, 'Andhra Pradesh', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1416, 105, 'Arunachal Pradesh', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1417, 105, 'Assam', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1418, 105, 'Bihar', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1419, 105, 'Chandigarh', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1420, 105, 'Chhattisgarh', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1421, 105, 'Dadra and Nagar Haveli', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1422, 105, 'Daman and Diu', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1423, 105, 'Delhi', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1424, 105, 'Goa', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1425, 105, 'Gujarat', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1426, 105, 'Haryana', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1427, 105, 'Himachal Pradesh', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1428, 105, 'Jammu and Kashmir', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1429, 105, 'Jharkhand', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1430, 105, 'Karnataka', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1431, 105, 'Kerala', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1432, 105, 'Lakshadweep', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1433, 105, 'Madhya Pradesh', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1434, 105, 'Maharashtra', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1435, 105, 'Manipur', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1436, 105, 'Meghalaya', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1437, 105, 'Mizoram', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1438, 105, 'Nagaland', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1439, 105, 'Orissa', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1440, 105, 'Puducherry', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1441, 105, 'Punjab', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1442, 105, 'Rajasthan', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1443, 105, 'Sikkim', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1444, 105, 'Tamil Nadu', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1445, 105, 'Tripura', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1446, 105, 'Uttar Pradesh', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1447, 105, 'Uttarakhand', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1448, 105, 'West Bengal', '2021-04-28 16:35:59', '2021-04-28 16:35:59'),
(1449, 106, 'Aceh', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1450, 106, 'Bali', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1451, 106, 'Bangka-Belitung', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1452, 106, 'Banten', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1453, 106, 'Bengkulu', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1454, 106, 'Gorontalo', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1455, 106, 'Irian Jaya Barat', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1456, 106, 'Jakarta Raya', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1457, 106, 'Jambi', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1458, 106, 'Jawa Barat', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1459, 106, 'Jawa Tengah', '2021-04-28 16:36:00', '2021-04-28 16:36:00');
INSERT INTO `states` (`id`, `country_id`, `name`, `created_at`, `updated_at`) VALUES
(1460, 106, 'Jawa Timur', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1461, 106, 'Kalimantan Barat', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1462, 106, 'Kalimantan Selatan', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1463, 106, 'Kalimantan Tengah', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1464, 106, 'Kalimantan Timur', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1465, 106, 'Kepulauan Riau', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1466, 106, 'Lampung', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1467, 106, 'Maluku', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1468, 106, 'Maluku Utara', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1469, 106, 'Nusa Tenggara Barat', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1470, 106, 'Nusa Tenggara Timur', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1471, 106, 'Papua', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1472, 106, 'Riau', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1473, 106, 'Sulawesi Barat', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1474, 106, 'Sulawesi Selatan', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1475, 106, 'Sulawesi Tengah', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1476, 106, 'Sulawesi Tenggara', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1477, 106, 'Sulawesi Utara', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1478, 106, 'Sumatera Barat', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1479, 106, 'Sumatera Selatan', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1480, 106, 'Sumatera Utara', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1481, 106, 'Yogyakarta', '2021-04-28 16:36:00', '2021-04-28 16:36:00'),
(1482, 107, 'Agnéby', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1483, 107, 'Bafing', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1484, 107, 'Bas-Sassandra', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1485, 107, 'Denguélé', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1486, 107, 'Dix-Huit Montagnes', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1487, 107, 'Fromager', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1488, 107, 'Haut-Sassandra', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1489, 107, 'Lacs', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1490, 107, 'Lagunes', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1491, 107, 'Marahoué', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1492, 107, 'Moyen-Cavally', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1493, 107, 'Moyen-Comoé', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1494, 107, 'N\'zi-Comoé', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1495, 107, 'Savanes', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1496, 107, 'Sud-Bandama', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1497, 107, 'Sud-Comoé', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1498, 107, 'Vallée du Bandama', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1499, 107, 'Worodougou', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1500, 107, 'Zanzan', '2021-04-28 16:36:02', '2021-04-28 16:36:02'),
(1501, 108, 'Ardebil', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1502, 108, 'Bushehr', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1503, 108, 'Chahar Mahall and Bakhtiari', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1504, 108, 'East Azarbaijan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1505, 108, 'Esfahan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1506, 108, 'Fars', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1507, 108, 'Gilan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1508, 108, 'Golestan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1509, 108, 'Hamadan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1510, 108, 'Hormozgan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1511, 108, 'Ilam', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1512, 108, 'Kerman', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1513, 108, 'Kermanshah', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1514, 108, 'Khuzestan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1515, 108, 'Kohgiluyeh and Buyer Ahmad', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1516, 108, 'Kordestan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1517, 108, 'Lorestan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1518, 108, 'Markazi', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1519, 108, 'Mazandaran', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1520, 108, 'North Khorasan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1521, 108, 'Qazvin', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1522, 108, 'Qom', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1523, 108, 'Razavi Khorasan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1524, 108, 'Semnan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1525, 108, 'Sistan and Baluchestan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1526, 108, 'South Khorasan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1527, 108, 'Tehran', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1528, 108, 'West Azarbaijan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1529, 108, 'Yazd', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1530, 108, 'Zanjan', '2021-04-28 16:36:03', '2021-04-28 16:36:03'),
(1531, 109, 'Al-Anbar', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1532, 109, 'Al-Basrah', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1533, 109, 'Al-Muthanna', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1534, 109, 'Al-Qadisiyah', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1535, 109, 'An-Najaf', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1536, 109, 'Arbil', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1537, 109, 'As-Sulaymaniyah', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1538, 109, 'At-Ta\'mim', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1539, 109, 'Babil', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1540, 109, 'Baghdad', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1541, 109, 'Dahuk', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1542, 109, 'Dhi Qar', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1543, 109, 'Diyala', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1544, 109, 'Karbala\'', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1545, 109, 'Maysan', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1546, 109, 'Ninawa', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1547, 109, 'Salah ad-Din', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1548, 109, 'Wasit', '2021-04-28 16:36:04', '2021-04-28 16:36:04'),
(1549, 110, 'Carlow', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1550, 110, 'Cavan', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1551, 110, 'Clare', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1552, 110, 'Cork', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1553, 110, 'Donegal', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1554, 110, 'Dublin', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1555, 110, 'Galway', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1556, 110, 'Kerry', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1557, 110, 'Kildare', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1558, 110, 'Kilkenny', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1559, 110, 'Laoighis', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1560, 110, 'Leitrim', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1561, 110, 'Limerick', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1562, 110, 'Longford', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1563, 110, 'Louth', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1564, 110, 'Mayo', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1565, 110, 'Meath', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1566, 110, 'Monaghan', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1567, 110, 'Offaly', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1568, 110, 'Roscommon', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1569, 110, 'Sligo', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1570, 110, 'Tipperary', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1571, 110, 'Waterford', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1572, 110, 'Westmeath', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1573, 110, 'Wexford', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1574, 110, 'Wicklow', '2021-04-28 16:36:05', '2021-04-28 16:36:05'),
(1575, 112, 'HaDarom', '2021-04-28 16:36:07', '2021-04-28 16:36:07'),
(1576, 112, 'Haifa', '2021-04-28 16:36:07', '2021-04-28 16:36:07'),
(1577, 112, 'HaMerkaz', '2021-04-28 16:36:07', '2021-04-28 16:36:07'),
(1578, 112, 'HaZafon', '2021-04-28 16:36:07', '2021-04-28 16:36:07'),
(1579, 112, 'Jerusalem', '2021-04-28 16:36:07', '2021-04-28 16:36:07'),
(1580, 112, 'Tel Aviv', '2021-04-28 16:36:07', '2021-04-28 16:36:07'),
(1581, 113, 'Agrigento', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1582, 113, 'Alessandria', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1583, 113, 'Ancona', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1584, 113, 'Aosta', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1585, 113, 'Arezzo', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1586, 113, 'Ascoli Piceno', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1587, 113, 'Asti', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1588, 113, 'Avellino', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1589, 113, 'Bari', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1590, 113, 'Barletta-Andria-Trani', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1591, 113, 'Belluno', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1592, 113, 'Benevento', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1593, 113, 'Bergamo', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1594, 113, 'Biella', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1595, 113, 'Bologna', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1596, 113, 'Bolzano', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1597, 113, 'Brescia', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1598, 113, 'Brindisi', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1599, 113, 'Cagliari', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1600, 113, 'Caltanissetta', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1601, 113, 'Campobasso', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1602, 113, 'Carbonia-Iglesias', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1603, 113, 'Caserta', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1604, 113, 'Catania', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1605, 113, 'Catanzaro', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1606, 113, 'Chieti', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1607, 113, 'Como', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1608, 113, 'Cosenza', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1609, 113, 'Cremona', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1610, 113, 'Crotone', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1611, 113, 'Cuneo', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1612, 113, 'Enna', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1613, 113, 'Fermo', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1614, 113, 'Ferrara', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1615, 113, 'Florence', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1616, 113, 'Foggia', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1617, 113, 'Forlì-Cesena', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1618, 113, 'Frosinone', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1619, 113, 'Genoa', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1620, 113, 'Gorizia', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1621, 113, 'Grosseto', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1622, 113, 'Imperia', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1623, 113, 'Isernia', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1624, 113, 'L\'Aquila', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1625, 113, 'La Spezia', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1626, 113, 'Latina', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1627, 113, 'Lecce', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1628, 113, 'Lecco', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1629, 113, 'Livorno', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1630, 113, 'Lodi', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1631, 113, 'Lucca', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1632, 113, 'Macerata', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1633, 113, 'Mantua', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1634, 113, 'Massa-Carrara', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1635, 113, 'Matera', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1636, 113, 'Medio Campidano', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1637, 113, 'Messina', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1638, 113, 'Milan', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1639, 113, 'Modena', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1640, 113, 'Monza e Brianza', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1641, 113, 'Naples', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1642, 113, 'Novara', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1643, 113, 'Nuoro', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1644, 113, 'Ogliastra', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1645, 113, 'Olbia-Tempio', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1646, 113, 'Oristano', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1647, 113, 'Padua', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1648, 113, 'Palermo', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1649, 113, 'Parma', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1650, 113, 'Pavia', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1651, 113, 'Perugia', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1652, 113, 'Pesaro e Urbino', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1653, 113, 'Pescara', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1654, 113, 'Piacenza', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1655, 113, 'Pisa', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1656, 113, 'Pistoia', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1657, 113, 'Pordenone', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1658, 113, 'Potenza', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1659, 113, 'Prato', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1660, 113, 'Ragusa', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1661, 113, 'Ravenna', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1662, 113, 'Reggio di Calabria', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1663, 113, 'Reggio nell\'Emilia', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1664, 113, 'Rieti', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1665, 113, 'Rimini', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1666, 113, 'Rome', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1667, 113, 'Rovigo', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1668, 113, 'Salerno', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1669, 113, 'Sassari', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1670, 113, 'Savona', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1671, 113, 'Siena', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1672, 113, 'Sondrio', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1673, 113, 'Syracuse', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1674, 113, 'Taranto', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1675, 113, 'Teramo', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1676, 113, 'Terni', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1677, 113, 'Trapani', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1678, 113, 'Trento', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1679, 113, 'Treviso', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1680, 113, 'Trieste', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1681, 113, 'Turin', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1682, 113, 'Udine', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1683, 113, 'Varese', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1684, 113, 'Venice', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1685, 113, 'Verbano-Cusio-Ossola', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1686, 113, 'Vercelli', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1687, 113, 'Verona', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1688, 113, 'Vibo Valentia', '2021-04-28 16:36:08', '2021-04-28 16:36:08'),
(1689, 113, 'Vicenza', '2021-04-28 16:36:09', '2021-04-28 16:36:09'),
(1690, 113, 'Viterbo', '2021-04-28 16:36:09', '2021-04-28 16:36:09'),
(1691, 114, 'Clarendon', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1692, 114, 'Hanover', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1693, 114, 'Kingston', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1694, 114, 'Manchester', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1695, 114, 'Portland', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1696, 114, 'Saint Andrew', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1697, 114, 'Saint Ann', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1698, 114, 'Saint Catherine', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1699, 114, 'Saint Elizabeth', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1700, 114, 'Saint James', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1701, 114, 'Saint Mary', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1702, 114, 'Saint Thomas', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1703, 114, 'Trelawny', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1704, 114, 'Westmoreland', '2021-04-28 16:36:10', '2021-04-28 16:36:10'),
(1705, 115, 'Aichi', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1706, 115, 'Akita', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1707, 115, 'Aomori', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1708, 115, 'Chiba', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1709, 115, 'Ehime', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1710, 115, 'Fukui', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1711, 115, 'Fukuoka', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1712, 115, 'Fukushima', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1713, 115, 'Gifu', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1714, 115, 'Gumma', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1715, 115, 'Hiroshima', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1716, 115, 'Hokkaido', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1717, 115, 'Hyogo', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1718, 115, 'Ibaraki', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1719, 115, 'Ishikawa', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1720, 115, 'Iwate', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1721, 115, 'Kagawa', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1722, 115, 'Kagoshima', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1723, 115, 'Kanagawa', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1724, 115, 'Kochi', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1725, 115, 'Kumamoto', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1726, 115, 'Kyoto', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1727, 115, 'Mie', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1728, 115, 'Miyagi', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1729, 115, 'Miyazaki', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1730, 115, 'Nagano', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1731, 115, 'Nagasaki', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1732, 115, 'Nara', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1733, 115, 'Niigata', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1734, 115, 'Oita', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1735, 115, 'Okayama', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1736, 115, 'Okinawa', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1737, 115, 'Osaka', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1738, 115, 'Saga', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1739, 115, 'Saitama', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1740, 115, 'Shiga', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1741, 115, 'Shimane', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1742, 115, 'Shizuoka', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1743, 115, 'Tochigi', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1744, 115, 'Tokushima', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1745, 115, 'Tokyo', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1746, 115, 'Tottori', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1747, 115, 'Toyama', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1748, 115, 'Wakayama', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1749, 115, 'Yamagata', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1750, 115, 'Yamaguchi', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1751, 115, 'Yamanashi', '2021-04-28 16:36:11', '2021-04-28 16:36:11'),
(1752, 116, 'Grouville', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1753, 116, 'Saint Brelade', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1754, 116, 'Saint Clement', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1755, 116, 'Saint Helier', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1756, 116, 'Saint John', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1757, 116, 'Saint Lawrence', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1758, 116, 'Saint Martin', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1759, 116, 'Saint Mary', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1760, 116, 'Saint Ouen', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1761, 116, 'Saint Peter', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1762, 116, 'Saint Saviour', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1763, 116, 'Trinity', '2021-04-28 16:36:12', '2021-04-28 16:36:12'),
(1764, 117, 'Ajlun', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1765, 117, 'Amman', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1766, 117, 'Aqaba', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1767, 117, 'Balqa', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1768, 117, 'Estuaire', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1769, 117, 'Haut-Ogooué', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1770, 117, 'Irbid', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1771, 117, 'Jarash', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1772, 117, 'Karak', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1773, 117, 'Madaba', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1774, 117, 'Mafraq', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1775, 117, 'Ma`an', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1776, 117, 'Moyen-Ogooué', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1777, 117, 'Ngounié', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1778, 117, 'Nyanga', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1779, 117, 'Ogooué-Ivindo', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1780, 117, 'Ogooué-Lolo', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1781, 117, 'Ogooué-Maritime', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1782, 117, 'Tafilah', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1783, 117, 'Woleu-Ntem', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1784, 117, 'Zarqa', '2021-04-28 16:36:13', '2021-04-28 16:36:13'),
(1785, 118, 'Almaty', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1786, 118, 'Almaty [City]', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1787, 118, 'Aqmola', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1788, 118, 'Aqtöbe', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1789, 118, 'Astana', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1790, 118, 'Atyrau', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1791, 118, 'Bayqonyr(1)', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1792, 118, 'East Kazakhstan', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1793, 118, 'Mangghystau', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1794, 118, 'North Kazakhstan', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1795, 118, 'Pavlodar', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1796, 118, 'Qaraghandy', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1797, 118, 'Qostanay', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1798, 118, 'Qyzylorda', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1799, 118, 'South Kazakhstan', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1800, 118, 'West Kazakhstan', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1801, 118, 'Zhambyl', '2021-04-28 16:36:14', '2021-04-28 16:36:14'),
(1802, 119, 'Central', '2021-04-28 16:36:15', '2021-04-28 16:36:15'),
(1803, 119, 'Coast', '2021-04-28 16:36:15', '2021-04-28 16:36:15'),
(1804, 119, 'Eastern', '2021-04-28 16:36:15', '2021-04-28 16:36:15'),
(1805, 119, 'Nairobi Area', '2021-04-28 16:36:15', '2021-04-28 16:36:15'),
(1806, 119, 'North-Eastern', '2021-04-28 16:36:15', '2021-04-28 16:36:15'),
(1807, 119, 'Nyanza', '2021-04-28 16:36:15', '2021-04-28 16:36:15'),
(1808, 119, 'Rift Valley', '2021-04-28 16:36:15', '2021-04-28 16:36:15'),
(1809, 119, 'Western', '2021-04-28 16:36:15', '2021-04-28 16:36:15'),
(1810, 120, 'Gilbert Islands', '2021-04-28 16:36:16', '2021-04-28 16:36:16'),
(1811, 120, 'Line Islands', '2021-04-28 16:36:16', '2021-04-28 16:36:16'),
(1812, 120, 'Phoenix Islands', '2021-04-28 16:36:16', '2021-04-28 16:36:16'),
(1813, 121, 'Al Ahmadi', '2021-04-28 16:36:17', '2021-04-28 16:36:17'),
(1814, 121, 'Al Farwānīyah', '2021-04-28 16:36:17', '2021-04-28 16:36:17'),
(1815, 121, 'Al Jahrah', '2021-04-28 16:36:17', '2021-04-28 16:36:17'),
(1816, 121, 'Al Kuwayt', '2021-04-28 16:36:17', '2021-04-28 16:36:17'),
(1817, 121, 'Hawallī', '2021-04-28 16:36:17', '2021-04-28 16:36:17'),
(1818, 121, 'Mubarak Al-Kabir', '2021-04-28 16:36:17', '2021-04-28 16:36:17'),
(1819, 122, 'Batken', '2021-04-28 16:36:18', '2021-04-28 16:36:18'),
(1820, 122, 'Bishkek', '2021-04-28 16:36:18', '2021-04-28 16:36:18'),
(1821, 122, 'Chüy', '2021-04-28 16:36:18', '2021-04-28 16:36:18'),
(1822, 122, 'Jalal-Abad', '2021-04-28 16:36:18', '2021-04-28 16:36:18'),
(1823, 122, 'Naryn', '2021-04-28 16:36:18', '2021-04-28 16:36:18'),
(1824, 122, 'Osh', '2021-04-28 16:36:18', '2021-04-28 16:36:18'),
(1825, 122, 'Talas', '2021-04-28 16:36:18', '2021-04-28 16:36:18'),
(1826, 122, 'Ysyk-Köl', '2021-04-28 16:36:18', '2021-04-28 16:36:18'),
(1827, 123, 'Attapu', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1828, 123, 'Bokeo', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1829, 123, 'Bolikhamxai', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1830, 123, 'Champasak', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1831, 123, 'Houaphan', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1832, 123, 'Khammouan', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1833, 123, 'Louang Namtha', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1834, 123, 'Louangphrabang', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1835, 123, 'Oudômxai', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1836, 123, 'Phôngsali', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1837, 123, 'Saravan', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1838, 123, 'Savannakhét', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1839, 123, 'Vientiane', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1840, 123, 'Vientiane [prefecture]', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1841, 123, 'Xaignabouri', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1842, 123, 'Xaisômboun', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1843, 123, 'Xékong', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1844, 123, 'Xiangkhoang', '2021-04-28 16:36:20', '2021-04-28 16:36:20'),
(1845, 124, 'Aizkraukle', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1846, 124, 'Alūksne', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1847, 124, 'Balvi', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1848, 124, 'Bauska', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1849, 124, 'Cēsis', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1850, 124, 'Daugavpils', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1851, 124, 'Daugavpils', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1852, 124, 'Dobele', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1853, 124, 'Gulbene', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1854, 124, 'Jelgava', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1855, 124, 'Jelgava', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1856, 124, 'Jūrmala', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1857, 124, 'Jēkabpils', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1858, 124, 'Krāslava', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1859, 124, 'Kuldīga', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1860, 124, 'Liepāja', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1861, 124, 'Liepāja', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1862, 124, 'Limbaži', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1863, 124, 'Ludza', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1864, 124, 'Madona', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1865, 124, 'Ogre', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1866, 124, 'Preiļi', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1867, 124, 'Riga', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1868, 124, 'Riga', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1869, 124, 'Rēzekne', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1870, 124, 'Rēzekne', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1871, 124, 'Saldus', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1872, 124, 'Talsi', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1873, 124, 'Tukums', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1874, 124, 'Valka', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1875, 124, 'Valmiera', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1876, 124, 'Ventspils', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1877, 124, 'Ventspils', '2021-04-28 16:36:21', '2021-04-28 16:36:21'),
(1878, 125, 'An Nabaţīyah', '2021-04-28 16:36:22', '2021-04-28 16:36:22'),
(1879, 125, 'Beirut', '2021-04-28 16:36:22', '2021-04-28 16:36:22'),
(1880, 125, 'Beqaa', '2021-04-28 16:36:22', '2021-04-28 16:36:22'),
(1881, 125, 'Mount Lebanon', '2021-04-28 16:36:22', '2021-04-28 16:36:22'),
(1882, 125, 'North Lebanon', '2021-04-28 16:36:22', '2021-04-28 16:36:22'),
(1883, 125, 'South Lebanon', '2021-04-28 16:36:22', '2021-04-28 16:36:22'),
(1884, 126, 'Berea', '2021-04-28 16:36:23', '2021-04-28 16:36:23'),
(1885, 126, 'Butha-Buthe', '2021-04-28 16:36:23', '2021-04-28 16:36:23'),
(1886, 126, 'Leribe', '2021-04-28 16:36:23', '2021-04-28 16:36:23'),
(1887, 126, 'Mafeteng', '2021-04-28 16:36:23', '2021-04-28 16:36:23'),
(1888, 126, 'Maseru', '2021-04-28 16:36:23', '2021-04-28 16:36:23'),
(1889, 126, 'Mohale\'s Hoek', '2021-04-28 16:36:23', '2021-04-28 16:36:23'),
(1890, 126, 'Mokhotlong', '2021-04-28 16:36:23', '2021-04-28 16:36:23'),
(1891, 126, 'Qacha\'s Nek', '2021-04-28 16:36:23', '2021-04-28 16:36:23'),
(1892, 126, 'Quthing', '2021-04-28 16:36:23', '2021-04-28 16:36:23'),
(1893, 126, 'Thaba-Tseka', '2021-04-28 16:36:23', '2021-04-28 16:36:23'),
(1894, 127, 'Bomi', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1895, 127, 'Bong', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1896, 127, 'Gbarpolu', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1897, 127, 'Grand Bassa', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1898, 127, 'Grand Cape Mount', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1899, 127, 'Grand Gedeh', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1900, 127, 'Grand Kru', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1901, 127, 'Lofa', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1902, 127, 'Margibi', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1903, 127, 'Maryland', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1904, 127, 'Montserrado', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1905, 127, 'Nimba', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1906, 127, 'River Gee', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1907, 127, 'Rivercess', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1908, 127, 'Sinoe', '2021-04-28 16:36:24', '2021-04-28 16:36:24'),
(1909, 128, 'Ajdabiya', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1910, 128, 'Al Butnan', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1911, 128, 'Al Hizam al Akhdar', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1912, 128, 'Al Jabal al Akhdar', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1913, 128, 'Al Jifarah', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1914, 128, 'Al Jufrah', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1915, 128, 'Al Kufrah', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1916, 128, 'Al Marj', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1917, 128, 'Al Marqab', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1918, 128, 'Al Qatrun', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1919, 128, 'Al Qubbah', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1920, 128, 'Al Wahah', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1921, 128, 'An Nuqat al Khams', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1922, 128, 'Ash Shati\'', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1923, 128, 'Az Zawiyah', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1924, 128, 'Bani Walid', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1925, 128, 'Benghazi', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1926, 128, 'Darnah', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1927, 128, 'Ghadamis', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1928, 128, 'Gharyan', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1929, 128, 'Ghat', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1930, 128, 'Jaghbub', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1931, 128, 'Misratah', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1932, 128, 'Mizdah', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1933, 128, 'Murzuq', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1934, 128, 'Nalut', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1935, 128, 'Sabha', '2021-04-28 16:36:25', '2021-04-28 16:36:25'),
(1936, 128, 'Sabratah Surman', '2021-04-28 16:36:26', '2021-04-28 16:36:26'),
(1937, 128, 'Surt', '2021-04-28 16:36:26', '2021-04-28 16:36:26'),
(1938, 128, 'Tajura\' wa an Nawahi al Arba`', '2021-04-28 16:36:26', '2021-04-28 16:36:26'),
(1939, 128, 'Tarabulus', '2021-04-28 16:36:26', '2021-04-28 16:36:26'),
(1940, 128, 'Tarhunah-Masallatah', '2021-04-28 16:36:26', '2021-04-28 16:36:26'),
(1941, 128, 'Wadi al Hayat', '2021-04-28 16:36:26', '2021-04-28 16:36:26'),
(1942, 128, 'Yafran-Jadu', '2021-04-28 16:36:26', '2021-04-28 16:36:26'),
(1943, 129, 'Balzers', '2021-04-28 16:36:27', '2021-04-28 16:36:27'),
(1944, 129, 'Eschen', '2021-04-28 16:36:27', '2021-04-28 16:36:27'),
(1945, 129, 'Gamprin', '2021-04-28 16:36:27', '2021-04-28 16:36:27'),
(1946, 129, 'Mauren', '2021-04-28 16:36:27', '2021-04-28 16:36:27'),
(1947, 129, 'Planken', '2021-04-28 16:36:27', '2021-04-28 16:36:27'),
(1948, 129, 'Ruggell', '2021-04-28 16:36:27', '2021-04-28 16:36:27'),
(1949, 129, 'Schaan', '2021-04-28 16:36:27', '2021-04-28 16:36:27'),
(1950, 129, 'Schellenberg', '2021-04-28 16:36:27', '2021-04-28 16:36:27'),
(1951, 129, 'Triesen', '2021-04-28 16:36:27', '2021-04-28 16:36:27'),
(1952, 129, 'Triesenberg', '2021-04-28 16:36:27', '2021-04-28 16:36:27'),
(1953, 129, 'Vaduz', '2021-04-28 16:36:27', '2021-04-28 16:36:27'),
(1954, 130, 'Alytus', '2021-04-28 16:36:28', '2021-04-28 16:36:28'),
(1955, 130, 'Kaunas', '2021-04-28 16:36:28', '2021-04-28 16:36:28'),
(1956, 130, 'Klaipėda', '2021-04-28 16:36:28', '2021-04-28 16:36:28'),
(1957, 130, 'Marijampolė', '2021-04-28 16:36:28', '2021-04-28 16:36:28'),
(1958, 130, 'Panevėžys', '2021-04-28 16:36:28', '2021-04-28 16:36:28'),
(1959, 130, 'Tauragė', '2021-04-28 16:36:28', '2021-04-28 16:36:28'),
(1960, 130, 'Telšiai', '2021-04-28 16:36:28', '2021-04-28 16:36:28'),
(1961, 130, 'Utena', '2021-04-28 16:36:28', '2021-04-28 16:36:28'),
(1962, 130, 'Vilnius', '2021-04-28 16:36:28', '2021-04-28 16:36:28'),
(1963, 130, 'Šiauliai', '2021-04-28 16:36:28', '2021-04-28 16:36:28'),
(1964, 131, 'Diekirch', '2021-04-28 16:36:29', '2021-04-28 16:36:29'),
(1965, 131, 'Grevenmacher', '2021-04-28 16:36:29', '2021-04-28 16:36:29'),
(1966, 131, 'Luxembourg', '2021-04-28 16:36:29', '2021-04-28 16:36:29'),
(1967, 132, 'Ilhas', '2021-04-28 16:36:30', '2021-04-28 16:36:30'),
(1968, 132, 'Macau', '2021-04-28 16:36:30', '2021-04-28 16:36:30'),
(1969, 133, 'Aerodrom', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1970, 133, 'Araãinovo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1971, 133, 'Îelino', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1972, 133, 'âa‰ka', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1973, 133, 'âair', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1974, 133, 'âe‰inovo-Oble‰evo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1975, 133, 'âuãer Sandevo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1976, 133, 'Berovo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1977, 133, 'Bitola', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1978, 133, 'Bogdanci', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1979, 133, 'Bogovinje', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1980, 133, 'Bosilovo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1981, 133, 'Brvenica', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1982, 133, 'Butel', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1983, 133, 'Centar', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1984, 133, 'Centar Îupa', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1985, 133, 'Debar', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1986, 133, 'Debartsa', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1987, 133, 'Delãevo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1988, 133, 'Demir Hisar', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1989, 133, 'Demir Kapija', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1990, 133, 'Dojran', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1991, 133, 'Dolneni', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1992, 133, 'Drugovo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1993, 133, 'Gazi Baba', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1994, 133, 'Gevgelija', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1995, 133, 'Gjorãe Petrov', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1996, 133, 'Gostivar', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1997, 133, 'Gradsko', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1998, 133, 'Ilinden', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(1999, 133, 'Jegunovtse', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2000, 133, 'Karbinci', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2001, 133, 'Karpo‰', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2002, 133, 'Kavadartsi', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2003, 133, 'Kiãevo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2004, 133, 'Kisela Voda', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2005, 133, 'Koãani', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2006, 133, 'Konãe', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2007, 133, 'Kratovo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2008, 133, 'Kriva Palanka', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2009, 133, 'Krivoga‰tani', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2010, 133, 'Kru‰evo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2011, 133, 'Kumanovo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2012, 133, 'Lipkovo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2013, 133, 'Lozovo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2014, 133, 'Makedonska Kamenica', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2015, 133, 'Makedonski Brod', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2016, 133, 'Mavrovo and Rostu‰a', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2017, 133, 'Mogila', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2018, 133, 'Negotino', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2019, 133, 'Novatsi', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2020, 133, 'Novo Selo', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2021, 133, 'Ohrid', '2021-04-28 16:36:31', '2021-04-28 16:36:31'),
(2022, 133, 'Oslomej', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2023, 133, 'Pehãevo', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2024, 133, 'Petrovec', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2025, 133, 'Plasnica', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2026, 133, 'Prilep', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2027, 133, 'Probi‰tip', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2028, 133, 'Radovi‰', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2029, 133, 'Rankovce', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2030, 133, 'Resen', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2031, 133, 'Rosoman', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2032, 133, 'Saraj', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2033, 133, 'Sopi‰te', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2034, 133, 'Staro Nagoriãane', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2035, 133, 'Struga', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2036, 133, 'Strumitsa', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2037, 133, 'Studeniãani', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2038, 133, 'Sveti Nikole', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2039, 133, 'Tearce', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2040, 133, 'Tetovo', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2041, 133, 'tip', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2042, 133, 'uto Orizari', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2043, 133, 'Valandovo', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2044, 133, 'Vasilevo', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2045, 133, 'Veles', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2046, 133, 'Vevãani', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2047, 133, 'Vinitsa', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2048, 133, 'Vrane‰tica', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2049, 133, 'Vrapãi‰te', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2050, 133, 'Zajas', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2051, 133, 'Zelenikovo', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2052, 133, 'Zrnovci', '2021-04-28 16:36:32', '2021-04-28 16:36:32'),
(2053, 134, 'Antananarivo', '2021-04-28 16:36:33', '2021-04-28 16:36:33'),
(2054, 134, 'Antsiranana', '2021-04-28 16:36:33', '2021-04-28 16:36:33'),
(2055, 134, 'Fianarantsoa', '2021-04-28 16:36:33', '2021-04-28 16:36:33'),
(2056, 134, 'Mahajanga', '2021-04-28 16:36:33', '2021-04-28 16:36:33'),
(2057, 134, 'Toamasina', '2021-04-28 16:36:33', '2021-04-28 16:36:33'),
(2058, 134, 'Toliara', '2021-04-28 16:36:33', '2021-04-28 16:36:33'),
(2059, 135, 'Balaka', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2060, 135, 'Blantyre', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2061, 135, 'Chikwawa', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2062, 135, 'Chiradzulu', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2063, 135, 'Chitipa', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2064, 135, 'Dedza', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2065, 135, 'Dowa', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2066, 135, 'Karonga', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2067, 135, 'Kasungu', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2068, 135, 'Likoma', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2069, 135, 'Lilongwe', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2070, 135, 'Machinga', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2071, 135, 'Mangochi', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2072, 135, 'Mchinji', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2073, 135, 'Mulanje', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2074, 135, 'Mwanza', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2075, 135, 'Mzimba', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2076, 135, 'Nkhata Bay', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2077, 135, 'Nkhotakota', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2078, 135, 'Nsanje', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2079, 135, 'Ntcheu', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2080, 135, 'Ntchisi', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2081, 135, 'Phalombe', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2082, 135, 'Rumphi', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2083, 135, 'Salima', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2084, 135, 'Thyolo', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2085, 135, 'Zomba', '2021-04-28 16:36:34', '2021-04-28 16:36:34'),
(2086, 136, 'Johor', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2087, 136, 'Kedah', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2088, 136, 'Kelantan', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2089, 136, 'Kuala Lumpur', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2090, 136, 'Labuan', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2091, 136, 'Melaka', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2092, 136, 'Negeri Sembilan', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2093, 136, 'Pahang', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2094, 136, 'Perak', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2095, 136, 'Perlis', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2096, 136, 'Pulau Pinang', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2097, 136, 'Putrajaya', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2098, 136, 'Sabah', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2099, 136, 'Sarawak', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2100, 136, 'Selangor', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2101, 136, 'Terengganu', '2021-04-28 16:36:35', '2021-04-28 16:36:35'),
(2102, 137, 'Aliff Aliff', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2103, 137, 'Aliff Daalu', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2104, 137, 'Baa', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2105, 137, 'Daalu', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2106, 137, 'Faafu', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2107, 137, 'Gaafu Aliff', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2108, 137, 'Gaafu Daalu', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2109, 137, 'Haa Aliff', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2110, 137, 'Haa Daalu', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2111, 137, 'Kaafu', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2112, 137, 'Laamu', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2113, 137, 'Laviyani', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2114, 137, 'Male', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2115, 137, 'Meemu', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2116, 137, 'Naviyani', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2117, 137, 'Noonu', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2118, 137, 'Raa', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2119, 137, 'Seenu', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2120, 137, 'Shaviyani', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2121, 137, 'Thaa', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2122, 137, 'Waavu', '2021-04-28 16:36:36', '2021-04-28 16:36:36'),
(2123, 138, 'Bamako', '2021-04-28 16:36:37', '2021-04-28 16:36:37'),
(2124, 138, 'Gao', '2021-04-28 16:36:37', '2021-04-28 16:36:37'),
(2125, 138, 'Kayes', '2021-04-28 16:36:37', '2021-04-28 16:36:37'),
(2126, 138, 'Kidal', '2021-04-28 16:36:37', '2021-04-28 16:36:37'),
(2127, 138, 'Koulikoro', '2021-04-28 16:36:37', '2021-04-28 16:36:37'),
(2128, 138, 'Mopti', '2021-04-28 16:36:37', '2021-04-28 16:36:37'),
(2129, 138, 'Ségou', '2021-04-28 16:36:37', '2021-04-28 16:36:37'),
(2130, 138, 'Sikasso', '2021-04-28 16:36:37', '2021-04-28 16:36:37'),
(2131, 138, 'Timbuktu', '2021-04-28 16:36:37', '2021-04-28 16:36:37'),
(2132, 139, 'Gozo', '2021-04-28 16:36:38', '2021-04-28 16:36:38'),
(2133, 139, 'Malta Majjistral', '2021-04-28 16:36:38', '2021-04-28 16:36:38'),
(2134, 139, 'Malta Xlokk', '2021-04-28 16:36:39', '2021-04-28 16:36:39'),
(2135, 140, 'Marshall Islands', '2021-04-28 16:36:40', '2021-04-28 16:36:40'),
(2136, 141, 'Fort-de-France', '2021-04-28 16:36:42', '2021-04-28 16:36:42'),
(2137, 141, 'Marin', '2021-04-28 16:36:42', '2021-04-28 16:36:42'),
(2138, 141, 'Saint-Pierre', '2021-04-28 16:36:42', '2021-04-28 16:36:42'),
(2139, 141, 'Trinité', '2021-04-28 16:36:42', '2021-04-28 16:36:42'),
(2140, 142, 'Adrar', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2141, 142, 'Assaba', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2142, 142, 'Brakna', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2143, 142, 'Dakhlet Nouadhibou', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2144, 142, 'Gorgol', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2145, 142, 'Guidimaka', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2146, 142, 'Hodh ech Chargui', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2147, 142, 'Hodh el Gharbi', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2148, 142, 'Inchiri', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2149, 142, 'Nouakchott', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2150, 142, 'Tagant', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2151, 142, 'Tiris Zemmour', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2152, 142, 'Trarza', '2021-04-28 16:36:43', '2021-04-28 16:36:43'),
(2153, 143, 'Agalega Islands', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2154, 143, 'Black River', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2155, 143, 'Cargados Carajos', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2156, 143, 'Flacq', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2157, 143, 'Grand Port', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2158, 143, 'Moka', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2159, 143, 'Pamplemousses', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2160, 143, 'Plaines Wilhems', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2161, 143, 'Port Louis', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2162, 143, 'Rivière du Rempart', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2163, 143, 'Rodrigues', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2164, 143, 'Savanne', '2021-04-28 16:36:44', '2021-04-28 16:36:44'),
(2165, 145, 'Aguascalientes', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2166, 145, 'Baja California', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2167, 145, 'Baja California Sur', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2168, 145, 'Campeche', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2169, 145, 'Chiapas', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2170, 145, 'Chihuahua', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2171, 145, 'Coahuila', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2172, 145, 'Colima', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2173, 145, 'Distrito Federal', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2174, 145, 'Durango', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2175, 145, 'Guanajuato', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2176, 145, 'Guerrero', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2177, 145, 'Hidalgo', '2021-04-28 16:36:46', '2021-04-28 16:36:46');
INSERT INTO `states` (`id`, `country_id`, `name`, `created_at`, `updated_at`) VALUES
(2178, 145, 'Jalisco', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2179, 145, 'México', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2180, 145, 'Michoacán', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2181, 145, 'Morelos', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2182, 145, 'Nayarit', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2183, 145, 'Nuevo León', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2184, 145, 'Oaxaca', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2185, 145, 'Puebla', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2186, 145, 'Querétaro', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2187, 145, 'Quintana Roo', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2188, 145, 'San Luis Potosí', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2189, 145, 'Sinaloa', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2190, 145, 'Sonora', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2191, 145, 'Tabasco', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2192, 145, 'Tamaulipas', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2193, 145, 'Tlaxcala', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2194, 145, 'Veracruz', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2195, 145, 'Yucatán', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2196, 145, 'Zacatecas', '2021-04-28 16:36:46', '2021-04-28 16:36:46'),
(2197, 146, 'Chuuk', '2021-04-28 16:36:47', '2021-04-28 16:36:47'),
(2198, 146, 'Kosrae', '2021-04-28 16:36:47', '2021-04-28 16:36:47'),
(2199, 146, 'Pohnpei', '2021-04-28 16:36:47', '2021-04-28 16:36:47'),
(2200, 146, 'Yap', '2021-04-28 16:36:47', '2021-04-28 16:36:47'),
(2201, 147, 'Anenii Noi', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2202, 147, 'Basarabeasca', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2203, 147, 'Bender', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2204, 147, 'Briceni', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2205, 147, 'Bălţi', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2206, 147, 'Cahul', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2207, 147, 'Cantemir', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2208, 147, 'Chişinău', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2209, 147, 'Cimişlia', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2210, 147, 'Criuleni', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2211, 147, 'Călăraşi', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2212, 147, 'Căuşeni', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2213, 147, 'Donduşeni', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2214, 147, 'Drochia', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2215, 147, 'Dubăsari', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2216, 147, 'Edineţ', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2217, 147, 'Floreşti', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2218, 147, 'Făleşti', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2219, 147, 'Glodeni', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2220, 147, 'Găgăuzia', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2221, 147, 'Hînceşti', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2222, 147, 'Ialoveni', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2223, 147, 'Leova', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2224, 147, 'Nisporeni', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2225, 147, 'Ocniţa', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2226, 147, 'Orhei', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2227, 147, 'Rîşcani', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2228, 147, 'Rezina', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2229, 147, 'Sîngerei', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2230, 147, 'Soroca', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2231, 147, 'Străşeni', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2232, 147, 'Taraclia', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2233, 147, 'Teleneşti', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2234, 147, 'Transnistria', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2235, 147, 'Ungheni', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2236, 147, 'Şoldăneşti', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2237, 147, 'Ştefan Voda', '2021-04-28 16:36:48', '2021-04-28 16:36:48'),
(2238, 148, 'Monaco', '2021-04-28 16:36:49', '2021-04-28 16:36:49'),
(2239, 149, 'Arhangay', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2240, 149, 'Ömnögovi', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2241, 149, 'Övörhangay', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2242, 149, 'Bayan-Ölgiy', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2243, 149, 'Bayanhongor', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2244, 149, 'Bulgan', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2245, 149, 'Darhan-Uul', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2246, 149, 'Dornod', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2247, 149, 'Dornogovi', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2248, 149, 'Dundgovi', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2249, 149, 'Dzavhan', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2250, 149, 'Govi-Altay', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2251, 149, 'Govisumber', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2252, 149, 'Hövsgöl', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2253, 149, 'Hentiy', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2254, 149, 'Hovd', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2255, 149, 'Orhon', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2256, 149, 'Sühbaatar', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2257, 149, 'Selenge', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2258, 149, 'Töv', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2259, 149, 'Ulaanbaatar', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2260, 149, 'Uvs', '2021-04-28 16:36:51', '2021-04-28 16:36:51'),
(2261, 150, 'Andrijevica', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2262, 150, 'avnik', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2263, 150, 'Îabljak', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2264, 150, 'Bar', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2265, 150, 'Berane', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2266, 150, 'Bijelo Polje', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2267, 150, 'Budva', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2268, 150, 'Cetinje', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2269, 150, 'Danilovgrad', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2270, 150, 'Herceg Novi', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2271, 150, 'Kola‰in', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2272, 150, 'Kotor', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2273, 150, 'Mojkovac', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2274, 150, 'Nik‰iç', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2275, 150, 'Plav', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2276, 150, 'Pljevlja', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2277, 150, 'PluÏine', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2278, 150, 'Podgorica', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2279, 150, 'RoÏaje', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2280, 150, 'Tivat', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2281, 150, 'Ulcinj', '2021-04-28 16:36:52', '2021-04-28 16:36:52'),
(2282, 151, 'Saint Anthony', '2021-04-28 16:36:58', '2021-04-28 16:36:58'),
(2283, 151, 'Saint Georges', '2021-04-28 16:36:58', '2021-04-28 16:36:58'),
(2284, 151, 'Saint Peter', '2021-04-28 16:36:58', '2021-04-28 16:36:58'),
(2285, 152, 'Chaouia - Ouardigha', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2286, 152, 'Doukkala - Abda', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2287, 152, 'Fès - Boulemane', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2288, 152, 'Gharb - Chrarda - Béni Hssen', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2289, 152, 'Grand Casablanca', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2290, 152, 'Guelmim - Es-Semara', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2291, 152, 'Laâyoune - Boujdour - Sakia El', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2292, 152, 'Marrakech - Tensift - Al Haouz', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2293, 152, 'Meknès - Tafilalet', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2294, 152, 'Oriental', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2295, 152, 'Oued Ed-Dahab - Lagouira', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2296, 152, 'Rabat - Salé - Zemmour - Zaer', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2297, 152, 'Souss - Massa - Draâ', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2298, 152, 'Tadla - Azilal', '2021-04-28 16:36:59', '2021-04-28 16:36:59'),
(2299, 153, 'Cabo Delgado', '2021-04-28 16:37:00', '2021-04-28 16:37:00'),
(2300, 153, 'Gaza', '2021-04-28 16:37:00', '2021-04-28 16:37:00'),
(2301, 153, 'Inhambane', '2021-04-28 16:37:00', '2021-04-28 16:37:00'),
(2302, 153, 'Manica', '2021-04-28 16:37:00', '2021-04-28 16:37:00'),
(2303, 153, 'Maputo', '2021-04-28 16:37:00', '2021-04-28 16:37:00'),
(2304, 153, 'Maputo [city]', '2021-04-28 16:37:00', '2021-04-28 16:37:00'),
(2305, 153, 'Nampula', '2021-04-28 16:37:00', '2021-04-28 16:37:00'),
(2306, 153, 'Niassa', '2021-04-28 16:37:00', '2021-04-28 16:37:00'),
(2307, 153, 'Sofala', '2021-04-28 16:37:00', '2021-04-28 16:37:00'),
(2308, 153, 'Tete', '2021-04-28 16:37:00', '2021-04-28 16:37:00'),
(2309, 153, 'Zambezia', '2021-04-28 16:37:00', '2021-04-28 16:37:00'),
(2310, 154, 'Ayeyarwady', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2311, 154, 'Bago', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2312, 154, 'Chin', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2313, 154, 'Kachin', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2314, 154, 'Kayah', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2315, 154, 'Kayin', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2316, 154, 'Magway', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2317, 154, 'Mandalay', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2318, 154, 'Mon', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2319, 154, 'Rakhine', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2320, 154, 'Sagaing', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2321, 154, 'Shan', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2322, 154, 'Tanintharyi', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2323, 154, 'Yangon', '2021-04-28 16:37:01', '2021-04-28 16:37:01'),
(2324, 155, 'Caprivi', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2325, 155, 'Erongo', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2326, 155, 'Hardap', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2327, 155, 'Karas', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2328, 155, 'Khomas', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2329, 155, 'Kunene', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2330, 155, 'Ohangwena', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2331, 155, 'Okavango', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2332, 155, 'Omaheke', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2333, 155, 'Omusati', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2334, 155, 'Oshana', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2335, 155, 'Oshikoto', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2336, 155, 'Otjozondjupa', '2021-04-28 16:37:02', '2021-04-28 16:37:02'),
(2337, 156, 'Aiwo', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2338, 156, 'Anabar', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2339, 156, 'Anetan', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2340, 156, 'Anibare', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2341, 156, 'Baiti', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2342, 156, 'Boe', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2343, 156, 'Buada', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2344, 156, 'Denigomodu', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2345, 156, 'Ewa', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2346, 156, 'Ijuw', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2347, 156, 'Meneng', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2348, 156, 'Nibok', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2349, 156, 'Uaboe', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2350, 156, 'Yaren', '2021-04-28 16:37:04', '2021-04-28 16:37:04'),
(2351, 157, 'Madhya Pashchimanchal', '2021-04-28 16:37:05', '2021-04-28 16:37:05'),
(2352, 157, 'Madhyamanchal', '2021-04-28 16:37:05', '2021-04-28 16:37:05'),
(2353, 157, 'Pashchimanchal', '2021-04-28 16:37:05', '2021-04-28 16:37:05'),
(2354, 157, 'Purwanchal', '2021-04-28 16:37:05', '2021-04-28 16:37:05'),
(2355, 157, 'Sudur Pashchimanchal', '2021-04-28 16:37:05', '2021-04-28 16:37:05'),
(2356, 158, 'Drenthe', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2357, 158, 'Flevoland', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2358, 158, 'Friesland', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2359, 158, 'Gelderland', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2360, 158, 'Groningen', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2361, 158, 'Limburg', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2362, 158, 'Noord-Brabant', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2363, 158, 'Noord-Holland', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2364, 158, 'Overijssel', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2365, 158, 'Utrecht', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2366, 158, 'Zeeland', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2367, 158, 'Zuid-Holland', '2021-04-28 16:37:06', '2021-04-28 16:37:06'),
(2368, 159, 'Îles Loyauté', '2021-04-28 16:37:07', '2021-04-28 16:37:07'),
(2369, 159, 'Nord', '2021-04-28 16:37:07', '2021-04-28 16:37:07'),
(2370, 159, 'Sud', '2021-04-28 16:37:07', '2021-04-28 16:37:07'),
(2371, 160, 'Auckland', '2021-04-28 16:37:08', '2021-04-28 16:37:08'),
(2372, 160, 'Bay of Plenty', '2021-04-28 16:37:08', '2021-04-28 16:37:08'),
(2373, 160, 'Canterbury', '2021-04-28 16:37:08', '2021-04-28 16:37:08'),
(2374, 160, 'Chatham Islands', '2021-04-28 16:37:08', '2021-04-28 16:37:08'),
(2375, 160, 'Gisborne', '2021-04-28 16:37:08', '2021-04-28 16:37:08'),
(2376, 160, 'Hawke\'s Bay', '2021-04-28 16:37:08', '2021-04-28 16:37:08'),
(2377, 160, 'Manawatu-Wanganui', '2021-04-28 16:37:08', '2021-04-28 16:37:08'),
(2378, 160, 'Marlborough', '2021-04-28 16:37:08', '2021-04-28 16:37:08'),
(2379, 161, 'Boaco', '2021-04-28 16:37:09', '2021-04-28 16:37:09'),
(2380, 161, 'Carazo', '2021-04-28 16:37:09', '2021-04-28 16:37:09'),
(2381, 161, 'Chinandega', '2021-04-28 16:37:09', '2021-04-28 16:37:09'),
(2382, 161, 'Chontales', '2021-04-28 16:37:09', '2021-04-28 16:37:09'),
(2383, 161, 'Estelí', '2021-04-28 16:37:09', '2021-04-28 16:37:09'),
(2384, 161, 'Granada', '2021-04-28 16:37:09', '2021-04-28 16:37:09'),
(2385, 161, 'Jinotega', '2021-04-28 16:37:09', '2021-04-28 16:37:09'),
(2386, 161, 'León', '2021-04-28 16:37:09', '2021-04-28 16:37:09'),
(2387, 161, 'Madriz', '2021-04-28 16:37:09', '2021-04-28 16:37:09'),
(2388, 161, 'Managua', '2021-04-28 16:37:09', '2021-04-28 16:37:09'),
(2389, 161, 'Masaya', '2021-04-28 16:37:09', '2021-04-28 16:37:09'),
(2390, 162, 'Agadez', '2021-04-28 16:37:10', '2021-04-28 16:37:10'),
(2391, 162, 'Diffa', '2021-04-28 16:37:10', '2021-04-28 16:37:10'),
(2392, 162, 'Dosso', '2021-04-28 16:37:10', '2021-04-28 16:37:10'),
(2393, 162, 'Maradi', '2021-04-28 16:37:10', '2021-04-28 16:37:10'),
(2394, 162, 'Niamey', '2021-04-28 16:37:10', '2021-04-28 16:37:10'),
(2395, 162, 'Tahoua', '2021-04-28 16:37:10', '2021-04-28 16:37:10'),
(2396, 162, 'Tillabéry', '2021-04-28 16:37:10', '2021-04-28 16:37:10'),
(2397, 162, 'Zinder', '2021-04-28 16:37:10', '2021-04-28 16:37:10'),
(2398, 163, 'Abia', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2399, 163, 'Adamawa', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2400, 163, 'Akwa Ibom', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2401, 163, 'Anambra', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2402, 163, 'Bauchi', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2403, 163, 'Bayelsa', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2404, 163, 'Benue', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2405, 163, 'Borno', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2406, 163, 'Cross River', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2407, 163, 'Delta', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2408, 163, 'Ebonyi', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2409, 163, 'Edo', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2410, 163, 'Ekiti', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2411, 163, 'Enugu', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2412, 163, 'Federal Capital Territory', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2413, 163, 'Gombe', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2414, 163, 'Imo', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2415, 163, 'Jigawa', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2416, 163, 'Kaduna', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2417, 163, 'Kano', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2418, 163, 'Katsina', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2419, 163, 'Kebbi', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2420, 163, 'Kogi', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2421, 163, 'Kwara', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2422, 163, 'Lagos', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2423, 163, 'Nassarawa', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2424, 163, 'Niger', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2425, 163, 'Ogun', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2426, 163, 'Ondo', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2427, 163, 'Osun', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2428, 163, 'Oyo', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2429, 163, 'Plateau', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2430, 163, 'Rivers', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2431, 163, 'Sokoto', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2432, 163, 'Taraba', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2433, 163, 'Yobe', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2434, 163, 'Zamfara', '2021-04-28 16:37:11', '2021-04-28 16:37:11'),
(2435, 164, 'Niue', '2021-04-28 16:37:12', '2021-04-28 16:37:12'),
(2436, 166, 'Chagang-do', '2021-04-28 16:37:14', '2021-04-28 16:37:14'),
(2437, 166, 'Hamgyŏng-bukto', '2021-04-28 16:37:14', '2021-04-28 16:37:14'),
(2438, 166, 'Hamgyŏng-namdo', '2021-04-28 16:37:14', '2021-04-28 16:37:14'),
(2439, 166, 'Hwanghae-bukto', '2021-04-28 16:37:14', '2021-04-28 16:37:14'),
(2440, 166, 'Hwanghae-namdo', '2021-04-28 16:37:14', '2021-04-28 16:37:14'),
(2441, 166, 'Kaesŏng-si', '2021-04-28 16:37:14', '2021-04-28 16:37:14'),
(2442, 166, 'Kangwŏn-do', '2021-04-28 16:37:15', '2021-04-28 16:37:15'),
(2443, 166, 'Najin Sŏnbong-si', '2021-04-28 16:37:15', '2021-04-28 16:37:15'),
(2444, 166, 'Namp\'o-si', '2021-04-28 16:37:15', '2021-04-28 16:37:15'),
(2445, 166, 'P\'yŏngan-bukto', '2021-04-28 16:37:15', '2021-04-28 16:37:15'),
(2446, 166, 'P\'yŏngan-namdo', '2021-04-28 16:37:15', '2021-04-28 16:37:15'),
(2447, 166, 'P\'yŏngyang-si', '2021-04-28 16:37:15', '2021-04-28 16:37:15'),
(2448, 166, 'Yanggang-do', '2021-04-28 16:37:15', '2021-04-28 16:37:15'),
(2449, 167, 'Northern Islands', '2021-04-28 16:37:16', '2021-04-28 16:37:16'),
(2450, 167, 'Rota', '2021-04-28 16:37:16', '2021-04-28 16:37:16'),
(2451, 167, 'Saipan', '2021-04-28 16:37:16', '2021-04-28 16:37:16'),
(2452, 167, 'Tinian', '2021-04-28 16:37:16', '2021-04-28 16:37:16'),
(2453, 168, 'Akershus', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2454, 168, 'Aust-Agder', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2455, 168, 'Østfold', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2456, 168, 'Buskerud', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2457, 168, 'Finnmark', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2458, 168, 'Hedmark', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2459, 168, 'Hordaland', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2460, 168, 'Møre og Romsdal', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2461, 168, 'Nord-Trøndelag', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2462, 168, 'Nordland', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2463, 168, 'Oppland', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2464, 168, 'Oslo', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2465, 168, 'Rogaland', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2466, 168, 'Sør-Trøndelag', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2467, 168, 'Sogn og Fjordane', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2468, 168, 'Telemark', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2469, 168, 'Troms', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2470, 168, 'Vest-Agder', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2471, 168, 'Vestfold', '2021-04-28 16:37:17', '2021-04-28 16:37:17'),
(2472, 169, 'Ad Dakhliyah', '2021-04-28 16:37:18', '2021-04-28 16:37:18'),
(2473, 169, 'Adh Dhahirah', '2021-04-28 16:37:18', '2021-04-28 16:37:18'),
(2474, 169, 'Al Batinah', '2021-04-28 16:37:18', '2021-04-28 16:37:18'),
(2475, 169, 'Al Wusta', '2021-04-28 16:37:18', '2021-04-28 16:37:18'),
(2476, 169, 'Ash Sharqiyah', '2021-04-28 16:37:18', '2021-04-28 16:37:18'),
(2477, 169, 'Dhofar', '2021-04-28 16:37:18', '2021-04-28 16:37:18'),
(2478, 169, 'Musandam', '2021-04-28 16:37:18', '2021-04-28 16:37:18'),
(2479, 169, 'Muscat', '2021-04-28 16:37:18', '2021-04-28 16:37:18'),
(2480, 170, 'Azad Kashmir', '2021-04-28 16:37:19', '2021-04-28 16:37:19'),
(2481, 170, 'Balochistan', '2021-04-28 16:37:19', '2021-04-28 16:37:19'),
(2482, 170, 'Federally Administered Tribal Ar', '2021-04-28 16:37:19', '2021-04-28 16:37:19'),
(2483, 170, 'Islamabad', '2021-04-28 16:37:19', '2021-04-28 16:37:19'),
(2484, 170, 'North-West Frontier', '2021-04-28 16:37:19', '2021-04-28 16:37:19'),
(2485, 170, 'Northern Areas', '2021-04-28 16:37:19', '2021-04-28 16:37:19'),
(2486, 170, 'Punjab', '2021-04-28 16:37:19', '2021-04-28 16:37:19'),
(2487, 170, 'Sindh', '2021-04-28 16:37:19', '2021-04-28 16:37:19'),
(2488, 171, 'Aimeliik', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2489, 171, 'Airai', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2490, 171, 'Angaur', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2491, 171, 'Hatohobei', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2492, 171, 'Kayangel', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2493, 171, 'Koror', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2494, 171, 'Melekeok', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2495, 171, 'Ngaraard', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2496, 171, 'Ngarchelong', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2497, 171, 'Ngardmau', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2498, 171, 'Ngatpang', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2499, 171, 'Ngchesar', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2500, 171, 'Ngeremlengui', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2501, 171, 'Ngiwal', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2502, 171, 'Peleliu', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2503, 171, 'Sonsorol', '2021-04-28 16:37:20', '2021-04-28 16:37:20'),
(2504, 172, 'Gaza', '2021-04-28 16:37:21', '2021-04-28 16:37:21'),
(2505, 172, 'West Bank', '2021-04-28 16:37:21', '2021-04-28 16:37:21'),
(2506, 173, 'Bocas del Toro', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2507, 173, 'Chiriquí', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2508, 173, 'Coclé', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2509, 173, 'Colón', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2510, 173, 'Darién', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2511, 173, 'Emberá', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2512, 173, 'Herrera', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2513, 173, 'Kuna de Madungandí', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2514, 173, 'Kuna de Wargandi', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2515, 173, 'Kuna Yala', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2516, 173, 'Los Santos', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2517, 173, 'Ngöbe Buglé', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2518, 173, 'Panamá', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2519, 173, 'Veraguas', '2021-04-28 16:37:22', '2021-04-28 16:37:22'),
(2520, 174, 'Central', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2521, 174, 'Chimbu', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2522, 174, 'East New Britain', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2523, 174, 'East Sepik', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2524, 174, 'Eastern Highlands', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2525, 174, 'Enga', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2526, 174, 'Gulf', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2527, 174, 'Madang', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2528, 174, 'Manus', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2529, 174, 'Milne Bay', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2530, 174, 'Morobe', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2531, 174, 'National Capital District', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2532, 174, 'New Ireland', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2533, 174, 'North Solomons', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2534, 174, 'Northern', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2535, 174, 'Sandaun', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2536, 174, 'Southern Highlands', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2537, 174, 'West New Britain', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2538, 174, 'Western', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2539, 174, 'Western Highlands', '2021-04-28 16:37:23', '2021-04-28 16:37:23'),
(2540, 175, 'Alto Paraguay', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2541, 175, 'Alto Paraná', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2542, 175, 'Amambay', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2543, 175, 'Asunción', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2544, 175, 'Ñeembucú', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2545, 175, 'Boquerón', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2546, 175, 'Caaguazú', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2547, 175, 'Caazapá', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2548, 175, 'Canindeyú', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2549, 175, 'Central', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2550, 175, 'Concepción', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2551, 175, 'Cordillera', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2552, 175, 'Guairá', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2553, 175, 'Itapúa', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2554, 175, 'Misiones', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2555, 175, 'Paraguarí', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2556, 175, 'Presidente Hayes', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2557, 175, 'San Pedro', '2021-04-28 16:37:24', '2021-04-28 16:37:24'),
(2558, 176, 'Amazonas', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2559, 176, 'Ancash', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2560, 176, 'Apurímac', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2561, 176, 'Arequipa', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2562, 176, 'Ayacucho', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2563, 176, 'Cajamarca', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2564, 176, 'Callao', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2565, 176, 'Cusco', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2566, 176, 'Huancavelica', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2567, 176, 'Huánuco', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2568, 176, 'Ica', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2569, 176, 'Junín', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2570, 176, 'La Libertad', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2571, 176, 'Lambayeque', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2572, 176, 'Lima', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2573, 176, 'Lima [Province]', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2574, 176, 'Loreto', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2575, 176, 'Madre de Dios', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2576, 176, 'Moquegua', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2577, 176, 'Pasco', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2578, 176, 'Piura', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2579, 176, 'Puno', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2580, 176, 'San Martín', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2581, 176, 'Tacna', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2582, 176, 'Tumbes', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2583, 176, 'Ucayali', '2021-04-28 16:37:25', '2021-04-28 16:37:25'),
(2584, 177, 'Abra', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2585, 177, 'Agusan del Norte', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2586, 177, 'Agusan del Sur', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2587, 177, 'Aklan', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2588, 177, 'Albay', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2589, 177, 'Antique', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2590, 177, 'Apayao', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2591, 177, 'Aurora', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2592, 177, 'Basilan', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2593, 177, 'Bataan', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2594, 177, 'Batanes', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2595, 177, 'Batangas', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2596, 177, 'Benguet', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2597, 177, 'Biliran', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2598, 177, 'Bohol', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2599, 177, 'Bukidnon', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2600, 177, 'Bulacan', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2601, 177, 'Cagayan', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2602, 177, 'Camarines Norte', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2603, 177, 'Camarines Sur', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2604, 177, 'Camiguin', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2605, 177, 'Capiz', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2606, 177, 'Catanduanes', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2607, 177, 'Cavite', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2608, 177, 'Cebu', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2609, 177, 'Compostela Valley', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2610, 177, 'Cotabato', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2611, 177, 'Davao del Norte', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2612, 177, 'Davao del Sur', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2613, 177, 'Davao Oriental', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2614, 177, 'Dinagat Islands', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2615, 177, 'Eastern Samar', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2616, 177, 'Guimaras', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2617, 177, 'Ifugao', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2618, 177, 'Ilocos Norte', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2619, 177, 'Ilocos Sur', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2620, 177, 'Iloilo', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2621, 177, 'Isabela', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2622, 177, 'Kalinga', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2623, 177, 'La Union', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2624, 177, 'Laguna', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2625, 177, 'Lanao del Norte', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2626, 177, 'Lanao del Sur', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2627, 177, 'Leyte', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2628, 177, 'Maguindanao', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2629, 177, 'Marinduque', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2630, 177, 'Masbate', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2631, 177, 'Metropolitan Manila', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2632, 177, 'Misamis Occidental', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2633, 177, 'Misamis Oriental', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2634, 177, 'Mountain', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2635, 177, 'Negros Occidental', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2636, 177, 'Negros Oriental', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2637, 177, 'Northern Samar', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2638, 177, 'Nueva Ecija', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2639, 177, 'Nueva Vizcaya', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2640, 177, 'Occidental Mindoro', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2641, 177, 'Oriental Mindoro', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2642, 177, 'Palawan', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2643, 177, 'Pampanga', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2644, 177, 'Pangasinan', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2645, 177, 'Quezon', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2646, 177, 'Quirino', '2021-04-28 16:37:26', '2021-04-28 16:37:26'),
(2647, 177, 'Rizal', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2648, 177, 'Romblon', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2649, 177, 'Samar', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2650, 177, 'Sarangani', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2651, 177, 'Shariff Kabunsuan', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2652, 177, 'Siquijor', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2653, 177, 'Sorsogon', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2654, 177, 'South Cotabato', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2655, 177, 'Southern Leyte', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2656, 177, 'Sultan Kudarat', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2657, 177, 'Sulu', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2658, 177, 'Surigao del Norte', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2659, 177, 'Surigao del Sur', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2660, 177, 'Tarlac', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2661, 177, 'Tawi-Tawi', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2662, 177, 'Zambales', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2663, 177, 'Zamboanga del Norte', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2664, 177, 'Zamboanga del Sur', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2665, 177, 'Zamboanga-Sibugay', '2021-04-28 16:37:27', '2021-04-28 16:37:27'),
(2666, 178, 'Pitcairn Islands', '2021-04-28 16:37:28', '2021-04-28 16:37:28'),
(2667, 179, 'dolnośląskie', '2021-04-28 16:37:28', '2021-04-28 16:37:28'),
(2668, 179, 'kujawsko-pomorskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2669, 179, 'lubelskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2670, 179, 'lubuskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2671, 179, 'mazowieckie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2672, 179, 'małopolskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2673, 179, 'opolskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2674, 179, 'podkarpackie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2675, 179, 'podlaskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2676, 179, 'pomorskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2677, 179, 'warmińsko-mazurskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2678, 179, 'wielkopolskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2679, 179, 'zachodniopomorskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2680, 179, 'łódzkie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2681, 179, 'śląskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2682, 179, 'świętokrzyskie', '2021-04-28 16:37:29', '2021-04-28 16:37:29'),
(2683, 180, 'Aveiro', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2684, 180, 'Azores', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2685, 180, 'Évora', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2686, 180, 'Beja', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2687, 180, 'Braga', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2688, 180, 'Bragança', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2689, 180, 'Castelo Branco', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2690, 180, 'Coimbra', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2691, 180, 'Faro', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2692, 180, 'Guarda', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2693, 180, 'Leiria', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2694, 180, 'Lisboa', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2695, 180, 'Madeira', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2696, 180, 'Portalegre', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2697, 180, 'Porto', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2698, 180, 'Santarém', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2699, 180, 'Setúbal', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2700, 180, 'Viana do Castelo', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2701, 180, 'Vila Real', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2702, 180, 'Viseu', '2021-04-28 16:37:30', '2021-04-28 16:37:30'),
(2703, 181, 'Añasco', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2704, 181, 'Adjuntas', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2705, 181, 'Aguada', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2706, 181, 'Aguadilla', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2707, 181, 'Aguas Buenas', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2708, 181, 'Aibonito', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2709, 181, 'Arecibo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2710, 181, 'Arroyo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2711, 181, 'Barceloneta', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2712, 181, 'Barranquitas', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2713, 181, 'Bayamón', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2714, 181, 'Cabo Rojo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2715, 181, 'Caguas', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2716, 181, 'Camuy', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2717, 181, 'Canóvanas', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2718, 181, 'Carolina', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2719, 181, 'Cataño', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2720, 181, 'Cayey', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2721, 181, 'Ceiba', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2722, 181, 'Ciales', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2723, 181, 'Cidra', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2724, 181, 'Coamo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2725, 181, 'Comerío', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2726, 181, 'Corozal', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2727, 181, 'Culebra', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2728, 181, 'Dorado', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2729, 181, 'Fajardo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2730, 181, 'Florida', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2731, 181, 'Guayama', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2732, 181, 'Guayanilla', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2733, 181, 'Guaynabo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2734, 181, 'Guánica', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2735, 181, 'Gurabo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2736, 181, 'Hatillo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2737, 181, 'Hormigueros', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2738, 181, 'Humacao', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2739, 181, 'Isabela', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2740, 181, 'Jayuya', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2741, 181, 'Juana Díaz', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2742, 181, 'Juncos', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2743, 181, 'Lajas', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2744, 181, 'Lares', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2745, 181, 'Las Marías', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2746, 181, 'Las Piedras', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2747, 181, 'Loíza', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2748, 181, 'Luquillo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2749, 181, 'Manatí', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2750, 181, 'Maricao', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2751, 181, 'Maunabo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2752, 181, 'Mayagüez', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2753, 181, 'Moca', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2754, 181, 'Morovis', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2755, 181, 'Naguabo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2756, 181, 'Naranjito', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2757, 181, 'Orocovis', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2758, 181, 'Patillas', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2759, 181, 'Peñuelas', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2760, 181, 'Ponce', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2761, 181, 'Quebradillas', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2762, 181, 'Río Grande', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2763, 181, 'Rincón', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2764, 181, 'Sabana Grande', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2765, 181, 'Salinas', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2766, 181, 'San Germán', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2767, 181, 'San Juan', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2768, 181, 'San Lorenzo', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2769, 181, 'San Sebastián', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2770, 181, 'Santa Isabel', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2771, 181, 'Toa Alta', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2772, 181, 'Toa Baja', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2773, 181, 'Trujillo Alto', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2774, 181, 'Utuado', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2775, 181, 'Vega Alta', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2776, 181, 'Vega Baja', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2777, 181, 'Vieques', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2778, 181, 'Villalba', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2779, 181, 'Yabucoa', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2780, 181, 'Yauco', '2021-04-28 16:37:31', '2021-04-28 16:37:31'),
(2781, 182, 'Ad Dawhah', '2021-04-28 16:37:32', '2021-04-28 16:37:32'),
(2782, 182, 'Al Ghuwariyah', '2021-04-28 16:37:32', '2021-04-28 16:37:32'),
(2783, 182, 'Al Jumaliyah', '2021-04-28 16:37:32', '2021-04-28 16:37:32'),
(2784, 182, 'Al Khawr', '2021-04-28 16:37:32', '2021-04-28 16:37:32'),
(2785, 182, 'Al Wakrah', '2021-04-28 16:37:32', '2021-04-28 16:37:32'),
(2786, 182, 'Ar Rayyan', '2021-04-28 16:37:32', '2021-04-28 16:37:32'),
(2787, 182, 'Jariyan al Batnah', '2021-04-28 16:37:32', '2021-04-28 16:37:32'),
(2788, 182, 'Madinat ach Shamal', '2021-04-28 16:37:32', '2021-04-28 16:37:32'),
(2789, 182, 'Umm Salal', '2021-04-28 16:37:32', '2021-04-28 16:37:32'),
(2790, 184, 'Saint-Benoît', '2021-04-28 16:37:34', '2021-04-28 16:37:34'),
(2791, 184, 'Saint-Denis', '2021-04-28 16:37:34', '2021-04-28 16:37:34'),
(2792, 184, 'Saint-Paul', '2021-04-28 16:37:34', '2021-04-28 16:37:34'),
(2793, 184, 'Saint-Pierre', '2021-04-28 16:37:34', '2021-04-28 16:37:34'),
(2794, 185, 'Alba', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2795, 185, 'Arad', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2796, 185, 'Argeș', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2797, 185, 'Bacău', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2798, 185, 'Bihor', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2799, 185, 'Bistrița-Năsăud', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2800, 185, 'Botoșani', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2801, 185, 'Brașov', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2802, 185, 'Brăila', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2803, 185, 'Bucharest', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2804, 185, 'Buzău', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2805, 185, 'Caraș-Severin', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2806, 185, 'Cluj', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2807, 185, 'Constanța', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2808, 185, 'Covasna', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2809, 185, 'Călărași', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2810, 185, 'Dâmbovița', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2811, 185, 'Dolj', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2812, 185, 'Galați', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2813, 185, 'Giurgiu', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2814, 185, 'Gorj', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2815, 185, 'Harghita', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2816, 185, 'Hunedoara', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2817, 185, 'Iași', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2818, 185, 'Ialomița', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2819, 185, 'Ilfov', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2820, 185, 'Maramureș', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2821, 185, 'Mehedinți', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2822, 185, 'Mureș', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2823, 185, 'Neamț', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2824, 185, 'Olt', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2825, 185, 'Prahova', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2826, 185, 'Satu Mare', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2827, 185, 'Sibiu', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2828, 185, 'Suceava', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2829, 185, 'Sălaj', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2830, 185, 'Teleorman', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2831, 185, 'Timiș', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2832, 185, 'Tulcea', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2833, 185, 'Vaslui', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2834, 185, 'Vâlcea', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2835, 185, 'Vrancea', '2021-04-28 16:37:35', '2021-04-28 16:37:35'),
(2836, 186, 'Adygey', '2021-04-28 16:37:36', '2021-04-28 16:37:36'),
(2837, 186, 'Aga Buryat', '2021-04-28 16:37:36', '2021-04-28 16:37:36'),
(2838, 186, 'Altay', '2021-04-28 16:37:36', '2021-04-28 16:37:36'),
(2839, 186, 'Amur', '2021-04-28 16:37:36', '2021-04-28 16:37:36'),
(2840, 186, 'Arkhangel\'sk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2841, 186, 'Astrakhan\'', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2842, 186, 'Bashkortostan', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2843, 186, 'Belgorod', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2844, 186, 'Bryansk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2845, 186, 'Buryat', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2846, 186, 'Chechnya', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2847, 186, 'Chelyabinsk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2848, 186, 'Chita', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2849, 186, 'Chukot', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2850, 186, 'Chuvash', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2851, 186, 'Dagestan', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2852, 186, 'Gorno-Altay', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2853, 186, 'Ingush', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2854, 186, 'Irkutsk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2855, 186, 'Ivanovo', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2856, 186, 'Kabardin-Balkar', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2857, 186, 'Kaliningrad', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2858, 186, 'Kalmyk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2859, 186, 'Kaluga', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2860, 186, 'Kamchatka', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2861, 186, 'Karachay-Cherkess', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2862, 186, 'Karelia', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2863, 186, 'Kemerovo', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2864, 186, 'Khabarovsk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2865, 186, 'Khakass', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2866, 186, 'Khanty-Mansiy', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2867, 186, 'Kirov', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2868, 186, 'Komi', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2869, 186, 'Koryak', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2870, 186, 'Kostroma', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2871, 186, 'Krasnodar', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2872, 186, 'Krasnoyarsk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2873, 186, 'Kurgan', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2874, 186, 'Kursk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2875, 186, 'Leningrad', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2876, 186, 'Lipetsk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2877, 186, 'Magadan', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2878, 186, 'Mariy-El', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2879, 186, 'Mordovia', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2880, 186, 'Moscow City', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2881, 186, 'Moskva', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2882, 186, 'Murmansk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2883, 186, 'Nenets', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2884, 186, 'Nizhegorod', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2885, 186, 'North Ossetia', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2886, 186, 'Novgorod', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2887, 186, 'Novosibirsk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2888, 186, 'Omsk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2889, 186, 'Orel', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2890, 186, 'Orenburg', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2891, 186, 'Penza', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2892, 186, 'Perm\'', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2893, 186, 'Primor\'ye', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2894, 186, 'Pskov', '2021-04-28 16:37:37', '2021-04-28 16:37:37');
INSERT INTO `states` (`id`, `country_id`, `name`, `created_at`, `updated_at`) VALUES
(2895, 186, 'Rostov', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2896, 186, 'Ryazan\'', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2897, 186, 'Saint Petersburg City', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2898, 186, 'Sakha', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2899, 186, 'Sakhalin', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2900, 186, 'Samara', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2901, 186, 'Saratov', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2902, 186, 'Smolensk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2903, 186, 'Stavropol\'', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2904, 186, 'Sverdlovsk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2905, 186, 'Tambov', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2906, 186, 'Tatarstan', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2907, 186, 'Tomsk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2908, 186, 'Tula', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2909, 186, 'Tuva', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2910, 186, 'Tver\'', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2911, 186, 'Tyumen\'', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2912, 186, 'Udmurt', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2913, 186, 'Ul\'yanovsk', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2914, 186, 'Ust-Orda Buryat', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2915, 186, 'Vladimir', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2916, 186, 'Volgograd', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2917, 186, 'Vologda', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2918, 186, 'Voronezh', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2919, 186, 'Yamal-Nenets', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2920, 186, 'Yaroslavl\'', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2921, 186, 'Yevrey', '2021-04-28 16:37:37', '2021-04-28 16:37:37'),
(2922, 187, 'Eastern', '2021-04-28 16:37:38', '2021-04-28 16:37:38'),
(2923, 187, 'Kigali City', '2021-04-28 16:37:38', '2021-04-28 16:37:38'),
(2924, 187, 'Northern', '2021-04-28 16:37:38', '2021-04-28 16:37:38'),
(2925, 187, 'Southern', '2021-04-28 16:37:38', '2021-04-28 16:37:38'),
(2926, 187, 'Western', '2021-04-28 16:37:38', '2021-04-28 16:37:38'),
(2927, 189, 'Ascension', '2021-04-28 16:37:40', '2021-04-28 16:37:40'),
(2928, 189, 'Saint Helena', '2021-04-28 16:37:40', '2021-04-28 16:37:40'),
(2929, 189, 'Tristan da Cunha', '2021-04-28 16:37:40', '2021-04-28 16:37:40'),
(2930, 190, 'Christ Church Nichola Town', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2931, 190, 'Saint Anne Sandy Point', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2932, 190, 'Saint George Basseterre', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2933, 190, 'Saint George Gingerland', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2934, 190, 'Saint James Windward', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2935, 190, 'Saint John Capisterre', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2936, 190, 'Saint John Figtree', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2937, 190, 'Saint Mary Cayon', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2938, 190, 'Saint Paul Capisterre', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2939, 190, 'Saint Paul Charlestown', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2940, 190, 'Saint Peter Basseterre', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2941, 190, 'Saint Thomas Lowland', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2942, 190, 'Saint Thomas Middle Island', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2943, 190, 'Trinity Palmetto Point', '2021-04-28 16:37:41', '2021-04-28 16:37:41'),
(2944, 191, 'Anse-la-Raye', '2021-04-28 16:37:42', '2021-04-28 16:37:42'),
(2945, 191, 'Canaries', '2021-04-28 16:37:42', '2021-04-28 16:37:42'),
(2946, 191, 'Castries', '2021-04-28 16:37:42', '2021-04-28 16:37:42'),
(2947, 191, 'Choiseul', '2021-04-28 16:37:42', '2021-04-28 16:37:42'),
(2948, 191, 'Dennery', '2021-04-28 16:37:42', '2021-04-28 16:37:42'),
(2949, 191, 'Gros Islet', '2021-04-28 16:37:42', '2021-04-28 16:37:42'),
(2950, 191, 'Laborie', '2021-04-28 16:37:42', '2021-04-28 16:37:42'),
(2951, 191, 'Micoud', '2021-04-28 16:37:42', '2021-04-28 16:37:42'),
(2952, 191, 'Soufrière', '2021-04-28 16:37:42', '2021-04-28 16:37:42'),
(2953, 191, 'Vieux Fort', '2021-04-28 16:37:42', '2021-04-28 16:37:42'),
(2954, 193, 'Miquelon-Langlade', '2021-04-28 16:37:45', '2021-04-28 16:37:45'),
(2955, 193, 'Saint-Pierre', '2021-04-28 16:37:45', '2021-04-28 16:37:45'),
(2956, 194, 'Charlotte', '2021-04-28 16:37:45', '2021-04-28 16:37:45'),
(2957, 194, 'Grenadines', '2021-04-28 16:37:45', '2021-04-28 16:37:45'),
(2958, 194, 'Saint Andrew', '2021-04-28 16:37:46', '2021-04-28 16:37:46'),
(2959, 194, 'Saint David', '2021-04-28 16:37:46', '2021-04-28 16:37:46'),
(2960, 194, 'Saint George', '2021-04-28 16:37:46', '2021-04-28 16:37:46'),
(2961, 194, 'Saint Patrick', '2021-04-28 16:37:46', '2021-04-28 16:37:46'),
(2962, 195, 'Aiga-i-le-Tai', '2021-04-28 16:37:46', '2021-04-28 16:37:46'),
(2963, 195, 'Atua', '2021-04-28 16:37:47', '2021-04-28 16:37:47'),
(2964, 195, 'A`ana', '2021-04-28 16:37:47', '2021-04-28 16:37:47'),
(2965, 195, 'Fa`asaleleaga', '2021-04-28 16:37:47', '2021-04-28 16:37:47'),
(2966, 195, 'Gagaifomauga', '2021-04-28 16:37:47', '2021-04-28 16:37:47'),
(2967, 195, 'Gaga`emauga', '2021-04-28 16:37:47', '2021-04-28 16:37:47'),
(2968, 195, 'Palauli', '2021-04-28 16:37:47', '2021-04-28 16:37:47'),
(2969, 195, 'Satupa`itea', '2021-04-28 16:37:47', '2021-04-28 16:37:47'),
(2970, 195, 'Tuamasaga', '2021-04-28 16:37:47', '2021-04-28 16:37:47'),
(2971, 195, 'Vaisigano', '2021-04-28 16:37:47', '2021-04-28 16:37:47'),
(2972, 195, 'Va`a-o-Fonoti', '2021-04-28 16:37:47', '2021-04-28 16:37:47'),
(2973, 196, 'Acquaviva', '2021-04-28 16:37:48', '2021-04-28 16:37:48'),
(2974, 196, 'Borgo Maggiore', '2021-04-28 16:37:48', '2021-04-28 16:37:48'),
(2975, 196, 'Chiesanuova', '2021-04-28 16:37:48', '2021-04-28 16:37:48'),
(2976, 196, 'Domagnano', '2021-04-28 16:37:48', '2021-04-28 16:37:48'),
(2977, 196, 'Faetano', '2021-04-28 16:37:48', '2021-04-28 16:37:48'),
(2978, 196, 'Fiorentino', '2021-04-28 16:37:48', '2021-04-28 16:37:48'),
(2979, 196, 'Montegiardino', '2021-04-28 16:37:48', '2021-04-28 16:37:48'),
(2980, 196, 'San Marino', '2021-04-28 16:37:48', '2021-04-28 16:37:48'),
(2981, 196, 'Serravalle', '2021-04-28 16:37:48', '2021-04-28 16:37:48'),
(2982, 197, 'Príncipe', '2021-04-28 16:37:49', '2021-04-28 16:37:49'),
(2983, 197, 'São Tomé', '2021-04-28 16:37:49', '2021-04-28 16:37:49'),
(2984, 198, 'Al Ḥudūd ash Shamāliyah', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2985, 198, 'Al Bāhah', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2986, 198, 'Al Jawf', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2987, 198, 'Al Madīnah', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2988, 198, 'Al Qaṣīm', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2989, 198, 'Ar Riyāḍ', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2990, 198, 'Ash Sharqīyah', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2991, 198, 'Ḥā\'il', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2992, 198, 'Jīzan', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2993, 198, 'Makkah', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2994, 198, 'Najrān', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2995, 198, 'Tabūk', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2996, 198, '`Asīr', '2021-04-28 16:37:50', '2021-04-28 16:37:50'),
(2997, 199, 'Dakar', '2021-04-28 16:37:51', '2021-04-28 16:37:51'),
(2998, 199, 'Diourbel', '2021-04-28 16:37:51', '2021-04-28 16:37:51'),
(2999, 199, 'Fatick', '2021-04-28 16:37:51', '2021-04-28 16:37:51'),
(3000, 199, 'Kaolack', '2021-04-28 16:37:51', '2021-04-28 16:37:51'),
(3001, 199, 'Kolda', '2021-04-28 16:37:51', '2021-04-28 16:37:51'),
(3002, 199, 'Louga', '2021-04-28 16:37:51', '2021-04-28 16:37:51'),
(3003, 199, 'Matam', '2021-04-28 16:37:51', '2021-04-28 16:37:51'),
(3004, 199, 'Saint-Louis', '2021-04-28 16:37:51', '2021-04-28 16:37:51'),
(3005, 199, 'Tambacounda', '2021-04-28 16:37:51', '2021-04-28 16:37:51'),
(3006, 199, 'Thiès', '2021-04-28 16:37:51', '2021-04-28 16:37:51'),
(3007, 199, 'Ziguinchor', '2021-04-28 16:37:51', '2021-04-28 16:37:51'),
(3008, 200, 'Belgrade', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3009, 200, 'Bor', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3010, 200, 'Braničevo', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3011, 200, 'Central Banat', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3012, 200, 'Danube', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3013, 200, 'Jablanica', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3014, 200, 'Kolubara', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3015, 200, 'Kosovski', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3016, 200, 'Kosovsko-Mitrovački', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3017, 200, 'Kosovsko-Pomoravski', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3018, 200, 'Mačva', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3019, 200, 'Morava', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3020, 200, 'Nišava', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3021, 200, 'North Banat', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3022, 200, 'North Bačka', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3023, 200, 'Pećki', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3024, 200, 'Pirot', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3025, 200, 'Pomoravlje', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3026, 200, 'Prizrenski', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3027, 200, 'Pčinja', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3028, 200, 'Rasina', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3029, 200, 'Raška', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3030, 200, 'South Banat', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3031, 200, 'South Bačka', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3032, 200, 'Srem', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3033, 200, 'Toplica', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3034, 200, 'West Bačka', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3035, 200, 'Zaječar', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3036, 200, 'Zlatibor', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3037, 200, 'Šumadija', '2021-04-28 16:37:52', '2021-04-28 16:37:52'),
(3038, 201, 'Anse aux Pins', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3039, 201, 'Anse Étoile', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3040, 201, 'Anse Boileau', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3041, 201, 'Anse Royale', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3042, 201, 'Au Cap', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3043, 201, 'Baie Lazare', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3044, 201, 'Baie Sainte Anne', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3045, 201, 'Beau Vallon', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3046, 201, 'Bel Air', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3047, 201, 'Belombre', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3048, 201, 'Cascade', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3049, 201, 'English River', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3050, 201, 'Glacis', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3051, 201, 'Grand\' Anse (Mahé)', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3052, 201, 'Grand\' Anse (Praslin)', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3053, 201, 'La Digue and Inner Islands', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3054, 201, 'Les Mamelles', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3055, 201, 'Mont Buxton', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3056, 201, 'Mont Fleuri', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3057, 201, 'Outer Islands', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3058, 201, 'Plaisance', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3059, 201, 'Pointe La Rue', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3060, 201, 'Port Glaud', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3061, 201, 'Roche Caïman', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3062, 201, 'Saint Louis', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3063, 201, 'Takamaka', '2021-04-28 16:37:53', '2021-04-28 16:37:53'),
(3064, 202, 'Eastern', '2021-04-28 16:37:54', '2021-04-28 16:37:54'),
(3065, 202, 'Northern', '2021-04-28 16:37:54', '2021-04-28 16:37:54'),
(3066, 202, 'Southern', '2021-04-28 16:37:54', '2021-04-28 16:37:54'),
(3067, 202, 'Western', '2021-04-28 16:37:54', '2021-04-28 16:37:54'),
(3068, 203, 'Central Singapore', '2021-04-28 16:37:55', '2021-04-28 16:37:55'),
(3069, 203, 'North East', '2021-04-28 16:37:55', '2021-04-28 16:37:55'),
(3070, 203, 'North West', '2021-04-28 16:37:55', '2021-04-28 16:37:55'),
(3071, 203, 'South East', '2021-04-28 16:37:55', '2021-04-28 16:37:55'),
(3072, 203, 'South West', '2021-04-28 16:37:55', '2021-04-28 16:37:55'),
(3073, 205, 'Banskobystrický', '2021-04-28 16:37:57', '2021-04-28 16:37:57'),
(3074, 205, 'Bratislavský', '2021-04-28 16:37:57', '2021-04-28 16:37:57'),
(3075, 205, 'Košický', '2021-04-28 16:37:57', '2021-04-28 16:37:57'),
(3076, 205, 'Nitriansky', '2021-04-28 16:37:57', '2021-04-28 16:37:57'),
(3077, 205, 'Prešovský', '2021-04-28 16:37:57', '2021-04-28 16:37:57'),
(3078, 205, 'Trenčiansky', '2021-04-28 16:37:57', '2021-04-28 16:37:57'),
(3079, 205, 'Trnavský', '2021-04-28 16:37:57', '2021-04-28 16:37:57'),
(3080, 205, 'Žilinský', '2021-04-28 16:37:57', '2021-04-28 16:37:57'),
(3081, 206, 'DO', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3082, 206, 'GO', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3083, 206, 'Koroška', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3084, 206, 'Notranjsko-kraška', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3085, 206, 'Obalno-kraška', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3086, 206, 'Osrednjeslovenska', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3087, 206, 'Podravska', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3088, 206, 'Pomurska', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3089, 206, 'Savinjska', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3090, 206, 'SP', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3091, 206, 'Spodnjeposavska', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3092, 206, 'Zasavska', '2021-04-28 16:37:58', '2021-04-28 16:37:58'),
(3093, 207, 'Central', '2021-04-28 16:37:59', '2021-04-28 16:37:59'),
(3094, 207, 'Choiseul', '2021-04-28 16:37:59', '2021-04-28 16:37:59'),
(3095, 207, 'Guadalcanal', '2021-04-28 16:37:59', '2021-04-28 16:37:59'),
(3096, 207, 'Isabel', '2021-04-28 16:37:59', '2021-04-28 16:37:59'),
(3097, 207, 'Makira', '2021-04-28 16:37:59', '2021-04-28 16:37:59'),
(3098, 207, 'Malaita', '2021-04-28 16:37:59', '2021-04-28 16:37:59'),
(3099, 207, 'Rennell and Bellona', '2021-04-28 16:37:59', '2021-04-28 16:37:59'),
(3100, 207, 'Temotu', '2021-04-28 16:37:59', '2021-04-28 16:37:59'),
(3101, 207, 'Western', '2021-04-28 16:37:59', '2021-04-28 16:37:59'),
(3102, 208, 'Awdal', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3103, 208, 'Bakool', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3104, 208, 'Banaadir', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3105, 208, 'Bari', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3106, 208, 'Bay', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3107, 208, 'Galguduud', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3108, 208, 'Gedo', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3109, 208, 'Hiiraan', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3110, 208, 'Jubbada Dhexe', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3111, 208, 'Jubbada Hoose', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3112, 208, 'Mudug', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3113, 208, 'Nugaal', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3114, 208, 'Sanaag', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3115, 208, 'Shabeellaha Dhexe', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3116, 208, 'Shabeellaha Hoose', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3117, 208, 'Sool', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3118, 208, 'Togdheer', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3119, 208, 'Woqooyi Galbeed', '2021-04-28 16:38:01', '2021-04-28 16:38:01'),
(3120, 209, 'Eastern Cape', '2021-04-28 16:38:02', '2021-04-28 16:38:02'),
(3121, 209, 'Free State', '2021-04-28 16:38:02', '2021-04-28 16:38:02'),
(3122, 209, 'Gauteng', '2021-04-28 16:38:02', '2021-04-28 16:38:02'),
(3123, 209, 'KwaZulu-Natal', '2021-04-28 16:38:02', '2021-04-28 16:38:02'),
(3124, 209, 'Limpopo', '2021-04-28 16:38:02', '2021-04-28 16:38:02'),
(3125, 209, 'Mpumalanga', '2021-04-28 16:38:02', '2021-04-28 16:38:02'),
(3126, 209, 'North-West', '2021-04-28 16:38:02', '2021-04-28 16:38:02'),
(3127, 209, 'Northern Cape', '2021-04-28 16:38:02', '2021-04-28 16:38:02'),
(3128, 209, 'Western Cape', '2021-04-28 16:38:02', '2021-04-28 16:38:02'),
(3129, 211, 'Busan', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3130, 211, 'Chungcheongbuk-do', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3131, 211, 'Chungcheongnam-do', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3132, 211, 'Daegu', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3133, 211, 'Daejeon', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3134, 211, 'Gangwon-do', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3135, 211, 'Gwangju', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3136, 211, 'Gyeonggi-do', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3137, 211, 'Gyeongsangbuk-do', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3138, 211, 'Gyeongsangnam-do', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3139, 211, 'Incheon', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3140, 211, 'Jeju-do', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3141, 211, 'Jeollabuk-do', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3142, 211, 'Jeollanam-do', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3143, 211, 'Seoul', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3144, 211, 'Ulsan', '2021-04-28 16:38:04', '2021-04-28 16:38:04'),
(3145, 213, 'Andalusia', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3146, 213, 'Aragon', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3147, 213, 'Asturias', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3148, 213, 'Balearic Islands', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3149, 213, 'Basque Country', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3150, 213, 'Canary Islands', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3151, 213, 'Cantabria', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3152, 213, 'Castile and Leon', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3153, 213, 'Castile-La Mancha', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3154, 213, 'Catalonia', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3155, 213, 'Ceuta', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3156, 213, 'Extremadura', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3157, 213, 'Galicia', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3158, 213, 'La Rioja', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3159, 213, 'Madrid', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3160, 213, 'Melilla', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3161, 213, 'Murcia', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3162, 213, 'Navarra', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3163, 213, 'Valencia', '2021-04-28 16:38:06', '2021-04-28 16:38:06'),
(3164, 214, 'Ampara', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3165, 214, 'Anuradhapura', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3166, 214, 'Badulla', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3167, 214, 'Batticaloa', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3168, 214, 'Colombo', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3169, 214, 'Galle', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3170, 214, 'Gampaha', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3171, 214, 'Hambantota', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3172, 214, 'Jaffna', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3173, 214, 'Kalutara', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3174, 214, 'Kandy', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3175, 214, 'Kegalle', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3176, 214, 'Kilinochchi', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3177, 214, 'Kurunegala', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3178, 214, 'Mannar', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3179, 214, 'Matale', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3180, 214, 'Matara', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3181, 214, 'Moneragala', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3182, 214, 'Mullaitivu', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3183, 214, 'Nuwara Eliya', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3184, 214, 'Polonnaruwa', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3185, 214, 'Puttalam', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3186, 214, 'Ratnapura', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3187, 214, 'Trincomalee', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3188, 214, 'Vavuniya', '2021-04-28 16:38:07', '2021-04-28 16:38:07'),
(3189, 215, 'Blue Nile', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3190, 215, 'Central Equatoria', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3191, 215, 'East Equatoria', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3192, 215, 'Gedarif', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3193, 215, 'Gezira', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3194, 215, 'Jungoli', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3195, 215, 'Kassala', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3196, 215, 'Khartoum', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3197, 215, 'Lakes', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3198, 215, 'North Bahr-al-Ghazal', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3199, 215, 'North Darfur', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3200, 215, 'North Kordufan', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3201, 215, 'Northern', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3202, 215, 'Red Sea', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3203, 215, 'River Nile', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3204, 215, 'Sennar', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3205, 215, 'South Darfur', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3206, 215, 'South Kordufan', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3207, 215, 'Unity', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3208, 215, 'Upper Nile', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3209, 215, 'Warap', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3210, 215, 'West Bahr-al-Ghazal', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3211, 215, 'West Darfur', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3212, 215, 'West Equatoria', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3213, 215, 'West Kordufan', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3214, 215, 'White Nile', '2021-04-28 16:38:08', '2021-04-28 16:38:08'),
(3215, 216, 'Brokopondo', '2021-04-28 16:38:14', '2021-04-28 16:38:14'),
(3216, 216, 'Commewijne', '2021-04-28 16:38:14', '2021-04-28 16:38:14'),
(3217, 216, 'Coronie', '2021-04-28 16:38:14', '2021-04-28 16:38:14'),
(3218, 216, 'Marowijne', '2021-04-28 16:38:14', '2021-04-28 16:38:14'),
(3219, 216, 'Nickerie', '2021-04-28 16:38:14', '2021-04-28 16:38:14'),
(3220, 216, 'Para', '2021-04-28 16:38:14', '2021-04-28 16:38:14'),
(3221, 216, 'Paramaribo', '2021-04-28 16:38:14', '2021-04-28 16:38:14'),
(3222, 216, 'Saramacca', '2021-04-28 16:38:14', '2021-04-28 16:38:14'),
(3223, 216, 'Sipaliwini', '2021-04-28 16:38:14', '2021-04-28 16:38:14'),
(3224, 216, 'Wanica', '2021-04-28 16:38:14', '2021-04-28 16:38:14'),
(3225, 217, 'Jan Mayen', '2021-04-28 16:38:15', '2021-04-28 16:38:15'),
(3226, 217, 'Svalbard', '2021-04-28 16:38:15', '2021-04-28 16:38:15'),
(3227, 218, 'Hhohho', '2021-04-28 16:38:16', '2021-04-28 16:38:16'),
(3228, 218, 'Lubombo', '2021-04-28 16:38:16', '2021-04-28 16:38:16'),
(3229, 218, 'Manzini', '2021-04-28 16:38:16', '2021-04-28 16:38:16'),
(3230, 218, 'Shiselweni', '2021-04-28 16:38:16', '2021-04-28 16:38:16'),
(3231, 219, 'Örebro', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3232, 219, 'Östergötland', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3233, 219, 'Blekinge', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3234, 219, 'Dalarna', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3235, 219, 'Gävleborg', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3236, 219, 'Gotland', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3237, 219, 'Halland', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3238, 219, 'Jämtland', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3239, 219, 'Jönköping', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3240, 219, 'Kalmar', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3241, 219, 'Kronoberg', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3242, 219, 'Norrbotten', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3243, 219, 'Södermanland', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3244, 219, 'Skåne', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3245, 219, 'Stockholm', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3246, 219, 'Uppsala', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3247, 219, 'Värmland', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3248, 219, 'Västerbotten', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3249, 219, 'Västernorrland', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3250, 219, 'Västmanland', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3251, 219, 'Västra Götaland', '2021-04-28 16:38:18', '2021-04-28 16:38:18'),
(3252, 220, 'Aargau (Argovia)', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3253, 220, 'Appenzell Ausserrhoden (Outer Rh', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3254, 220, 'Appenzell Innerrhoden (Inner Rho', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3255, 220, 'Basel-Land (Basle-Country)', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3256, 220, 'Basel-Stadt (Basle-City)', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3257, 220, 'Berne (Bern)', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3258, 220, 'Fribourg', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3259, 220, 'Geneva', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3260, 220, 'Glarus', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3261, 220, 'Graubünden (Grisons)', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3262, 220, 'Jura', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3263, 220, 'Lucerne', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3264, 220, 'Neuchâtel', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3265, 220, 'Nidwalden (Nidwald)', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3266, 220, 'Obwalden (Obwald)', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3267, 220, 'Schaffhausen', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3268, 220, 'Schwyz', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3269, 220, 'Solothurn', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3270, 220, 'St. Gallen (St. Gall)', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3271, 220, 'Thurgau (Thurgovia)', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3272, 220, 'Ticino', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3273, 220, 'Uri', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3274, 220, 'Valais', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3275, 220, 'Vaud', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3276, 220, 'Zürich (Zurich)', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3277, 220, 'Zug', '2021-04-28 16:38:19', '2021-04-28 16:38:19'),
(3278, 221, 'Al Ḥasakah', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3279, 221, 'Aleppo', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3280, 221, 'Ar Raqqah', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3281, 221, 'As Suwaydā\'', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3282, 221, 'Ḥimṣ', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3283, 221, 'Damascus', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3284, 221, 'Dar`ā', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3285, 221, 'Dayr az Zawr', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3286, 221, 'Hama', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3287, 221, 'Idlib', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3288, 221, 'Latakia', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3289, 221, 'Quneitra', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3290, 221, 'Rif Dimashq', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3291, 221, 'Ţarţūs', '2021-04-28 16:38:20', '2021-04-28 16:38:20'),
(3292, 222, 'Fukien', '2021-04-28 16:38:21', '2021-04-28 16:38:21'),
(3293, 222, 'Kaohsiung', '2021-04-28 16:38:21', '2021-04-28 16:38:21'),
(3294, 222, 'Taipei', '2021-04-28 16:38:21', '2021-04-28 16:38:21'),
(3295, 222, 'Taiwan', '2021-04-28 16:38:21', '2021-04-28 16:38:21'),
(3296, 223, 'Badakhshoni Kuni', '2021-04-28 16:38:23', '2021-04-28 16:38:23'),
(3297, 223, 'Dushanbe', '2021-04-28 16:38:23', '2021-04-28 16:38:23'),
(3298, 223, 'Khatlon', '2021-04-28 16:38:23', '2021-04-28 16:38:23'),
(3299, 223, 'Regions of Republican Subordinat', '2021-04-28 16:38:23', '2021-04-28 16:38:23'),
(3300, 223, 'Sogd', '2021-04-28 16:38:23', '2021-04-28 16:38:23'),
(3301, 224, 'Arusha', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3302, 224, 'Dar es Salaam', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3303, 224, 'Dodoma', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3304, 224, 'Iringa', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3305, 224, 'Kagera', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3306, 224, 'Kigoma', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3307, 224, 'Kilimanjaro', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3308, 224, 'Lindi', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3309, 224, 'Manyara', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3310, 224, 'Mara', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3311, 224, 'Mbeya', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3312, 224, 'Morogoro', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3313, 224, 'Mtwara', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3314, 224, 'Mwanza', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3315, 224, 'Pemba North', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3316, 224, 'Pemba South', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3317, 224, 'Pwani', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3318, 224, 'Rukwa', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3319, 224, 'Ruvuma', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3320, 224, 'Shinyanga', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3321, 224, 'Singida', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3322, 224, 'Tabora', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3323, 224, 'Tanga', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3324, 224, 'Zanzibar North', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3325, 224, 'Zanzibar South and Central', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3326, 224, 'Zanzibar West', '2021-04-28 16:38:24', '2021-04-28 16:38:24'),
(3327, 225, 'Amnat Charoen', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3328, 225, 'Ang Thong', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3329, 225, 'Bangkok Metropolis', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3330, 225, 'Buri Ram', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3331, 225, 'Chachoengsao', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3332, 225, 'Chai Nat', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3333, 225, 'Chaiyaphum', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3334, 225, 'Chanthaburi', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3335, 225, 'Chiang Mai', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3336, 225, 'Chiang Rai', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3337, 225, 'Chon Buri', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3338, 225, 'Chumphon', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3339, 225, 'Kalasin', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3340, 225, 'Kamphaeng Phet', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3341, 225, 'Kanchanaburi', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3342, 225, 'Khon Kaen', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3343, 225, 'Krabi', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3344, 225, 'Lampang', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3345, 225, 'Lamphun', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3346, 225, 'Loei', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3347, 225, 'Lop Buri', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3348, 225, 'Mae Hong Son', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3349, 225, 'Maha Sarakham', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3350, 225, 'Mukdahan', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3351, 225, 'Nakhon Nayok', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3352, 225, 'Nakhon Pathom', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3353, 225, 'Nakhon Phanom', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3354, 225, 'Nakhon Ratchasima', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3355, 225, 'Nakhon Sawan', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3356, 225, 'Nakhon Si Thammarat', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3357, 225, 'Nan', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3358, 225, 'Narathiwat', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3359, 225, 'Nong Bua Lam Phu', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3360, 225, 'Nong Khai', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3361, 225, 'Nonthaburi', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3362, 225, 'Pathum Thani', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3363, 225, 'Pattani', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3364, 225, 'Phangnga', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3365, 225, 'Phatthalung', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3366, 225, 'Phayao', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3367, 225, 'Phetchabun', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3368, 225, 'Phetchaburi', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3369, 225, 'Phichit', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3370, 225, 'Phitsanulok', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3371, 225, 'Phra Nakhon Si Ayutthaya', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3372, 225, 'Phrae', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3373, 225, 'Phuket', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3374, 225, 'Prachin Buri', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3375, 225, 'Prachuap Khiri Khan', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3376, 225, 'Ranong', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3377, 225, 'Ratchaburi', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3378, 225, 'Rayong', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3379, 225, 'Roi Et', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3380, 225, 'Sa Kaeo', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3381, 225, 'Sakon Nakhon', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3382, 225, 'Samut Prakan', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3383, 225, 'Samut Sakhon', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3384, 225, 'Samut Songkhram', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3385, 225, 'Saraburi', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3386, 225, 'Satun', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3387, 225, 'Si Sa Ket', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3388, 225, 'Sing Buri', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3389, 225, 'Songkhla', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3390, 225, 'Sukhothai', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3391, 225, 'Suphan Buri', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3392, 225, 'Surat Thani', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3393, 225, 'Surin', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3394, 225, 'Tak', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3395, 225, 'Trang', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3396, 225, 'Trat', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3397, 225, 'Ubon Ratchathani', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3398, 225, 'Udon Thani', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3399, 225, 'Uthai Thani', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3400, 225, 'Uttaradit', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3401, 225, 'Yala', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3402, 225, 'Yasothon', '2021-04-28 16:38:25', '2021-04-28 16:38:25'),
(3403, 226, 'Aileu', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3404, 226, 'Ainaro', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3405, 226, 'Ambeno', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3406, 226, 'Baucau', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3407, 226, 'Bobonaro', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3408, 226, 'Cova Lima', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3409, 226, 'Dili', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3410, 226, 'Ermera', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3411, 226, 'Lautem', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3412, 226, 'Liquiça', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3413, 226, 'Manatuto', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3414, 226, 'Manufahi', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3415, 226, 'Viqueque', '2021-04-28 16:38:26', '2021-04-28 16:38:26'),
(3416, 227, 'Centre', '2021-04-28 16:38:27', '2021-04-28 16:38:27'),
(3417, 227, 'Kara', '2021-04-28 16:38:27', '2021-04-28 16:38:27'),
(3418, 227, 'Maritime', '2021-04-28 16:38:27', '2021-04-28 16:38:27'),
(3419, 227, 'Plateaux', '2021-04-28 16:38:27', '2021-04-28 16:38:27'),
(3420, 227, 'Savanes', '2021-04-28 16:38:27', '2021-04-28 16:38:27'),
(3421, 228, 'Atafu', '2021-04-28 16:38:28', '2021-04-28 16:38:28'),
(3422, 228, 'Fakaofo', '2021-04-28 16:38:28', '2021-04-28 16:38:28'),
(3423, 228, 'Nukunonu', '2021-04-28 16:38:28', '2021-04-28 16:38:28'),
(3424, 229, 'Eua', '2021-04-28 16:38:29', '2021-04-28 16:38:29'),
(3425, 229, 'Ha\'apai', '2021-04-28 16:38:29', '2021-04-28 16:38:29'),
(3426, 229, 'Niuas', '2021-04-28 16:38:29', '2021-04-28 16:38:29'),
(3427, 229, 'Tongatapu', '2021-04-28 16:38:29', '2021-04-28 16:38:29'),
(3428, 229, 'Vava\'u', '2021-04-28 16:38:29', '2021-04-28 16:38:29'),
(3429, 230, 'Arima', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3430, 230, 'Chaguanas', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3431, 230, 'Couva/Tabaquite/Talparo', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3432, 230, 'Diego Martin', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3433, 230, 'Eastern Tobago', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3434, 230, 'Mayaro/Rio Claro', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3435, 230, 'Penal/Debe', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3436, 230, 'Point Fortin', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3437, 230, 'Port of Spain', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3438, 230, 'Princes Town', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3439, 230, 'San Fernando', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3440, 230, 'San Juan/Laventille', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3441, 230, 'Sangre Grande', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3442, 230, 'Siparia', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3443, 230, 'Tunapuna/Piarco', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3444, 230, 'Western Tobago', '2021-04-28 16:38:31', '2021-04-28 16:38:31'),
(3445, 231, 'Ariana', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3446, 231, 'Béja', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3447, 231, 'Ben Arous', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3448, 231, 'Bizerte', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3449, 231, 'Gabès', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3450, 231, 'Gafsa', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3451, 231, 'Jendouba', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3452, 231, 'Kairouan', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3453, 231, 'Kassérine', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3454, 231, 'Kebili', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3455, 231, 'Le Kef', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3456, 231, 'Mahdia', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3457, 231, 'Manouba', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3458, 231, 'Médenine', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3459, 231, 'Monastir', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3460, 231, 'Nabeul', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3461, 231, 'Sfax', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3462, 231, 'Sidi Bou Zid', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3463, 231, 'Siliana', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3464, 231, 'Sousse', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3465, 231, 'Tataouine', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3466, 231, 'Tozeur', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3467, 231, 'Tunis', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3468, 231, 'Zaghouan', '2021-04-28 16:38:32', '2021-04-28 16:38:32'),
(3469, 232, 'Adana', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3470, 232, 'Adıyaman', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3471, 232, 'Afyon', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3472, 232, 'Aksaray', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3473, 232, 'Amasya', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3474, 232, 'Ankara', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3475, 232, 'Antalya', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3476, 232, 'Ardahan', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3477, 232, 'Artvin', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3478, 232, 'Aydın', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3479, 232, 'Ağri', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3480, 232, 'Çanakkale', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3481, 232, 'Çankırı', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3482, 232, 'Çorum', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3483, 232, 'Balıkesir', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3484, 232, 'Bartın', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3485, 232, 'Batman', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3486, 232, 'Bayburt', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3487, 232, 'Bilecik', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3488, 232, 'Bingöl', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3489, 232, 'Bitlis', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3490, 232, 'Bolu', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3491, 232, 'Burdur', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3492, 232, 'Bursa', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3493, 232, 'Düzce', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3494, 232, 'Denizli', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3495, 232, 'Diyarbakır', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3496, 232, 'Edirne', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3497, 232, 'Elazığ', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3498, 232, 'Erzincan', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3499, 232, 'Erzurum', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3500, 232, 'Eskişehir', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3501, 232, 'Gaziantep', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3502, 232, 'Gümüşhane', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3503, 232, 'Giresun', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3504, 232, 'Hakkari', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3505, 232, 'Hatay', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3506, 232, 'Isparta', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3507, 232, 'Iğdir', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3508, 232, 'Kahramanmaraş', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3509, 232, 'Karabük', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3510, 232, 'Karaman', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3511, 232, 'Kars', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3512, 232, 'Kastamonu', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3513, 232, 'Kayseri', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3514, 232, 'Kütahya', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3515, 232, 'Kilis', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3516, 232, 'Kocaeli', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3517, 232, 'Konya', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3518, 232, 'Kırklareli', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3519, 232, 'Kırşehir', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3520, 232, 'Kırıkkale', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3521, 232, 'Malatya', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3522, 232, 'Manisa', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3523, 232, 'Mardin', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3524, 232, 'Mersin', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3525, 232, 'Muş', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3526, 232, 'Muğla', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3527, 232, 'Nevşehir', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3528, 232, 'Niğde', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3529, 232, 'Ordu', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3530, 232, 'Osmaniye', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3531, 232, 'Rize', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3532, 232, 'Sakarya', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3533, 232, 'Samsun', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3534, 232, 'Siirt', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3535, 232, 'Sinop', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3536, 232, 'Sivas', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3537, 232, 'Tekirdağ', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3538, 232, 'Tokat', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3539, 232, 'Trabzon', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3540, 232, 'Tunceli', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3541, 232, 'Uşak', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3542, 232, 'Van', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3543, 232, 'Yalova', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3544, 232, 'Yozgat', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3545, 232, 'Zonguldak', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3546, 232, 'Şanlıurfa', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3547, 232, 'Şırnak', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3548, 232, 'İstanbul', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3549, 232, 'İzmir', '2021-04-28 16:38:33', '2021-04-28 16:38:33'),
(3550, 233, 'Ahal', '2021-04-28 16:38:34', '2021-04-28 16:38:34'),
(3551, 233, 'Ashgabat', '2021-04-28 16:38:34', '2021-04-28 16:38:34'),
(3552, 233, 'Balkan', '2021-04-28 16:38:34', '2021-04-28 16:38:34'),
(3553, 233, 'Dashoguz', '2021-04-28 16:38:34', '2021-04-28 16:38:34'),
(3554, 233, 'Lebap', '2021-04-28 16:38:34', '2021-04-28 16:38:34'),
(3555, 233, 'Mary', '2021-04-28 16:38:34', '2021-04-28 16:38:34'),
(3556, 234, 'Turks and Caicos Islands', '2021-04-28 16:38:35', '2021-04-28 16:38:35'),
(3557, 235, 'Funafuti', '2021-04-28 16:38:36', '2021-04-28 16:38:36'),
(3558, 235, 'Nanumanga', '2021-04-28 16:38:36', '2021-04-28 16:38:36'),
(3559, 235, 'Nanumea', '2021-04-28 16:38:36', '2021-04-28 16:38:36'),
(3560, 235, 'Niutao', '2021-04-28 16:38:36', '2021-04-28 16:38:36'),
(3561, 235, 'Nui', '2021-04-28 16:38:36', '2021-04-28 16:38:36'),
(3562, 235, 'Nukufetau', '2021-04-28 16:38:36', '2021-04-28 16:38:36'),
(3563, 235, 'Nukulaelae', '2021-04-28 16:38:36', '2021-04-28 16:38:36'),
(3564, 235, 'Vaitupu', '2021-04-28 16:38:36', '2021-04-28 16:38:36'),
(3565, 236, 'Abim', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3566, 236, 'Adjumani', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3567, 236, 'Amolatar', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3568, 236, 'Amuria', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3569, 236, 'Amuru', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3570, 236, 'Apac', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3571, 236, 'Arua', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3572, 236, 'Budaka', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3573, 236, 'Bugiri', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3574, 236, 'Bukwa', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3575, 236, 'Bulisa', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3576, 236, 'Bundibugyo', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3577, 236, 'Bushenyi', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3578, 236, 'Busia', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3579, 236, 'Busiki', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3580, 236, 'Butaleja', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3581, 236, 'Dokolo', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3582, 236, 'Gulu', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3583, 236, 'Hoima', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3584, 236, 'Ibanda', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3585, 236, 'Iganga', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3586, 236, 'Jinja', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3587, 236, 'Kaabong', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3588, 236, 'Kabale', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3589, 236, 'Kabarole', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3590, 236, 'Kaberamaido', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3591, 236, 'Kabingo', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3592, 236, 'Kalangala', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3593, 236, 'Kaliro', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3594, 236, 'Kampala', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3595, 236, 'Kamuli', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3596, 236, 'Kamwenge', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3597, 236, 'Kanungu', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3598, 236, 'Kapchorwa', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3599, 236, 'Kasese', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3600, 236, 'Katakwi', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3601, 236, 'Kayunga', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3602, 236, 'Kibale', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3603, 236, 'Kiboga', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3604, 236, 'Kiruhura', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3605, 236, 'Kisoro', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3606, 236, 'Kitgum', '2021-04-28 16:38:37', '2021-04-28 16:38:37');
INSERT INTO `states` (`id`, `country_id`, `name`, `created_at`, `updated_at`) VALUES
(3607, 236, 'Koboko', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3608, 236, 'Kotido', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3609, 236, 'Kumi', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3610, 236, 'Kyenjojo', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3611, 236, 'Lira', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3612, 236, 'Luwero', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3613, 236, 'Manafwa', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3614, 236, 'Maracha', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3615, 236, 'Masaka', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3616, 236, 'Masindi', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3617, 236, 'Mayuge', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3618, 236, 'Mbale', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3619, 236, 'Mbarara', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3620, 236, 'Mityana', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3621, 236, 'Moroto', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3622, 236, 'Moyo', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3623, 236, 'Mpigi', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3624, 236, 'Mubende', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3625, 236, 'Mukono', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3626, 236, 'Nakapiripirit', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3627, 236, 'Nakaseke', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3628, 236, 'Nakasongola', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3629, 236, 'Nebbi', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3630, 236, 'Ntungamo', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3631, 236, 'Oyam', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3632, 236, 'Pader', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3633, 236, 'Pallisa', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3634, 236, 'Rakai', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3635, 236, 'Rukungiri', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3636, 236, 'Sembabule', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3637, 236, 'Sironko', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3638, 236, 'Soroti', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3639, 236, 'Tororo', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3640, 236, 'Wakiso', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3641, 236, 'Yumbe', '2021-04-28 16:38:37', '2021-04-28 16:38:37'),
(3642, 237, 'Cherkasy', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3643, 237, 'Chernihiv', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3644, 237, 'Chernivtsi', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3645, 237, 'Crimea', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3646, 237, 'Dnipropetrovs\'k', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3647, 237, 'Donets\'k', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3648, 237, 'Ivano-Frankivs\'k', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3649, 237, 'Kharkiv', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3650, 237, 'Kherson', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3651, 237, 'Khmel\'nyts\'kyy', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3652, 237, 'Kiev', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3653, 237, 'Kiev City', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3654, 237, 'Kirovohrad', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3655, 237, 'L\'viv', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3656, 237, 'Luhans\'k', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3657, 237, 'Mykolayiv', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3658, 237, 'Odessa', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3659, 237, 'Poltava', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3660, 237, 'Rivne', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3661, 237, 'Sevastopol\' City', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3662, 237, 'Sumy', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3663, 237, 'Ternopil\'', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3664, 237, 'Transcarpathia', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3665, 237, 'Vinnytsya', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3666, 237, 'Volyn', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3667, 237, 'Zaporizhzhya', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3668, 237, 'Zhytomyr', '2021-04-28 16:38:39', '2021-04-28 16:38:39'),
(3669, 238, 'Abu Dhabi', '2021-04-28 16:38:40', '2021-04-28 16:38:40'),
(3670, 238, 'Ajman', '2021-04-28 16:38:40', '2021-04-28 16:38:40'),
(3671, 238, 'Dubay', '2021-04-28 16:38:40', '2021-04-28 16:38:40'),
(3672, 238, 'Fujayrah', '2021-04-28 16:38:40', '2021-04-28 16:38:40'),
(3673, 238, 'Ras al Khaymah', '2021-04-28 16:38:40', '2021-04-28 16:38:40'),
(3674, 238, 'Sharjah', '2021-04-28 16:38:40', '2021-04-28 16:38:40'),
(3675, 238, 'Umm al Qaywayn', '2021-04-28 16:38:40', '2021-04-28 16:38:40'),
(3676, 239, 'Aberdeen', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3677, 239, 'Aberdeenshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3678, 239, 'Anglesey', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3679, 239, 'Angus', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3680, 239, 'Antrim', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3681, 239, 'Ards', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3682, 239, 'Argyll and Bute', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3683, 239, 'Armagh', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3684, 239, 'Ballymena', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3685, 239, 'Ballymoney', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3686, 239, 'Banbridge', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3687, 239, 'Barnsley', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3688, 239, 'Bath and North East Somerset', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3689, 239, 'Bedfordshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3690, 239, 'Belfast', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3691, 239, 'Birmingham', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3692, 239, 'Blackburn with Darwen', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3693, 239, 'Blackpool', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3694, 239, 'Blaenau Gwent', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3695, 239, 'Bolton', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3696, 239, 'Bournemouth', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3697, 239, 'Bracknell Forest', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3698, 239, 'Bradford', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3699, 239, 'Bridgend', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3700, 239, 'Brighton and Hove', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3701, 239, 'Bristol', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3702, 239, 'Buckinghamshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3703, 239, 'Bury', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3704, 239, 'Caerphilly', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3705, 239, 'Calderdale', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3706, 239, 'Cambridgeshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3707, 239, 'Cardiff', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3708, 239, 'Carmarthenshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3709, 239, 'Carrickfergus', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3710, 239, 'Castlereagh', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3711, 239, 'Ceredigion', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3712, 239, 'Cheshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3713, 239, 'Clackmannanshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3714, 239, 'Coleraine', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3715, 239, 'Conwy', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3716, 239, 'Cookstown', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3717, 239, 'Cornwall', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3718, 239, 'Coventry', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3719, 239, 'Craigavon', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3720, 239, 'Cumbria', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3721, 239, 'Darlington', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3722, 239, 'Denbighshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3723, 239, 'Derby', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3724, 239, 'Derbyshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3725, 239, 'Derry', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3726, 239, 'Devon', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3727, 239, 'Doncaster', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3728, 239, 'Dorset', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3729, 239, 'Down', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3730, 239, 'Dudley', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3731, 239, 'Dumfries and Galloway', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3732, 239, 'Dundee', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3733, 239, 'Dungannon', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3734, 239, 'Durham', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3735, 239, 'East Ayrshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3736, 239, 'East Dunbartonshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3737, 239, 'East Lothian', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3738, 239, 'East Renfrewshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3739, 239, 'East Riding of Yorkshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3740, 239, 'East Sussex', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3741, 239, 'Edinburgh', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3742, 239, 'Eilean Siar', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3743, 239, 'Essex', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3744, 239, 'Falkirk', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3745, 239, 'Fermanagh', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3746, 239, 'Fife', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3747, 239, 'Flintshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3748, 239, 'Gateshead', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3749, 239, 'Glasgow', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3750, 239, 'Gloucestershire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3751, 239, 'Greater London', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3752, 239, 'Gwynedd', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3753, 239, 'Halton', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3754, 239, 'Hampshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3755, 239, 'Hartlepool', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3756, 239, 'Herefordshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3757, 239, 'Hertfordshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3758, 239, 'Highland', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3759, 239, 'Inverclyde', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3760, 239, 'Isle of Wight', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3761, 239, 'Isles of Scilly', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3762, 239, 'Kent', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3763, 239, 'Kingston upon Hull', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3764, 239, 'Kirklees', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3765, 239, 'Knowsley', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3766, 239, 'Lancashire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3767, 239, 'Larne', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3768, 239, 'Leeds', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3769, 239, 'Leicester', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3770, 239, 'Leicestershire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3771, 239, 'Limavady', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3772, 239, 'Lincolnshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3773, 239, 'Lisburn', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3774, 239, 'Liverpool', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3775, 239, 'Luton', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3776, 239, 'Magherafelt', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3777, 239, 'Manchester', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3778, 239, 'Medway', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3779, 239, 'Merthyr Tydfil', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3780, 239, 'Middlesbrough', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3781, 239, 'Midlothian', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3782, 239, 'Milton Keynes', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3783, 239, 'Monmouthshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3784, 239, 'Moray', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3785, 239, 'Moyle', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3786, 239, 'Neath Port Talbot', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3787, 239, 'Newcastle upon Tyne', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3788, 239, 'Newport', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3789, 239, 'Newry and Mourne', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3790, 239, 'Newtownabbey', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3791, 239, 'Norfolk', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3792, 239, 'North Ayrshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3793, 239, 'North Down', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3794, 239, 'North East Lincolnshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3795, 239, 'North Lanarkshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3796, 239, 'North Lincolnshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3797, 239, 'North Somerset', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3798, 239, 'North Tyneside', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3799, 239, 'North Yorkshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3800, 239, 'Northamptonshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3801, 239, 'Northumberland', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3802, 239, 'Nottingham', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3803, 239, 'Nottinghamshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3804, 239, 'Oldham', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3805, 239, 'Omagh', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3806, 239, 'Orkney Islands', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3807, 239, 'Oxfordshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3808, 239, 'Pembrokeshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3809, 239, 'Perthshire and Kinross', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3810, 239, 'Peterborough', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3811, 239, 'Plymouth', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3812, 239, 'Poole', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3813, 239, 'Portsmouth', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3814, 239, 'Powys', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3815, 239, 'Reading', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3816, 239, 'Redcar and Cleveland', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3817, 239, 'Renfrewshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3818, 239, 'Rhondda, Cynon, Taff', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3819, 239, 'Rochdale', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3820, 239, 'Rotherham', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3821, 239, 'Rutland', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3822, 239, 'Saint Helens', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3823, 239, 'Salford', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3824, 239, 'Sandwell', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3825, 239, 'Scottish Borders', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3826, 239, 'Sefton', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3827, 239, 'Sheffield', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3828, 239, 'Shetland Islands', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3829, 239, 'Shropshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3830, 239, 'Slough', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3831, 239, 'Solihull', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3832, 239, 'Somerset', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3833, 239, 'South Ayrshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3834, 239, 'South Gloucestershire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3835, 239, 'South Lanarkshire', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3836, 239, 'South Tyneside', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3837, 239, 'Southampton', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3838, 239, 'Southend-on-Sea', '2021-04-28 16:38:41', '2021-04-28 16:38:41'),
(3839, 239, 'Staffordshire', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3840, 239, 'Stirling', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3841, 239, 'Stockport', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3842, 239, 'Stockton-on-Tees', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3843, 239, 'Stoke-on-Trent', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3844, 239, 'Strabane', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3845, 239, 'Suffolk', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3846, 239, 'Sunderland', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3847, 239, 'Surrey', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3848, 239, 'Swansea', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3849, 239, 'Swindon', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3850, 239, 'Tameside', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3851, 239, 'Telford and Wrekin', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3852, 239, 'Thurrock', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3853, 239, 'Torbay', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3854, 239, 'Torfaen', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3855, 239, 'Trafford', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3856, 239, 'Vale of Glamorgan', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3857, 239, 'Wakefield', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3858, 239, 'Walsall', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3859, 239, 'Warrington', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3860, 239, 'Warwickshire', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3861, 239, 'West Berkshire', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3862, 239, 'West Dunbartonshire', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3863, 239, 'West Lothian', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3864, 239, 'West Sussex', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3865, 239, 'Wigan', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3866, 239, 'Wiltshire', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3867, 239, 'Windsor and Maidenhead', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3868, 239, 'Wirral', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3869, 239, 'Wokingham', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3870, 239, 'Wolverhampton', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3871, 239, 'Worcestershire', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3872, 239, 'Wrexham', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3873, 239, 'York', '2021-04-28 16:38:42', '2021-04-28 16:38:42'),
(3874, 240, 'Alabama', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3875, 240, 'Alaska', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3876, 240, 'Arizona', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3877, 240, 'Arkansas', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3878, 240, 'California', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3879, 240, 'Colorado', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3880, 240, 'Connecticut', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3881, 240, 'Delaware', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3882, 240, 'Dist of Columbia', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3883, 240, 'Florida', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3884, 240, 'Georgia', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3885, 240, 'Hawaii', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3886, 240, 'Idaho', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3887, 240, 'Illinois', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3888, 240, 'Indiana', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3889, 240, 'Iowa', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3890, 240, 'Kansas', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3891, 240, 'Kentucky', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3892, 240, 'Louisiana', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3893, 240, 'Maine', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3894, 240, 'Maryland', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3895, 240, 'Massachusetts', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3896, 240, 'Michigan', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3897, 240, 'Minnesota', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3898, 240, 'Mississippi', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3899, 240, 'Missouri', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3900, 240, 'Montana', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3901, 240, 'Nebraska', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3902, 240, 'Nevada', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3903, 240, 'New Hampshire', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3904, 240, 'New Jersey', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3905, 240, 'New Mexico', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3906, 240, 'New York', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3907, 240, 'North Carolina', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3908, 240, 'North Dakota', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3909, 240, 'Ohio', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3910, 240, 'Oklahoma', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3911, 240, 'Oregon', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3912, 240, 'Pennsylvania', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3913, 240, 'Rhode Island', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3914, 240, 'South Carolina', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3915, 240, 'South Dakota', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3916, 240, 'Tennessee', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3917, 240, 'Texas', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3918, 240, 'Utah', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3919, 240, 'Vermont', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3920, 240, 'Virginia', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3921, 240, 'Washington', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3922, 240, 'West Virginia', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3923, 240, 'Wisconsin', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3924, 240, 'Wyoming', '2021-04-28 16:38:43', '2021-04-28 16:38:43'),
(3925, 241, 'Artigas', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3926, 241, 'Canelones', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3927, 241, 'Cerro Largo', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3928, 241, 'Colonia', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3929, 241, 'Durazno', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3930, 241, 'Flores', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3931, 241, 'Florida', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3932, 241, 'Lavalleja', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3933, 241, 'Maldonado', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3934, 241, 'Montevideo', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3935, 241, 'Paysandú', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3936, 241, 'Río Negro', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3937, 241, 'Rivera', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3938, 241, 'Rocha', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3939, 241, 'Salto', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3940, 241, 'San José', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3941, 241, 'Soriano', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3942, 241, 'Tacuarembó', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3943, 241, 'Treinta y Tres', '2021-04-28 16:38:44', '2021-04-28 16:38:44'),
(3944, 242, 'Andijon', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3945, 242, 'Buxoro', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3946, 242, 'Farg`ona', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3947, 242, 'Jizzax', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3948, 242, 'Karakalpakstan', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3949, 242, 'Kashkadarya', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3950, 242, 'Namangan', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3951, 242, 'Navoi', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3952, 242, 'Samarkand', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3953, 242, 'Sirdaryo', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3954, 242, 'Surxondaryo', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3955, 242, 'Tashkent', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3956, 242, 'Tashkent City', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3957, 242, 'Xorazm', '2021-04-28 16:38:45', '2021-04-28 16:38:45'),
(3958, 243, 'Malampa', '2021-04-28 16:38:47', '2021-04-28 16:38:47'),
(3959, 243, 'Pénama', '2021-04-28 16:38:47', '2021-04-28 16:38:47'),
(3960, 243, 'Sanma', '2021-04-28 16:38:47', '2021-04-28 16:38:47'),
(3961, 243, 'Shéfa', '2021-04-28 16:38:47', '2021-04-28 16:38:47'),
(3962, 243, 'Taféa', '2021-04-28 16:38:47', '2021-04-28 16:38:47'),
(3963, 243, 'Torba', '2021-04-28 16:38:47', '2021-04-28 16:38:47'),
(3964, 244, 'Amazonas', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3965, 244, 'Anzoátegui', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3966, 244, 'Apure', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3967, 244, 'Aragua', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3968, 244, 'Barinas', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3969, 244, 'Bolívar', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3970, 244, 'Carabobo', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3971, 244, 'Cojedes', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3972, 244, 'Delta Amacuro', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3973, 244, 'Dependencias Federales', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3974, 244, 'Distrito Capital', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3975, 244, 'Falcón', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3976, 244, 'Guárico', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3977, 244, 'Lara', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3978, 244, 'Mérida', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3979, 244, 'Miranda', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3980, 244, 'Monagas', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3981, 244, 'Nueva Esparta', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3982, 244, 'Portuguesa', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3983, 244, 'Sucre', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3984, 244, 'Táchira', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3985, 244, 'Trujillo', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3986, 244, 'Vargas', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3987, 244, 'Yaracuy', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3988, 244, 'Zulia', '2021-04-28 16:38:48', '2021-04-28 16:38:48'),
(3989, 245, '', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(3990, 245, 'An Giang', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(3991, 245, 'Ba Ria-Vung Tau', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(3992, 245, 'Bac Can', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(3993, 245, 'Bac Giang', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(3994, 245, 'Bac Lieu', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(3995, 245, 'Bac Ninh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(3996, 245, 'Ben Tre', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(3997, 245, 'Binh Dinh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(3998, 245, 'Binh Duong', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(3999, 245, 'Binh Phuoc', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4000, 245, 'Binh Thuan', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4001, 245, 'Ca Mau', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4002, 245, 'Can Tho', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4003, 245, 'Cao Bang', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4004, 245, 'Da Nang', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4005, 245, 'Dac Lac', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4006, 245, 'Dac Nong', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4007, 245, 'Dien Bien', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4008, 245, 'Dong Nai', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4009, 245, 'Dong Thap', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4010, 245, 'Gia Lai', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4011, 245, 'Ha Giang', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4012, 245, 'Ha Nam', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4013, 245, 'Ha Tay', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4014, 245, 'Ha Tinh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4015, 245, 'Hai Duong', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4016, 245, 'Haiphong', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4017, 245, 'Hanoi', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4018, 245, 'Hau Giang', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4019, 245, 'Ho Chi Minh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4020, 245, 'Hoa Binh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4021, 245, 'Hung Yen', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4022, 245, 'Khanh Hoa', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4023, 245, 'Kien Giang', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4024, 245, 'Kon Tum', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4025, 245, 'Lai Chau', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4026, 245, 'Lam Dong', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4027, 245, 'Lang Son', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4028, 245, 'Lao Cai', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4029, 245, 'Long An', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4030, 245, 'Nam Dinh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4031, 245, 'Nghe An', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4032, 245, 'Ninh Binh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4033, 245, 'Ninh Thuan', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4034, 245, 'Phu Tho', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4035, 245, 'Phu Yen', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4036, 245, 'Quang Binh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4037, 245, 'Quang Nam', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4038, 245, 'Quang Ngai', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4039, 245, 'Quang Ninh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4040, 245, 'Quang Tri', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4041, 245, 'Soc Trang', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4042, 245, 'Son La', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4043, 245, 'Tay Ninh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4044, 245, 'Thai Binh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4045, 245, 'Thai Nguyen', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4046, 245, 'Thanh Hoa', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4047, 245, 'Thua Thien-Hue', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4048, 245, 'Tien Giang', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4049, 245, 'Tra Vinh', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4050, 245, 'Tuyen Quang', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4051, 245, 'Vinh Long', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4052, 245, 'Vinh Phuc', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4053, 245, 'Yen Bai', '2021-04-28 16:38:49', '2021-04-28 16:38:49'),
(4054, 246, 'Alo', '2021-04-28 16:38:50', '2021-04-28 16:38:50'),
(4055, 246, 'Sigave', '2021-04-28 16:38:50', '2021-04-28 16:38:50'),
(4056, 246, 'Uvéa', '2021-04-28 16:38:50', '2021-04-28 16:38:50'),
(4057, 247, 'Boujdour', '2021-04-28 16:38:51', '2021-04-28 16:38:51'),
(4058, 247, 'Es Semara', '2021-04-28 16:38:51', '2021-04-28 16:38:51'),
(4059, 247, 'Laayoune', '2021-04-28 16:38:51', '2021-04-28 16:38:51'),
(4060, 247, 'Oued el Dahab', '2021-04-28 16:38:51', '2021-04-28 16:38:51'),
(4061, 248, 'Abyan', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4062, 248, 'Al Bayda\'', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4063, 248, 'Al Dali\'', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4064, 248, 'Al Hudaydah', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4065, 248, 'Al Jawf', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4066, 248, 'Al Mahrah', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4067, 248, 'Al Mahwit', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4068, 248, 'Amran', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4069, 248, 'Dhamar', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4070, 248, 'Hadramawt', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4071, 248, 'Hajjah', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4072, 248, 'Ibb', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4073, 248, 'Lahij', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4074, 248, 'Ma\'rib', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4075, 248, 'San`a\'', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4076, 248, 'San`a\' [City]', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4077, 248, 'Sa`dah', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4078, 248, 'Shabwah', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4079, 248, 'Ta`izz', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4080, 248, '`Adan', '2021-04-28 16:38:54', '2021-04-28 16:38:54'),
(4081, 249, 'Central', '2021-04-28 16:38:55', '2021-04-28 16:38:55'),
(4082, 249, 'Copperbelt', '2021-04-28 16:38:55', '2021-04-28 16:38:55'),
(4083, 249, 'Eastern', '2021-04-28 16:38:55', '2021-04-28 16:38:55'),
(4084, 249, 'Luapula', '2021-04-28 16:38:55', '2021-04-28 16:38:55'),
(4085, 249, 'Lusaka', '2021-04-28 16:38:55', '2021-04-28 16:38:55'),
(4086, 249, 'North-Western', '2021-04-28 16:38:55', '2021-04-28 16:38:55'),
(4087, 249, 'Northern', '2021-04-28 16:38:55', '2021-04-28 16:38:55'),
(4088, 249, 'Southern', '2021-04-28 16:38:55', '2021-04-28 16:38:55'),
(4089, 249, 'Western', '2021-04-28 16:38:55', '2021-04-28 16:38:55'),
(4090, 250, 'Bulawayo', '2021-04-28 16:38:56', '2021-04-28 16:38:56'),
(4091, 250, 'Harare', '2021-04-28 16:38:56', '2021-04-28 16:38:56'),
(4092, 250, 'Manicaland', '2021-04-28 16:38:56', '2021-04-28 16:38:56'),
(4093, 250, 'Mashonaland Central', '2021-04-28 16:38:56', '2021-04-28 16:38:56'),
(4094, 250, 'Mashonaland East', '2021-04-28 16:38:56', '2021-04-28 16:38:56'),
(4095, 250, 'Mashonaland West', '2021-04-28 16:38:56', '2021-04-28 16:38:56'),
(4096, 250, 'Masvingo', '2021-04-28 16:38:56', '2021-04-28 16:38:56'),
(4097, 250, 'Matabeleland North', '2021-04-28 16:38:56', '2021-04-28 16:38:56'),
(4098, 250, 'Matabeleland South', '2021-04-28 16:38:56', '2021-04-28 16:38:56'),
(4099, 250, 'Midlands', '2021-04-28 16:38:56', '2021-04-28 16:38:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `address`, `role`, `email_verified_at`, `password`, `slug`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'admin@localhost', '', '', 'admin', NULL, '$2y$10$RdWykwZHFYVVss/RLdjXjOpIbfu/OBi8C8ErDMD7Wu6bNxDJ/WwzW', 'oonfb7qV05MnoNMP', 'P2I9W2f1PILmfdq4uEOwwLBqyKlLTCZJqaMSpo03zqhpKzLiRPcJ3EyLmSDD', NULL, '2021-05-18 12:08:13', '2021-05-18 12:08:13'),
(2, 'Natalia Daugherty', 'dulce42@example.org', '+1.843.283.8469', NULL, 'shipper', '2021-05-18 12:08:13', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7mGahg2hU1Ry6zCb', 'mJKci14P7ZYWrKxe', NULL, '2021-05-18 12:08:13', '2021-05-18 12:08:13'),
(3, 'Prof. King Schinner', 'darrin53@example.com', '302.696.8724 x0575', NULL, 'receiver', '2021-05-18 12:08:13', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'RCHxIVqMqhJUzHS8', '3yOPC5L0Ahc0Nqyr', NULL, '2021-05-18 12:08:13', '2021-05-18 12:08:13'),
(4, 'Prof. Sylvan Quigley DVM', 'jarret41@example.net', '+1-785-948-3264', NULL, 'agent', '2021-05-18 12:08:13', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'jsfNiVWgp8jwsMNM', '3Cr0mjlmAmSNBc46', NULL, '2021-05-18 12:08:13', '2021-05-18 12:08:13'),
(5, 'Angeline Ankunding', 'mcclure.ludwig@example.org', '549.977.3172 x4917', NULL, 'agent', '2021-05-18 12:08:13', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4wOCrhPaVhBL1Tui', '5YvoJbuYzW4ktGP1', NULL, '2021-05-18 12:08:13', '2021-05-18 12:08:13'),
(6, 'Thomas Brown', 'alva.steuber@example.org', '663.644.7766 x69505', NULL, 'receiver', '2021-05-18 12:08:13', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'AvIPoiHb47Zk6aTZ', 'a62IAWTsTpJnklk7', NULL, '2021-05-18 12:08:13', '2021-05-18 12:08:13'),
(7, 'Dr. Natasha Connelly PhD', 'tod.ryan@example.com', '+1-248-957-1889', NULL, 'client', '2021-05-18 12:08:13', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3C9ZbbI51HufOL5D', 'Ch1CDkqf00Tz9Gww', NULL, '2021-05-18 12:08:13', '2021-05-18 12:08:13'),
(8, 'Eric Vandervort', 'btrantow@example.net', '985.266.2451', NULL, 'agent', '2021-05-18 12:08:13', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'MBmTItQIY0OLNDXx', 'rq9BG1TdQ1GRKKET', NULL, '2021-05-18 12:08:13', '2021-05-18 12:08:13'),
(9, 'Alysa Hand', 'dzulauf@example.org', '669-306-5826', NULL, 'agent', '2021-05-18 12:08:13', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'viQsxp0cPe2DW9Mo', 'Ldv7zav0s2xNvpj4', NULL, '2021-05-18 12:08:13', '2021-05-18 12:08:13'),
(10, 'Mrs. Aleen Vandervort', 'issac46@example.org', '(472) 353-7314 x474', NULL, 'shipper', '2021-05-18 12:08:13', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'KmcXXoPmn8d7cSYf', 'ScthWUxbYWtZ2ruF', NULL, '2021-05-18 12:08:13', '2021-05-18 12:08:13'),
(11, 'Dr. Keanu Funk DVM', 'sschamberger@example.org', '(934) 770-3673 x31398', NULL, 'shipper', '2021-05-18 12:08:13', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'uma3dB2h5OzVy09W', 'pnlfkykmxe5tGSzD', NULL, '2021-05-18 12:08:13', '2021-05-18 12:08:13'),
(12, 'Justus Ali', 'okwuchukwu.alieze@gmail.com', '+2348101554233', 'Achara Road Nsukka', 'client', NULL, '$2y$10$w/6kluL8YOWykXblYZcaLuWPcO7aZNS8iEFhxjlLneklsmAqfRqiG', 'i8KVX9A6dakIV2Xj', NULL, NULL, '2021-05-18 12:39:57', '2021-05-18 12:39:57'),
(13, 'Super Admin', 'super.admin@locahost', NULL, NULL, NULL, NULL, '$2y$10$pJnvewppSDQAfIGEZcEZHuYHbMtlr3LCS8Nb5Krsm9sn81I3irM4G', 'j8lz1jqxA5uRH8j4\"', NULL, NULL, '2021-05-18 13:41:16', '2021-05-18 13:41:16'),
(14, 'Donald', 'donaldgeology@gmail.com', '08187322297', 'Unn', 'receiver', NULL, '$2y$10$oel3fwty0fWlw0j8l48RAOOhACNRuqeJ596GIti79hgwlHUd3FsNm', 'U7F8ktbOmw7AxVoP', NULL, NULL, '2021-05-18 15:48:40', '2021-05-18 15:48:40'),
(18, 'JD', 'jd@yopmail.com', '809997454', 'Dummy address', 'receiver', NULL, '$2y$10$Vz.ampFLuMKJZmK6WzehkOdLSbtd8p5HA486tSgeuPqHaq8LRhDfe', 'V7JvwmA1AALdZWgX', NULL, NULL, '2021-05-18 16:38:37', '2021-05-18 16:38:37'),
(19, 'Justuuuuuu', 'j.swiftlaunchme@gmail.com', '+2348101554233', 'Achara Road Nsukka', 'receiver', NULL, '$2y$10$upqkS5vdTR7F7o1HfAV1c.9pw5.YKVBqnVvHJMdynQxPhK9dlxKbq', 'd5BlNzezCSl40rXk', NULL, NULL, '2021-05-18 16:45:30', '2021-05-18 16:45:30'),
(20, 'Jay Dee', 'payee57@gmail.com', '08166533091', 'Unn', 'receiver', NULL, '$2y$10$Ns10bd0pBBkvwbBu1mtnPOtfyz4gMbtSb6EhGIXGqVB0/vjrK2JIu', '6quc1ML5xgo5SVWR', NULL, NULL, '2021-05-18 16:55:24', '2021-05-18 16:55:24'),
(21, 'Jay Dee', 'dgifted@yopmail.com', '08166533091', 'Unn', 'receiver', NULL, '$2y$10$Fdcixf/7LM7C1k5l3k5ICea2IxYSk4wMC6L0ebBkdxy99Rq7p9UUK', '95JwKasfCZCdMyrZ', NULL, NULL, '2021-05-18 20:38:52', '2021-05-18 20:38:52'),
(22, 'Michael Holland', 'hollandmichael021@gmail.com', '+1 (530) 230‑341', '2217-2399 middle ground church Rd Eastman , GA 31023 United States', 'shipper', NULL, '$2y$10$wg3zAjJjA.XQ2LfTI76bNuzOS4Ap9H3o7pNrwvWLGjXirHd9v6uMq', 'w8K7A7xBSKNTqu6z', NULL, NULL, '2021-06-30 08:00:53', '2021-06-30 08:00:53');

-- --------------------------------------------------------

--
-- Table structure for table `weight_units`
--

CREATE TABLE `weight_units` (
  `id` bigint UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbreviation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_use` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `weight_units`
--

INSERT INTO `weight_units` (`id`, `full_name`, `abbreviation`, `in_use`, `created_at`, `updated_at`) VALUES
(1, 'Kilogram', 'kg', 1, '2021-05-18 11:53:53', '2021-05-18 15:40:05'),
(2, 'Grain', 'gr', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(3, 'Ounce', 'oz', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(4, 'Stone', 'st', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(5, 'Ton', 'ton', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(6, 'Pound', 'lb', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53'),
(7, 'Gram', 'g', 0, '2021-05-18 11:53:53', '2021-05-18 11:53:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carriers`
--
ALTER TABLE `carriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carrier_shipment_info`
--
ALTER TABLE `carrier_shipment_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dimension_units`
--
ALTER TABLE `dimension_units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dimension_unit_package_setting`
--
ALTER TABLE `dimension_unit_package_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dimension_unit_package_setting_dimension_unit_id_foreign` (`dimension_unit_id`),
  ADD KEY `dimension_unit_package_setting_package_setting_id_foreign` (`package_setting_id`);

--
-- Indexes for table `email_settings`
--
ALTER TABLE `email_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `histories_shipment_id_foreign` (`shipment_id`);

--
-- Indexes for table `history_location`
--
ALTER TABLE `history_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history_shipment_status`
--
ALTER TABLE `history_shipment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_settings`
--
ALTER TABLE `map_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_setting_type`
--
ALTER TABLE `map_setting_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `map_setting_type_map_type_id_foreign` (`map_type_id`),
  ADD KEY `map_setting_type_map_setting_id_foreign` (`map_setting_id`);

--
-- Indexes for table `map_setting_zoom_level`
--
ALTER TABLE `map_setting_zoom_level`
  ADD PRIMARY KEY (`id`),
  ADD KEY `map_setting_zoom_level_map_zoom_level_id_foreign` (`map_zoom_level_id`),
  ADD KEY `map_setting_zoom_level_map_setting_id_foreign` (`map_setting_id`);

--
-- Indexes for table `map_types`
--
ALTER TABLE `map_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_zoom_levels`
--
ALTER TABLE `map_zoom_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `packages_shipment_id_foreign` (`shipment_id`),
  ADD KEY `packages_piece_type_id_foreign` (`piece_type_id`);

--
-- Indexes for table `package_piece_type`
--
ALTER TABLE `package_piece_type`
  ADD KEY `package_piece_type_package_id_foreign` (`package_id`),
  ADD KEY `package_piece_type_piece_type_id_foreign` (`piece_type_id`);

--
-- Indexes for table `package_settings`
--
ALTER TABLE `package_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_setting_weight_unit`
--
ALTER TABLE `package_setting_weight_unit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_setting_weight_unit_package_setting_id_foreign` (`package_setting_id`),
  ADD KEY `package_setting_weight_unit_weight_unit_id_foreign` (`weight_unit_id`);

--
-- Indexes for table `passwords`
--
ALTER TABLE `passwords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `passwords_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_modes`
--
ALTER TABLE `payment_modes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `piece_types`
--
ALTER TABLE `piece_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipments`
--
ALTER TABLE `shipments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipment_infos`
--
ALTER TABLE `shipment_infos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shipment_infos_shipment_id_foreign` (`shipment_id`);

--
-- Indexes for table `shipment_info_mode`
--
ALTER TABLE `shipment_info_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipment_info_type`
--
ALTER TABLE `shipment_info_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipment_modes`
--
ALTER TABLE `shipment_modes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipment_payment_mode`
--
ALTER TABLE `shipment_payment_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipment_statuses`
--
ALTER TABLE `shipment_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipment_types`
--
ALTER TABLE `shipment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `weight_units`
--
ALTER TABLE `weight_units`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carriers`
--
ALTER TABLE `carriers`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `carrier_shipment_info`
--
ALTER TABLE `carrier_shipment_info`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT for table `dimension_units`
--
ALTER TABLE `dimension_units`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `dimension_unit_package_setting`
--
ALTER TABLE `dimension_unit_package_setting`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `email_settings`
--
ALTER TABLE `email_settings`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `histories`
--
ALTER TABLE `histories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `history_location`
--
ALTER TABLE `history_location`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `history_shipment_status`
--
ALTER TABLE `history_shipment_status`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `map_settings`
--
ALTER TABLE `map_settings`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `map_setting_type`
--
ALTER TABLE `map_setting_type`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `map_setting_zoom_level`
--
ALTER TABLE `map_setting_zoom_level`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `map_types`
--
ALTER TABLE `map_types`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `map_zoom_levels`
--
ALTER TABLE `map_zoom_levels`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `package_settings`
--
ALTER TABLE `package_settings`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `package_setting_weight_unit`
--
ALTER TABLE `package_setting_weight_unit`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `passwords`
--
ALTER TABLE `passwords`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `payment_modes`
--
ALTER TABLE `payment_modes`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `piece_types`
--
ALTER TABLE `piece_types`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `shipments`
--
ALTER TABLE `shipments`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `shipment_infos`
--
ALTER TABLE `shipment_infos`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `shipment_info_mode`
--
ALTER TABLE `shipment_info_mode`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `shipment_info_type`
--
ALTER TABLE `shipment_info_type`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `shipment_modes`
--
ALTER TABLE `shipment_modes`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shipment_payment_mode`
--
ALTER TABLE `shipment_payment_mode`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `shipment_statuses`
--
ALTER TABLE `shipment_statuses`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `shipment_types`
--
ALTER TABLE `shipment_types`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4100;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `weight_units`
--
ALTER TABLE `weight_units`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dimension_unit_package_setting`
--
ALTER TABLE `dimension_unit_package_setting`
  ADD CONSTRAINT `dimension_unit_package_setting_dimension_unit_id_foreign` FOREIGN KEY (`dimension_unit_id`) REFERENCES `dimension_units` (`id`),
  ADD CONSTRAINT `dimension_unit_package_setting_package_setting_id_foreign` FOREIGN KEY (`package_setting_id`) REFERENCES `package_settings` (`id`);

--
-- Constraints for table `histories`
--
ALTER TABLE `histories`
  ADD CONSTRAINT `histories_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`);

--
-- Constraints for table `map_setting_type`
--
ALTER TABLE `map_setting_type`
  ADD CONSTRAINT `map_setting_type_map_setting_id_foreign` FOREIGN KEY (`map_setting_id`) REFERENCES `map_settings` (`id`),
  ADD CONSTRAINT `map_setting_type_map_type_id_foreign` FOREIGN KEY (`map_type_id`) REFERENCES `map_types` (`id`);

--
-- Constraints for table `map_setting_zoom_level`
--
ALTER TABLE `map_setting_zoom_level`
  ADD CONSTRAINT `map_setting_zoom_level_map_setting_id_foreign` FOREIGN KEY (`map_setting_id`) REFERENCES `map_settings` (`id`),
  ADD CONSTRAINT `map_setting_zoom_level_map_zoom_level_id_foreign` FOREIGN KEY (`map_zoom_level_id`) REFERENCES `map_zoom_levels` (`id`);

--
-- Constraints for table `packages`
--
ALTER TABLE `packages`
  ADD CONSTRAINT `packages_piece_type_id_foreign` FOREIGN KEY (`piece_type_id`) REFERENCES `piece_types` (`id`),
  ADD CONSTRAINT `packages_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`);

--
-- Constraints for table `package_piece_type`
--
ALTER TABLE `package_piece_type`
  ADD CONSTRAINT `package_piece_type_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`),
  ADD CONSTRAINT `package_piece_type_piece_type_id_foreign` FOREIGN KEY (`piece_type_id`) REFERENCES `piece_types` (`id`);

--
-- Constraints for table `package_setting_weight_unit`
--
ALTER TABLE `package_setting_weight_unit`
  ADD CONSTRAINT `package_setting_weight_unit_package_setting_id_foreign` FOREIGN KEY (`package_setting_id`) REFERENCES `package_settings` (`id`),
  ADD CONSTRAINT `package_setting_weight_unit_weight_unit_id_foreign` FOREIGN KEY (`weight_unit_id`) REFERENCES `weight_units` (`id`);

--
-- Constraints for table `passwords`
--
ALTER TABLE `passwords`
  ADD CONSTRAINT `passwords_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `shipment_infos`
--
ALTER TABLE `shipment_infos`
  ADD CONSTRAINT `shipment_infos_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
