"use strict";
// Class definition

var KTDatatableHtmlTableDemo = function() {
    // Private functions

    // demo initializer
    var demo = function() {

		var datatable = $('#kt_datatable').KTDatatable({
			data: {
				saveState: {cookie: false},
			},
			search: {
				input: $('#kt_datatable_search_query'),
				key: 'generalSearch'
			},
			columns: [
                // {
				// 	field: 'Role',
				// 	title: 'Role',
				// 	autoHide: false,
				// 	// callback function support for column rendering
				// 	template: function(row) {
				// 		var role = {
				// 			1: {
                //                 'title': 'Admin',
                //                 'class': ' label-light-warning'
                //             },
				// 			2: {
                //                 'title': 'Agent',
                //                 'class': ' label-light-danger'
                //             },
				// 			3: {
                //                 'title': 'Client',
                //                 'class': ' label-light-primary'
                //             },
				// 			4: {
                //                 'title': 'Receiver',
                //                 'class': ' label-light-success'
                //             },
				// 			5: {
                //                 'title': 'Shipper',
                //                 'class': ' label-light-info'
                //             }
				// 		};
				// 		// return '<span class="label font-weight-bold label-lg' + role[row.Role].class + ' label-inline">' + role[row.Role].title + '</span>';
				// 		// return '<span>' + role[row.Role].title + '</span>';
				// 	},
				// }
			],
		});



        $('#kt_datatable_search_role').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Role');
        });

        $('#kt_datatable_search_role').selectpicker();

    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function() {
	KTDatatableHtmlTableDemo.init();
});
