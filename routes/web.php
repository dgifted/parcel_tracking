<?php

use Illuminate\Support\Facades\Route;


Auth::routes(['register' => false]);
Route::redirect('/', '/home', 301);

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/country-setup', 'CountryController@setupCountries')->name('admin.setup-country');
    Route::get('/country-cities/{id}', 'CountryController@apiFetchCountryCities');

    //Users
    Route::get('/users', 'UserController@getAllUsers')->name('admin.get-all-users');
    Route::get('/users-index', 'UserController@showAllUsers')->name('admin.show-all-users');
    Route::get('/users/create', 'UserController@createUser')->name('admin.create-user');
    Route::post('/users/store', 'UserController@storeUser')->name('admin.store-user');
    Route::get('/users/{id}', 'UserController@getUser')->name('admin.get-user');
    Route::post('/users/{id}/update', 'UserController@updateUser')->name('admin.update-user');
    Route::get('/users/{id}/delete', 'UserController@deleteUser')->name('admin.delete-user');
    Route::get('/receivers/{email}', 'UserController@getReceiverFromEmail')->name('admin.get-receiver');

    //Role
    Route::get('/roles', 'RoleController@getAllRoles')->name('admin.get-all-roles');

    //Shipment
    Route::get('/shipments', 'ShipmentController@index')->name('admin.view-shipments');
    Route::get('/shipment-create', 'ShipmentController@create')->name('admin.create-shipment');
    Route::post('/shipment-store', 'ShipmentController@storeShipment')->name('admin.store-shipment');
    Route::get('/shipment-edit/{ref_id}', 'ShipmentController@edit')->name('admin.edit-shipment');
    Route::get('/shipments/{tracking_id}/delete', 'ShipmentController@deleteShipment')->name('admin.delete-shipment');
    Route::get('/shipment-location', 'ShipmentController@location')->name('admin.location-shipment');
    Route::get('/generate-tracking-id', 'ShipmentController@generateTrackingId')->name('admin.generate-tracking-id');
    Route::get('/shipments/{tracking_id}', 'ShipmentController@getShipment')->name('admin.get-shipment');
    Route::post('/shipments/{tracking_id}/location-update', 'ShipmentController@updateShipmentLocation')->name('admin.shipment-location-update');
    Route::get('/shipments/{tracking_id}/locations', 'ShipmentController@getShipmentLocations')->name('admin.get-shipment-locations');

    //Settings
    Route::get('/settings-general', 'SettingController@general')->name('admin.settings-general');
    Route::get('/settings-email', 'SettingController@email')->name('admin.settings-email');
    Route::get('/settings-package', 'SettingController@package')->name('admin.settings-package');
    Route::get('/settings-map', 'SettingController@map')->name('admin.settings-map');

    Route::post('/settings-map', 'SettingController@storeMapSettings');
    Route::get('/get-map_api_key', 'SettingController@getMapApiKey')->name('admin.get-map_api_key');

    //Piece Type
    Route::get('/piece-types', 'PieceTypeController@getAllPieceTypes')->name('admin.get-all-piece-type');
    Route::post('/piece-type', 'PieceTypeController@storePieceType')->name('admin.add-piece-type');
    Route::get('/piece-type/{id}', 'PieceTypeController@getPieceType')->name('admin.get-piece-type');
    Route::post('/piece-type/{id}', 'PieceTypeController@updatePieceType');
    Route::get('/piece-type-delete/{id}', 'PieceTypeController@deletePieceType')->name('admin.delete-piece-type');

    //Package setting
    Route::post('/package-setting', 'PackageSettingController@updatePackageSetting')->name('admin.update-package-setting');

    //Email setting
    Route::post('/email-setting', 'EmailSettingController@updateEmailSetting')->name('admin.update-email-setting');

    //General settings
    Route::post('/shipment-type-add', 'GeneralSettingController@addShipmentType')->name('admin.add-shipment-type');
    Route::post('/shipment-mode-add', 'GeneralSettingController@addShipmentMode')->name('admin.add-shipment-mode');
    Route::post('/carrier-add', 'GeneralSettingController@addCarrier')->name('admin.add-carrier');
    Route::post('/payment-mode-add', 'GeneralSettingController@addPaymentMode')->name('admin.add-payment-mode');
    Route::post('/shipment-status-add', 'GeneralSettingController@addShipmentStatus')->name('admin.add-shipment-status');
    Route::get('/shipment-type-delete/{id}', 'GeneralSettingController@deleteShipmentType')->name('admin.delete-shipment-type');
    Route::post('/shipment-type/{id}', 'GeneralSettingController@updateShipmentType')->name('admin.update-shipment-type');
    Route::get('/shipment-mode-delete/{id}', 'GeneralSettingController@deleteShipmentMode')->name('admin.delete-shipment-mode');
    Route::post('/shipment-mode/{id}', 'GeneralSettingController@updateShipmentMode')->name('admin.update-shipment-mode');
    Route::get('/carrier-delete/{id}', 'GeneralSettingController@deleteCarrier')->name('admin.delete-carrier');
    Route::post('/carrier/{id}', 'GeneralSettingController@updateCarrier')->name('admin.update-carrier');
    Route::get('/payment-mode-delete/{id}', 'GeneralSettingController@deletePaymentMode')->name('admin.delete-payment-mode');
    Route::post('/payment-mode/{id}', 'GeneralSettingController@updatePaymentMode')->name('admin.update-payment-mode');
    Route::get('/shipment-status-delete/{id}', 'GeneralSettingController@deleteShipmentStatus')->name('admin.delete-shipment-status');
    Route::post('/shipment-status/{id}', 'GeneralSettingController@updateShipmentStatus')->name('admin.update-shipment-status');

});

