<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Shipments
Route::get('/shipments', 'API\ShipmentController@index');
Route::get('/shipments/{tracking_id}', 'API\ShipmentController@show');

//Settings
Route::get('/settings/email', 'API\SettingController@emailSetting');
