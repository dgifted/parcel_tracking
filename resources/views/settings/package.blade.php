@extends('layouts.main-master')
@section('title', 'Package Settings')

@section('page-styles')
@stop

@section('page-title', 'Package Settings')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label"><i class="fas fa-gift text-secondary"></i> Package Settings</h3>
                </div>
            </div>
            <div class="card-body">
                <div class="example-preview">
                    <ul class="nav nav-pills" id="myTab1" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="piece-type-tab-1" data-toggle="tab" href="#piece-type">
                                <span class="nav-icon">
                                    <i class="fas fa-box-open text-warning"></i>
                                </span>
                                <span class="nav-text text-uppercase"><b>Piece Type</b> @if($errors->any()) <i class="fas fa-exclamation-triangle text-danger"></i> @endif</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="other-tab-1" data-toggle="tab" href="#other">
                                <span class="nav-icon">
                                    <i class="fas fa-cogs text-warning"></i>
                                </span>
                                <span class="nav-text text-uppercase"><b>Other Setups</b></span>
                            </a>
                        </li>
                    </ul>
                    <hr>
                    <div class="tab-content mt-5" id="myTabContent1">
                        <div class="tab-pane fade show active" id="piece-type" role="tabpanel" aria-labelledby="piece-type-tab-1">
                            <div class="d-flex justify-content-end">
                                <button id="add-piece-type" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add</button>
                            </div>
                            @if ($errors->any())
                                    <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                                        <div class="alert-icon">
                                            <i class="flaticon-warning"></i>
                                        </div>
                                        <div class="alert-text">
                                            <b>Following errors were found in your form!</b><br>
                                            @foreach ($errors->all() as $message)
                                                <i class="fas fa-exclamation text-danger"></i> {{ $message }}
                                            @endforeach
                                        </div>
                                        <div class="alert-close">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">
                                                    <i class="ki ki-close"></i>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                            @endif
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-responsive table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th scope="col">Name</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($piece_types as $type)
                                            <tr>
                                                <td class="text-uppercase">{{ $type->name }}</td>
                                                <td title="{{ $type->description ?? '' }}" data-toggle="tooltip" data-container="body" data-placement="bottom" style="max-width: 180px;">
                                                    {{ $type->shortDescription() ?? '' }}
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-sm p-1 btn-edit" title="Edit" data-model="Piece Type" data-id="{{ $type->id }}"
                                                        data-title="{{ $type->name }}" data-description="{{ $type->description ?? '' }}">
                                                        <i class="far fa-edit"></i> Edit
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-sm p-1 btn-delete" title="Delete" data-model="Piece Type" data-id="{{ $type->id }}">
                                                        <i class="far fa-trash-alt"></i> Delete
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <div id="piece-type-add" class="pt-5" style="display: none;">
                                        <br>
                                        <form action="{{ route('admin.add-piece-type') }}" method="POST">
                                            @csrf

                                            @include('includes.forms.create-edit')
                                            <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="other" role="tabpanel" aria-labelledby="other-tab-1">
                            <form action="{{ route('admin.update-package-setting') }}" method="POST">
                                @csrf

                                <div class="form-group row mb-4">
                                    <label for="" class="col-md-3 col-form-label text-right"><b class="text-uppercase">Enable Multiple Package on Admin?</b></label>
                                    <div class="col-md-7">
                                        <input data-switch="true" name="multi_package_admin" type="checkbox"
                                            @if($package_setting !== null)
                                                @if ($package_setting->enable_multi_package_admin === 1)
                                                    checked="checked"
                                                @endif
                                            @endif />
                                        <div class="text-muted">
                                            If checked you will enable the multiple package on your shipment admin dashboard.
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label for="" class="col-md-3 col-form-label text-right"><b class="text-uppercase">Enable Multiple Package on Results?</b></label>
                                    <div class="col-md-7">
                                        <input data-switch="true" name="multi_package_result" type="checkbox"
                                            @if($package_setting !== null)
                                                @if ($package_setting->enable_multi_package_result === 1)
                                                    checked="checked"
                                                @endif
                                            @endif />

                                        <div class="text-muted">
                                            If checked you will enable the multiple package on your shipment results.
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label for="" class="col-md-3 col-form-label text-right"><b class="text-uppercase">Enable Dimension Unit?</b></label>
                                    <div class="col-md-7">
                                        <input data-switch="true" name="dimension_unit_on" type="checkbox"
                                            @if($package_setting !== null)
                                                @if ($package_setting->enable_dimension === 1)
                                                    checked="checked"
                                                @endif
                                            @endif />
                                        <div class="text-muted">
                                            If checked you will enable the dimension unit.
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-4 mt-2">
                                    <label for="" class="col-md-3 col-form-label text-right"><b class="text-uppercase">Dimension Unit<br></b></label>
                                    <div class="col-md-3 text-left">
                                        {{-- <input class="form-control" type="text" value="cm" id="example-text-input"
                                        placeholder="cm | m | inches"/> --}}
                                        <select name="dimension_unit" id="" class="form-control">
                                            <option value="">Select one</option>
                                            @if ($dimension_units->count() > 0)
                                                @foreach ($dimension_units as $unit)
                                                    <option value="{{ $unit->id }}" @if($unit->in_use === 1) selected @endif title="{{ $unit->full_name }}" data-toggle="tooltip" data-container="body" data-placement="bottom">
                                                        {{ $unit->abbreviation }}
                                                    </option>
                                                @endforeach
                                            @else
                                                <option value="cm">No dimension unit in database</option>
                                            @endif
                                        </select>
                                        <div class="text-muted">This will be display in the package Dimension. Example: mm, cm, inch, etc. The default is cm.</div>
                                    </div>
                                </div>

                                <div class="form-group row mb-4 mt-2">
                                    <label for="" class="col-md-3 col-form-label text-right"><b class="text-uppercase">Weight Unit<br></b></label>
                                    <div class="col-md-3 text-left">
                                        {{-- <input class="form-control" type="text" value="kg" id="example-text-input"
                                        placeholder="kg | g | lbs"/> --}}
                                        <select name="weight_unit" id="" class="form-control">
                                            <option value="">Select one</option>
                                            @if ($weight_units->count() > 0)
                                                @foreach ($weight_units as $w_unit)
                                                    <option value="{{ $w_unit->id }}" @if($w_unit->in_use === 1) selected @endif title="{{ $w_unit->full_name }}" data-toggle="tooltip" data-container="body" data-placement="bottom">
                                                        {{ $w_unit->abbreviation }}
                                                    </option>
                                                @endforeach
                                            @else
                                                <option value="cm">No weight units in database</option>
                                            @endif
                                        </select>
                                        <div class="text-muted">This will be display in the package Weight. Example: lbs, g, gr, kg, etc. The default is lbs.</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('includes.modals.model-update')
@include('includes.modals.model-delete')
@stop

@section('page-scripts')
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-switch.js?v=7.0.4') }}"></script>
<script>
    $('#add-piece-type').on('click', function (event) {
        console.log(event);
        $(this).prop('disabled', true);
        $('#piece-type-add').show('slow');
    });

    var data;

    $('.btn-edit').each(function() {
        $(this).on('click', function (event) {
            event.preventDefault();
            var model = $(this).data('model');
            var modelId = $(this).data('id');
            var name = $(this).data('title');
            var description = $(this).data('description');

            // console.log(model, ' ', modelId);

            // KTApp.blockPage({
            //     overlayColor: '#000000',
            //     state: 'danger',
            //     message: 'Please wait...'
            // });

            $('#staticBackdrop').modal();
            $('#staticBackdrop').on('shown.bs.modal', function (event) {
                var modal = $(this);
                modal.find('form').attr('action', '/piece-type/' + modelId);
                modal.find('.modal-title').text('Edit ' + model);
                modal.find('#name').val(name);
                modal.find('#description').val(description);
            });

        });
    });

    $('.btn-delete').each(function() {
        $(this).on('click', function(event) {
            var model = $(this).data('model');
            var modelId = $(this).data('id');

            // console.log(model, ' ', modelId);

            var modal = $('#staticBackdropDelete').modal();
            modal.find('#model-name').text(model);
            modal.find('#btn-delete').attr('href', '/piece-type-delete/' + modelId);

        });
    });

</script>
@endsection
