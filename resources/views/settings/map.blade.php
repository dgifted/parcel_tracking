@extends('layouts.main-master')
@section('title', 'Map Settings')

@section('page-styles')
@stop

@section('page-title', 'Map Settings')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label"><i class="fas fa-map-marked-alt text-secondary"></i> Map Settings</h3>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('admin.settings-map') }}" method="POST">
                    @csrf

                    <div class="form-group row">
                        <label class="col-form-label col-md-3 text-right"><b class="text-uppercase">Google Map API Key</b></label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="api_key" value="{{ $map_setting->api_key ?? '' }}">
                            <div class="text-muted">Please click <a href="https://developers.google.com/maps/documentation/embed/get-api-key">here</a>  to get Google Map API Key</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 text-right"><b class="text-uppercase">Map Type</b></label>
                        <div class="col-md-3">
                            <select name="map_type_id" id="" class="form-control">
                                <option value="">SELECT ONE</option>
                                @if ($map_types->count() > 0)
                                    @foreach ($map_types as $type)
                                        <option value="{{ $type->id }}" @if ($type->in_use === 1) selected @endif>{{ $type->name }}</option>
                                    @endforeach
                                @else
                                    <option value="terrain">Terrain</option>
                                    <option value="satellite">Satellite</option>
                                    <option value="roadmap">Roadmap</option>
                                    <option value="hybrid">Hybrid</option>
                                @endif
                            </select>
                            <div class="text-muted">Note: Default - Terrain</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 text-right"><b class="text-uppercase">Zoom Level</b></label>
                        <div class="col-md-3">
                            <select name="zoom_level_id" id="" class="form-control">
                                <option value="">SELECT ONE</option>
                                @if ($map_zoom_levels->count() > 0)
                                    @foreach ($map_zoom_levels as $level)
                                        <option value="{{ $level->id }}" @if ($level->in_use === 1) selected @endif>{{ $level->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="text-muted">Note: Default - Street</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 text-right"><b class="text-uppercase">Latitude</b></label>
                        <div class="col-md-3">
                            <input type="text" name="latitude" class="form-control" value="{{ $map_setting->latitude ?? '' }}">
                            <div class="text-muted">This will be used to initialize location on the map.</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 text-right"><b class="text-uppercase">Longitude</b></label>
                        <div class="col-md-3">
                            <input type="text" name="longitude" class="form-control" value="{{ $map_setting->longitude ?? '' }}">
                            <div class="text-muted">This will be used to initialize location on the map.</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                        </div>
                    </div>
                    {{-- <input type="hidden" name="map_setting_id" value="{{ $map_setting_id ?? null }}"> --}}
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-scripts')
@endsection
