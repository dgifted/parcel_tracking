@extends('layouts.main-master')
@section('title', 'General Settings')

@section('page-styles')
@stop

@section('page-title', 'General Settings')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label"><i class="fas fa-cog  text-secondary"></i> General Settings</h3>
                </div>
            </div>
            <div class="card-body">
                @if (session('error'))               
                <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                    <div class="alert-icon">
                        <i class="flaticon-warning"></i>
                    </div>
                    <div class="alert-text">
                        <i class="fas fa-exclamation text-danger"></i> {{ session('error') }}
                    </div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="ki ki-close"></i>
                            </span>
                        </button>
                    </div>
                </div>
                @endif
                <div class="example-preview">
                    <ul class="nav nav-pills" id="myTab1" role="tablist">
                        <!--begin::Shipment type tab-->
                        <li class="nav-item">
                            <a class="nav-link active" id="shipment-type-tab-1" data-toggle="tab" href="#shipment-type">
                                <span class="nav-icon">
                                    <i class="fab la-avianex text-warning"></i>
                                </span>
                                <span class="nav-text text-uppercase"><b>Shipment Type</b></span>
                            </a>
                        </li>
                        <!--end::Shipment type tab-->

                        <!--begin::Shipment mode tab-->
                        <li class="nav-item">
                            <a class="nav-link" id="shipment-mode-tab-1" data-toggle="tab" href="#shipment-mode">
                                <span class="nav-icon">
                                    <i class="fab la-fly text-warning"></i>
                                </span>
                                <span class="nav-text text-uppercase"><b>Shipment Mode</b></span>
                            </a>
                        </li>
                        <!--end::Shipment mode tab-->

                        <!--begin::Shipment carrier tab-->
                        <li class="nav-item">
                            <a class="nav-link" id="shipment-carrier-tab-1" data-toggle="tab" href="#shipment-carrier">
                                <span class="nav-icon">
                                    <i class="fab la-ups text-warning"></i>
                                </span>
                                <span class="nav-text text-uppercase"><b>Carrier</b></span>
                            </a>
                        </li>
                        <!--end::Shipment carrier tab-->

                        <!--begin::Payment mode tab-->
                        <li class="nav-item">
                            <a class="nav-link" id="payment-mode-tab-1" data-toggle="tab" href="#payment-mode">
                                <span class="nav-icon">
                                    <i class="far fa-credit-card text-warning"></i>
                                </span>
                                <span class="nav-text text-uppercase"><b>Payment Mode</b></span>
                            </a>
                        </li>
                        <!--end::Payment mode tab-->

                        <!--begin::Shipment status tab-->
                        <li class="nav-item">
                            <a class="nav-link" id="shipment-status-tab-1" data-toggle="tab" href="#shipment-status">
                                <span class="nav-icon">
                                    <i class="flaticon2-hourglass-1 text-warning"></i>
                                </span>
                                <span class="nav-text text-uppercase"><b>Shipment Status</b></span>
                            </a>
                        </li>
                        <!--end::Shipment status tab-->
                    </ul>
                    <hr>
                    <div class="tab-content mt-5" id="myTabContent1">
                        <!--begin::Shipment type content-->
                        <div class="tab-pane fade show active" id="shipment-type" role="tabpanel" aria-labelledby="shipment-type-tab-1">
                            <div class="d-flex justify-content-end">
                                <button id="add-shipment-type" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add</button>
                            </div>

                            <div class="row">
                                <div class="col-md-6 type-table">
                                    <table class="table table-responsive table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th scope="col">Name</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($shipment_types as $type)
                                            <tr>
                                                <td class="text-uppercase">{{ $type->name }}</td>
                                                <td title="{{ $type->description }}" data-toggle="tooltip" data-container="body" data-placement="right" style="max-width: 150px;">{{ $type->shortDescription() ?? '' }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-light btn-sm p-1" title="Edit"
                                                        data-toggle="modal" data-target="#staticBackdrop"
                                                        data-id="{{ $type->id }}" data-model="Shipment Type"
                                                        data-name="{{ $type->name }}" data-description="{{ $type->description }}">
                                                        <i class="far fa-edit text-primary"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-light btn-sm p-1" title="Delete" data-toggle="modal" data-target="#staticBackdropDelete" data-id="{{ $type->id }}" data-model="Shipment Type">
                                                        <i class="far fa-trash-alt text-danger"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <div id="shipment-type-add" class="pt-5" style="display: none;">
                                        <br>
                                        <form action="{{ route('admin.add-shipment-type') }}" method="POST">
                                            @csrf
                                            @include('includes.forms.create-edit')
                                            <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Shipment type content-->

                        <!--begin::Shipment mode content-->
                        <div class="tab-pane fade" id="shipment-mode" role="tabpanel" aria-labelledby="shipment-mode-tab-1">
                            <div class="d-flex justify-content-end">
                                <button id="add-shipment-mode" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add</button>
                            </div>

                            <div class="row">
                                <div class="col-md-6 mode-table">
                                    <div class="container-fluid">
                                    <table class="table table-responsive table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th scope="col">Name</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($shipment_modes as $mode)
                                            <tr>
                                                <td class="text-uppercase">{{ $mode->name }}</td>
                                                <td title="{{ $mode->description }}" data-toggle="tooltip" data-container="body" data-placement="right" style="max-width: 150px;">{{ $mode->shortDescription() ?? '' }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-light btn-sm p-1" title="Edit"
                                                        data-toggle="modal" data-target="#staticBackdrop"
                                                        data-id="{{ $mode->id }}" data-model="Shipment Mode"
                                                        data-name="{{ $mode->name }}" data-description="{{ $mode->description }}">
                                                        <i class="far fa-edit text-primary"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-light btn-sm p-1" title="Delete" data-toggle="modal" data-target="#staticBackdropDelete" data-id="{{ $mode->id }}" data-model="Shipment Mode">
                                                        <i class="far fa-trash-alt text-danger"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="shipment-mode-add" class="pt-5" style="display: none;">
                                        <br>
                                        <form action="{{ route('admin.add-shipment-mode') }}" method="POST">
                                            @csrf
                                            @include('includes.forms.create-edit')
                                            <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Shipment mode content-->

                        <!--begin::Shipment carrier content-->
                        <div class="tab-pane fade" id="shipment-carrier" role="tabpanel" aria-labelledby="shipment-carrier-tab-1">
                            <div class="d-flex justify-content-end">
                                <button id="add-shipment-carrier" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add</button>
                            </div>

                            <div class="row">
                                <div class="col-md-6 carrier-table">
                                    <div class="container-fluid">
                                    <table class="table table-responsive table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th scope="col">Name</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($carriers as $carrier)
                                            <tr>
                                                <td class="text-uppercase">{{ $carrier->name }}</td>
                                                <td title="{{ $carrier->description }}" data-toggle="tooltip" data-container="body" data-placement="right" style="max-width: 150px;">{{ $carrier->shortDescription() ?? '' }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-light btn-sm p-1" title="Edit"
                                                    data-toggle="modal" data-target="#staticBackdrop"
                                                    data-id="{{ $carrier->id }}" data-model="Carrier"
                                                    data-name="{{ $carrier->name }}" data-description="{{ $carrier->description }}">
                                                        <i class="far fa-edit text-primary"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-light btn-sm p-1" title="Delete" data-toggle="modal" data-target="#staticBackdropDelete" data-id="{{ $carrier->id }}" data-model="Carrier">
                                                        <i class="far fa-trash-alt text-danger"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="shipment-carrier-add" class="pt-5" style="display: none;">
                                        <br>
                                        <form action="{{ route('admin.add-carrier') }}" method="POST">
                                            @csrf
                                            @include('includes.forms.create-edit')
                                            <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Shipment Carrier content-->

                        <!--begin::Payment mode content-->
                        <div class="tab-pane fade" id="payment-mode" role="tabpanel" aria-labelledby="payment-mode-tab-1">
                            <div class="d-flex justify-content-end">
                                <button id="add-payment-mode" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add</button>
                            </div>

                            <div class="row">
                                <div class="col-md-6 payment-table">
                                    <div class="container-fluid">
                                    <table class="table table-responsive table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th scope="col">Name</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($payment_modes as $p_mode)
                                            <tr>
                                                <td class="text-uppercase">{{ $p_mode->name }}</td>
                                                <td title="{{ $p_mode->description }}" data-toggle="tooltip" data-container="body" data-placement="right" style="max-width: 150px;">{{ $p_mode->shortDescription() ?? '' }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-light btn-sm p-1" title="Edit"
                                                        data-toggle="modal" data-target="#staticBackdrop"
                                                        data-id="{{ $p_mode->id }}" data-model="Payment Mode"
                                                        data-name="{{ $p_mode->name }}" data-description="{{ $p_mode->description }}">
                                                        <i class="far fa-edit text-primary"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-light btn-sm p-1" title="Delete" data-toggle="modal" data-target="#staticBackdropDelete" data-id="{{ $p_mode->id }}" data-model="Payment Mode">
                                                        <i class="far fa-trash-alt text-danger"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="payment-mode-add" class="pt-5" style="display: none;">
                                        <br>
                                        <form action="{{ route('admin.add-payment-mode') }}" method="POST">
                                            @csrf
                                            @include('includes.forms.create-edit')
                                            <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Payment mode content-->

                        <!--begin::Shipment status content-->
                        <div class="tab-pane fade" id="shipment-status" role="tabpanel" aria-labelledby="shipment-status-tab-1">
                            <div class="d-flex justify-content-end">
                                <button id="add-shipment-status" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add</button>
                            </div>

                            <div class="row">
                                <div class="col-md-6 status-table">
                                    <div class="container-fluid">
                                    <table class="table table-responsive table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th scope="col">Name</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($shipment_statuses as $status)
                                            <tr>
                                                <td class="text-uppercase">{{ $status->name }}</td>
                                                <td title="{{ $status->description }}" data-toggle="tooltip" data-container="body" data-placement="right" style="max-width: 150px;">{{ $status->shortDescription() ?? '' }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-white btn-sm p-1" title="Edit"
                                                        data-toggle="modal" data-target="#staticBackdrop"
                                                        data-id="{{ $status->id }}" data-model="Shipment Status"
                                                        data-name="{{ $status->name }}" data-description="{{ $status->description }}">
                                                        <i class="far fa-edit text-primary"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-light btn-sm p-1" title="Delete" data-toggle="modal" data-target="#staticBackdropDelete" data-id="{{ $status->id }}" data-model="Shipment Status">
                                                        <i class="far fa-trash-alt text-danger"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="shipment-status-add" class="pt-5" style="display: none;">
                                        <br>
                                        <form action="{{ route('admin.add-shipment-status') }}" method="POST">
                                            @csrf
                                            @include('includes.forms.create-edit')
                                            <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Payment mode content-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.modals.model-update')
@include('includes.modals.model-delete')
@stop

@section('page-scripts')
<script>
$('#add-shipment-type').on('click', function(event) {
    $('#shipment-type-add').show();
    $('.col-md-6.type-table').addClass('d-none d-sm-block');
    $(this).prop('disabled', true);
});

$('#add-shipment-mode').on('click', function(event) {
    $('#shipment-mode-add').show();
    $('.col-md-6.mode-table').addClass('d-none d-sm-block');
    $(this).prop('disabled', true);
});

$('#add-shipment-carrier').on('click', function(event) {
    $('#shipment-carrier-add').show();
    $('.col-md-6.carrier-table').addClass('d-none d-sm-block');
    $(this).prop('disabled', true);
});

$('#add-payment-mode').on('click', function(event) {
    $('#payment-mode-add').show();
    $('.col-md-6.payment-table').addClass('d-none d-sm-block');
    $(this).prop('disabled', true);
});

$('#add-shipment-status').on('click', function(event) {
    $('#shipment-status-add').show();
    $('.col-md-6.status-table').addClass('d-none d-sm-block');
    $(this).prop('disabled', true);
});

$('#staticBackdrop').on('shown.bs.modal', function(event) {
    // event.preventDefault();

    var button = $(event.relatedTarget);
    var id = +button.data('id');
    var model = button.data('model');
    var modelName = button.data('name');
    var modelDescription = button.data('description');

    modelText = model.replace(' ', '-');
    var modelUrl = modelText.toLowerCase();
    // console.log(modelUrl);

    var modal = $(this);
    modal.find('.modal-title').text('Edit ' + model);
    modal.find('#name').val(modelName);
    modal.find('#description').val(modelDescription);

    modal.find('form').attr('action', '/' + modelUrl + '/' + id);

});

$('#staticBackdropDelete').on('shown.bs.modal', function(event) {
    var button = $(event.relatedTarget);
    var id = +button.data('id');
    var model = button.data('model');
    modelText = model.replace(' ', '-');
    var modelUrl = modelText.toLowerCase();
    
    var modal = $(this);
    modal.find('#model-name').text(model);
    modal.find('#btn-delete').attr('href', '/' + modelUrl + '-delete/' + id);
});
</script>

@endsection
