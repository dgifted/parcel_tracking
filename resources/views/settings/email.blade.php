@extends('layouts.main-master')
@section('title', 'Email Settings')

@section('page-styles')
@stop

@section('page-title', 'Email Settings')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label"><i class="fas fa-envelope text-secondary"></i> Email Setup</h3>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.update-email-setting') }}" method="POST">
                        @csrf

                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-4 col-sm-12">
                                <b class="text-uppercase">Activate Client Email Notification?</b>
                            </label>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <input data-switch="true" type="checkbox" name="client_email_notify"
                                    @if ($email_setting->client_email_notify === 1)
                                        checked="checked"
                                    @endif
                                />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-4 col-sm-12">
                                <b class="text-uppercase">Select Shipment Status to send email Notification</b>
                            </label>
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                {{-- <div class="checkbox-inline"> --}}
                                @foreach ($shipment_statuses as $status)
                                    <div class="row pt-2">
                                        <span class="col-3">{{ $status->name }}</span>
                                        <div class="col-3 text-left">
                                            <span class="switch switch-outline switch-icon switch-success">
                                                <label>
                                                    <input type="checkbox"  name="{{ $status->input_name }}"
                                                        @if (in_array($status->input_name, $email_setting->status_email_to_notify))
                                                            checked="checked"
                                                        @endif
                                                    />
                                                    <span></span>
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                                {{-- </div> --}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-4 col-sm-12">
                                <b class="text-uppercase">Domain Email</b>
                            </label>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <input class="form-control" name="domain_email" type="email" placeholder="info&commat;{{ parse_url(config('app.url'))['host'] }}" id="example-text-input"
                                value="{{ $email_setting->domain_email }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-4 col-sm-12">
                                <b class="text-uppercase">CC</b>
                            </label>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <input id="kt_tagify_1" class="form-control tagify" name='cc' placeholder='type...' value="{{ $email_setting->cc }}" data-blacklist='.NET,PHP' />
                                <div class="mt-3">
                                    <a href="javascript:;" id="kt_tagify_1_remove" class="btn btn-sm btn-light-primary font-weight-bold">Clear emails</a>
                                </div>
                                <div class="mt-3 text-muted">You can add multiple email. Hit enter at the end of each email</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-4 col-sm-12">
                                <b class="text-uppercase">BCC</b>
                            </label>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <input id="kt_tagify_1_2" class="form-control tagify" name='bcc' placeholder='type...' value="{{ $email_setting->bcc }}" data-blacklist='.NET,PHP' />
                                <div class="mt-3">
                                    <a href="javascript:;" id="kt_tagify_1_remove_2" class="btn btn-sm btn-light-primary font-weight-bold">Clear emails</a>
                                </div>
                                <div class="mt-3 text-muted">You can add multiple email. Hit enter at the end of each email</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-4 col-sm-12">
                                <b class="text-uppercase">Subject</b>
                            </label>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <input type="text" class="form-control" name="subject" id="" placeholder="enter mail subject" value="{{ $email_setting->subject }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-4 col-sm-12">
                                <b class="text-uppercase">Message Content</b>
                            </label>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <textarea class="form-control" name="content" id="" rows="5">{{ $email_setting->content }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-2 col-sm-12">
                                <button type="submit" class="btn btn-block btn-primary">Save Changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-scripts')
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-switch.js?v=7.0.4') }}"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/tagify.js?v=7.0.4') }}"></script>
@endsection
