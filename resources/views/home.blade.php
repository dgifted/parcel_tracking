@extends('layouts.main-master')
@section('title', 'Admin Dashboard')

@section('page-styles')
<link href="{{ url('//www.amcharts.com/lib/3/plugins/export/export.css?v=7.0.4') }}" rel="stylesheet" type="text/css" />
@stop

@section('page-title', 'Dashboard')

@section('content')
<!--begin::Dashboard-->
<!--begin::Row-->
<div class="row">
    <div class="col-xl-3">
        <!--begin::Stats Widget 25-->
        <div class="card card-custom bg-light-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
                <i class="flaticon2-hourglass-1 icon-2x"></i>
                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $shipment_statuses[0]->shipment_count }}</span>
                <span class="font-weight-bold text-muted font-size-md text-uppercase">{{ $shipment_statuses[0]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 25-->
    </div>
    <div class="col-xl-3">
        <!--begin::Stats Widget 26-->
        <div class="card card-custom bg-light-danger card-stretch gutter-b">
            <!--begin::ody-->
            <div class="card-body">
                <i class="flaticon2-checkmark icon-2x"></i>
                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $shipment_statuses[1]->shipment_count }}</span>
                <span class="font-weight-bold text-muted font-size-md text-uppercase">{{ $shipment_statuses[1]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 26-->
    </div>
    <div class="col-xl-3">
        <!--begin::Stats Widget 27-->
        <div class="card card-custom bg-light-secondary card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
                <i class="flaticon-location icon-2x text-dark"></i>
                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $shipment_statuses[2]->shipment_count }}</span>
                <span class="font-weight-bold text-muted font-size-md text-uppercase">{{ $shipment_statuses[2]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 27-->
    </div>
    <div class="col-xl-3">
        <!--begin::Stats Widget 28-->
        <div class="card card-custom bg-light-warning card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
                <i class="flaticon2-paperplane icon-2x"></i>
                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $shipment_statuses[3]->shipment_count }}</span>
                <span class="font-weight-bold text-muted font-size-md text-uppercase">{{ $shipment_statuses[3]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stat: Widget 28-->
    </div>
</div>
<!--end::Row-->
<div class="row">
    <div class="col-xl-3">
        <!--begin::Stats Widget 29-->
        <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(assets/media/svg/shapes/abstract-1.svg)">
            <!--begin::Body-->
            <div class="card-body">
                <i class="flaticon2-delivery-truck icon-2x"></i>
                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $shipment_statuses[4]->shipment_count }}</span>
                <span class="font-weight-bold text-muted font-size-md text-uppercase">{{ $shipment_statuses[4]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 29-->
    </div>
    <div class="col-xl-3">
        <!--begin::Stats Widget 30-->
        <div class="card card-custom bg-info card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
                <i class="fas fa-road icon-2x text-light"></i>
                <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $shipment_statuses[5]->shipment_count }}</span>
                <span class="font-weight-bold text-white font-size-md text-uppercase">{{ $shipment_statuses[5]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 30-->
    </div>
    <div class="col-xl-3">
        <!--begin::Stats Widget 31-->
        <div class="card card-custom bg-danger card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
                <i class="flaticon-cancel icon-2x text-ligth"></i>
                <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $shipment_statuses[6]->shipment_count }}</span>
                <span class="font-weight-bold text-white font-size-md text-uppercase">{{ $shipment_statuses[6]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 31-->
    </div>
    <div class="col-xl-3">
        <!--begin::Stats Widget 32-->
        <div class="card card-custom bg-dark card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
                <i class="flaticon-security icon-2x text-ligth"></i>
                <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 text-hover-primary d-block">{{ $shipment_statuses[7]->shipment_count }}</span>
                <span class="font-weight-bold text-white font-size-md text-uppercase">{{ $shipment_statuses[7]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 32-->
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <!--begin::Stats Widget 25-->
        <div class="card card-custom bg-light-success card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
                <i class="flaticon2-reply icon-2x text-ligth"></i>
                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $shipment_statuses[8]->shipment_count }}</span>
                <span class="font-weight-bold text-muted font-size-md text-uppercase">{{ $shipment_statuses[8]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 25-->
    </div>
    <div class="col-md-3">
        <!--begin::Stats Widget 25-->
        <div class="card card-custom bg-light-danger card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
                <i class="fab la-first-order-alt icon-2x text-ligth"></i>
                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $shipment_statuses[9]->shipment_count ?? 0 }}</span>
                <span class="font-weight-bold text-muted font-size-md text-uppercase">{{ $shipment_statuses[9]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 25-->
    </div>
    <div class="col-md-3">
        <!--begin::Stats Widget 25-->
        <div class="card card-custom bg-light-secondary card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
                <i class="fas fa-blender icon-2x text-ligth"></i>
                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $shipment_statuses[10]->shipment_count ?? 0 }}</span>
                <span class="font-weight-bold text-muted font-size-md text-uppercase">{{ $shipment_statuses[10]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 25-->
    </div>
    <div class="col-md-3">
        <!--begin::Stats Widget 25-->
        <div class="card card-custom bg-light-warning card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body">
                <i class="fas fa-archive icon-2x text-ligth"></i>
                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $shipment_statuses[11]->shipment_count ?? 0 }}</span>
                <span class="font-weight-bold text-muted font-size-md text-uppercase">{{ $shipment_statuses[11]->name ?? '' }}</span>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 25-->
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">Date Based Data</h3>
                </div>
            </div>
            <div class="card-body">
                <div id="kt_amcharts_6" style="height: 500px;"></div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-scripts')
<script src="{{ url('//www.amcharts.com/lib/3/amcharts.js?v=7.0.4') }}"></script>
<script src="{{ url('//www.amcharts.com/lib/3/serial.js?v=7.0.4') }}"></script>
<script src="{{ url('//www.amcharts.com/lib/3/radar.js?v=7.0.4') }}"></script>
<script src="{{ url('//www.amcharts.com/lib/3/pie.js?v=7.0.4') }}"></script>
<script src="{{ url('//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js?v=7.0.4') }}"></script>
<script src="{{ url('//www.amcharts.com/lib/3/plugins/animate/animate.min.js?v=7.0.4') }}"></script>
<script src="{{ url('//www.amcharts.com/lib/3/plugins/export/export.min.js?v=7.0.4') }}"></script>
<script src="{{ url('//www.amcharts.com/lib/3/themes/light.js?v=7.0.4') }}"></script>

<script src="{{ asset('assets/js/pages/features/charts/amcharts/charts.js?v=7.0.4') }}"></script>

<!--begin::Page Vendors(used by this page)-->
<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.0.4') }}"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script src="{{ asset('assets/js/pages/widgets.js?v=7.0.4') }}"></script>
<!--end::Page Scripts-->
@endsection
