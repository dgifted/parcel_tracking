<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title" id="exampleModalLabel">Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form action="" method="POST">
                @csrf

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-3 text-right"><b class="text-uppercase">Name</b></label>
                        <div class="col-9">
                            <input type="text" name="name" id="name" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-3 text-right"><b class="text-uppercase">Description</b></label>
                        <div class="col-9">
                            <textarea name="description" id="description" cols="" rows="3" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
