<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-light" id="exampleModalLabel">Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form action="" method="POST">
                @csrf

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-3 text-right"><b class="text-uppercase">Name</b></label>
                        <div class="col-9">
                            <input type="text" name="name" id="name" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-3 text-right"><b class="text-uppercase">Email</b></label>
                        <div class="col-9">
                            <input type="email" name="email" id="email" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class="col-3 text-right"><b class="text-uppercase">Phone</b></label>
                        <div class="col-9">
                            <input type="tel" name="phone" id="phone" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address" class="col-3 text-right"><b class="text-uppercase">Address</b></label>
                        <div class="col-9">
                            <input type="text" name="address" id="address" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="role" class="col-3 text-right"><b class="text-uppercase">Role</b></label>
                        <div class="col-9" id="role-slot">
                            <select name="role" id="role" class="form-control">
                                <option value="">Select one</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
