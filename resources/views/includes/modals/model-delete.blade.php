<div class="modal fade" id="staticBackdropDelete" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title text-light text-uppercase" id="exampleModalLabel">Are you sure to delete <span id="model-name"></span>?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cancel</button>
                <a href="#" id="btn-delete" class="btn btn-danger font-weight-bold">Confirm</a>
            </div>
        </div>
    </div>
</div>
