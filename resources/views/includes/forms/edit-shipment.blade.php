<form action="{{ route('admin.u') }}" method="post">
    <div class="row">
        <div class="col-12">
            {{-- <div class="card"> --}}
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="la la-exclamation-triangle icon-lg mr-4"></i>
                                Shipment Number
                            </span>
                        </div>
                        <input type="text" class="form-control" name="shipment_number" value="{{ $shipment->tracking_id ?? '' }}"/>
                    </div>
                </div>
            {{-- </div> --}}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Shipper Information</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Shipper Name</label>
                        <input type="text" name="shipper_name" class="form-control"  value="{{ $shipment->shipper->name }}"/>
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="tel" name="shipper_phone" class="form-control"  value="{{ $shipment->shipper->phone ?? '' }}"/>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="shipper_address" class="form-control"  value="{{ $shipment->shipper->address ?? '' }}"/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="shipper_email" class="form-control"  value="{{ $shipment->shipper->email ?? '' }}"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Receiver Information</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Receiver Name</label>
                        <input type="text" name="receiver_name" class="form-control"  value="{{ $shipment->receiver->name }}"/>
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="tel" name="receiver_phone" class="form-control"  value="{{ $shipment->receiver->phone }}"/>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="receiver_address" class="form-control"  value="{{ $shipment->receiver->address }}"/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="receiver_email" class="form-control"  value="{{ $shipment->receiver->email }}"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Shipment Information</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Type of shipment</label>
                                <select class="form-control select2" id="kt_select2_1" name="param">
                                    @if ($shipment_types->count() > 0)
                                        @foreach ($shipment_types as $s_type)
                                            <option value="{{ $s_type->id }}" @if ($shipment->shipmentInfo->shipmentTypes->first()->id === $s_type->id) selected @endif>
                                                {{ $s_type->name }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Mode</label>
                                <select class="form-control select2" id="kt_select2_1_1" name="param">
                                    @if ($shipment_modes->count() > 0)
                                        @foreach ($shipment_modes as $mode)
                                            <option value="{{ $mode->id }}" @if ($shipment->shipmentInfo->shipmentModes->first()->id === $mode->id) selected @endif>
                                                {{ $mode->name }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Total Freight</label>
                                <input type="number" name="receiver_name" class="form-control"  value="{{ $shipment->shipmentInfo->total_freight ?? '' }}"/>
                            </div>
                            <div class="form-group">
                                <label>Departure Time</label>
                                {{-- <div class="col-lg-4 col-md-9 col-sm-12"> --}}
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="Select date and time" id="kt_datetimepicker_5" value="{{ $shipment->shipmentInfo->departure_time }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                {{-- </div> --}}
                            </div>
                            <div class="form-group">
                                <label>Destination</label>
                                <select class="form-control select2" id="kt_select2_1_2" name="param">
                                    @if ($countries->count()  > 0)
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->id }}" @if($shipment->shipmentInfo->destination->country_id === $country->id) selected @endif>{{ $country->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pickup Time</label>
                                {{-- <div class="col-lg-4 col-md-9 col-sm-12"> --}}
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="Select date and time" id="kt_datetimepicker_5_1" value="{{ $shipment->shipmentInfo->pickup_time ?? '' }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                {{-- </div> --}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Courier</label>
                                <input type="text" name="receiver_name" class="form-control"  value="{{ $shipment->shipmentInfo->carriers->first()->name }}"/>
                            </div>
                            <div class="form-group">
                                <label>Payment Mode</label>
                                <select class="form-control select2" id="kt_select2_1_3" name="param">
                                    <option value="AK">Select one</option>
                                    @if ($payment_modes->count() > 0)
                                        @foreach ($payment_modes as $p_mode)
                                            <option value="{{ $p_mode->id }}" @if($shipment->shipmentInfo->paymentModes->first()->id === $p_mode->id) selected @endif>{{ $p_mode->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Carrier</label>
                                <select class="form-control select2" id="kt_select2_1_4" name="param">
                                    @if ($carriers->count() > 0)
                                        @foreach ($carriers as $carrier)
                                            <option value="{{ $carrier->id }}" @if($shipment->shipmentInfo->carriers->first()->id === $carrier->id) selected @endif>
                                                {{ $carrier->name }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Origin</label>
                                <select class="form-control select2" id="kt_select2_1_5" name="param">
                                    @if ($countries->count() > 0)
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->id }}" @if($shipment->shipmentInfo->origin->country_id === $country->id) selected @endif>
                                                {{ $country->name }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">City</label>
                                <input type="text" name="origin_city" id="" class="form-control" readonly value="{{ $shipment->shipmentInfo->origin->city ?? '' }}">
                            </div>
                            <div class="form-group">
                                <label>Pickup Date</label>
                                {{-- <div class="col-lg-4 col-md-9 col-sm-12"> --}}
                                    <div class="input-group date">
                                        <input type="text" class="form-control" readonly="readonly" value="{{ $shipment->shipmentInfo->pickup_date }}" id="kt_datepicker_3" />
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                {{-- </div> --}}
                            </div>
                            <div class="form-group">
                                <label>Expected Delivery Date</label>
                                {{-- <div class="col-lg-4 col-md-9 col-sm-12"> --}}
                                    <div class="input-group date">
                                        <input type="text" class="form-control" readonly="readonly" value="{{ $shipment->shipmentInfo->expected_delivery_date }}" id="kt_datepicker_3" />
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                {{-- </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="exampleTextarea">Comments</label>
                                <textarea class="form-control" rows="3">{{ $shipment->shipmentInfo->comment ?? '' }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Package(s)</h3>
                </div>
                <div class="card-body">
                    <div id="kt_repeater_1">
                        <div class="form-group row">
                            <div data-repeater-list="packages" class="col-lg-12">
                                @if ($shipment->packages->count() > 0)
                                @foreach ($shipment->packages as $k => $package)
                                    <div data-repeater-item="{{ 'package_' . $k }}" class="form-group row align-items-start">
                                        <div class="col-md-2">
                                            <label>Qty:</label>
                                            <input type="number" class="form-control" value="{{ $package->quantity }}"/>
                                            <div class="d-md-none mb-2"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Piece Type:</label>
                                            <select class="form-control select2" id="kt_select2_1_6" name="piece_type">
                                                @if ($piece_types->count() > 0 )
                                                @foreach ($piece_types as $p_type)
                                                    <option value="{{ $p_type->id }}" @if($package->pieceType->id === $p_type->id) selected @endif>
                                                        {{ $p_type->name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                            </select>
                                            <div class="d-md-none mb-2"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Description</label>
                                            <textarea class="form-control" rows="3">{{ $package->description }}</textarea>
                                            <div class="d-md-none mb-2"></div>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Weight @if($package_setting->enable_dimension === 1) ({{ $package_setting->weightUnits->first()->abbreviation }}) @endif</label>
                                            <input type="text" class="form-control" value="{{ $package->unit_weight }}"/>
                                            <div class="d-md-none mb-2"></div>
                                        </div>
                                        <div class="col-md-2">
                                            <label style="visibility: hidden">Button</label><br>
                                            <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                                            <i class="la la-trash-o"></i>Delete</a>
                                        </div>
                                    </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                        <br>
                        {{-- <div class="form-group row"> --}}
                            {{-- <label class="col-lg-2 col-form-label text-right"></label> --}}
                            {{-- <div class="col-lg-12 text-left">
                                <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">
                                <i class="la la-plus"></i>Add package</a>
                            </div> --}}
                        {{-- </div> --}}
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="d-flex justify-content-between">
                                <span>Total Volume Weight: <b> @if($package_setting->enable_dimension === 1) {{ $package_setting->weightUnits->first()->abbreviation }} @endif</b><input class="form-control" type="text" name="total_volume" value="{{ $volume_weight }}"></span>
                                <span>Total Actual Weight: <b> @if($package_setting->enable_dimension === 1) {{ $package_setting->weightUnits->first()->abbreviation }} @endif</b><input class="form-control" type="text" name="total_weight" value="{{ $weight }}"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Assign Shipment To</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Client</label>
                                <select class="form-control select2" id="kt_select2_1_7" name="param">
                                    @if ($clients->count() > 0)
                                        @foreach ($clients as $client)
                                            <option value="{{ $client->id }}" @if($shipment->client_id === $client->id) selected @endif>
                                                {{ $client->name }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Agent</label>
                                <select class="form-control select2" id="kt_select2_1_8" name="param">
                                    @if ($agents->count() > 0)
                                        @foreach ($agents as $agent)
                                            <option value="{{ $agent->id }}" @if($shipment->agent_id === $agent->id) selected @endif>
                                                {{ $agent->name }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">History</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Date</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" readonly="readonly" value="{{ $shipment->histories->first()->date }}" id="kt_datepicker_3" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Time</label>
                                <div class="input-group timepicker">
                                    <input class="form-control" id="kt_timepicker_3" readonly="readonly" placeholder="Select time" type="text" value="{{ $shipment->histories->first()->time }}"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-clock-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Location</label>
                                <select class="form-control select2" id="kt_select2_1_9" name="param">
                                    @if ($countries->count() > 0)
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->id }}" @if($shipment->histories->first()->locations->first()->country_id === $country->id) selected @endif>
                                                {{ $country->name }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">City</label>
                                <input type="text" class="form-control" readonly value="{{ $shipment->histories->first()->locations->first()->city ?? '' }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control select2" id="kt_select2_1_10" name="param">
                                   @if ($shipment_statuses->count() > 0)
                                        @foreach ($shipment_statuses as $status)
                                            <option value="{{ $status->id }}" @if($shipment->histories->first()->shipmentStatuses->first()->id === $status->id) selected @endif>
                                                {{ $status->name }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            {{-- <div class="col-12"> --}}
                                <div class="form-group">
                                    <label for="exampleTextarea">Remark</label>
                                    <textarea class="form-control" rows="5">{{ $shipment->histories->first()->remark }}</textarea>
                                </div>
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-block">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>

