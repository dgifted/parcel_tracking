<form action="{{ route('admin.store-shipment') }}" method="POST">
    @csrf

    <div class="row">
        <div class="col-12">
            {{-- <div class="card"> --}}
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-barcode icon-lg mr-4 text-primary"></i>
                                <b class="text-uppercase">Shipment Number</b>
                            </span>
                        </div>
                        <input type="text" class="form-control" name="shipment_track_id" value="{{ $tracking_id }}" style="line-height: 1.2em;"/>
                        <div class="input-group-append">
                            <button class="btn btn-light" type="button" id="btn-generate-id">CHANGE ID<i class="fas fa-redo-alt ml-3 text-primary"></i></button>
                        </div>
                    </div>
                </div>
            {{-- </div> --}}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Shipper Information</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Shipper Name</label>
                        @if ($shippers->count() > 0)
                            <select class="form-control select2" id="kt_select2_1_11" name="shipper_name">
                                <option value="">Select one</option>
                                @foreach ($shippers as $shipper)
                                    <option value="{{ $shipper->id }}">{{ $shipper->name }}</option>
                                @endforeach
                            </select>
                        @else
                            <input type="text" name="shipper_name" class="form-control"  value="{{ old('shipper_name') }}"/>
                        @endif

                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="shipper_email" class="form-control"  value="{{ old('shipper_email') }}"/>
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="tel" name="shipper_phone" class="form-control"  value="{{ old('shipper_phone') }}"/>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="shipper_address" class="form-control"  value="{{ old('shipper_address') }}"/>
                    </div>

                    {{-- <div class="form-group" style="visibility: hidden;">
                        <label>Email</label>
                        <input type="email" disabled/>
                    </div> --}}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Receiver Information</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Receiver Name</label>
                        <input type="text" name="receiver_name" class="form-control"  value="{{ old('receiver_name') }}" autocomplete="off"/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="receiver_email" id="receiver_email" class="form-control"  value="{{ old('receiver_email') }}"/>
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="tel" name="receiver_phone" class="form-control"  value="{{ old('receiver_phone') }}" autocomplete="off"/>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="receiver_address" class="form-control"  value="{{ old('receiver_address') }}" autocomplete="off"/>
                    </div>

                    {{-- <div class="form-group">
                        <label>Carrier Reference No.</label>
                        <input type="text" name="receiver_name" class="form-control"  value="{{ $data->carrier_ref_number ?? '' }}"/>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Shipment Information</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Type of shipment</label>
                                <select class="form-control select2" id="kt_select2_1" name="shipment_type">
                                    <option value="">Select one</option>
                                    @if ($shipment_types->count() > 0)
                                        @foreach ($shipment_types as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Mode</label>
                                <select class="form-control select2" id="kt_select2_1_1" name="shipment_mode">
                                    <option value="">Select one</option>
                                    @if ($shipment_modes->count() > 0)
                                        @foreach ($shipment_modes as $mode)
                                            <option value="{{ $mode->id }}">{{ $mode->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Total Freight</label>
                                <input type="number" name="total_freight" class="form-control"  value="{{ $data->carrier_ref_number ?? '' }}"/>
                            </div>
                            <div class="form-group">
                                <label>Departure Time</label>
                                {{-- <div class="col-lg-4 col-md-9 col-sm-12"> --}}
                                    <div class="input-group date">
                                        <input type="text" class="form-control" name="departure_time" placeholder="Select date and time" id="kt_datetimepicker_5" autocomplete="off"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                {{-- </div> --}}
                            </div>
                            <div class="form-group">
                                <label>Destination</label>
                                <select class="form-control select2" id="kt_select2_1_2" name="destination">
                                    <option value="">Select one</option>
                                    @if ($countries->count() > 0)
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group" id="city" style="display: none;">
                                <label>City</label><br>
                                <select class="form-control select2" id="kt_select2_1_12" name="destination_city" style="width: 100%;">
                                    {{-- <option value="">Select One</option> --}}
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pickup Time</label>
                                {{-- <div class="col-lg-4 col-md-9 col-sm-12"> --}}
                                    <div class="input-group date">
                                        <input type="text" class="form-control" name="pickup_time" placeholder="Select date and time" id="kt_datetimepicker_5_1" autocomplete="off"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                {{-- </div> --}}
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="currency">Currency</label>
                                        <select name="currency" id="currency" class="form-control">
                                            <option value="&dollar;">&dollar;</option>
                                            <option value="&euro;">&euro;</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="cost">Shipment Cost (in USD)</label>
                                        <input type="text" name="cost" id="cost" class="form-control" placeholder="$ 2000">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Courier</label>
                                <input type="text" name="courier" class="form-control"  value=""/>
                            </div>
                                <div class="form-group">
                                    <label>Payment Mode</label>
                                    <select class="form-control select2" id="kt_select2_1_3" name="payment_mode">
                                        <option value="">Select one</option>
                                        @if ($payment_modes->count() > 0)
                                            @foreach ($payment_modes as $p_mode)
                                                <option value="{{ $p_mode->id }}">{{ $p_mode->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            <div class="form-group">
                                <label>Carrier</label>
                                <select class="form-control select2" id="kt_select2_1_4" name="carrier">
                                    <option value="">Select one</option>
                                    @if ($carriers->count() > 0)
                                        @foreach ($carriers as $carrier)
                                            <option value="{{ $carrier->id }}">{{ $carrier->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Origin</label>
                                <select class="form-control select2" id="kt_select2_1_5" name="origin">
                                    <option value="">Select one</option>
                                    @if ($countries->count() > 0)
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group" id="city_1" style="display: none;">
                                <label>City</label><br>
                                <select class="form-control select2" id="kt_select2_1_13" name="origin_city" style="width: 100%;">
                                    {{-- <option value="">Select One</option> --}}
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pickup Date</label>
                                {{-- <div class="col-lg-4 col-md-9 col-sm-12"> --}}
                                    <div class="input-group date">
                                        <input type="text" name="pickup_date" class="form-control" readonly="readonly" value="" id="kt_datepicker_3" />
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                {{-- </div> --}}
                            </div>
                            <div class="form-group">
                                <label>Expected Delivery Date</label>
                                {{-- <div class="col-lg-4 col-md-9 col-sm-12"> --}}
                                    <div class="input-group date">
                                        <input type="text" name="expected_delivery_date" class="form-control" readonly="readonly" value="" id="kt_datepicker_3" />
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                {{-- </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="exampleTextarea">Comments</label>
                                <textarea name="comment" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Package(s)</h3>
                </div>
                <div class="card-body">
                    <div id="kt_repeater_1">
                        <div class="form-group row">
                            <div data-repeater-list="packages" class="col-lg-12">
                                <div data-repeater-item="" class="form-group row align-items-start">
                                    <div class="col-md-2">
                                        <label>Qty:</label>
                                        <input type="text" name="quantity" class="form-control" />
                                        <div class="d-md-none mb-2"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Piece Type:</label>
                                        <select class="form-control select2" id="kt_select2_1_6" name="piece_type">
                                            <option value="">Select one</option>
                                            @if ($piece_types->count() > 0 )
                                                @foreach ($piece_types as $p_type)
                                                    <option value="{{ $p_type->id }}">{{ $p_type->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="d-md-none mb-2"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Description</label>
                                        <textarea name="description" class="form-control" rows="3"></textarea>
                                        <div class="d-md-none mb-2"></div>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Weight @if($package_setting->enable_dimension === 1) ({{ $package_setting->weightUnits->first()->abbreviation }}) @endif</label>
                                        <input type="text" name="weight" class="form-control" />
                                        <div class="d-md-none mb-2"></div>
                                    </div>
                                    <div class="col-md-2">
                                        <label style="visibility: hidden">Button</label><br>
                                        <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                                        <i class="la la-trash-o"></i>Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        @if ($package_setting->enable_multi_package_admin === 1)
                            <div class="form-group row">
                                {{-- <label class="col-lg-2 col-form-label text-right"></label> --}}
                                <div class="col-lg-12 text-left">
                                    <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">
                                    <i class="la la-plus"></i>Add package</a>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="d-flex justify-content-between">
                                <span>Total Volume Weight: <b> @if($package_setting->enable_dimension === 1) {{ $package_setting->weightUnits->first()->abbreviation }} @endif</b><input class="form-control" type="text" name="total_volume" value=""></span>
                                <span>Total Actual Weight: <b> @if($package_setting->enable_dimension === 1) {{ $package_setting->weightUnits->first()->abbreviation }} @endif</b><input class="form-control" type="text" name="total_weight" value=""></span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Assign Shipment To</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Client</label>
                                <select class="form-control select2" id="kt_select2_1_7" name="client">
                                    <option value="">Select one</option>
                                    @if ($clients->count() > 0)
                                        @foreach ($clients as $client)
                                            <option value="{{ $client->id }}">{{ $client->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Agent</label>
                                <select class="form-control select2" id="kt_select2_1_8" name="agent">
                                    <option value="">Select one</option>
                                    @if ($agents->count() > 0)
                                        @foreach ($agents as $agent)
                                            <option value="{{ $agent->id }}">{{ $agent->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">History</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Date</label>
                                <div class="input-group date">
                                    <input type="text" name="history_date" class="form-control" readonly="readonly" value="" id="kt_datepicker_3" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Time</label>
                                <div class="input-group timepicker">
                                    <input class="form-control" name="history_time" id="kt_timepicker_3" readonly="readonly" placeholder="Select time" type="text" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-clock-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Location</label>
                                <select class="form-control select2" id="kt_select2_1_9" name="location">
                                    <option value="">Select one</option>
                                    @if ($countries->count() > 0)
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group" id="city_2" style="display: none;">
                                <label>City</label><br>
                                <select class="form-control select2" id="kt_select2_1_14" name="history_city" style="width: 100%;">
                                    {{-- <option value="">Select One</option> --}}
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control select2" id="kt_select2_1_10" name="shipment_status">
                                    <option value="">Select one</option>
                                    @if ($shipment_statuses->count() > 0)
                                        @foreach ($shipment_statuses as $status)
                                            <option value="{{ $status->id }}">{{ $status->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            {{-- <div class="col-12"> --}}
                                <div class="form-group">
                                    <label for="exampleTextarea">Remark</label>
                                    <textarea name="remark" class="form-control" rows="5"></textarea>
                                </div>
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </div>
            </div>
        </div>
    </div>
</form>

