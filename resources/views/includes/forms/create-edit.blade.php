<div class="form-group">
    <label>Name</label>
    <input type="text" name="name" class="form-control form-control-solid" placeholder="Enter name"
    value="{{ old('name') ?? '' }}"/>
    {{-- value="{{ $data->name ?? '' }}"/> --}}
</div>
<div class="form-group">
    <label for="exampleTextarea">Description</label>
    <textarea name="description" class="form-control form-control-solid" rows="3">{{ old('description') ?? '' }}</textarea>
    {{-- <textarea name="description" class="form-control form-control-solid" rows="3">{{ $data->description ?? '' }}</textarea> --}}
</div>
