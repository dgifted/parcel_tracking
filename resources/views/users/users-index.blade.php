@extends('layouts.main-master')
@section('title', 'Users')

@section('page-styles')
@stop

@section('page-title', 'Users')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card custom-card">
                {{-- <div class="card-header flex-wrap border-0 pt-6 pb-0"> --}}
                <div class="card-header d-flex justify-content-between pb-0">
                    <div class="card-title">
                        <h3 class="card-label text-uppercase">All User</h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <a href="#" id="btn-add" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#staticBackdrop" data-type="add">
                        <span class="svg-icon svg-icon-md">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <circle fill="#000000" cx="9" cy="15" r="6" />
                                    <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>Add User</a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin::Search Form-->
                    <div class="mb-7">
                        <div class="row align-items-center">
                            <div class="col-lg-9 col-xl-8">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="d-flex align-items-center">
                                            <label class="mr-3 mb-0 d-none d-md-block">Role:</label>
                                            <select class="form-control" id="kt_datatable_search_role">
                                                <option value="all">All</option>
                                                @if ($roles->count() > 0)
                                                    @foreach ($roles as $role)
                                                        <option value="{{ $role->title }}">{{ ucwords($role->title) }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                            </div>
                        </div>
                    </div>
                    <!--end::Search Form-->
                    <!--begin: Datatable-->
                    <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                        <thead>
                            <tr>
                                <th title="Field #1">Name</th>
                                <th title="Field #2">Email</th>
                                <th title="Field #3">Phone</th>
                                <th title="Field #4">Address</th>
                                <th title="Field #5">Role</th>
                                <th title="Field #6">Password</th>
                                {{-- <th title="Field #7"></th> --}}
                                <th title="Field #8">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- <tr> --}}
                            @if ($users->count() > 0)
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->name ?? '' }}</td>
                                        <td>{{ $user->email ?? '' }}</td>
                                        <td>{{ $user->phone ?? '' }}</td>
                                        <td style="min-width: 200px;">{{ $user->address ?? 'N/A' }}</td>
                                        <td><span class="role">{{ $user->role ?? '' }}</span></td>
                                        <td>{{ $user->plainPassword->password_string ?? 'N/A' }}</td>
                                        {{-- <td class="text-right"></td> --}}
                                        <td class="text-right">
                                            <a href="#" class="btn-edit" data-toggle="modal" data-target="#staticBackdrop"  data-id="{{ $user->id }}" data-type="edit">
                                                <i class="far fa-edit text-primary"></i>
                                            </a> &nbsp;&nbsp;
                                            <a href="#" class="btn-delete" data-toggle="modal" data-target="#staticBackdropDelete" data-id="{{ $user->id }}">
                                                <i class="far fa-trash-alt text-danger"></i>
                                            </a>
                                        </td>
                                        {{-- <td></td> --}}
                                    </tr>
                                @endforeach
                            @else

                            @endif
                            {{-- </tr> --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('includes.modals.user-update')
    @include('includes.modals.model-delete')
@stop

@section('page-scripts')
<!--begin::Page Scripts(used by this page)-->
<script src="{{ asset('assets/js/pages/crud/ktdatatable/base/html-table.js?v=7.0.4') }}"></script>
<script>
    $('span.role').each(function() {
        $(this).prepend('<i class="fas fa-user mr-2"></i>');
        // $(this)
        var value = $(this).text();

        switch (value) {
            case 'admin':
                $(this).addClass('text-primary p-2 text-uppercase');
                break;
            case 'agent':
                $(this).addClass('text-info p-2 text-uppercase');
                break;
            case 'client':
                $(this).addClass('text-secondary p-2 text-uppercase');
                break;
            case 'receiver':
                $(this).addClass('text-warning p-2 text-uppercase');
                break;
            case 'shipper':
                $(this).addClass('text-danger p-2 text-uppercase');
                break;
        }
    });

    // $('.btn-edit').each(function() {
    //     $(this).on('click', function(event) {
    //         event.preventDefault();
    //         var userId = $(this).data('id');
    //         console.log(userId);
    //     });
    // });

    $('#staticBackdrop').on('shown.bs.modal', function(event) {
        var tBtn = $(event.relatedTarget);
        var modal = $(this);
        var actionType = tBtn.data('type');
        var userid = tBtn.data('id');
        var selRole = modal.find('#role');

        if(actionType === 'edit') {
            KTApp.block('#staticBackdrop .modal-content', {
                overlayColor: '#000000',
                state: 'danger',
                message: 'Please wait...'
            });
        }

        if(actionType === 'add') {
            modal.find('.modal-title').text('Add New User');
            modal.find('form').attr('action', '/users/store')
            modal.find('form').append('<input type="hidden" name="password" value="' + generateRandomPassword(8) + '"/>');
            selRole.text('');
            $.ajax({
                url: '/roles',
                type: 'GET',
                success: function(resp) {
                    resp.forEach(role => {
                        selRole.append('<option value="' + role.title + '">' + role.title +'</option>');
                    });
                },
                error: function(_,__,msg) {
                    KTApp.unblock();
                        alert(msg);
                }
            });
        } else if(actionType === 'edit') {
            modal.find('.modal-title').text('Edit User');
            modal.find('form').attr('action', '/users/' + userid + '/update');
            modal.find('#role').empty();
            $.ajax({
                url: '/users/' + userid,
                type: 'GET',
                success: function(resp) {
                    KTApp.unblock('#staticBackdrop .modal-content');
                    console.log(resp);
                    $('#name').val(resp.name);
                    $('#email').val(resp.email);
                    $('#phone').val(resp.phone);
                    $('#address').val(resp.address);
                    var item = `<option value="${resp.role}">${resp.role}</option>`;
                    modal.find('#role').append(item);
                },
                error: function(_, __, msg) {
                    KTApp.unblock('#staticBackdrop .modal-content');
                    alert(msg);
                }
            });
        }
    });

    $('#staticBackdrop').on('hidden.bs.modal', function(event) {
        $(this).find('.modal-title').text('');
        $(this).find('#name').val('');
        $(this).find('#email').val('');
        $(this).find('#phone').val('');
        $(this).find('#address').val('');
        $(this).find('#role').removeClass('d-none');
    });

    $('#staticBackdropDelete').on('shown.bs.modal', function(event) {
        var btn = $(event.relatedTarget);

        var userId = btn.data('id');
        var modal = $(this);
        modal.find('#model-name').text('User');
        modal.find('#btn-delete').attr('href', '/users/' + userId + '/delete');

    });

    function generateRandomPassword(length) {
        var stringSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-!@#$^&*';
        var password = '';
        var i = 0;
        for (i = 0; i < length; i++) {
            var index = Math.floor(Math.random() * Math.floor(stringSet.length + 1));
            password += stringSet.charAt(index);
        }

        return password;
    }
</script>
<!--end::Page Scripts-->
@endsection
