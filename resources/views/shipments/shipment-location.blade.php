@extends('layouts.main-master')
@section('title', 'Shipment Location')

@section('page-styles')
{{-- <script>
    #shipment_location_update {

    }
</script> --}}
@stop

@section('page-title', 'Shipment Location')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fas fa-barcode icon-lg mr-4 text-primary"></i>
                            <b class="text-uppercase">Tracking ID</b>
                        </span>
                    </div>
                    {{-- <input type="text" class="form-control" id="shipment_track_id" name="shipment_track_id" value="" style="line-height: 1.2em;"/> --}}
                    <select name="shipment_track_id" id="shipment_track_id" class="form-control">
                        <option value="">Select Tracking ID</option>
                        @if ($shipments->count() > 0)
                            @foreach ($shipments as $shipment)
                                <option value="{{ $shipment }}">{{ $shipment }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div id="location-map-canvas" class="row" style="display: none;">
        <div class="col-md-8">
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">History Map</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="#" class="btn btn-icon btn-light-primary mr-1" data-placement="top" title="View shipment details">
                            <i class="ki ki-eye icon-nm"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body mb-1">
                    <div id="kt_gmap_5" style="min-height: 600px; max-height: 600px;"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="card card-custom">
                <div class="card-header">
                    <h3 class="card-title">Update Location</h3>
                </div>
                <div class="card-body">
                    <form action="" method="POST" id="shipment_location_update" style="min-height: 600px; max-height: 600px; overflow: scroll;">
                        {{-- @csrf --}}

                    <div class="form-group">
                        <label>Date</label>
                        <div class="input-group date">
                            <input type="text" class="form-control" name="date" readonly="readonly" value="" id="kt_datepicker_3"/>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Time</label>
                        <div class="input-group timepicker">
                            <input class="form-control" id="kt_timepicker_3" name="time" readonly="readonly" placeholder="Select time" type="text" required/>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-clock-o"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Location</label><br>
                        <select class="form-control select2" id="kt_select2_1_9" name="country" style="width: 100%;" required>
                            <option value="">Select one</option>
                            @if ($countries->count() > 0)
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div id="city" class="form-group" style="display: none;">
                        <label>City</label><br>
                        <select class="form-control select2" id="kt_select2_1_15" name="city" style="width: 100%;" required>

                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status</label><br>
                        <select class="form-control select2" id="kt_select2_1_10" name="status" style="width: 100%;" required>
                            <option value="">Select one</option>
                            @if ($statuses->count() > 0)
                                @foreach ($statuses as $status)
                                    <option value="{{ $status->id }}">{{ $status->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Remark</label>
                        <textarea id="remark" class="form-control" rows="5" name="remark"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" id="submit" class="btn btn-primary btn-block">Update</button>
                    </div>
                    <input type="hidden" name="tracking_id" id="tracking_id">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="location-table-canvas" style="display: none;">
        <div class="col-md-12">
            <div class="card card-custom">
                <div class="card-header">
                    <h3 class="card-title">
                        Shipment Location History
                    </h3>
                </div>
                <div class="card-body">
                    {{-- <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"> --}}
                    <table class="table table-striped table-responsive-sm" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>Tracking ID</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Status</th>
                                <th>Agent</th>
                                {{-- <th>Receiver</th> --}}
                                {{-- <th>Actions</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-scripts')
{{-- <script src="{{ asset('assets/js/pages/crud/ktdatatable/base/html-table.js?v=7.0.4') }}"></script> --}}

{{-- <script src="{{ url('//maps.google.com/maps/api/js?key=AIzaSyBgyB2XeKekCOEhLGxOvZcEf36ZBtFo11c') }}"></script> --}}
{{-- <script src="{{ asset('assets/plugins/custom/gmaps/gmaps.js?v=7.0.4') }}"></script> --}}
<script defer src="" id="map-url"></script>
{{-- <script src="{{ asset('assets/js/pages/features/maps/google-maps.js?v=7.0.4') }}"></script> --}}
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js?v=7.0.4') }}"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js?v=7.0.4') }}"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/select2.js?v=7.0.4') }}"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js?v=7.0.4') }}"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/form-repeater.js?v=7.0.4') }}"></script>


<script src="{{ asset('assets/js/pages/features/miscellaneous/blockui.js?v=7.0.4') }}"></script>
<script>
    var mapData = {};
    var markerLocations = [];
    var pathCoordinates = [];
    var request;

    $('#shipment_track_id').change(function() {
        KTApp.blockPage({
            overlayColor: '#000000',
            state: 'danger',
            message: 'Please wait...'
        });

        var trackingId = $(this).val();
        // console.log(trackingId);
        $.ajax({
            url: '/shipments/' + trackingId + '/locations',
            type: 'GET',
            async: false,
            success: function(resp) {
                KTApp.unblockPage();
                // fetchAndSetupMapEnv();
                $('form#shipment_location_update').attr('action', '/shipments/' + trackingId + '/location-update');
                $('#tracking_id').val(trackingId);
                // console.log('AJAX call 1', resp);
                // $('#location-map-canvas').show('slow');
                $('tbody').empty();

                $('tbody')
                    .append(`<tr>
                                <td>${resp.tracking_id}</td>
                                <td>${resp.histories[0].date}</td>
                                <td>${resp.histories[0].time}</td>
                                <td>${resp.shipment_info.origin.country.name}</td>
                                <td>${resp.shipment_info.origin.city}</td>
                                <td>${resp.histories[0].shipment_statuses[0].name}</td>
                                <td>${resp.agent.name}</td>
                            </tr>`);
                var pos = new google.maps.LatLng(resp.shipment_info.origin.country.coordinates.latitude, resp.shipment_info.origin.country.coordinates.longitude);
                markerLocations.push(pos);
                var coord = {
                    lat: +resp.shipment_info.origin.country.coordinates.latitude,
                    lng: +resp.shipment_info.origin.country.coordinates.longitude
                };
                pathCoordinates.push(coord);

                pos = null;
                coord = null;

                if(resp.histories.length > 1) {
                    var index = 1;
                    for(index; index < resp.histories.length; index++) {

                        // console.log('History ' + [])
                        $('tbody')
                        .append(
                            `<tr>
                                <td>${resp.tracking_id}</td>
                                <td>${resp.histories[index].date}</td>
                                <td>${resp.histories[index].time}</td>
                                <td>${resp.histories[index].locations[0].country.name}</td>
                                <td>${resp.histories[index].locations[0].city}</td>
                                <td>${resp.histories[index].shipment_statuses[0].name}</td>
                                <td>${resp.agent.name}</td>
                            </tr>`
                        );
                        pos = new google.maps.LatLng(resp.histories[index].locations[0].country.coordinates.latitude, resp.histories[index].locations[0].country.coordinates.longitude);
                        markerLocations.push(pos);
                        coord = {
                            lat: +resp.histories[index].locations[0].country.coordinates.latitude,
                            lng: +resp.histories[index].locations[0].country.coordinates.longitude
                        };
                        pathCoordinates.push(coord);

                        pos = null;
                        coord = null;
                    }
                }
                fetchAndSetupMapEnv(markerLocations, pathCoordinates);
                $('#location-map-canvas').show('slow');
                $('#location-table-canvas').show('slow');

            },
            error: function(_, __, msg) {
                KTApp.unblockPage();
                alert(msg);
            }
        });

        // console.log('markerLocations: ', markerLocations);
    });

    $('#kt_select2_1_9').change(function() {
        var countryId = $(this).val();
        // console.log(countryId);
         KTApp.blockPage({
            overlayColor: '#000000',
            state: 'danger',
            message: 'Please wait...'
        });
        $('#kt_select2_1_15').empty();
        $.ajax({
            url: '/country-cities/' + countryId,
            type: 'GET',
            success: function(resp) {
                KTApp.unblockPage();
                console.log('resp cities => ', resp);
                $('#city').show();
                for(var prop in resp) {
                    $('#kt_select2_1_15').append('<option value="' + resp[prop] + '">' + resp[prop] + '</option>');
                }
            },
            error: function() {
                KTApp.unblockPage();
            },
        });
    });

    $('#shipment_location_update').on('submit', function(event) {
        event.preventDefault();
        // console.log($(this).serialize());

        if(request) {
            request.abort();
        }

        var trackingId = $('#shipment_track_id').val();
        var $form = $(this);
        var $inputs = $form.find('input, select, textarea, button');
        var serializeFormData = $form.serialize();

        $inputs.prop('disabled', true);

        request = $.ajax({
            url: '/shipments/' + trackingId + '/location-update',
            type: 'POST',
            data: serializeFormData,
        });

        console.log(request);

        request.done(function(resp) {
            console.log(resp);
            alert('Shipment update successful!');
        });

        request.fail(function(_, __, msg) {
            alert('Shipment update failed!');
        });

        request.always(function() {
            $inputs.prop('disabled', false);
        });
        // var actionUrl = '/shipments/' + trackingId + '/location-update';

        // $.ajax({
        //     url: actionUrl,
        //     type: 'POST',
        //     data: {
        //         'date': $('#date').val(),
        //         'time': $('#kt_timepicker_3').val(),
        //         'country': $('#kt_select2_1_9').val(),
        //         'city': $('#kt_select2_1_15').val(),
        //         'status': $('#kt_select2_1_10').val(),
        //         'remark': $('#remark').val(),
        //         'tracking_id': $('#tracking_id')
        //     },
        //     success: function(resp) {
        //         console.log(resp);
        //     },
        //     error: function(_, __, msg) {

        //     }
        // });
    });

     //Fetch map api key from the database
    function fetchAndSetupMapEnv(pos, coords) {
        $.ajax({
            url: '/get-map_api_key',
            type: 'GET',
            success: function(resp) {
                console.log('RESP', resp);
                if(resp != null) {
                    mapData = resp;
                    $('#map-url').attr('src', 'https://maps.googleapis.com/maps/api/js?key=' + resp.api_key + '&callback=initMap');
                }
                else {
                    $('#map-url').attr('src', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBgyB2XeKekCOEhLGxOvZcEf36ZBtFo11c&callback=initMap');
                }
                initMap(resp, pos, coords);
            },
            error: function(_, __, msg) {
                alert(msg);
            }
        });
    }


    function initMap(mapData, pos = [], coords = []) {
        var mapDiv = document.getElementById('kt_gmap_5');
        // var lat = mapData != undefined ? mapData.latitude : 0;
        // var lng = mapData != undefined ? mapData.longitude : 0;
        var zoomLevel = mapData != undefined ? mapData.zoom_levels[0].map_zoom_level_to_number : 8;
        var mapType = mapData != undefined ? mapData.map_types[0].name.toString().toUpperCase() : '';
        // console.log('Map Type: ', mapType);
        var mapProps = {
            mapTypeId: google.maps.MapTypeId.mapType,
            center: pos[0],
            zoom: zoomLevel,
            // mapTypeId: mapType,
            // center: new google.maps.LatLng(lat, lng),
        };

        var map = new google.maps.Map(mapDiv, mapProps);

        if(pos.length > 0) {
            for(var i = 0; i < pos.length; i++) {
                console.log(pos[i]);
                addMarker(map, pos[i]);
            }
        }

        if(coords.length > 0) {
            for(var i = 0; i < coords.length; i++) {
                console.log(coords[i]);
            }

            addPolyline(map, coords);
        }

    }

    function addMarker(mapObject, position) {
        var marker = new google.maps.Marker({
            position: position,
            animation: google.maps.Animation.BOUNCE,
        });

        marker.setMap(mapObject);
    }

    function addPolyline(mapObject, coords) {
        var linePath = new google.maps.Polyline({
            path: coords,
            geodesic: true,
            strokeColor: "#FF0000",
            strokeOpacity: 1.0,
            strokeWeight: 2,
        });

        linePath.setMap(mapObject);
    }


</script>
<script>

</script>
@endsection
