@extends('layouts.main-master')
@section('title', 'Create Shipment')

@section('page-styles')
@stop

@section('page-title', 'New Shipment')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-custom alert-light-warning fade show mb-5" role="alert">
                    <div class="alert-icon">
                        <i class="flaticon-warning"></i>
                    </div>
                    <div class="alert-text">
                        <h3>Fix the following error(s)</h3>
                        @foreach ($errors->all() as $message)
                            {{ $message }}<br>
                        @endforeach
                    </div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="ki ki-close"></i>
                            </span>
                        </button>
                    </div>
                </div>
            @endif

            @include('includes.forms.create-shipment')
        </div>
    </div>
@stop

@section('page-scripts')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js?v=7.0.4') }}"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js?v=7.0.4') }}"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/select2.js?v=7.0.4') }}"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js?v=7.0.4') }}"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/form-repeater.js?v=7.0.4') }}"></script>
    <script>
        $('#kt_select2_1_11').change(function(event) {
            var id = $(this).val();
            KTApp.blockPage({
                overlayColor: '#000000',
                state: 'danger',
                message: 'Please wait...'
            });

            $.ajax({
                url: '/users/' + id,
                type: 'GET',
                success: function(resp, status, xhr) {
                    KTApp.unblock();
                    // console.log(resp);
                    $('input[name=shipper_phone]').val(resp.phone);
                    $('input[name=shipper_address]').val('Dummy address');
                    $('input[name=shipper_email]').val(resp.email);
                },
                error: function(_, __, errorMessage) {
                    KTApp.unblock();
                    alert(errorMessage);
                }
            });
        });

        $('#btn-generate-id').on('click', function(event) {
            event.preventDefault();

            KTApp.blockPage({
                overlayColor: '#000000',
                state: 'danger',
                message: 'Please wait...'
            });

            $.ajax({
                url: '/generate-tracking-id',
                type: 'GET',
                success: function(resp, status, xhr) {
                    KTApp.unblock();
                    console.log(resp);
                    $('input[name=shipment_track_id]').val(resp);
                },
                error: function(_, __, errorMessage) {
                    KTApp.unblock();
                    window.alert(errorMessage);
                }
            });

        });

        $('#kt_select2_1_2').change(function(event) {
            var id = +$(this).val();
            console.log(id);
            KTApp.blockPage({
                overlayColor: '#000000',
                state: 'danger',
                message: 'Please wait...',
            })

            $.ajax({
                url: '/country-cities/' + id,
                type: 'GET',
                success: function(resp) {
                    KTApp.unblock();
                    console.log(resp);
                    $('#city').attr('style', '');
                    var sElement = $('#city')
                        .find('#kt_select2_1_12');
                    sElement.html('');
                    for (var prop in resp) {
                        // console.log(`${prop}: ${resp[prop]}`);
                        sElement.append('<option value="' + resp[prop] + '">' + resp[prop] +
                            '  </option>');
                    }
                },
                error: function(_, __, errorMessage) {
                    KTApp.unblock();
                    alert(errorMessage);
                    return;
                }
            });

        });

        $('#kt_select2_1_5').change(function(event) {
            var id = +$(this).val();
            console.log(id);
            KTApp.blockPage({
                overlayColor: '#000000',
                state: 'danger',
                message: 'Please wait...',
            })

            $.ajax({
                url: '/country-cities/' + id,
                type: 'GET',
                success: function(resp) {
                    KTApp.unblock();
                    $('#city_1').attr('style', '');
                    var sElement = $('#city_1')
                        .find('#kt_select2_1_13');
                    sElement.html('');
                    for (var prop in resp) {
                        // console.log(`${prop}: ${resp[prop]}`);
                        sElement.append('<option value="' + resp[prop] + '">' + resp[prop] +
                            '  </option>');
                    }
                },
                error: function(_, __, errorMessage) {
                    KTApp.unblock();
                    alert(errorMessage);
                    return;
                }
            });
        });

        $('#kt_select2_1_9').change(function(event) {
            var id = +$(this).val();
            console.log(id);
            KTApp.blockPage({
                overlayColor: '#000000',
                state: 'danger',
                message: 'Please wait...',
            })

            $.ajax({
                url: '/country-cities/' + id,
                type: 'GET',
                success: function(resp) {
                    KTApp.unblock();
                    // console.log(resp);
                    $('#city_2').attr('style', '');
                    var sElement = $('#city_2')
                        .find('#kt_select2_1_14');
                    sElement.html('');
                    for (var prop in resp) {
                        // console.log(`${prop}: ${resp[prop]}`);
                        sElement.append('<option value="' + resp[prop] + '">' + resp[prop] +
                            '  </option>');
                    }
                },
                error: function(_, __, errorMessage) {
                    KTApp.unblock();
                    alert(errorMessage);
                    return;
                }
            });
        });

        $('#receiver_email').on('blur', function(event) {
            event.preventDefault();

            var email = $(this).val();
            if (!email) return;
            KTApp.blockPage({
                overlayColor: '#000000',
                state: 'danger',
                message: 'Please wait...',
            });
            $.ajax({
                url: '/receivers/' + email,
                type: 'GET',
                success: function(resp) {
                    console.log(resp);
                    KTApp.unblock();
                    if (resp === null || resp === '') {
                        return;
                    } else {
                        $('input[name=receiver_name]').val(resp.name);
                        $('input[name=receiver_phone]').val(resp.phone);
                        $('input[name=receiver_address]').val(resp.address);
                    }
                },
                error: function(_, __, errorMessage) {
                    alert(errorMessage);
                }
            });
        });
    </script>
@endsection
