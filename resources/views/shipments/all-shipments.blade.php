@extends('layouts.main-master')
@section('title', 'All Shipments')

@section('page-styles')
<!--begin::Page Vendors Styles(used by this page)-->
<link href="assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles-->
@stop

@section('page-title', 'All Shipments')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header flex-wrap py-5">
                <div class="card-title">
                    <h3 class="card-label">All Shipments</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="{{ route('admin.create-shipment') }}" class="btn btn-primary font-weight-bolder">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <circle fill="#000000" cx="9" cy="15" r="6" />
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>Create New Shipment</a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                    <thead>
                        <tr>
                            <th>Tracking ID</th>
                            <th>Shipper</th>
                            <th>Receiver</th>
                            <th>Shipment Mode</th>
                            <th>Shipment Type</th>
                            <th>Company Agent</th>
                            <th>Ship Date</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($shipments->count() > 0)
                            @foreach ($shipments as $shipment)
                                <tr>
                                    <td>{{ $shipment->tracking_id ?? 'N/A'}}</td>
                                    <td>{{ $shipment->shipper->name ?? 'N/A' }}</td>
                                    <td>{{ $shipment->receiver->name ?? ''}}</td>
                                    <td>{{ $shipment->shipmentInfo->shipmentModes->first()->name ?? '' }}</td>
                                    <td>{{ $shipment->shipmentInfo->shipmentTypes->first()->name ?? '' }}</td>
                                    <td>{{ $shipment->agent->name ?? '' }}</td>
                                    <td>{{ $shipment->histories->first()->date ?? '' }}</td>
                                    <td>{{ $shipment->histories->first()->shipmentStatuses->first()->name ?? '' }}</td>
                                    <td nowrap="nowrap">
                                        {{-- <a href="#"><i class="far fa-edit text-primary"></i></a> &nbsp; --}}
                                        <a href="#" data-toggle="modal" data-target="#staticBackdropDelete" data-id="{{ $shipment->tracking_id }}">
                                            <i class="fas fa-trash-alt text-danger">
                                        </i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else

                        @endif
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>
@include('includes.modals.model-delete')
@stop

@section('page-scripts')
<!--begin::Page Vendors(used by this page)-->
<script src="{{ asset('assets/js/pages/crud/ktdatatable/base/html-table.js?v=7.0.4') }}"></script>
{{-- <script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}"></script> --}}
<!--end::Page Vendors-->
<script>
    $('#staticBackdropDelete').on('shown.bs.modal', function(event) {
        var btn = $(event.relatedTarget);

        var trackingId = btn.data('id');
        var modal = $(this);
        modal.find('#model-name').text('Shipment');
        modal.find('#btn-delete').attr('href', '/shipments/' + trackingId + '/delete');
    });
</script>
@endsection
