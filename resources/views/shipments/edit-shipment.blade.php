@extends('layouts.main-master')
@section('title', 'Create Shipment')

@section('page-styles')
@stop

@section('page-title', 'Edit Shipment')

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('includes.forms.edit-shipment')
    </div>
</div>
@stop

@section('page-scripts')
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js?v=7.0.4') }}"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js?v=7.0.4') }}"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/select2.js?v=7.0.4') }}"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js?v=7.0.4') }}"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/form-repeater.js?v=7.0.4') }}"></script>
@endsection
