<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @param Faker $faker
     * @return void
     */
    public function run(Faker $faker)
    {
        (new User)->create([
            'name' => 'Administrator',
            'email' => 'admin@excelxpress.net',
            'phone' => '',
            'address' => '',
            'role' => 'admin',
            // 'email_verified' => now(),
            'password' => Hash::make('@password1917'),
            'slug' => User::generateUserSlug(),

        ]);

        if (config('app.env') == 'local') {
            // for ($i = 0; $i < 10; $i++)')) {

            factory(User::class, 10)->create()->each(function ($user) use ($faker) {
                $role_count = Role::all()->count();
                $role_id =  random_int(2, $role_count);

                $user->roles()->attach($role_id);
            });
        }
    }
}
