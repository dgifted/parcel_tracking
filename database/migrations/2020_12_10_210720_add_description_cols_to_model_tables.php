<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescriptionColsToModelTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipment_types', function (Blueprint $table) {
            $table->text('description')->nullable();
        });
        Schema::table('shipment_modes', function (Blueprint $table) {
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipment_types', function (Blueprint $table) {
            $table->dropColumn('description');
        });
        Schema::table('shipment_modes', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
}
