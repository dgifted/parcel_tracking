<?php

use App\PaymentMode;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentModesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_modes', function (Blueprint $table) {
            $table->id();
            $table->string('ref_id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        (new PaymentMode())->create([
            'ref_id' => PaymentMode::generateReferenceId(),
            'name' => 'CASH'
        ]);
        (new PaymentMode)->create([
            'ref_id' => PaymentMode::generateReferenceId(),
            'name' => 'Cheque'
        ]);
        (new PaymentMode)->create([
            'ref_id' => PaymentMode::generateReferenceId(),
            'name' => 'BACS'
        ]);
        (new PaymentMode)->create([
            'ref_id' => PaymentMode::generateReferenceId(),
            'name' => 'Credit/Debit Card'
        ]);
        (new PaymentMode)->create([
            'ref_id' => PaymentMode::generateReferenceId(),
            'name' => 'BitCoin'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_modes');
    }
}
