<?php

use App\PieceType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePieceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('piece_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        (new PieceType)->create([
            'name' => 'Pallet'
        ]);
        (new PieceType)->create([
            'name' => 'Carton'
        ]);
        (new PieceType)->create([
            'name' => 'Crate'
        ]);
        (new PieceType)->create([
            'name' => 'Loose'
        ]);
        (new PieceType)->create([
            'name' => 'Others'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('piece_types');
    }
}
