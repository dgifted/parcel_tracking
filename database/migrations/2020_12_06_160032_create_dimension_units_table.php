<?php

use App\DimensionUnit;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDimensionUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dimension_units', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('abbreviation');
            $table->tinyInteger('in_use')->default(DimensionUnit::IN_USE_OFF);
            $table->timestamps();
        });

        (new DimensionUnit)->create([
            'full_name' => 'millimeters',
            'abbreviation' => 'mm',
            'in_use' => DimensionUnit::IN_USE_OFF,
        ]);
        (new DimensionUnit)->create([
            'full_name' => 'centimeters',
            'abbreviation' => 'cm',
            'in_use' => DimensionUnit::IN_USE_ON,
        ]);
        (new DimensionUnit)->create([
            'full_name' => 'decimeters',
            'abbreviation' => 'dm',
            'in_use' => DimensionUnit::IN_USE_OFF,
        ]);
        (new DimensionUnit)->create([
            'full_name' => 'meters',
            'abbreviation' => 'm',
            'in_use' => DimensionUnit::IN_USE_OFF,
        ]);
        (new DimensionUnit)->create([
            'full_name' => 'dekameters',
            'abbreviation' => 'dam',
            'in_use' => DimensionUnit::IN_USE_OFF,
        ]);
        (new DimensionUnit)->create([
            'full_name' => 'kilometer',
            'abbreviation' => 'km',
            'in_use'
        ]);
        (new DimensionUnit)->create([
            'full_name' => 'inch',
            'abbreviation' => 'in',
            'in_use' => DimensionUnit::IN_USE_ON,
        ]);
        (new DimensionUnit)->create([
            'full_name' => 'foot',
            'abbreviation' => 'ft',
            'in_use' => DimensionUnit::IN_USE_OFF,
        ]);
        (new DimensionUnit)->create([
            'full_name' => 'yard',
            'abbreviation' => 'yd',
            'in_use' => DimensionUnit::IN_USE_OFF,
        ]);
        (new DimensionUnit)->create([
            'full_name' => 'mile',
            'abbreviation' => 'mi',
            'in_use' => DimensionUnit::IN_USE_OFF,
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dimension_units');
    }
}
