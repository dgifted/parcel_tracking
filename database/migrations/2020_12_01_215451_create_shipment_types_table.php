<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ShipmentType;

class CreateShipmentTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment_types', function (Blueprint $table) {
            $table->id();
            $table->string('ref_id');
            $table->string('name');
            $table->timestamps();
        });

        (new ShipmentType)->create([
            'ref_id' => ShipmentType::generateReferenceId(),
            'name' => 'Air Freight'
        ]);
        (new ShipmentType)->create([
            'ref_id' => ShipmentType::generateReferenceId(),
            'name' => 'International Shipping'
        ]);
        (new ShipmentType)->create([
            'ref_id' => ShipmentType::generateReferenceId(),
            'name' => 'Truckload'
        ]);
        (new ShipmentType)->create([
            'ref_id' => ShipmentType::generateReferenceId(),
            'name' => 'Van Move'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment_types');
    }
}
