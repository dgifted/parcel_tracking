<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ShipmentMode;

class CreateShipmentModesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment_modes', function (Blueprint $table) {
            $table->id();
            $table->string('ref_id');
            $table->string('name');
            $table->timestamps();
        });

        (new ShipmentMode)->create([
            'ref_id' => ShipmentMode::generateReferenceId(),
            'name' => 'Sea Transport'
        ]);
        (new ShipmentMode)->create([
            'ref_id' => ShipmentMode::generateReferenceId(),
            'name' => 'Land Shipping'
        ]);
        (new ShipmentMode)->create([
            'ref_id' => ShipmentMode::generateReferenceId(),
            'name' => 'Air Freight'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment_modes');
    }
}
