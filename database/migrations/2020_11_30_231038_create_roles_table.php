<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Role;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('ref_id');
            $table->string('title');
            $table->string('alias')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });

        (new Role)->create([
            'ref_id' => Role::generateReferenceId(),
            'title' => 'admin',
            'alias' => 'admin',
            'description' => 'Site administrator.'
        ]);

        (new Role)->create([
            'ref_id' => Role::generateReferenceId(),
            'title' => 'shipper',
            'alias' => 'shipper',
            'description' => 'Shipper of the package/s.'
        ]);

        (new Role)->create([
            'ref_id' => Role::generateReferenceId(),
            'title' => 'receiver',
            'alias' => 'receiver',
            'description' => 'Receiver of the shipment.'
        ]);

        (new Role)->create([
            'ref_id' => Role::generateReferenceId(),
            'title' => 'agent',
            'alias' => 'agent',
            'description' => 'Agent of the shipment.'
        ]);

        (new Role)->create([
            'ref_id' => Role::generateReferenceId(),
            'title' => 'client',
            'alias' => 'client',
            'description' => 'Client of the shipment.'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
