<?php

use App\MapZoomLevel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapZoomLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_zoom_levels', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->tinyInteger('in_use')->default(MapZoomLevel::IN_USE_OFF);
            $table->timestamps();
        });

        (new MapZoomLevel)->create([
            'name' => 'World',
        ]);
        (new MapZoomLevel)->create([
            'name' => 'Continent',
        ]);
        (new MapZoomLevel)->create([
            'name' => 'City',
        ]);
        (new MapZoomLevel)->create([
            'name' => 'Streets',
            'in_use' => MapZoomLevel::IN_USE_ON,
        ]);
        (new MapZoomLevel)->create([
            'name' => 'Buildings',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_zoom_levels');
    }
}
