<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ShipmentStatus;

class CreateShipmentStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('ref_id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        (new ShipmentStatus)->create([
            'ref_id' => ShipmentStatus::generateReferenceId(),
            'name' => 'Pending'
        ]);
        (new ShipmentStatus)->create([
            'ref_id' => ShipmentStatus::generateReferenceId(),
            'name' => 'Picked up'
        ]);
        (new ShipmentStatus)->create([
            'ref_id' => ShipmentStatus::generateReferenceId(),
            'name' => 'On Hold'
        ]);
        (new ShipmentStatus)->create([
            'ref_id' => ShipmentStatus::generateReferenceId(),
            'name' => 'Out for delivery'
        ]);
        (new ShipmentStatus)->create([
            'ref_id' => ShipmentStatus::generateReferenceId(),
            'name' => 'In Transit'
        ]);
        (new ShipmentStatus)->create([
            'ref_id' => ShipmentStatus::generateReferenceId(),
            'name' => 'Enroute'
        ]);
        (new ShipmentStatus)->create([
            'ref_id' => ShipmentStatus::generateReferenceId(),
            'name' => 'Cancelled'
        ]);
        (new ShipmentStatus)->create([
            'ref_id' => ShipmentStatus::generateReferenceId(),
            'name' => 'Delivered'
        ]);
        (new ShipmentStatus)->create([
            'ref_id' => ShipmentStatus::generateReferenceId(),
            'name' => 'Returned'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment_statuses');
    }
}
