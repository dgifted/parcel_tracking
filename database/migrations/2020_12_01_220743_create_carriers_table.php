<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Carrier;

class CreateCarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carriers', function (Blueprint $table) {
            $table->id();
            $table->string('ref_id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        (new Carrier)->create([
            'ref_id' => Carrier::generateReferenceId(),
            'name' => 'DHL'
        ]);
        (new Carrier)->create([
            'ref_id' => Carrier::generateReferenceId(),
            'name' => 'USPS'
        ]);
        (new Carrier)->create([
            'ref_id' => Carrier::generateReferenceId(),
            'name' => 'FedEx'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carriers');
    }
}
