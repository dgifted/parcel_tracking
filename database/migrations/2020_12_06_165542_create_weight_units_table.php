<?php

use App\WeightUnit;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeightUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weight_units', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('abbreviation');
            $table->tinyInteger('in_use')->default(WeightUnit::IN_USE_OFF);
            $table->timestamps();
        });

        (new WeightUnit)->create([
            'full_name' => 'Kilogram',
            'abbreviation' => 'kg',
            'in_use' => WeightUnit::IN_USE_ON,
        ]);
        (new WeightUnit)->create([
            'full_name' => 'Grain',
            'abbreviation' => 'gr',
            'in_use' => WeightUnit::IN_USE_OFF,
        ]);
        (new WeightUnit)->create([
            'full_name' => 'Ounce',
            'abbreviation' => 'oz',
            'in_use' => WeightUnit::IN_USE_OFF,
        ]);
        (new WeightUnit)->create([
            'full_name' => 'Stone',
            'abbreviation' => 'st',
            'in_use' => WeightUnit::IN_USE_OFF,
        ]);
        (new WeightUnit)->create([
            'full_name' => 'Ton',
            'abbreviation' => 'ton',
            'in_use' => WeightUnit::IN_USE_OFF,
        ]);
        (new WeightUnit)->create([
            'full_name' => 'Pound',
            'abbreviation' => 'lb',
            'in_use' => WeightUnit::IN_USE_OFF,
        ]);
        (new WeightUnit)->create([
            'full_name' => 'Gram',
            'abbreviation' => 'g',
            'in_use' => WeightUnit::IN_USE_OFF,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weight_units');
    }
}
