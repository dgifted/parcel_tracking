<?php

use App\MapType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->tinyInteger('in_use')->default(MapType::IN_USE_OFF);
            $table->timestamps();
        });

        (new MapType)->create([
            'name' => 'Terrain',
            'in_use' => MapType::IN_USE_ON,
        ]);
        (new MapType)->create([
            'name' => 'Satellite',
        ]);
        (new MapType)->create([
            'name' => 'Roadmap',
        ]);
        (new MapType)->create([
            'name' => 'Hybrid',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_types');
    }
}
