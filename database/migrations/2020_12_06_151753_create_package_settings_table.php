<?php

use App\PackageSetting;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackageSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_settings', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('enable_multi_package_admin')->default(PackageSetting::MULTI_PACKAGE_ADMIN_ON);
            $table->tinyInteger('enable_multi_package_result')->default(PackageSetting::MULTI_PACKAGE_RESULT_ON);
            $table->tinyInteger('enable_dimension')->default(PackageSetting::DIMENSION_UNITS_ON);
            $table->timestamps();
        });

        (new PackageSetting)->create([]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_settings');
    }
}
